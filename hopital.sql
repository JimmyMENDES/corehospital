-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : ven. 04 juin 2021 à 12:56
-- Version du serveur :  10.4.18-MariaDB
-- Version de PHP : 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `hopital`
--

-- --------------------------------------------------------

--
-- Structure de la table `analyse`
--

CREATE TABLE `analyse` (
  `reference_analyse` int(10) NOT NULL,
  `id_patient` int(8) NOT NULL,
  `id_personnel` int(5) NOT NULL,
  `code_appareil` varchar(3) NOT NULL,
  `date_heure_analyse` datetime NOT NULL,
  `contenu_analyse` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `analyse`
--

INSERT INTO `analyse` (`reference_analyse`, `id_patient`, `id_personnel`, `code_appareil`, `date_heure_analyse`, `contenu_analyse`) VALUES
(1, 1, 14, 'SC', '2021-05-07 01:02:46', 'Le scanner révèle l\'absence totale d\'os'),
(2, 13, 14, 'IRM', '2021-05-07 14:56:52', 'Os du crâne fissuré, pas de danger pour le cerveau'),
(6, 3, 14, 'ECG', '2021-05-07 15:32:07', 'ECG normal, aucune deficiance cardiaque detectée');

-- --------------------------------------------------------

--
-- Structure de la table `appareil`
--

CREATE TABLE `appareil` (
  `code_appareil` varchar(3) NOT NULL,
  `type_appareil` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `appareil`
--

INSERT INTO `appareil` (`code_appareil`, `type_appareil`) VALUES
('ECG', 'Électrocardiogramme'),
('EEG', 'Électroencéphalogramme'),
('EG', 'Échographe'),
('ES', 'Endoscope'),
('IRM', 'Imagerie par résonnance magnétique'),
('PG', 'Pléthysmographe'),
('SC', 'Scanner'),
('TEP', 'Tomographie à émission de positron');

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

CREATE TABLE `article` (
  `reference_article` varchar(10) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `quantite` int(5) NOT NULL,
  `type` enum('article_general','medicament','equipement') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `article`
--

INSERT INTO `article` (`reference_article`, `nom`, `quantite`, `type`) VALUES
('AMO', 'Amoxicilline', 10, 'medicament'),
('ASP', 'Aspirine', 10, 'medicament'),
('ATR', 'Atropine', 10, 'medicament'),
('BEQ', 'Béquille', 10, 'equipement'),
('BRD', 'Brosse à Dent', 10, 'article_general'),
('CHL', 'Chloroquine', 10, 'medicament'),
('COD', 'Codéine', 10, 'medicament'),
('COR', 'Cortisol', 10, 'medicament'),
('EPI', 'Épinéphrine', 10, 'medicament'),
('IBU', 'Ibuprofène', 10, 'medicament'),
('INS', 'Insuline', 10, 'medicament'),
('LOR', 'Lorazépam', 10, 'medicament'),
('MCH', 'Masques Chirurgicaux', 10, 'equipement'),
('PAR', 'Paracétamol', 10, 'medicament'),
('TER', 'Terbinafine', 10, 'medicament');

-- --------------------------------------------------------

--
-- Structure de la table `chambre`
--

CREATE TABLE `chambre` (
  `code_chambre` varchar(3) NOT NULL,
  `code_service` varchar(1) NOT NULL,
  `num_chambre` int(2) NOT NULL,
  `capacite` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `chambre`
--

INSERT INTO `chambre` (`code_chambre`, `code_service`, `num_chambre`, `capacite`) VALUES
('A1', 'A', 1, 1),
('A10', 'A', 10, 2),
('A11', 'A', 11, 2),
('A12', 'A', 12, 2),
('A13', 'A', 13, 2),
('A14', 'A', 14, 2),
('A15', 'A', 15, 2),
('A16', 'A', 16, 2),
('A17', 'A', 17, 4),
('A18', 'A', 18, 4),
('A19', 'A', 19, 4),
('A2', 'A', 2, 1),
('A20', 'A', 20, 4),
('A3', 'A', 3, 1),
('A4', 'A', 4, 1),
('A5', 'A', 5, 1),
('A6', 'A', 6, 1),
('A7', 'A', 7, 1),
('A8', 'A', 8, 1),
('A9', 'A', 9, 2),
('B1', 'B', 1, 1),
('B10', 'B', 10, 2),
('B11', 'B', 11, 2),
('B12', 'B', 12, 2),
('B13', 'B', 13, 2),
('B14', 'B', 14, 2),
('B15', 'B', 15, 2),
('B16', 'B', 16, 2),
('B17', 'B', 17, 4),
('B18', 'B', 18, 4),
('B19', 'B', 19, 4),
('B2', 'B', 2, 1),
('B20', 'B', 20, 4),
('B3', 'B', 3, 1),
('B4', 'B', 4, 1),
('B5', 'B', 5, 1),
('B6', 'B', 6, 1),
('B7', 'B', 7, 1),
('B8', 'B', 8, 1),
('B9', 'B', 9, 2),
('C1', 'C', 1, 1),
('C10', 'C', 10, 2),
('C11', 'C', 11, 2),
('C12', 'C', 12, 2),
('C13', 'C', 13, 2),
('C14', 'C', 14, 2),
('C15', 'C', 15, 2),
('C16', 'C', 16, 2),
('C17', 'C', 17, 4),
('C18', 'C', 18, 4),
('C19', 'C', 19, 4),
('C2', 'C', 2, 1),
('C20', 'C', 20, 4),
('C3', 'C', 3, 1),
('C4', 'C', 4, 1),
('C5', 'C', 5, 1),
('C6', 'C', 6, 1),
('C7', 'C', 7, 1),
('C8', 'C', 8, 1),
('C9', 'C', 9, 2),
('D1', 'D', 1, 1),
('D10', 'D', 10, 2),
('D11', 'D', 11, 2),
('D12', 'D', 12, 2),
('D13', 'D', 13, 2),
('D14', 'D', 14, 2),
('D15', 'D', 15, 2),
('D16', 'D', 16, 2),
('D17', 'D', 17, 4),
('D18', 'D', 18, 4),
('D19', 'D', 19, 4),
('D2', 'D', 2, 1),
('D20', 'D', 20, 4),
('D3', 'D', 3, 1),
('D4', 'D', 4, 1),
('D5', 'D', 5, 1),
('D6', 'D', 6, 1),
('D7', 'D', 7, 1),
('D8', 'D', 8, 1),
('D9', 'D', 9, 2);

-- --------------------------------------------------------

--
-- Structure de la table `consultation`
--

CREATE TABLE `consultation` (
  `reference_consultation` int(11) NOT NULL,
  `id_patient` int(8) NOT NULL,
  `id_personnel` int(5) NOT NULL,
  `date_consultation` date NOT NULL,
  `contenu_consultation` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `consultation`
--

INSERT INTO `consultation` (`reference_consultation`, `id_patient`, `id_personnel`, `date_consultation`, `contenu_consultation`) VALUES
(1, 2, 3, '2021-06-02', 'Ulcères à l estomac'),
(2, 1, 3, '2021-06-03', 'Présence de brisures d os autour de l épaule');

-- --------------------------------------------------------

--
-- Structure de la table `hospitalisation`
--

CREATE TABLE `hospitalisation` (
  `id_patient` int(8) NOT NULL,
  `code_lit` varchar(4) NOT NULL,
  `date_entree` date NOT NULL,
  `date_sortie` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `hospitalisation`
--

INSERT INTO `hospitalisation` (`id_patient`, `code_lit`, `date_entree`, `date_sortie`) VALUES
(10, 'B101', '2021-05-01', '2021-05-14'),
(11, 'B102', '2021-05-01', '2021-05-14'),
(1, 'C61', '2021-05-04', NULL),
(3, 'A172', '2021-05-06', '2021-06-17'),
(2, 'A174', '2021-05-04', '2021-06-15'),
(12, 'D11', '2021-05-01', NULL),
(7, 'A121', '2021-05-02', NULL),
(8, 'A122', '2021-05-04', NULL),
(4, 'A31', '2021-06-02', NULL),
(5, 'A183', '2021-06-05', NULL),
(6, 'B112', '2021-06-03', '2021-06-06'),
(9, 'C41', '2021-06-03', '2021-06-05'),
(15, 'A51', '2021-06-04', '2021-06-09');

-- --------------------------------------------------------

--
-- Structure de la table `identification`
--

CREATE TABLE `identification` (
  `login_utilisateur` varchar(10) NOT NULL,
  `mdp_utilisateur` varchar(50) NOT NULL,
  `id_personnel` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `identification`
--

INSERT INTO `identification` (`login_utilisateur`, `mdp_utilisateur`, `id_personnel`) VALUES
('BIBR6', 'nimbus', 6),
('BINC7', 'rip', 7),
('BLAS9', 'patmol', 9),
('CHOP5', 'mandragore', 5),
('DUMA1', 'priori_incantatum', 1),
('FLIF4', 'wingardium_leviosa', 4),
('FUDC15', 'bowler_hat', 15),
('LUPR10', 'lunard', 10),
('MALN12', 'drago', 12),
('MCGM2', 'piertotum_locomotor', 2),
('POMP13', 'episkey', 13),
('POTJ11', 'cornedrue', 11),
('ROGS3', 'always', 3),
('SuperAdmin', 'SuperAdmin', 0),
('WEAA14', 'muggle', 14),
('WEAM8', 'prewett', 8);

-- --------------------------------------------------------

--
-- Structure de la table `lit`
--

CREATE TABLE `lit` (
  `code_lit` varchar(4) NOT NULL,
  `code_service` varchar(1) NOT NULL,
  `code_chambre` varchar(3) DEFAULT NULL,
  `num_lit` int(1) NOT NULL,
  `est_libre` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `lit`
--

INSERT INTO `lit` (`code_lit`, `code_service`, `code_chambre`, `num_lit`, `est_libre`) VALUES
('A101', 'A', 'A10', 1, 1),
('A102', 'A', 'A10', 2, 1),
('A11', 'A', 'A1', 1, 1),
('A111', 'A', 'A11', 1, 1),
('A112', 'A', 'A11', 2, 1),
('A121', 'A', 'A12', 1, 0),
('A122', 'A', 'A12', 2, 0),
('A131', 'A', 'A13', 1, 1),
('A132', 'A', 'A13', 2, 1),
('A141', 'A', 'A14', 1, 1),
('A142', 'A', 'A14', 2, 1),
('A151', 'A', 'A15', 1, 1),
('A152', 'A', 'A15', 2, 1),
('A161', 'A', 'A16', 1, 1),
('A162', 'A', 'A16', 2, 1),
('A171', 'A', 'A17', 1, 1),
('A172', 'A', 'A17', 2, 0),
('A173', 'A', 'A17', 3, 1),
('A174', 'A', 'A17', 4, 0),
('A181', 'A', 'A18', 1, 1),
('A182', 'A', 'A18', 2, 1),
('A183', 'A', 'A18', 3, 1),
('A184', 'A', 'A18', 4, 1),
('A191', 'A', 'A19', 1, 1),
('A192', 'A', 'A19', 2, 1),
('A193', 'A', 'A19', 3, 1),
('A194', 'A', 'A19', 4, 1),
('A201', 'A', 'A20', 1, 1),
('A202', 'A', 'A20', 2, 1),
('A203', 'A', 'A20', 3, 1),
('A204', 'A', 'A20', 4, 1),
('A21', 'A', 'A2', 1, 1),
('A31', 'A', 'A3', 1, 1),
('A41', 'A', 'A4', 1, 1),
('A51', 'A', 'A5', 1, 1),
('A61', 'A', 'A6', 1, 1),
('A71', 'A', 'A7', 1, 1),
('A81', 'A', 'A8', 1, 1),
('A91', 'A', 'A9', 1, 1),
('A92', 'A', 'A9', 2, 1),
('B101', 'B', 'B10', 1, 0),
('B102', 'B', 'B10', 2, 0),
('B11', 'B', 'B1', 1, 1),
('B111', 'B', 'B11', 1, 1),
('B112', 'B', 'B11', 2, 1),
('B121', 'B', 'B12', 1, 1),
('B122', 'B', 'B12', 2, 1),
('B131', 'B', 'B13', 1, 1),
('B132', 'B', 'B13', 2, 1),
('B141', 'B', 'B14', 1, 1),
('B142', 'B', 'B14', 2, 1),
('B151', 'B', 'B15', 1, 1),
('B152', 'B', 'B15', 2, 1),
('B161', 'B', 'B16', 1, 1),
('B162', 'B', 'B16', 2, 1),
('B171', 'B', 'B17', 1, 1),
('B172', 'B', 'B17', 2, 1),
('B173', 'B', 'B17', 3, 1),
('B174', 'B', 'B17', 4, 1),
('B181', 'B', 'B18', 1, 1),
('B182', 'B', 'B18', 2, 1),
('B183', 'B', 'B18', 3, 1),
('B184', 'B', 'B18', 4, 1),
('B191', 'B', 'B19', 1, 1),
('B192', 'B', 'B19', 2, 1),
('B193', 'B', 'B19', 3, 1),
('B194', 'B', 'B19', 4, 1),
('B201', 'B', 'B20', 1, 1),
('B202', 'B', 'B20', 2, 1),
('B203', 'B', 'B20', 3, 1),
('B204', 'B', 'B20', 4, 1),
('B21', 'B', 'B2', 1, 1),
('B31', 'B', 'B3', 1, 1),
('B41', 'B', 'B4', 1, 1),
('B51', 'B', 'B5', 1, 1),
('B61', 'B', 'B6', 1, 1),
('B71', 'B', 'B7', 1, 1),
('B81', 'B', 'B8', 1, 1),
('B91', 'B', 'B9', 1, 1),
('B92', 'B', 'B9', 2, 1),
('C101', 'C', 'C10', 1, 1),
('C102', 'C', 'C10', 2, 1),
('C11', 'C', 'C1', 1, 1),
('C111', 'C', 'C11', 1, 1),
('C112', 'C', 'C11', 2, 1),
('C121', 'C', 'C12', 1, 1),
('C122', 'C', 'C12', 2, 1),
('C131', 'C', 'C13', 1, 1),
('C132', 'C', 'C13', 2, 1),
('C141', 'C', 'C14', 1, 1),
('C142', 'C', 'C14', 2, 1),
('C151', 'C', 'C15', 1, 1),
('C152', 'C', 'C15', 2, 1),
('C161', 'C', 'C16', 1, 1),
('C162', 'C', 'C16', 2, 1),
('C171', 'C', 'C17', 1, 1),
('C172', 'C', 'C17', 2, 1),
('C173', 'C', 'C17', 3, 1),
('C174', 'C', 'C17', 4, 1),
('C181', 'C', 'C18', 1, 1),
('C182', 'C', 'C18', 2, 1),
('C183', 'C', 'C18', 3, 1),
('C184', 'C', 'C18', 4, 1),
('C191', 'C', 'C19', 1, 1),
('C192', 'C', 'C19', 2, 1),
('C193', 'C', 'C19', 3, 1),
('C194', 'C', 'C19', 4, 1),
('C201', 'C', 'C20', 1, 1),
('C202', 'C', 'C20', 2, 1),
('C203', 'C', 'C20', 3, 1),
('C204', 'C', 'C20', 4, 1),
('C21', 'C', 'C2', 1, 1),
('C31', 'C', 'C3', 1, 1),
('C41', 'C', 'C4', 1, 1),
('C51', 'C', 'C5', 1, 1),
('C61', 'C', 'C6', 1, 0),
('C71', 'C', 'C7', 1, 1),
('C81', 'C', 'C8', 1, 1),
('C91', 'C', 'C9', 1, 1),
('C92', 'C', 'C9', 2, 1),
('D101', 'D', 'D10', 1, 1),
('D102', 'D', 'D10', 2, 1),
('D11', 'D', 'D1', 1, 0),
('D111', 'D', 'D11', 1, 1),
('D112', 'D', 'D11', 2, 1),
('D121', 'D', 'D12', 1, 1),
('D122', 'D', 'D12', 2, 1),
('D131', 'D', 'D13', 1, 1),
('D132', 'D', 'D13', 2, 1),
('D141', 'D', 'D14', 1, 1),
('D142', 'D', 'D14', 2, 1),
('D151', 'D', 'D15', 1, 1),
('D152', 'D', 'D15', 2, 1),
('D161', 'D', 'D16', 1, 1),
('D162', 'D', 'D16', 2, 1),
('D171', 'D', 'D17', 1, 1),
('D172', 'D', 'D17', 2, 1),
('D173', 'D', 'D17', 3, 1),
('D174', 'D', 'D17', 4, 1),
('D181', 'D', 'D18', 1, 1),
('D182', 'D', 'D18', 2, 1),
('D183', 'D', 'D18', 3, 1),
('D184', 'D', 'D18', 4, 1),
('D191', 'D', 'D19', 1, 1),
('D192', 'D', 'D19', 2, 1),
('D193', 'D', 'D19', 3, 1),
('D194', 'D', 'D19', 4, 1),
('D201', 'D', 'D20', 1, 1),
('D202', 'D', 'D20', 2, 1),
('D203', 'D', 'D20', 3, 1),
('D204', 'D', 'D20', 4, 1),
('D21', 'D', 'D2', 1, 1),
('D31', 'D', 'D3', 1, 1),
('D41', 'D', 'D4', 1, 1),
('D51', 'D', 'D5', 1, 1),
('D61', 'D', 'D6', 1, 1),
('D71', 'D', 'D7', 1, 1),
('D81', 'D', 'D8', 1, 1),
('D91', 'D', 'D9', 1, 1),
('D92', 'D', 'D9', 2, 1),
('U1', 'U', NULL, 1, 1),
('U10', 'U', NULL, 10, 1),
('U2', 'U', NULL, 2, 1),
('U3', 'U', NULL, 3, 1),
('U4', 'U', NULL, 4, 1),
('U5', 'U', NULL, 5, 1),
('U6', 'U', NULL, 6, 1),
('U7', 'U', NULL, 7, 1),
('U8', 'U', NULL, 8, 1),
('U9', 'U', NULL, 9, 1);

-- --------------------------------------------------------

--
-- Structure de la table `modification_pharmacie`
--

CREATE TABLE `modification_pharmacie` (
  `reference_article` varchar(10) NOT NULL,
  `id_personnel` int(5) NOT NULL,
  `date_heure_modification` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ordonnance`
--

CREATE TABLE `ordonnance` (
  `reference_ordonnance` int(11) NOT NULL,
  `id_patient` int(8) NOT NULL,
  `id_personnel` int(5) NOT NULL,
  `date_ordonnance` date NOT NULL,
  `contenu_ordonnance` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ordonnance`
--

INSERT INTO `ordonnance` (`reference_ordonnance`, `id_patient`, `id_personnel`, `date_ordonnance`, `contenu_ordonnance`) VALUES
(1, 1, 3, '2021-06-03', 'Morphine pour la nuit'),
(2, 2, 3, '2021-06-04', '20 grammes de paracetamole toutes les deux heures.'),
(3, 14, 3, '2021-06-04', '8 heures de sommeil minimum par nuit. \nBoire beaucoup d `eau et manger sainement ( les sucrerires sont à éviter.\n Elles donnent de la \"fausse énergie\"\nUn gros câloin toutes les demi-heure.( Plus si somnolences.)');

-- --------------------------------------------------------

--
-- Structure de la table `patient`
--

CREATE TABLE `patient` (
  `id_patient` int(8) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(25) NOT NULL,
  `sexe` enum('masculin','feminin') NOT NULL,
  `date_de_naissance` date NOT NULL,
  `adresse` varchar(100) NOT NULL,
  `adresse_mail` varchar(50) NOT NULL,
  `numero_telephone` int(10) NOT NULL,
  `pathologies` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `patient`
--

INSERT INTO `patient` (`id_patient`, `nom`, `prenom`, `sexe`, `date_de_naissance`, `adresse`, `adresse_mail`, `numero_telephone`, `pathologies`) VALUES
(1, 'Potter', 'Harry', 'masculin', '1980-07-31', '4 privet Drive', 'harrypotter@gmail.com', 631917151, 'Bras droit cassé'),
(2, 'Weasley', 'Ronald', 'masculin', '1980-03-01', '1 rue du Terrier', 'ronaldweasley@gmail.com', 604050888, 'Vomissements importants'),
(3, 'Granger', 'Hermione', 'feminin', '1979-09-19', '19 chemin de l\'oubli', 'hermionegranger@gmail.com', 613131345, 'Amnésie'),
(4, 'Londubat', 'Neville', 'masculin', '1980-07-30', '30 avenue du chapeau vautour', 'nevillelondubat@gmail.com', 625854565, 'Migraine'),
(5, 'Finnigan', 'Seamus', 'masculin', '1980-05-04', '4 rue du rhum', 'seamusfinnigan@gmail.com', 614749625, 'Brûlures '),
(6, 'Thomas', 'Dean', 'masculin', '1980-02-19', '17 chemin des loups', 'deanthomas@gmail.com', 678456523, 'Contusions épaule gauche'),
(7, 'Brown', 'Lavande', 'feminin', '1980-06-18', '6 chemin des fleurs', 'lavandebrown@gmail.com', 632127895, 'Attaquée par des oiseaux '),
(8, 'Patil', 'Parvati', 'feminin', '1979-12-27', '27 rue de la victoire', 'parvatipatil@gmail.com', 657482315, 'Fémur luxé'),
(9, 'Weasley', 'Ginny', 'feminin', '1981-08-11', '1 rue du Terrier', 'ginnyweasley@gmail.com', 695847586, 'Arcade ouverte'),
(10, 'Weasley', 'Fred', 'masculin', '1978-04-01', '1 rue du Terrier', 'fredweasley@gmail.com', 652523575, 'Perforation ventriculaire'),
(11, 'Weasley', 'George', 'masculin', '1978-04-01', '1 rue du Terrier', 'georgeweasley@gmail.com', 645781265, 'Perte d\'audition'),
(12, 'Lovegood', 'Luna', 'feminin', '1981-02-13', '8 chemin de la lune', 'lunalovegood@gmail.com', 674132985, 'Délires psychotiques'),
(13, 'Dubois', 'Olivier', 'masculin', '1975-06-15', '8 chemin des sapins', 'olivierdubois@gmail.com', 647351289, 'Trauma cranien'),
(14, 'Mendes', 'Jim', 'masculin', '1992-04-17', 'Dans sa chambrer en haut vdes escaliers', '', 156120320, 'Surmenage'),
(15, 'Watson', 'Justine', 'feminin', '1990-04-01', '15 rue des trèfles', 'justinewatson@gmail.com', 745454545, 'Mal au dos');

-- --------------------------------------------------------

--
-- Structure de la table `personnel`
--

CREATE TABLE `personnel` (
  `id_personnel` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(25) NOT NULL,
  `sexe` enum('masculin','feminin') NOT NULL,
  `date_de_naissance` date NOT NULL,
  `adresse` varchar(100) NOT NULL,
  `adresse_mail` varchar(50) NOT NULL,
  `numero_telephone` int(10) NOT NULL,
  `date_anciennete` date NOT NULL,
  `salaire` double(10,2) NOT NULL,
  `profession` enum('medecin','infirmier','pharmacien','technicien','agent_administrateur','rh') NOT NULL,
  `rpps` bigint(11) DEFAULT NULL,
  `specialite` enum('medecine_generale','cardiologie','chirurgie','pediatrie') DEFAULT NULL,
  `code_service` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `personnel`
--

INSERT INTO `personnel` (`id_personnel`, `nom`, `prenom`, `sexe`, `date_de_naissance`, `adresse`, `adresse_mail`, `numero_telephone`, `date_anciennete`, `salaire`, `profession`, `rpps`, `specialite`, `code_service`) VALUES
(1, 'Dumbledore', 'Albus', 'masculin', '1881-07-31', '1 boulevard de l\'école', 'albusdumbledore@gmail.com', 612312132, '1937-09-01', 5000.00, 'medecin', 10123456789, 'pediatrie', 'D'),
(2, 'McGonagall', 'Minerva', 'feminin', '1935-04-10', '2 rue du chat', 'minervamcgonagall@gmail.com', 658143297, '1957-09-01', 3500.00, 'medecin', 98765432101, 'cardiologie', 'B'),
(3, 'Rogue', 'Severus', 'masculin', '1960-11-09', '3 avenue des potions', 'severusrogue@gmail.com', 669855748, '1981-09-01', 3000.00, 'medecin', 66699955510, 'chirurgie', 'C'),
(4, 'Flitwick', 'Filius', 'masculin', '1910-10-17', '4 chemin des plumes', 'filiusflitwick@gmail.com', 612134578, '1945-09-01', 3500.00, 'medecin', 12134561593, 'chirurgie', 'B'),
(5, 'Chourave', 'Pomona', 'feminin', '1931-05-15', '5 sentier des roses', 'pomonachourave@gmail.com', 698453215, '1958-09-01', 3500.00, 'medecin', 58545652159, 'medecine_generale', 'A'),
(6, 'Bibine', 'Renée', 'feminin', '1946-02-15', '6 rue du vent', 'reneebibine@gmail.com', 678595751, '1975-09-01', 2500.00, 'medecin', 12894576356, 'medecine_generale', 'A'),
(7, 'Binns', 'Cuthbert', 'masculin', '1745-12-26', '7 rue des esprits', 'cuthbertbinns@gmail.com', 600000001, '1780-09-01', 3000.00, 'medecin', 90000000000, 'chirurgie', 'C'),
(8, 'Weasley', 'Molly', 'feminin', '1949-10-30', '1 rue du Terrier', 'mollyweasley@gmail.com', 623456347, '1977-09-01', 2000.00, 'infirmier', 57369124853, NULL, 'D'),
(9, 'Black', 'Sirius', 'masculin', '1959-11-03', '12 square Grimmaurd', 'siriusblack@gmail.com', 657941328, '1985-09-01', 1900.00, 'infirmier', 42613790305, NULL, 'B'),
(10, 'Lupin', 'Remus', 'masculin', '1960-03-30', '43 chemin de la lune', 'remuslupin@gmail.com', 694378205, '1985-09-01', 1900.00, 'infirmier', 42561301057, NULL, 'C'),
(11, 'Potter', 'James', 'masculin', '1960-03-27', '17 godric\'s hollow', 'jamespotter@gmail.com', 642103698, '1984-09-01', 1900.00, 'infirmier', 42874103609, NULL, 'A'),
(12, 'Malefoy', 'Narcissa', 'feminin', '1955-04-30', '22 avenue du serpent', 'narcissamalefoy@gmail.com', 682465730, '1987-09-01', 1800.00, 'infirmier', 31478529160, NULL, 'U'),
(13, 'Pomfresh', 'Poppy', 'feminin', '1940-06-21', '6 boulevard de l\'hopital', 'poppypomfresh@gmail.com', 669587214, '1961-09-01', 2400.00, 'pharmacien', 34719602058, NULL, NULL),
(14, 'Weasley', 'Arthur', 'masculin', '1950-02-06', '1 rue du terrier', 'arthurweasley@gmail.com', 698765433, '1975-09-01', 2000.00, 'technicien', NULL, NULL, NULL),
(15, 'Fudge', 'Cornelius', 'masculin', '1943-07-15', '17 rue du ministère', 'corneliusfudge@gmail.com', 612324509, '1963-08-07', 3000.00, 'agent_administrateur', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `rdv`
--

CREATE TABLE `rdv` (
  `reference_rdv` int(11) NOT NULL,
  `id_patient` int(8) NOT NULL,
  `id_personnel` int(5) NOT NULL,
  `date_heure_rdv` datetime NOT NULL,
  `num_salle` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `rdv`
--

INSERT INTO `rdv` (`reference_rdv`, `id_patient`, `id_personnel`, `date_heure_rdv`, `num_salle`) VALUES
(1, 1, 3, '2021-06-03 17:30:00', 5),
(2, 2, 3, '2021-06-03 10:30:00', 5),
(3, 3, 3, '2021-06-03 10:00:00', 5),
(4, 5, 3, '2021-06-03 11:00:00', 5),
(5, 6, 3, '2021-06-03 11:30:00', 5),
(6, 4, 3, '2021-06-03 12:00:00', 5),
(7, 12, 3, '2021-06-03 17:00:00', 5),
(8, 9, 3, '2021-06-03 16:30:00', 5),
(9, 7, 3, '2021-06-03 15:00:00', 5),
(10, 8, 3, '2021-06-03 12:30:00', 5),
(11, 10, 3, '2021-06-03 15:30:00', 5),
(12, 11, 3, '2021-06-03 16:00:00', 5),
(13, 1, 3, '2021-06-04 10:00:00', 5),
(14, 1, 2, '2021-06-03 11:30:00', 1),
(15, 2, 2, '2021-06-03 12:00:00', 1),
(16, 3, 2, '2021-06-03 12:30:00', 1),
(17, 4, 1, '2021-06-03 11:30:00', 2);

-- --------------------------------------------------------

--
-- Structure de la table `service`
--

CREATE TABLE `service` (
  `code_service` varchar(1) NOT NULL,
  `nom_service` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `service`
--

INSERT INTO `service` (`code_service`, `nom_service`) VALUES
('A', 'Médecine Générale'),
('B', 'Cardiologie'),
('C', 'Chirurgie'),
('D', 'Pédiatrie'),
('U', 'Urgences');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `analyse`
--
ALTER TABLE `analyse`
  ADD PRIMARY KEY (`reference_analyse`),
  ADD KEY `id_patient` (`id_patient`),
  ADD KEY `id_personnel` (`id_personnel`),
  ADD KEY `code_appareil` (`code_appareil`);

--
-- Index pour la table `appareil`
--
ALTER TABLE `appareil`
  ADD PRIMARY KEY (`code_appareil`);

--
-- Index pour la table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`reference_article`);

--
-- Index pour la table `chambre`
--
ALTER TABLE `chambre`
  ADD PRIMARY KEY (`code_chambre`),
  ADD KEY `code_service` (`code_service`);

--
-- Index pour la table `consultation`
--
ALTER TABLE `consultation`
  ADD PRIMARY KEY (`reference_consultation`),
  ADD KEY `id_patient` (`id_patient`),
  ADD KEY `id_personnel` (`id_personnel`);

--
-- Index pour la table `hospitalisation`
--
ALTER TABLE `hospitalisation`
  ADD KEY `id_patient` (`id_patient`),
  ADD KEY `code_lit` (`code_lit`);

--
-- Index pour la table `identification`
--
ALTER TABLE `identification`
  ADD PRIMARY KEY (`login_utilisateur`),
  ADD KEY `id_personnel` (`id_personnel`);

--
-- Index pour la table `lit`
--
ALTER TABLE `lit`
  ADD PRIMARY KEY (`code_lit`),
  ADD KEY `code_service` (`code_service`),
  ADD KEY `code_chambre` (`code_chambre`);

--
-- Index pour la table `modification_pharmacie`
--
ALTER TABLE `modification_pharmacie`
  ADD KEY `reference_article` (`reference_article`),
  ADD KEY `id_personnel` (`id_personnel`);

--
-- Index pour la table `ordonnance`
--
ALTER TABLE `ordonnance`
  ADD PRIMARY KEY (`reference_ordonnance`),
  ADD KEY `id_patient` (`id_patient`),
  ADD KEY `id_personnel` (`id_personnel`);

--
-- Index pour la table `patient`
--
ALTER TABLE `patient`
  ADD PRIMARY KEY (`id_patient`);

--
-- Index pour la table `personnel`
--
ALTER TABLE `personnel`
  ADD PRIMARY KEY (`id_personnel`),
  ADD KEY `code_service` (`code_service`);

--
-- Index pour la table `rdv`
--
ALTER TABLE `rdv`
  ADD PRIMARY KEY (`reference_rdv`),
  ADD KEY `id_patient` (`id_patient`),
  ADD KEY `id_personnel` (`id_personnel`);

--
-- Index pour la table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`code_service`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `analyse`
--
ALTER TABLE `analyse`
  MODIFY `reference_analyse` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `consultation`
--
ALTER TABLE `consultation`
  MODIFY `reference_consultation` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `ordonnance`
--
ALTER TABLE `ordonnance`
  MODIFY `reference_ordonnance` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `patient`
--
ALTER TABLE `patient`
  MODIFY `id_patient` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT pour la table `personnel`
--
ALTER TABLE `personnel`
  MODIFY `id_personnel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT pour la table `rdv`
--
ALTER TABLE `rdv`
  MODIFY `reference_rdv` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `analyse`
--
ALTER TABLE `analyse`
  ADD CONSTRAINT `analyse_ibfk_1` FOREIGN KEY (`id_patient`) REFERENCES `patient` (`id_patient`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `analyse_ibfk_2` FOREIGN KEY (`code_appareil`) REFERENCES `appareil` (`code_appareil`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `analyse_ibfk_3` FOREIGN KEY (`id_personnel`) REFERENCES `personnel` (`id_personnel`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Contraintes pour la table `chambre`
--
ALTER TABLE `chambre`
  ADD CONSTRAINT `chambre_ibfk_1` FOREIGN KEY (`code_service`) REFERENCES `service` (`code_service`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `consultation`
--
ALTER TABLE `consultation`
  ADD CONSTRAINT `consultation_ibfk_1` FOREIGN KEY (`id_patient`) REFERENCES `patient` (`id_patient`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `consultation_ibfk_2` FOREIGN KEY (`id_personnel`) REFERENCES `personnel` (`id_personnel`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Contraintes pour la table `hospitalisation`
--
ALTER TABLE `hospitalisation`
  ADD CONSTRAINT `hospitalisation_ibfk_1` FOREIGN KEY (`id_patient`) REFERENCES `patient` (`id_patient`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `hospitalisation_ibfk_2` FOREIGN KEY (`code_lit`) REFERENCES `lit` (`code_lit`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Contraintes pour la table `lit`
--
ALTER TABLE `lit`
  ADD CONSTRAINT `lit_ibfk_1` FOREIGN KEY (`code_service`) REFERENCES `service` (`code_service`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `lit_ibfk_2` FOREIGN KEY (`code_chambre`) REFERENCES `chambre` (`code_chambre`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `modification_pharmacie`
--
ALTER TABLE `modification_pharmacie`
  ADD CONSTRAINT `modification_pharmacie_ibfk_1` FOREIGN KEY (`reference_article`) REFERENCES `article` (`reference_article`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `modification_pharmacie_ibfk_2` FOREIGN KEY (`id_personnel`) REFERENCES `personnel` (`id_personnel`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Contraintes pour la table `ordonnance`
--
ALTER TABLE `ordonnance`
  ADD CONSTRAINT `ordonnance_ibfk_1` FOREIGN KEY (`id_patient`) REFERENCES `patient` (`id_patient`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `ordonnance_ibfk_2` FOREIGN KEY (`id_personnel`) REFERENCES `personnel` (`id_personnel`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Contraintes pour la table `personnel`
--
ALTER TABLE `personnel`
  ADD CONSTRAINT `personnel_ibfk_1` FOREIGN KEY (`code_service`) REFERENCES `service` (`code_service`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Contraintes pour la table `rdv`
--
ALTER TABLE `rdv`
  ADD CONSTRAINT `rdv_ibfk_1` FOREIGN KEY (`id_patient`) REFERENCES `patient` (`id_patient`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `rdv_ibfk_2` FOREIGN KEY (`id_personnel`) REFERENCES `personnel` (`id_personnel`) ON DELETE NO ACTION ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
