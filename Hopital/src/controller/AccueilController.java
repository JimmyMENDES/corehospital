package controller;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.swing.JTable;

import modele.donnees.DonneesDates;
import modele.humain.Patient;
import modele.immateriel.RDV;
import modele.jdbc.DAOAppareil;
import modele.jdbc.DAOArticle;
import modele.jdbc.DAOChambre;
import modele.jdbc.DAOHospitalisation;
import modele.jdbc.DAOLit;
import modele.jdbc.DAOPatient;
import modele.jdbc.DAOPersonnel;
import modele.jdbc.DAORDV;
import modele.jdbc.DAOService;
import modele.materiel.Article;
import vues.FenetreAccueil;
import vues.Login;
import vues.conteneurgraphiquelit.ConteneurGraphique1Lit;
import vues.conteneurgraphiquelit.ConteneurGraphique2Lit;
import vues.conteneurgraphiquelit.ConteneurGraphique4Lit;
import vues.tablemodel.PharmacieTableModel;
import vues.tablemodel.RechercherPatientTableModel;

public class AccueilController {
	
	private Login vueLogin;
	private FenetreAccueil fenetreAccueil;
	private int idPersonnelEnCours;
	// n�cessaire pour la gestion du tableau recherchePatient (par nom ou date)
	ArrayList<Patient> patientEnCours;
	ArrayList<Patient> patientEnCoursMesRDV;

	public AccueilController(Login vueLogin, int idPersonnelEnCours) {
		this.vueLogin = vueLogin;
		this.idPersonnelEnCours = idPersonnelEnCours;
		DAOPersonnel connectionTablePersonnel = new DAOPersonnel();
		if(idPersonnelEnCours == 0) {
			fenetreAccueil = new FenetreAccueil("superadmin");
			fenetreAccueil.getAjoutPersonnelConteneurGauche().addActionListener(new BouttonAjouterPersonnelConteneurGaucheListener());
			fenetreAccueil.getValiderFormulaireAjouterPersonnel().addActionListener(new BouttonValiderFormulaireAjouterPersonnelListener());
			fenetreAccueil.getSelectionProfessionFormulaireAjouterPersonnelComboBox().addActionListener(new ComboBoxProfessionFormulaireAjoutPersonnelListener()); 
			fenetreAccueil.getSelectionSpecialiteFormulaireAjouterPersonnelComboBox().addActionListener(new ComboBoxSpecialiteFormulaireAjoutPersonnelListener()); 
			fenetreAccueil.getEffacerFormulaireAjoutPersonnel().addActionListener(new BouttonEffacerFormulairePersonnelListener()); 
			fenetreAccueil.getSuppressionPersonnelConteneurGauche().addActionListener(new BouttonSuppressionPersonnelConteneurGaucheListener());
			fenetreAccueil.getSupprimerFormulaireSuppressionPersonnel().addActionListener(new BouttonSupprimerFormulaireSuppressionPersonnelListener());
			fenetreAccueil.getAjoutInstallationConteneurGauche().addActionListener(new BouttonAjoutInstallationsConteneurGaucheListener());
			fenetreAccueil.getAjouterFormulaireAjoutInstallations().addActionListener(new BouttonAjouterFormulaireAjoutInstallationsListener());
			fenetreAccueil.getServiceVideRadio().addActionListener(new RadioAjoutInstallationVideListener()); 
			fenetreAccueil.getServiceRempliRadio().addActionListener(new RadioAjoutInstallationRempliListener());
			fenetreAccueil.getChambreIndividuelleFormulaireAjoutInstallationsCheckBox().addActionListener(new CheckBoxChambreIndividuelleFormulaireAjouterInstallationsListener()); 
			fenetreAccueil.getChambreDoubleFormulaireAjoutInstallationsCheckBox().addActionListener(new CheckBoxChambreDoubleFormulaireAjouterInstallationsListener());
			fenetreAccueil.getChambreQuadrupleFormulaireAjoutInstallationsCheckBox().addActionListener(new CheckBoxChambreQuadrupleFormulaireAjouterInstallationsListener());
			fenetreAccueil.getSuppressionInstallationConteneurGauche().addActionListener(new BouttonSuppressionInstallationsConteneurGaucheListener());
			fenetreAccueil.getSupprimerFormulaireSuppressionInstallations().addActionListener(new BouttonSupprimerInstallationListener()); 
			fenetreAccueil.getChoixServiceFormulaireSuppressionInstallations().addActionListener(new ComboBoxChoixTypeInstallationFormulaireSupprimerInstallationListener()); 
			fenetreAccueil.getChoixChambreFormulaireSuppressionInstallations().addActionListener(new ComboBoxChoixTypeInstallationFormulaireSupprimerInstallationListener());
			fenetreAccueil.getChoixLitFormulaireSuppressionInstallations().addActionListener(new ComboBoxChoixTypeInstallationFormulaireSupprimerInstallationListener());
			fenetreAccueil.getAjoutMaterielConteneurGauche().addActionListener(new BouttonAjoutAppareilMedicalConteneurGaucheListener());
			fenetreAccueil.getAjouterAppareilFormulaireAjoutAppareilMedical().addActionListener(new BouttonAjouterAppareilListener());
			fenetreAccueil.getSuppressionMaterielConteneurGauche().addActionListener(new BouttonSuppressionAppareilMedicalListener()); 
			fenetreAccueil.getSupprimerAppareilFormulaireSuppressionAppareilMedical().addActionListener(new BouttonSupprimerAppareilListener());
			fenetreAccueil.getLogoutConteneurGauche().addActionListener(new BouttonLogoutConteneurGaucheListener());
		}
		else {
			fenetreAccueil = new FenetreAccueil(connectionTablePersonnel.professionPersonnel(idPersonnelEnCours));
			String profession = connectionTablePersonnel.professionPersonnel(idPersonnelEnCours);
			if(profession.equalsIgnoreCase("agent_administrateur")) {
				fenetreAccueil.getCreerPatientConteneurGauche().addActionListener(new BouttonCreerPatientConteneurGaucheListener());
				fenetreAccueil.getValiderFormulaireCreerPatient().addActionListener(new BouttonCreerPatientConteneurDroitListener());
				fenetreAccueil.getEffacerFormulaireCreerPatient().addActionListener(new BouttonEffacerFormulaireCreerPatientListener()); 
				fenetreAccueil.getRechercherPatientConteneurGauche().addActionListener(new BouttonRechercherPatientConteneurGaucheListener());
				fenetreAccueil.getParIdentifiantFormulaireRechercherPatientRadio().addActionListener(new RadioParIdentifiantFormulaireRechercherPatientListener()); 
				fenetreAccueil.getParNomFormulaireRechercherPatientRadio().addActionListener(new RadioParNomFormulaireRechercherPatientListener());
				fenetreAccueil.getParDateDeNaissanceFormulaireRechercherPatientRadio().addActionListener(new RadioParDateFormulaireRechercherPatientListener());
				setListenerRechercherPatient();
				fenetreAccueil.getLogoutConteneurGauche().addActionListener(new BouttonLogoutConteneurGaucheListener());
			}
			else if(profession.equalsIgnoreCase("rh")) {}
			
			else if(profession.equalsIgnoreCase("technicien")) {
				fenetreAccueil.getRechercherPatientConteneurGauche().addActionListener(new BouttonRechercherPatientConteneurGaucheListener());
				fenetreAccueil.getParIdentifiantFormulaireRechercherPatientRadio().addActionListener(new RadioParIdentifiantFormulaireRechercherPatientListener()); 
				fenetreAccueil.getParNomFormulaireRechercherPatientRadio().addActionListener(new RadioParNomFormulaireRechercherPatientListener());
				fenetreAccueil.getParDateDeNaissanceFormulaireRechercherPatientRadio().addActionListener(new RadioParDateFormulaireRechercherPatientListener());
				setListenerRechercherPatient();
				fenetreAccueil.getLogoutConteneurGauche().addActionListener(new BouttonLogoutConteneurGaucheListener());
			}
			else if(profession.equalsIgnoreCase("medecin")) {
				fenetreAccueil.getCreerPatientConteneurGauche().addActionListener(new BouttonCreerPatientConteneurGaucheListener());
				fenetreAccueil.getValiderFormulaireCreerPatient().addActionListener(new BouttonCreerPatientConteneurDroitListener());
				fenetreAccueil.getEffacerFormulaireCreerPatient().addActionListener(new BouttonEffacerFormulaireCreerPatientListener()); 
				fenetreAccueil.getRechercherPatientConteneurGauche().addActionListener(new BouttonRechercherPatientConteneurGaucheListener());
				fenetreAccueil.getParIdentifiantFormulaireRechercherPatientRadio().addActionListener(new RadioParIdentifiantFormulaireRechercherPatientListener()); 
				fenetreAccueil.getParNomFormulaireRechercherPatientRadio().addActionListener(new RadioParNomFormulaireRechercherPatientListener());
				fenetreAccueil.getParDateDeNaissanceFormulaireRechercherPatientRadio().addActionListener(new RadioParDateFormulaireRechercherPatientListener());
				setListenerRechercherPatient();
				fenetreAccueil.getMesRDVConteneurGauche().addActionListener(new BouttonMesRDVListener());
				fenetreAccueil.getLogoutConteneurGauche().addActionListener(new BouttonLogoutConteneurGaucheListener());
			}
			else if(profession.equalsIgnoreCase("infirmier")) {
				fenetreAccueil.getRechercherPatientConteneurGauche().addActionListener(new BouttonRechercherPatientConteneurGaucheListener());
				fenetreAccueil.getParIdentifiantFormulaireRechercherPatientRadio().addActionListener(new RadioParIdentifiantFormulaireRechercherPatientListener()); 
				fenetreAccueil.getParNomFormulaireRechercherPatientRadio().addActionListener(new RadioParNomFormulaireRechercherPatientListener());
				fenetreAccueil.getParDateDeNaissanceFormulaireRechercherPatientRadio().addActionListener(new RadioParDateFormulaireRechercherPatientListener());
				setListenerRechercherPatient();
				fenetreAccueil.getEtatServiceConteneurGauche().addActionListener(new BouttonEtatServiceListener());
				fenetreAccueil.getLogoutConteneurGauche().addActionListener(new BouttonLogoutConteneurGaucheListener());
			}
			else if(profession.equalsIgnoreCase("pharmacien")) {
				updateEtatPharmacie();
				fenetreAccueil.getEtatPharmacieConteneurGauche().addActionListener(new BouttonEtatPharmacieConteneurGaucheListener()); 
				fenetreAccueil.getRechercherPatientConteneurGauche().addActionListener(new BouttonRechercherPatientConteneurGaucheListener());
				fenetreAccueil.getParIdentifiantFormulaireRechercherPatientRadio().addActionListener(new RadioParIdentifiantFormulaireRechercherPatientListener()); 
				fenetreAccueil.getParNomFormulaireRechercherPatientRadio().addActionListener(new RadioParNomFormulaireRechercherPatientListener());
				fenetreAccueil.getParDateDeNaissanceFormulaireRechercherPatientRadio().addActionListener(new RadioParDateFormulaireRechercherPatientListener());
				setListenerRechercherPatient();
				fenetreAccueil.getAjoutRetraitArticleConteneurGauche().addActionListener(new BouttonAjoutRetraitArticleConteneurGaucheListener());  
				fenetreAccueil.getAjouterArticleFormulaireAjoutRetraitArticle().addActionListener(new BouttonAjouterArticleListener());
				fenetreAccueil.getRetraitArticleFormulaireAjoutRetraitArticle().addActionListener(new BouttonRetirerArticleListener());
				fenetreAccueil.getCreerArticleConteneurGauche().addActionListener(new BouttonCreerArticleConteneurGaucheListener()); 
				fenetreAccueil.getValiderFormulaireCreerArticle().addActionListener(new BouttonCreerArticleListener());
				fenetreAccueil.getEffacerFormulaireCreerArticle().addActionListener(new BouttonEffacerFormulaireCreerArticleListener()); 
				fenetreAccueil.getSupprimerArticleConteneurGauche().addActionListener(new BouttonSuppressionArticleConteneurGaucheListener()); 
				fenetreAccueil.getSupprimerArticleFormulaire().addActionListener(new BouttonSupprimerArticleListener());
				fenetreAccueil.getLogoutConteneurGauche().addActionListener(new BouttonLogoutConteneurGaucheListener());
			}
		}
	}
	
	// M�thodes
	
	/**
	 * M�thode appel�e par le constructeur pour ajouter les Listener li� � la recherche de Patients.
	 */
	private void setListenerRechercherPatient() {
		fenetreAccueil.getIdentifiantFormulaireRechercherPatientTextField().addKeyListener(new ChampsRechercheListener());
		fenetreAccueil.getNomFormulaireRechercherPatientTextField().addKeyListener(new ChampsRechercheListener());
		fenetreAccueil.getDateDeNaissanceFormulaireRechercherPatientTextField().addKeyListener(new ChampsRechercheListener());
		fenetreAccueil.getValiderFormulaireRechercherPatient().addActionListener(new BouttonRechercherPatientListener());
	}
	
	/**
	 * M�thode permettant de v�rifier si une date est au bon format.
	 * @param date
	 * @throws LettresPresentesDansUneDate si une lettre est pr�sente dans date
	 * @throws NombreCaracteresNonConforme si date.length() < 7 ou date.length() > 8
	 * @throws NombreDeMoisTropGrandOuNull si la valeur du mois est de 0 ou > 12
	 * @throws NombreDeJoursTropGrandOuNull  si la valeur du jour est de 0 ou > 31
	 * @throws DateTimeException si le jour n'existe pas dans le mois (exemple 30 f�vrier)
	 */
	private void verificationFormatDate(String date) {
		for(int i = 0 ; i < date.length() ; i++) {
			if(!Character.isDigit(date.charAt(i))) throw new LettresPresentesDansUneDate();
		}
		if(date.length() != 7 && date.length() != 8) throw new NombreCaracteresNonConforme();
		if(date.length() == 7) {
			int mois = Integer.parseInt(date.charAt(1)+""+date.charAt(2));
			if(mois == 0 || mois > 12) throw new NombreDeMoisTropGrandOuNull();
		}
		if(date.length() == 8) {
			int jour = Integer.parseInt(date.charAt(0)+""+date.charAt(1));
			if(jour == 0 || jour > 31) throw new NombreDeJoursTropGrandOuNull();
			int mois = Integer.parseInt(date.charAt(2)+""+date.charAt(3));
			if(mois == 0 || mois > 12) throw new NombreDeMoisTropGrandOuNull();
		}
	}
	
	/**
	 * M�thode permettant de v�rifier si un num�ro est au bon format.
	 * @param numero
	 * @throws LettresPresentesDansUnNumero si une lettre est pr�sente dans numero
	 * @throws NombreCaracteresNonConforme si numero.length() != 10
	 */
	private void verificationFormatTelephone(String numero) {
		for(int i = 0 ; i < numero.length() ; i++) {
			if(!Character.isDigit(numero.charAt(i))) throw new LettresPresentesDansUnNumero();
		}
		if(numero.length()!= 10) throw new NombreCaracteresNonConforme();
	}
	
	/**
	 * M�thode permettant de v�rifier si un salaire est au bon format.
	 * @param salaire
	 * @throws LettresPresentesDansUnNumero si une lettre est pr�sente dans numero
	 */
	private void verificationFormatSalaire(String salaire) {
		for(int i = 0 ; i < salaire.length() ; i++) {
			if(salaire.charAt(i) != '.') {
				if(!Character.isDigit(salaire.charAt(i))) throw new LettresPresentesDansUnSalaire();
			}
		}
	}
	
	/**
	 * M�thode permettant de v�rifier si un rpps est au bon format.
	 * @param rpps
	 * @throws LettresPresentesDansUnNumero si une lettre est pr�sente dans numero
	 * @throws NombreCaracteresNonConforme si rpps.length() != 11
	 */
	private void verificationFormatRPPS(String rpps) {
		for(int i = 0 ; i < rpps.length() ; i++) {
			if(!Character.isDigit(rpps.charAt(i))) throw new LettresPresentesDansUnRPPS();
		}
		if(rpps.length()!= 11) throw new NombreCaracteresNonConforme();
	}
	
	/**
	 * M�thode permettant de v�rifier si un N�Identifiant est au bon format.
	 * @param id
	 * @throws LettresPresentesDansUnNumero si une lettre est pr�sente dans numero
	 */
	private void verificationFormatID(String id) {
		for(int i = 0 ; i < id.length() ; i++) {
			if(!Character.isDigit(id.charAt(i))) throw new LettresPresentesDansUnID();
		}
	}
	
	/**
	 * M�thode retournant le conteneur de l'image de la chambre individuelle.
	 * @param numeroChambre
	 * @return numm si numeroChambre est > 8, le conteneur de la chambre sinon
	 */
	private ConteneurGraphique1Lit getConteneurChambreIndividuelle(int numeroChambre) {
		if(numeroChambre == 1) {
			return fenetreAccueil.getConteneurDroiteEtatServiceLigne1Cellule1();
		}
		else if(numeroChambre == 2) {
			return fenetreAccueil.getConteneurDroiteEtatServiceLigne1Cellule2();
		}
		else if(numeroChambre == 3) {
			return fenetreAccueil.getConteneurDroiteEtatServiceLigne1Cellule3();
		}
		else if(numeroChambre == 4) {
			return fenetreAccueil.getConteneurDroiteEtatServiceLigne1Cellule4();
		}
		else if(numeroChambre == 5) {
			return fenetreAccueil.getConteneurDroiteEtatServiceLigne2Cellule1();
		}
		else if(numeroChambre == 6) {
			return fenetreAccueil.getConteneurDroiteEtatServiceLigne2Cellule2();
		}
		else if(numeroChambre == 7) {
			return fenetreAccueil.getConteneurDroiteEtatServiceLigne2Cellule3();
		}
		else if(numeroChambre == 8) {
			return fenetreAccueil.getConteneurDroiteEtatServiceLigne2Cellule4();
		}
		else return null;
	}
	
	/**
	 * M�thode retournant le conteneur de l'image de la chambredouble.
	 * @param numeroChambre
	 * @return numm si numeroChambre est > 16, le conteneur de la chambre sinon
	 */
	private ConteneurGraphique2Lit getConteneurChambreDouble(int numeroChambre) {
		if(numeroChambre == 9) {
			return fenetreAccueil.getConteneurDroiteEtatServiceLigne3Cellule1();
		}
		else if(numeroChambre == 10) {
			return fenetreAccueil.getConteneurDroiteEtatServiceLigne3Cellule2();
		}
		else if(numeroChambre == 11) {
			return fenetreAccueil.getConteneurDroiteEtatServiceLigne4Cellule1();
		}
		else if(numeroChambre == 12) {
			return fenetreAccueil.getConteneurDroiteEtatServiceLigne4Cellule2();
		}
		else if(numeroChambre == 13) {
			return fenetreAccueil.getConteneurDroiteEtatServiceLigne5Cellule1();
		}
		else if(numeroChambre == 14) {
			return fenetreAccueil.getConteneurDroiteEtatServiceLigne5Cellule2();
		}
		else if(numeroChambre == 15) {
			return fenetreAccueil.getConteneurDroiteEtatServiceLigne6Cellule1();
		}
		else if(numeroChambre == 16) {
			return fenetreAccueil.getConteneurDroiteEtatServiceLigne6Cellule2();
		}
		else return null;
	}
	
	/**
	 * M�thode retournant le conteneur de l'image de la chambre quadruple.
	 * @param numeroChambre
	 * @return numm si numeroChambre est > 17, le conteneur de la chambre sinon
	 */
	private ConteneurGraphique4Lit getConteneurChambreQuadruple(int numeroChambre) {
		if(numeroChambre == 17) {
			return fenetreAccueil.getConteneurDroiteEtatServiceLigne7();
		}
		else if(numeroChambre == 18) {
			return fenetreAccueil.getConteneurDroiteEtatServiceLigne8();
		}
		else if(numeroChambre == 19) {
			return fenetreAccueil.getConteneurDroiteEtatServiceLigne9();
		}
		else if(numeroChambre == 20) {
			return fenetreAccueil.getConteneurDroiteEtatServiceLigne10();
		}
		else return null;
	}
	
	/**
	 * M�thode permettant de mettre � jour la pharmacie.
	 */
	public void updateEtatPharmacie() {
		DAOArticle connectionTableArticle = new DAOArticle();
		ArrayList<Article> listeArticlePharmacie = connectionTableArticle.inventaireArticleDAO();
		fenetreAccueil.getEtatPharmacieTable().setModel(new PharmacieTableModel(listeArticlePharmacie.size()));
		for(int i = 0 ; i < listeArticlePharmacie.size() ; i++) {
			fenetreAccueil.getEtatPharmacieTable().setValueAt(listeArticlePharmacie.get(i).getReference(), i, 0);
			fenetreAccueil.getEtatPharmacieTable().setValueAt(listeArticlePharmacie.get(i).getNom(), i, 1);
			fenetreAccueil.getEtatPharmacieTable().setValueAt(listeArticlePharmacie.get(i).getQuantite(), i, 2);
			fenetreAccueil.getEtatPharmacieTable().setValueAt(listeArticlePharmacie.get(i).getClass().toString().substring(22), i, 3);
		}
	}
	
	// Exceptions
	
	@SuppressWarnings("serial")
	class LettresPresentesDansUneDate extends IllegalArgumentException {}
	@SuppressWarnings("serial")
	class LettresPresentesDansUnNumero extends IllegalArgumentException {}
	@SuppressWarnings("serial")
	class LettresPresentesDansUnSalaire extends IllegalArgumentException {}
	@SuppressWarnings("serial")
	class LettresPresentesDansUnRPPS extends IllegalArgumentException {}
	@SuppressWarnings("serial")
	class LettresPresentesDansUnID extends IllegalArgumentException {}
	@SuppressWarnings("serial")
	class NombreCaracteresNonConforme extends IllegalArgumentException {}
	@SuppressWarnings("serial")
	class NombreDeJoursTropGrandOuNull extends IllegalArgumentException {}
	@SuppressWarnings("serial")
	class NombreDeMoisTropGrandOuNull extends IllegalArgumentException {}

	
	// Listener
	
	class BouttonCreerPatientConteneurGaucheListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			fenetreAccueil.getLayoutMultiPanelDroite().show(fenetreAccueil.getConteneurDroite(),"panelCreer");
		}
	}
	
	@SuppressWarnings("unused")
	class BouttonCreerPatientConteneurDroitListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			String nom = fenetreAccueil.getNomFormulaireCreerPatientTextField().getText();
			if(nom.isEmpty() || nom.isBlank()) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner le nom du patient");
				return;
			}
			String prenom = fenetreAccueil.getPrenomFormulaireCreerPatientTextField().getText();
			if(prenom.isEmpty() || prenom.isBlank()) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner le pr�nom du patient");
				return;
			}
			String sexe = fenetreAccueil.getSelectionSexeFormulaireCreerPatientButtonGroup().getSelection().getActionCommand();
			String dateDeNaissanceString = fenetreAccueil.getDateDeNaissanceFormulaireCreerPatientTextField().getText();
			if(dateDeNaissanceString.isEmpty() || dateDeNaissanceString.isBlank()) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner la date de naissance du patient");
				return;
			}
			try {
				verificationFormatDate(dateDeNaissanceString);
			}
			catch(LettresPresentesDansUneDate e) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner une date comportant uniquement des chiffres");
				return;
			}
			catch(NombreCaracteresNonConforme e) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner une date au format jjmmaaaa");
				return;
			}
			catch(NombreDeJoursTropGrandOuNull e) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner un jour compris entre 1 et 31");
				return;
			}
			catch(NombreDeMoisTropGrandOuNull e) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner une mois compris entre 1 et 12");
				return;
			}
			catch(DateTimeException e) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Ce jour de ce mois n'existe pas");
				return;
			}
			LocalDate dateDeNaissance = DonneesDates.fromStringToLocalDate(dateDeNaissanceString);
			String adresse = fenetreAccueil.getAdresseFormulaireCreerPatientTextArea().getText();
			if(adresse.isEmpty() || adresse.isBlank()) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner l'adresse du patient");
				return;
			}
			String mail = fenetreAccueil.getMailFormulaireCreerPatientTextField().getText();
			String numeroString = fenetreAccueil.getTelephoneFormulaireCreerPatientTextField().getText();
			if(numeroString.isEmpty() || numeroString.isBlank()) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner le num�ro de t�l�phone du patient");
				return;
			}
			try {
				verificationFormatTelephone(numeroString);
			}
			catch(LettresPresentesDansUneDate e) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner un num�ro comportant uniquement des chiffres");
				return;
			}
			catch(NombreCaracteresNonConforme e) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner un num�ro au format 0123456789");
				return;
			}
			long numero = Long.parseLong(numeroString);
			String pathologie = fenetreAccueil.getPathologiesFormulaireCreerPatientTextArea().getText();
			if(pathologie.isEmpty() || pathologie.isBlank()) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner les pathologies du patient");
				return;
			}
			
			DAOPatient connectionTablePatient = new DAOPatient();
			int idPatient = connectionTablePatient.creerPatientDAO(nom, prenom, sexe, dateDeNaissance, adresse, mail, numero, pathologie);
			if(idPatient != -1) {
				fenetreAccueil.getEffacerFormulaireCreerPatient().doClick();
				OngletPatientController nouvelOngletPatientController = new OngletPatientController(fenetreAccueil, idPersonnelEnCours, idPatient, nom, prenom);
			}
			else {
				JOptionPane.showMessageDialog(fenetreAccueil, "Erreur");
			}
		}
	}
	
	class BouttonEffacerFormulaireCreerPatientListener implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			fenetreAccueil.getNomFormulaireCreerPatientTextField().setText("");
			fenetreAccueil.getPrenomFormulaireCreerPatientTextField().setText("");
			fenetreAccueil.getSelectionSexeFormulaireCreerPatientButtonGroup().clearSelection();
			fenetreAccueil.getDateDeNaissanceFormulaireCreerPatientTextField().setText("");
			fenetreAccueil.getAdresseFormulaireCreerPatientTextArea().setText("");
			fenetreAccueil.getMailFormulaireCreerPatientTextField().setText("");
			fenetreAccueil.getTelephoneFormulaireCreerPatientTextField().setText("");
			fenetreAccueil.getPathologiesFormulaireCreerPatientTextArea().setText("");
		}
	}
	
	class BouttonRechercherPatientConteneurGaucheListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			fenetreAccueil.getLayoutMultiPanelDroite().show(fenetreAccueil.getConteneurDroite(),"panelRechercher");
		}
	}
	
	@SuppressWarnings("unused")
	class BouttonRechercherPatientListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			DAOPatient connectionTablePatient = new DAOPatient();
			if(fenetreAccueil.getSelectionMethodeRechercheFormulaireRechercherPatientButtonGroup().getSelection().getActionCommand() == "identifiant") {
				String idPatientString = fenetreAccueil.getIdentifiantFormulaireRechercherPatientTextField().getText();
				if(idPatientString.isEmpty() || idPatientString.isBlank()) {
					JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner le num�ro d'identifiant du patient recherch�");
					return;
				}
				try {
					verificationFormatID(idPatientString);
				}
				catch(LettresPresentesDansUnID e) {
					JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner un N�Identifiant ne comportant que des chiffres");
					return;
				}
				int idPatient = Integer.parseInt(fenetreAccueil.getIdentifiantFormulaireRechercherPatientTextField().getText());  
				Patient patientEnCours = connectionTablePatient.rechercheParIDDAO(idPatient);
				if(patientEnCours != null) {
					OngletPatientController nouvelOngletPatientController = new OngletPatientController(fenetreAccueil, idPersonnelEnCours, idPatient, patientEnCours.getNom(), patientEnCours.getPrenom());
				}
				else {
					JOptionPane.showMessageDialog(fenetreAccueil, "Aucun patient trouv� avec ce num�ro d'identifiant");
				}
			}
			else if(fenetreAccueil.getSelectionMethodeRechercheFormulaireRechercherPatientButtonGroup().getSelection().getActionCommand() == "nom") {
				String nomPatient = fenetreAccueil.getNomFormulaireRechercherPatientTextField().getText();
				if(nomPatient.isEmpty() || nomPatient.isBlank()) {
					JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner le nom du patient recherch�");
					return;
				}
				patientEnCours = connectionTablePatient.rechercheParNomDAO(nomPatient);
				if(patientEnCours != null) {
					if(patientEnCours.isEmpty()) {
						JOptionPane.showMessageDialog(fenetreAccueil, "Aucun patient trouv� avec ce nom");
					}
					else if(patientEnCours.size() == 1) {
						OngletPatientController nouvelOngletPatientController = new OngletPatientController(fenetreAccueil, idPersonnelEnCours, patientEnCours.get(0).getIDPatient(), patientEnCours.get(0).getNom(), patientEnCours.get(0).getPrenom());
					}
					else {
						fenetreAccueil.getLayoutMultiPanelconteneurDroiteRechercherPatientBas().show(fenetreAccueil.getConteneurDroiteRechercherPatientBas(), "panelRecherche");
						fenetreAccueil.getRetourFormulaireRechercherPatient().setModel(new RechercherPatientTableModel(patientEnCours.size()));
						for(int i = 0 ; i < patientEnCours.size() ; i++) {
							fenetreAccueil.getRetourFormulaireRechercherPatient().setValueAt(patientEnCours.get(i).getNom(), i, 0);
							fenetreAccueil.getRetourFormulaireRechercherPatient().setValueAt(patientEnCours.get(i).getPrenom(), i, 1);
							fenetreAccueil.getRetourFormulaireRechercherPatient().setValueAt(DonneesDates.formatDate(patientEnCours.get(i).getDateDeNaissance()), i, 2);
							fenetreAccueil.getRetourFormulaireRechercherPatient().setValueAt(patientEnCours.get(i).getAdresse(), i, 3);
						}
						fenetreAccueil.getRetourFormulaireRechercherPatient().addMouseListener(new TableRechercherPatientListener());
					}
				}
			}
			
			else if(fenetreAccueil.getSelectionMethodeRechercheFormulaireRechercherPatientButtonGroup().getSelection().getActionCommand() == "dateDeNaissance") {
				String dateDeNaissanceString = fenetreAccueil.getDateDeNaissanceFormulaireRechercherPatientTextField().getText();
				if(dateDeNaissanceString.isEmpty() || dateDeNaissanceString.isBlank()) {
					JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner la date de naissance du patient");
					return;
				}
				try {
					verificationFormatDate(dateDeNaissanceString);
				}
				catch(LettresPresentesDansUneDate e) {
					JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner une date comportant uniquement des chiffres");
					return;
				}
				catch(NombreCaracteresNonConforme e) {
					JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner une date au format jjmmaaaa");
					return;
				}
				catch(NombreDeJoursTropGrandOuNull e) {
					JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner un jour compris entre 1 et 31");
					return;
				}
				catch(NombreDeMoisTropGrandOuNull e) {
					JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner une mois compris entre 1 et 12");
					return;
				}
				catch(DateTimeException e) {
					JOptionPane.showMessageDialog(fenetreAccueil, "Ce jour de ce mois n'existe pas");
					return;
				}
				LocalDate dateDeNaissancePatient = DonneesDates.fromStringToLocalDate(dateDeNaissanceString); 
				patientEnCours = connectionTablePatient.rechercheParDateDAO(dateDeNaissancePatient);
				if(patientEnCours != null) {
					if(patientEnCours.isEmpty()) {
						JOptionPane.showMessageDialog(fenetreAccueil, "Aucun patient trouv� avec cette date de naissance");
					}
					else if(patientEnCours.size() == 1) {
						OngletPatientController nouvelOngletPatientController = new OngletPatientController(fenetreAccueil, idPersonnelEnCours, patientEnCours.get(0).getIDPatient(), patientEnCours.get(0).getNom(), patientEnCours.get(0).getPrenom());
					}
					else {
						fenetreAccueil.getLayoutMultiPanelconteneurDroiteRechercherPatientBas().show(fenetreAccueil.getConteneurDroiteRechercherPatientBas(), "panelRecherche");
						fenetreAccueil.getRetourFormulaireRechercherPatient().setModel(new RechercherPatientTableModel(patientEnCours.size()));
						for(int i = 0 ; i < patientEnCours.size() ; i++) {
							fenetreAccueil.getRetourFormulaireRechercherPatient().setValueAt(patientEnCours.get(i).getNom(), i, 0);
							fenetreAccueil.getRetourFormulaireRechercherPatient().setValueAt(patientEnCours.get(i).getPrenom(), i, 1);
							fenetreAccueil.getRetourFormulaireRechercherPatient().setValueAt(DonneesDates.formatDate(patientEnCours.get(i).getDateDeNaissance()), i, 2);
							fenetreAccueil.getRetourFormulaireRechercherPatient().setValueAt(patientEnCours.get(i).getAdresse(), i, 3);
						}
						fenetreAccueil.getRetourFormulaireRechercherPatient().addMouseListener(new TableRechercherPatientListener());
					}
				}
			}
		}
	}
	
	class RadioParIdentifiantFormulaireRechercherPatientListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			fenetreAccueil.getIdentifiantFormulaireRechercherPatientTextField().setEnabled(true);
			fenetreAccueil.getNomFormulaireRechercherPatientTextField().setEnabled(false);
			fenetreAccueil.getNomFormulaireRechercherPatientTextField().setText("");
			fenetreAccueil.getDateDeNaissanceFormulaireRechercherPatientTextField().setEnabled(false);
			fenetreAccueil.getDateDeNaissanceFormulaireRechercherPatientTextField().setText("");
		}
	}
	
	class RadioParNomFormulaireRechercherPatientListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			fenetreAccueil.getIdentifiantFormulaireRechercherPatientTextField().setEnabled(false);
			fenetreAccueil.getIdentifiantFormulaireRechercherPatientTextField().setText("");
			fenetreAccueil.getNomFormulaireRechercherPatientTextField().setEnabled(true);
			fenetreAccueil.getDateDeNaissanceFormulaireRechercherPatientTextField().setEnabled(false);
			fenetreAccueil.getDateDeNaissanceFormulaireRechercherPatientTextField().setText("");
		}
	}

	class RadioParDateFormulaireRechercherPatientListener implements ActionListener {
	
		public void actionPerformed(ActionEvent event) {
			fenetreAccueil.getIdentifiantFormulaireRechercherPatientTextField().setEnabled(false);
			fenetreAccueil.getIdentifiantFormulaireRechercherPatientTextField().setText("");
			fenetreAccueil.getNomFormulaireRechercherPatientTextField().setEnabled(false);
			fenetreAccueil.getNomFormulaireRechercherPatientTextField().setText("");
			fenetreAccueil.getDateDeNaissanceFormulaireRechercherPatientTextField().setEnabled(true);
		}
	}
	
	@SuppressWarnings("unused")
	class TableRechercherPatientListener implements MouseListener {
		public void mousePressed(MouseEvent e) {
			 JTable table =(JTable) e.getSource();
			 Point point = e.getPoint();
			 int row = table.rowAtPoint(point);
		     if (e.getClickCount() == 2) {
				OngletPatientController nouvelOngletPatientController = new OngletPatientController(fenetreAccueil, idPersonnelEnCours, patientEnCours.get(row).getIDPatient(), patientEnCours.get(row).getNom(), patientEnCours.get(row).getPrenom());
		     }
		}
		public void mouseClicked(MouseEvent e) {}
		public void mouseReleased(MouseEvent e) {}
		public void mouseEntered(MouseEvent e) {}
		public void mouseExited(MouseEvent e) {}
	}
	
	class ChampsRechercheListener implements KeyListener {
		
		public void keyPressed(KeyEvent e) {
			if(e.getKeyCode() == KeyEvent.VK_ENTER) fenetreAccueil.getValiderFormulaireRechercherPatient().doClick();
		}
		public void keyTyped(KeyEvent e) {}
		public void keyReleased(KeyEvent e) {}
	}
	
	class BouttonLogoutConteneurGaucheListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			fenetreAccueil.dispose();
			vueLogin.setVisible(true);
		}
	}
	
	// Profil SuperAdmin
	
	class BouttonAjouterPersonnelConteneurGaucheListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			fenetreAccueil.getLayoutMultiPanelDroite().show(fenetreAccueil.getConteneurDroite(),"panelAjoutPersonnel");
		}
	}

	class BouttonValiderFormulaireAjouterPersonnelListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			DAOPersonnel connectionTablePersonnel = new DAOPersonnel();
			String nom = fenetreAccueil.getNomFormulaireAjouterPersonnelTextField().getText();
			if(nom.isEmpty() || nom.isBlank()) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner le nom du membre du personnel");
				return;
			}
			String prenom = fenetreAccueil.getPrenomFormulaireAjouterPersonnelTextField().getText();
			if(prenom.isEmpty() || prenom.isBlank()) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner le pr�nom du membre du personnel");
				return;
			}
			String sexe = fenetreAccueil.getSelectionSexeFormulaireAjouterPersonnelButtonGroup().getSelection().getActionCommand();
			String dateDeNaissanceString = fenetreAccueil.getDateDeNaissanceFormulaireAjouterPersonnelTextField().getText();
			if(dateDeNaissanceString.isEmpty() || dateDeNaissanceString.isBlank()) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner la date de naissance du membre du personnel");
				return;
			}
			try {
				verificationFormatDate(dateDeNaissanceString);
			}
			catch(LettresPresentesDansUneDate e) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner une date comportant uniquement des chiffres");
				return;
			}
			catch(NombreCaracteresNonConforme e) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner une date au format jjmmaaaa");
				return;
			}
			catch(NombreDeJoursTropGrandOuNull e) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner un jour compris entre 1 et 31");
				return;
			}
			catch(NombreDeMoisTropGrandOuNull e) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner une mois compris entre 1 et 12");
				return;
			}
			catch(DateTimeException e) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Ce jour de ce mois n'existe pas");
				return;
			}
			LocalDate dateDeNaissance = DonneesDates.fromStringToLocalDate(dateDeNaissanceString);
			String adresse = fenetreAccueil.getAdresseFormulaireAjouterPersonnelTextArea().getText();
			if(adresse.isEmpty() || adresse.isBlank()) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner l'adresse du membre du personnel");
				return;
			}
			String mail = fenetreAccueil.getMailFormulaireAjouterPersonnelTextField().getText();
			String numeroString = fenetreAccueil.getTelephoneFormulaireAjouterPersonnelTextField().getText();
			if(numeroString.isEmpty() || numeroString.isBlank()) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner le num�ro de t�l�phone du membre du personnel");
				return;
			}
			try {
				verificationFormatTelephone(numeroString);
			}
			catch(LettresPresentesDansUneDate e) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner un num�ro comportant uniquement des chiffres");
				return;
			}
			catch(NombreCaracteresNonConforme e) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner un num�ro au format 0123456789");
				return;
			}
			long numeroTelephone = Long.parseLong(numeroString);
			String salaireString = fenetreAccueil.getSalaireFormulaireAjouterPersonnelTextField().getText();
			if(salaireString.isEmpty() || salaireString.isBlank()) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner le salaire du membre du personnel");
				return;
			}
			try {
				verificationFormatSalaire(salaireString);
			}
			catch(LettresPresentesDansUnSalaire e) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner un salaire comportant uniquement des chiffres et un point pour la virgule (exemple 1200.00)");
				return;
			}
			double salaire = Double.parseDouble(salaireString);
			String profession = (String)fenetreAccueil.getSelectionProfessionFormulaireAjouterPersonnelComboBox().getSelectedItem();
			long rpps;
			String rppsString, specialite, codeService;
			boolean retourAjouterPersonnel;
			if(profession.equalsIgnoreCase("medecin")) {
				rppsString = fenetreAccueil.getRppsFormulaireAjouterPersonnelTextField().getText();
				if(rppsString.isEmpty() || rppsString.isBlank()) {
					JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner le num�ro rpps du membre du personnel");
					return;
				}
				try {
					verificationFormatRPPS(rppsString);
				}
				catch(LettresPresentesDansUnRPPS e) {
					JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner un rpps comportant uniquement des chiffres");
					return;
				}
				catch(NombreCaracteresNonConforme e) {
					JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner un rpps comportant 11 chiffres");
					return;
				}
				rpps = Long.parseLong(rppsString);
				specialite = (String)fenetreAccueil.getSelectionSpecialiteFormulaireAjouterPersonnelComboBox().getSelectedItem();
				codeService = (String)fenetreAccueil.getSelectionServiceFormulaireAjouterPersonnelComboBox().getSelectedItem();
				retourAjouterPersonnel = connectionTablePersonnel.ajouterPersonnel(nom, prenom, sexe, dateDeNaissance, adresse, mail, numeroTelephone, salaire, profession, rpps, specialite, codeService);
			}
			else if(profession.equalsIgnoreCase("infirmier")) {
				rppsString = fenetreAccueil.getRppsFormulaireAjouterPersonnelTextField().getText();
				if(rppsString.isEmpty() || rppsString.isBlank()) {
					JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner le num�ro rpps du membre du personnel");
					return;
				}
				try {
					verificationFormatRPPS(rppsString);
				}
				catch(LettresPresentesDansUnRPPS e) {
					JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner un rpps comportant uniquement des chiffres");
					return;
				}
				catch(NombreCaracteresNonConforme e) {
					JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner un rpps comportant 11 chiffres");
					return;
				}
				rpps = Long.parseLong(rppsString);
				specialite = null;
				codeService = (String)fenetreAccueil.getSelectionServiceFormulaireAjouterPersonnelComboBox().getSelectedItem();
				retourAjouterPersonnel = connectionTablePersonnel.ajouterPersonnel(nom, prenom, sexe, dateDeNaissance, adresse, mail, numeroTelephone, salaire, profession, rpps, specialite, codeService);
			}
			else if(profession.equalsIgnoreCase("pharmacien")) {
				rppsString = fenetreAccueil.getRppsFormulaireAjouterPersonnelTextField().getText();
				if(rppsString.isEmpty() || rppsString.isBlank()) {
					JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner le num�ro rpps du membre du personnel");
					return;
				}
				try {
					verificationFormatRPPS(rppsString);
				}
				catch(LettresPresentesDansUnRPPS e) {
					JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner un rpps comportant uniquement des chiffres");
					return;
				}
				catch(NombreCaracteresNonConforme e) {
					JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner un rpps comportant 11 chiffres");
					return;
				}
				rpps = Long.parseLong(rppsString);
				specialite = null;
				codeService = null;
				retourAjouterPersonnel = connectionTablePersonnel.ajouterPersonnel(nom, prenom, sexe, dateDeNaissance, adresse, mail, numeroTelephone, salaire, profession, rpps, specialite, codeService);
			}
			else {
				specialite = null;
				codeService = null;
				retourAjouterPersonnel = connectionTablePersonnel.ajouterPersonnel(nom, prenom, sexe, dateDeNaissance, adresse, mail, numeroTelephone, salaire, profession, -1, specialite, codeService);
			}
			if(retourAjouterPersonnel) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Personnel ajout� correctement");
			}
			else {
				JOptionPane.showMessageDialog(fenetreAccueil, "Erreur lors de l'ajout du membre du personnel");
			}
		}
	}
	
	class ComboBoxProfessionFormulaireAjoutPersonnelListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			if(fenetreAccueil.getSelectionProfessionFormulaireAjouterPersonnelComboBox().getSelectedItem() == "medecin") {
				fenetreAccueil.getRppsFormulaireAjouterPersonnelTextField().setEnabled(true);
				fenetreAccueil.getSelectionSpecialiteFormulaireAjouterPersonnelComboBox().setEnabled(true);
				fenetreAccueil.getSelectionServiceFormulaireAjouterPersonnelComboBox().setEnabled(true);
			}
			else if(fenetreAccueil.getSelectionProfessionFormulaireAjouterPersonnelComboBox().getSelectedItem() == "infirmier") {
				fenetreAccueil.getRppsFormulaireAjouterPersonnelTextField().setEnabled(true);
				fenetreAccueil.getSelectionSpecialiteFormulaireAjouterPersonnelComboBox().setEnabled(false);
				fenetreAccueil.getSelectionServiceFormulaireAjouterPersonnelComboBox().setEnabled(true);
			}
			else if(fenetreAccueil.getSelectionProfessionFormulaireAjouterPersonnelComboBox().getSelectedItem() == "pharmacien") {
				fenetreAccueil.getRppsFormulaireAjouterPersonnelTextField().setEnabled(true);
				fenetreAccueil.getSelectionSpecialiteFormulaireAjouterPersonnelComboBox().setEnabled(false);
				fenetreAccueil.getSelectionServiceFormulaireAjouterPersonnelComboBox().setEnabled(false);
			}
			else {
				fenetreAccueil.getRppsFormulaireAjouterPersonnelTextField().setText("");
				fenetreAccueil.getRppsFormulaireAjouterPersonnelTextField().setEnabled(false);
				fenetreAccueil.getSelectionSpecialiteFormulaireAjouterPersonnelComboBox().setEnabled(false);
				fenetreAccueil.getSelectionServiceFormulaireAjouterPersonnelComboBox().setEnabled(false);
			}
		}
	}
	
	class ComboBoxSpecialiteFormulaireAjoutPersonnelListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			if(fenetreAccueil.getSelectionSpecialiteFormulaireAjouterPersonnelComboBox().getSelectedItem() == "medecine_generale") {
				fenetreAccueil.getSelectionServiceFormulaireAjouterPersonnelComboBox().setSelectedIndex(0);
			}
			else if(fenetreAccueil.getSelectionSpecialiteFormulaireAjouterPersonnelComboBox().getSelectedItem() == "cardiologie") {
				fenetreAccueil.getSelectionServiceFormulaireAjouterPersonnelComboBox().setSelectedIndex(1);
			}
			else if(fenetreAccueil.getSelectionSpecialiteFormulaireAjouterPersonnelComboBox().getSelectedItem() == "chirurgie") {
				fenetreAccueil.getSelectionServiceFormulaireAjouterPersonnelComboBox().setSelectedIndex(2);
			}
			else if(fenetreAccueil.getSelectionSpecialiteFormulaireAjouterPersonnelComboBox().getSelectedItem() == "pediatrie") {
				fenetreAccueil.getSelectionServiceFormulaireAjouterPersonnelComboBox().setSelectedIndex(3);
			}
		}
	}
	
	class BouttonEffacerFormulairePersonnelListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			fenetreAccueil.getNomFormulaireAjouterPersonnelTextField().setText("");
			fenetreAccueil.getPrenomFormulaireAjouterPersonnelTextField().setText("");
			fenetreAccueil.getSelectionSexeFormulaireAjouterPersonnelButtonGroup().clearSelection();
			fenetreAccueil.getDateDeNaissanceFormulaireAjouterPersonnelTextField().setText("");
			fenetreAccueil.getAdresseFormulaireAjouterPersonnelTextArea().setText("");
			fenetreAccueil.getMailFormulaireAjouterPersonnelTextField().setText("");
			fenetreAccueil.getTelephoneFormulaireAjouterPersonnelTextField().setText("");
			fenetreAccueil.getRppsFormulaireAjouterPersonnelTextField().setText("");
			fenetreAccueil.getSalaireFormulaireAjouterPersonnelTextField().setText("");
		}
	}
	
	class BouttonSuppressionPersonnelConteneurGaucheListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			fenetreAccueil.getLayoutMultiPanelDroite().show(fenetreAccueil.getConteneurDroite(),"panelSuppressionPersonnel");
		}
	}
	
	class BouttonSupprimerFormulaireSuppressionPersonnelListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			DAOPersonnel connectionTablePersonnel = new DAOPersonnel();
			String idPersonnelString = fenetreAccueil.getIdFormulaireSuppressionPersonnelTextField().getText();
			if(idPersonnelString.isEmpty() || idPersonnelString.isBlank()) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner le num�ro d'id du personnel � supprimer");
				return;
			}
			try {
				verificationFormatID(idPersonnelString);
			}
			catch(LettresPresentesDansUnID e) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner un N�Identifiant ne comportant que des chiffres");
				return;
			}
			int idPersonnel = Integer.parseInt(idPersonnelString);
			boolean retourSuppressionPersonnel = connectionTablePersonnel.supprimerPersonnel(idPersonnel);
			if(retourSuppressionPersonnel) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Membre du personnel supprim� correctement");
				fenetreAccueil.getIdFormulaireSuppressionPersonnelTextField().setText("");
			}
			else {
				JOptionPane.showMessageDialog(fenetreAccueil, "Erreur lors de la suppression du membre du personnel");
			}
		}
		
	}
	
	class BouttonAjoutInstallationsConteneurGaucheListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			fenetreAccueil.getLayoutMultiPanelDroite().show(fenetreAccueil.getConteneurDroite(),"panelAjoutInstallations");
		}
	}
	
	class BouttonAjouterFormulaireAjoutInstallationsListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			DAOService connectionTableService = new DAOService();
			String codeService = fenetreAccueil.getCodeServiceFormulaireAjoutInstallationsTextField().getText();
			if(codeService.isEmpty() || codeService.isBlank()) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner le code du service");
				return;
			}
			String nomService = fenetreAccueil.getNomServiceFormulaireAjoutInstallationsTextField().getText();
			if(nomService.isEmpty() || nomService.isBlank()) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner le nom du service");
				return;
			}
			int nombreChambreIndividuelle = (Integer)fenetreAccueil.getChambreIndividuelleFormulaireAjoutInstallationsSpinner().getValue();
			int nombreChambreDouble = (Integer)fenetreAccueil.getChambreDoubleFormulaireAjoutInstallationsSpinner().getValue();
			int nombreChambreQuadruple = (Integer)fenetreAccueil.getChambreQuadrupleFormulaireAjoutInstallationsSpinner().getValue();
			boolean retourAjoutInstallation = connectionTableService.ajouterInstallationDAO(codeService, nomService, nombreChambreIndividuelle, nombreChambreDouble, nombreChambreQuadruple);
			if(retourAjoutInstallation) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Installation correctment ajout�e");
			}
			else {
				JOptionPane.showMessageDialog(fenetreAccueil, "Erreur lors de l'ajout de l'installation");
			}
			
		}
	}
	
	class RadioAjoutInstallationVideListener implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			fenetreAccueil.getChambreIndividuelleFormulaireAjoutInstallationsCheckBox().setEnabled(false);
			fenetreAccueil.getChambreDoubleFormulaireAjoutInstallationsCheckBox().setEnabled(false);
			fenetreAccueil.getChambreQuadrupleFormulaireAjoutInstallationsCheckBox().setEnabled(false);
			fenetreAccueil.getChambreIndividuelleFormulaireAjoutInstallationsSpinner().setEnabled(false);
			fenetreAccueil.getChambreIndividuelleFormulaireAjoutInstallationsSpinner().setValue(0);
			fenetreAccueil.getChambreDoubleFormulaireAjoutInstallationsSpinner().setEnabled(false);
			fenetreAccueil.getChambreDoubleFormulaireAjoutInstallationsSpinner().setValue(0);
			fenetreAccueil.getChambreQuadrupleFormulaireAjoutInstallationsSpinner().setEnabled(false);
			fenetreAccueil.getChambreQuadrupleFormulaireAjoutInstallationsSpinner().setValue(0);
		}
	}

	class RadioAjoutInstallationRempliListener implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			fenetreAccueil.getChambreIndividuelleFormulaireAjoutInstallationsCheckBox().setEnabled(true);
			fenetreAccueil.getChambreDoubleFormulaireAjoutInstallationsCheckBox().setEnabled(true);
			fenetreAccueil.getChambreQuadrupleFormulaireAjoutInstallationsCheckBox().setEnabled(true);
		}
	}
	
	class CheckBoxChambreIndividuelleFormulaireAjouterInstallationsListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			if(fenetreAccueil.getChambreIndividuelleFormulaireAjoutInstallationsCheckBox().isSelected()) {
				fenetreAccueil.getChambreIndividuelleFormulaireAjoutInstallationsSpinner().setEnabled(true);
			}
			else {
				fenetreAccueil.getChambreIndividuelleFormulaireAjoutInstallationsSpinner().setEnabled(false);
				fenetreAccueil.getChambreIndividuelleFormulaireAjoutInstallationsSpinner().setValue(0);
			}
		}
	}

	class CheckBoxChambreDoubleFormulaireAjouterInstallationsListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			if(fenetreAccueil.getChambreDoubleFormulaireAjoutInstallationsCheckBox().isSelected()) {
				fenetreAccueil.getChambreDoubleFormulaireAjoutInstallationsSpinner().setEnabled(true);
			}
			else {
				fenetreAccueil.getChambreDoubleFormulaireAjoutInstallationsSpinner().setEnabled(false);
				fenetreAccueil.getChambreDoubleFormulaireAjoutInstallationsSpinner().setValue(0);
			}
		}
	}

	class CheckBoxChambreQuadrupleFormulaireAjouterInstallationsListener implements ActionListener {
	
		public void actionPerformed(ActionEvent event) {
			if(fenetreAccueil.getChambreQuadrupleFormulaireAjoutInstallationsCheckBox().isSelected()) {
				fenetreAccueil.getChambreQuadrupleFormulaireAjoutInstallationsSpinner().setEnabled(true);
			}
			else {
				fenetreAccueil.getChambreQuadrupleFormulaireAjoutInstallationsSpinner().setEnabled(false);
				fenetreAccueil.getChambreQuadrupleFormulaireAjoutInstallationsSpinner().setValue(0);
			}
		}
	}
	
	class BouttonSuppressionInstallationsConteneurGaucheListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			fenetreAccueil.getLayoutMultiPanelDroite().show(fenetreAccueil.getConteneurDroite(),"panelSuppressionInstallations");
		}
	}
	
	class BouttonSupprimerInstallationListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			boolean retourSQL = false;
			if(fenetreAccueil.getChoixTypeInstallationFormulaireSuppressionInstallations().getSelection().getActionCommand().equalsIgnoreCase("service")) {
				DAOService connectionTableService = new DAOService();
				retourSQL = connectionTableService.supprimerServiceDAO(fenetreAccueil.getCodeServiceFormulaireSuppressionInstallations().getText().charAt(0));
			}
			else if(fenetreAccueil.getChoixTypeInstallationFormulaireSuppressionInstallations().getSelection().getActionCommand().equalsIgnoreCase("chambre")) {
				DAOChambre connectionTableChambre = new DAOChambre();
				retourSQL = connectionTableChambre.supprimerChambreDAO(fenetreAccueil.getCodeChambreFormulaireSuppressionInstallations().getText());
			}
			else if(fenetreAccueil.getChoixTypeInstallationFormulaireSuppressionInstallations().getSelection().getActionCommand().equalsIgnoreCase("lit")) {
				DAOLit connectionTableLit = new DAOLit();
				retourSQL = connectionTableLit.supprimerLitDAO(fenetreAccueil.getCodeLitFormulaireSuppressionInstallations().getText());
			}
			if(retourSQL) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Installation supprim�e correctement");
				fenetreAccueil.getCodeServiceFormulaireSuppressionInstallations().setText("");
				fenetreAccueil.getCodeChambreFormulaireSuppressionInstallations().setText("");
				fenetreAccueil.getCodeLitFormulaireSuppressionInstallations().setText("");
			}
			else {
				JOptionPane.showMessageDialog(fenetreAccueil, "Erreur lors de la suppression de l'appareil");
			}
		}
	}
	
	class ComboBoxChoixTypeInstallationFormulaireSupprimerInstallationListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			if(fenetreAccueil.getChoixTypeInstallationFormulaireSuppressionInstallations().getSelection().getActionCommand().equalsIgnoreCase("service")) {
				fenetreAccueil.getCodeServiceFormulaireSuppressionInstallations().setEnabled(true);
				fenetreAccueil.getCodeChambreFormulaireSuppressionInstallations().setEnabled(false);
				fenetreAccueil.getCodeChambreFormulaireSuppressionInstallations().setText("");
				fenetreAccueil.getCodeLitFormulaireSuppressionInstallations().setEnabled(false);
				fenetreAccueil.getCodeLitFormulaireSuppressionInstallations().setText("");
			}
			else if(fenetreAccueil.getChoixTypeInstallationFormulaireSuppressionInstallations().getSelection().getActionCommand().equalsIgnoreCase("chambre")) {
				fenetreAccueil.getCodeServiceFormulaireSuppressionInstallations().setEnabled(false);
				fenetreAccueil.getCodeServiceFormulaireSuppressionInstallations().setText("");
				fenetreAccueil.getCodeChambreFormulaireSuppressionInstallations().setEnabled(true);
				fenetreAccueil.getCodeLitFormulaireSuppressionInstallations().setEnabled(false);
				fenetreAccueil.getCodeLitFormulaireSuppressionInstallations().setText("");
			}
			else if(fenetreAccueil.getChoixTypeInstallationFormulaireSuppressionInstallations().getSelection().getActionCommand().equalsIgnoreCase("lit")) {
				fenetreAccueil.getCodeServiceFormulaireSuppressionInstallations().setEnabled(false);
				fenetreAccueil.getCodeServiceFormulaireSuppressionInstallations().setText("");
				fenetreAccueil.getCodeChambreFormulaireSuppressionInstallations().setEnabled(false);
				fenetreAccueil.getCodeChambreFormulaireSuppressionInstallations().setText("");
				fenetreAccueil.getCodeLitFormulaireSuppressionInstallations().setEnabled(true);
			}
		}
	}
	
	class BouttonAjoutAppareilMedicalConteneurGaucheListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			fenetreAccueil.getLayoutMultiPanelDroite().show(fenetreAccueil.getConteneurDroite(),"panelAjoutAppareilMedical");
		}
	}
	
	class BouttonAjouterAppareilListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			DAOAppareil connectionTableAppareil = new DAOAppareil();
			String codeAppareil = fenetreAccueil.getCodeAppareilFormulaireAjoutAppareilMedicalTextField().getText();
			if(codeAppareil.isEmpty() || codeAppareil.isBlank()) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner le code de l'appareil");
				return;
			}
			String typeAppareil = fenetreAccueil.getTypeAppareilFormulaireAjoutAppareilMedicalTextField().getText();
			if(typeAppareil.isEmpty() || typeAppareil.isBlank()) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner le nom de l'appareil");
				return;
			}
			boolean retourAjoutAppareil = connectionTableAppareil.ajouterAppareilMedical(codeAppareil, typeAppareil);
			if(retourAjoutAppareil) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Appareil ajout� correctement");
				fenetreAccueil.getCodeAppareilFormulaireAjoutAppareilMedicalTextField().setText("");
				fenetreAccueil.getTypeAppareilFormulaireAjoutAppareilMedicalTextField().setText("");
			}
			else {
				JOptionPane.showMessageDialog(fenetreAccueil, "Erreur lors de l'ajout de l'appareil");
			}
		}
	}
	
	class BouttonSuppressionAppareilMedicalListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			fenetreAccueil.getLayoutMultiPanelDroite().show(fenetreAccueil.getConteneurDroite(),"panelSuppressionAppareilMedical");
		}
	}
	
	class BouttonSupprimerAppareilListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			DAOAppareil connectionTableAppareil = new DAOAppareil();
			String codeAppareil = fenetreAccueil.getCodeAppareilFormulaireSuppressionAppareilMedical().getText();
			if(codeAppareil.isEmpty() || codeAppareil.isBlank()) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner le code de l'appareil");
				return;
			}
			boolean retourAjoutAppareil = connectionTableAppareil.supprimerAppareilDAO(codeAppareil);
			if(retourAjoutAppareil) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Appareil supprim� correctement");
				fenetreAccueil.getCodeAppareilFormulaireSuppressionAppareilMedical().setText("");
			}
			else {
				JOptionPane.showMessageDialog(fenetreAccueil, "Erreur lors de la suppression de l'appareil");
			}
		}
	}
	
	// Profil Medecin
	
	class BouttonMesRDVListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			DAORDV connectionTableRDV = new DAORDV();
			ArrayList<RDV> listeRDVJour = connectionTableRDV.rechercheRDVDuJourParMedecinDAO(idPersonnelEnCours);
			int[] tableauValeurHeuresRDV = {1000,1030,1100,1130,1200,1230,1500,1530,1600,1630,1700,1730}; 
			for(int i = 0 ; i < listeRDVJour.size() ; i++) {
				int heureRDV = Integer.parseInt(DonneesDates.formatHeureComparaison(listeRDVJour.get(i).getDateHeure()));
				for(int j = 0 ; j < tableauValeurHeuresRDV.length ; j++) {
					if(heureRDV == tableauValeurHeuresRDV[j]) {
						if(j < 6) {
							fenetreAccueil.getMesRDVMedecin().setValueAt(listeRDVJour.get(i).getPatient().getNom()+" "+listeRDVJour.get(i).getPatient().getPrenom(),j,1);
							fenetreAccueil.getMesRDVMedecin().setValueAt(listeRDVJour.get(i).getSalle(),j,2);
						}
						else {
							fenetreAccueil.getMesRDVMedecin().setValueAt(listeRDVJour.get(i).getPatient().getNom()+" "+listeRDVJour.get(i).getPatient().getPrenom(),j+1,1);
							fenetreAccueil.getMesRDVMedecin().setValueAt(listeRDVJour.get(i).getSalle(),j+1,2);
						}
					}
				}
			}
			patientEnCoursMesRDV = new ArrayList<Patient>();
			for(int k = 0 ; k < listeRDVJour.size() ; k++) {
				patientEnCoursMesRDV.add(listeRDVJour.get(k).getPatient());
			}
			fenetreAccueil.getLayoutMultiPanelDroite().show(fenetreAccueil.getConteneurDroite(),"panelMesRDV");
		}
	}
	
	// Profil Infirmier
	
	class BouttonEtatServiceListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			DAOHospitalisation connectionTableHospitalisation = new DAOHospitalisation();
			DAOPersonnel connectionTablePersonnel = new DAOPersonnel();
			char codeService = connectionTablePersonnel.servicePersonnel(idPersonnelEnCours).charAt(0);
			ArrayList<String> listeChambreIndividuelleVide = connectionTableHospitalisation.listeChambreIndividuelleVideService(codeService);
			ArrayList<String> listeChambreDoubleVide = connectionTableHospitalisation.listeChambreDoubleVideService(codeService);
			ArrayList<String> listeChambreQuadrupleVide = connectionTableHospitalisation.listeChambreQuadrupleVideService(codeService);
			try {
				BufferedImage litVert = ImageIO.read(new File("img/Lit_Vert_V2.png"));
				BufferedImage litRouge = ImageIO.read(new File("img/Lit_Rouge_V2.png"));
				int compteur;
				for(int i = 1 ; i < 9 ; i++) {
					compteur = 0;
					for(int j = 0 ; j < listeChambreIndividuelleVide.size() ; j++) {
						if(listeChambreIndividuelleVide.get(j).substring(1,2).equalsIgnoreCase(i+"")) compteur++;
					}
					if(compteur != 0) {
						getConteneurChambreIndividuelle(i).setImg(litVert);
					}
					else {
						getConteneurChambreIndividuelle(i).setImg(litRouge);
					}
					getConteneurChambreIndividuelle(i).repaint();
				}
				for(int i = 9 ; i < 17 ; i++) {
					for(int k = 1 ; k < 3 ; k++) {
						compteur = 0;
						for(int j = 0 ; j < listeChambreDoubleVide.size() ; j++) {
							if(i == 9 && listeChambreDoubleVide.get(j).substring(1,3).equalsIgnoreCase(i+""+k)) compteur++;
							else if(i >= 10 && listeChambreDoubleVide.get(j).substring(1).equalsIgnoreCase(i+""+k)) compteur++;
						}
						if(compteur != 0) {
							if(k == 1) getConteneurChambreDouble(i).setImg1(litVert);
							else getConteneurChambreDouble(i).setImg2(litVert);
						}
						else {
							if(k == 1) getConteneurChambreDouble(i).setImg1(litRouge);
							else getConteneurChambreDouble(i).setImg2(litRouge);
						}
						getConteneurChambreDouble(i).repaint();
					}
				}
				for(int i = 17 ; i < 21 ; i++) {
					for(int k = 1 ; k < 5 ; k++) {
						compteur = 0;
						for(int j = 0 ; j < listeChambreQuadrupleVide.size() ; j++) {
							if(listeChambreQuadrupleVide.get(j).substring(1).equalsIgnoreCase(i+""+k)) compteur++;
						}
						if(compteur != 0) {
							if(k == 1) getConteneurChambreQuadruple(i).setImg1(litVert);
							else if(k == 2) getConteneurChambreQuadruple(i).setImg2(litVert);
							else if(k == 3) getConteneurChambreQuadruple(i).setImg3(litVert);
							else getConteneurChambreQuadruple(i).setImg4(litVert);
						}
						else {
							if(k == 1) getConteneurChambreQuadruple(i).setImg1(litRouge);
							else if(k == 2) getConteneurChambreQuadruple(i).setImg2(litRouge);
							else if(k == 3) getConteneurChambreQuadruple(i).setImg3(litRouge);
							else getConteneurChambreQuadruple(i).setImg4(litRouge);
						}
						getConteneurChambreQuadruple(i).repaint();
					}
				}
			}
			catch(IOException e) {
				System.out.println("Erreur chargement photo");
			}
			
			fenetreAccueil.getLayoutMultiPanelDroite().show(fenetreAccueil.getConteneurDroite(),"panelEtatService");
		}
	}
	
	// Profil Pharmacien
	
	class BouttonEtatPharmacieConteneurGaucheListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			fenetreAccueil.getLayoutMultiPanelDroite().show(fenetreAccueil.getConteneurDroite(),"panelEtatPharmacie");
		}
	}
	
	class BouttonAjoutRetraitArticleConteneurGaucheListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			fenetreAccueil.getLayoutMultiPanelDroite().show(fenetreAccueil.getConteneurDroite(),"panelAjoutRetraitArticle");
		}
	}
	
	class BouttonAjouterArticleListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			DAOArticle connectionTableArticle = new DAOArticle();
			String referenceArticle = fenetreAccueil.getReferenceFormulaireAjoutRetraitArticleTextField().getText();
			if(referenceArticle.isEmpty() || referenceArticle.isBlank()) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner la reference de l'article");
				return;
			}
			int quantiteArticle = (Integer)fenetreAccueil.getQuantiteFormulaireAjoutRetraitArticleTextField().getValue();
			boolean retourAjoutArticle = false;
			try {
				retourAjoutArticle = connectionTableArticle.ajouterArticleDAO(referenceArticle, quantiteArticle);
			}
			catch(IllegalArgumentException e) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez indiquer le nombre d'article � ajouter");
				return;
			}
			if(retourAjoutArticle) {
				JOptionPane.showMessageDialog(fenetreAccueil, quantiteArticle+" "+referenceArticle+" ajout�");
				fenetreAccueil.getReferenceFormulaireAjoutRetraitArticleTextField().setText("");
				fenetreAccueil.getQuantiteFormulaireAjoutRetraitArticleTextField().setValue(0);
				updateEtatPharmacie();
			}
			else {
				JOptionPane.showMessageDialog(fenetreAccueil, "La r�f�rence n'a pas �t� trouv�");
			}
		}
	}
	
	class BouttonRetirerArticleListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			DAOArticle connectionTableArticle = new DAOArticle();
			String referenceArticle = fenetreAccueil.getReferenceFormulaireAjoutRetraitArticleTextField().getText();
			if(referenceArticle.isEmpty() || referenceArticle.isBlank()) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner la reference de l'article");
				return;
			}
			int quantiteArticle = (Integer)fenetreAccueil.getQuantiteFormulaireAjoutRetraitArticleTextField().getValue();
			boolean retourAjoutArticle = connectionTableArticle.retirerArticleDAO(referenceArticle, quantiteArticle);
			if(retourAjoutArticle) {
				JOptionPane.showMessageDialog(fenetreAccueil, quantiteArticle+" "+referenceArticle+" retir�");
				fenetreAccueil.getReferenceFormulaireAjoutRetraitArticleTextField().setText("");
				fenetreAccueil.getQuantiteFormulaireAjoutRetraitArticleTextField().setValue(0);
				updateEtatPharmacie();
			}
			else {
				JOptionPane.showMessageDialog(fenetreAccueil, "La r�f�rence n'a pas �t� trouv�");
			}
		}
	}
	
	class BouttonCreerArticleConteneurGaucheListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			fenetreAccueil.getLayoutMultiPanelDroite().show(fenetreAccueil.getConteneurDroite(),"panelCreerArticle");
		}
	}
	
	class BouttonCreerArticleListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			String reference = fenetreAccueil.getReferenceFormulaireCreerArticleTextField().getText();
			if(reference.isEmpty() || reference.isBlank()) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner la reference de l'article");
				return;
			}
			String nom = fenetreAccueil.getNomFormulaireCreerArticleTextField().getText();
			if(nom.isEmpty() || nom.isBlank()) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner le nom de l'article");
				return;
			}
			String type = (String)fenetreAccueil.getSelectionTypeArticleFormulaireCreerArticleComboBox().getSelectedItem();
			DAOArticle connectionTableArticle = new DAOArticle();
			boolean retourSQL = connectionTableArticle.creerArticleDAO(reference, nom, type);
			if(retourSQL) {
				JOptionPane.showMessageDialog(fenetreAccueil, "L'article a �t� correctment cr��");
				updateEtatPharmacie();
			}
			else {
				JOptionPane.showMessageDialog(fenetreAccueil, "Erreur lors de la cr�ation de l'article");
			}
		}
	}
	
	class BouttonEffacerFormulaireCreerArticleListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			fenetreAccueil.getReferenceFormulaireCreerArticleTextField().setText("");
			fenetreAccueil.getNomFormulaireCreerArticleTextField().setText("");
		}
	}
	
	class BouttonSuppressionArticleConteneurGaucheListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			fenetreAccueil.getLayoutMultiPanelDroite().show(fenetreAccueil.getConteneurDroite(),"panelSuppressionArticle");
		}
	}
	
	class BouttonSupprimerArticleListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			DAOArticle connectionTableArticle = new DAOArticle();
			String reference = fenetreAccueil.getReferenceFormulaireSupprimerArticleTextField().getText();
			if(reference.isEmpty() || reference.isBlank()) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner la reference de l'article");
				return;
			}
			Article articleEnCours = connectionTableArticle.rechercheParReferenceDAO(reference);
			if(articleEnCours == null) {
				JOptionPane.showMessageDialog(fenetreAccueil, "L'article n'a pas �t� trouv�");
				return;
			}
			boolean retourSQL = connectionTableArticle.supprimerArticleDAO(articleEnCours);
			if(retourSQL) {
				JOptionPane.showMessageDialog(fenetreAccueil, "L'article a �t� correctment supprim�");
				fenetreAccueil.getReferenceFormulaireSupprimerArticleTextField().setText("");
				updateEtatPharmacie();
			}
			else {
				JOptionPane.showMessageDialog(fenetreAccueil, "Erreur lors de la suppression de l'article");
			}
		}
	}
	
}