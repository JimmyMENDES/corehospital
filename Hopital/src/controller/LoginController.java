package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JOptionPane;

import modele.jdbc.DAOIdentification;
import vues.Login;

public class LoginController {
	
	private Login vueLogin;
	private DAOIdentification connectionTableIdentification;
	
	public LoginController(DAOIdentification connectionTableIdentification, Login vueLogin) {
		this.connectionTableIdentification = connectionTableIdentification;
		this.vueLogin = vueLogin;
		this.vueLogin.getMotDePassePasswordField().addKeyListener(new ChampMotDePasseListener());
		this.vueLogin.getBouttonValider().addActionListener(new BouttonValiderLoginListener());
	}
	
	@SuppressWarnings("unused")
	class BouttonValiderLoginListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			int retourSQL = connectionTableIdentification.identification(vueLogin.getLoginUtilisateur(), vueLogin.getMotDePasse());
			if(retourSQL != -1) {
				AccueilController accueilController = new AccueilController(vueLogin, retourSQL);
				vueLogin.setVisible(false);
				vueLogin.clearLoginUtilisateur();
				vueLogin.clearMotDePasse();
			}
			else {
				JOptionPane.showMessageDialog(vueLogin, "Le login et/ou le mot de passe est faux");
			}
		}
	}
	
	class ChampMotDePasseListener implements KeyListener {
		
		public void keyPressed(KeyEvent e) {
			if(e.getKeyCode() == KeyEvent.VK_ENTER) vueLogin.getBouttonValider().doClick();
		}
		public void keyTyped(KeyEvent e) {}
		public void keyReleased(KeyEvent e) {}
	}

}
