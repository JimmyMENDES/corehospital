package controller;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTable;

import modele.donnees.DonneesDates;
import modele.humain.Medecin;
import modele.humain.Patient;
import modele.immateriel.Analyse;
import modele.immateriel.Consultation;
import modele.immateriel.Ordonnance;
import modele.immateriel.RDV;
import modele.jdbc.DAOAnalyse;
import modele.jdbc.DAOAppareil;
import modele.jdbc.DAOConsultation;
import modele.jdbc.DAOHospitalisation;
import modele.jdbc.DAOOrdonnance;
import modele.jdbc.DAOPatient;
import modele.jdbc.DAOPersonnel;
import modele.jdbc.DAORDV;
import modele.materiel.AppareilMedical;
import vues.FenetreAccueil;
import vues.OngletPatient;
import vues.tablemodel.AnalyseTableModel;
import vues.tablemodel.ConsultationTableModel;
import vues.tablemodel.RDVTableModel;

public class OngletPatientController {
	
	private FenetreAccueil fenetreAccueil;
	private OngletPatient ongletPatient;
	private int idPersonnelEnCours;
	private int idPatientEnCours;
	
	ArrayList<RDV> listeRDVPatientEnCours;
	ArrayList<Consultation> listeConsultationPatientEnCours;
	ArrayList<Ordonnance> listeOrdonnancePatientEnCours;
	ArrayList<Analyse> listeAnalysePatientEnCours;
	
	JLabel fermerOngletPatient;
	
	public OngletPatientController(FenetreAccueil fenetreAccueil, int idPersonnelEnCours, int idPatient, String nom, String prenom) {
		this.fenetreAccueil = fenetreAccueil;
		this.idPersonnelEnCours = idPersonnelEnCours;
		this.idPatientEnCours = idPatient;
		
		DAOPersonnel connectionTablePersonnel = new DAOPersonnel();
		String profession = connectionTablePersonnel.professionPersonnel(idPersonnelEnCours);
		
		ongletPatient = new OngletPatient(connectionTablePersonnel.professionPersonnel(idPersonnelEnCours),idPatient);
		this.setDonneesPatientPanelHautGauche();
		fenetreAccueil.getConteneurPrincipal().add(nom+" "+prenom, ongletPatient);
		this.setCroixFermerOnglet(nom,prenom);
		fenetreAccueil.getConteneurPrincipal().setSelectedComponent(ongletPatient);
		
		if(profession.equalsIgnoreCase("agent_administrateur")) {
			etatRDV();
			ongletPatient.getTableauRDVPatient().addMouseListener(new TableauRDVListener());
			setDatesDebutFinFormulaireHospitalisation();
			ongletPatient.getChoixAutomatiqueFormulaireHospitalisation().addActionListener(new ChoixAutomatiqueFormulaireHospitalisationListener());  
			ongletPatient.getChoixManuelFormulaireHospitalisation().addActionListener(new ChoixManuelFormulaireHospitalisationListener());
			ongletPatient.getChoixChambreIndividuelleFormulaireHospitalisation().addActionListener(new ChoixIndividuelFormulaireHospitalisationListener());
			ongletPatient.getChoixChambreDoubleFormulaireHospitalisation().addActionListener(new ChoixDoubleFormulaireHospitalisationListener());
			ongletPatient.getChoixChambreQuadrupleFormulaireHospitalisation().addActionListener(new ChoixQuadrupleFormulaireHospitalisationListener());
			ongletPatient.getHospitaliserFormulaireHospitalisation().addActionListener(new BouttonHospitaliserFormulaireHospitalisationListener());
			setListeMedecinFormulaireNouveauRDV();
			ongletPatient.getChoixMedecinFormulaireNouveauRDVComboBox().addActionListener(new ChoixMedecinFormulaireCreerRDVListener());
			ongletPatient.getChoixDateFormulaireNouveauRDVComboBox().addActionListener(new ChoixDateFormulaireCreerRDVListener());
			setDonneesPatientPanelBasDroitModifierPatient();
			ongletPatient.getModifierFormulaireModifierPatientButton().addActionListener(new BouttonModifierFormulaireModifierPatientListener());
			ongletPatient.getOuiSupprimer().addActionListener(new BouttonOuiFormulaireSupprimerPatientListener());
		}
		else if(profession.equalsIgnoreCase("technicien")) {
			setListeAppareilFormulaireNouvelleAnalyse();
			ongletPatient.getAjouterAnalyse().addActionListener(new BouttonAjouterFormulaireAnalyseListener());
			etatAnalyse();
			ongletPatient.getTableauAnalysePatient().addMouseListener(new TableauAnalyseListener());
			etatOrdonnance();
			ongletPatient.getTableauOrdonnancePatient().addMouseListener(new TableauOrdonnanceListener());
		}
		else if(profession.equalsIgnoreCase("medecin")) {
			setPathologiesPanelBasGauche();
			ongletPatient.getModifierPathologiesFormulairePathologie().addActionListener(new BouttonModifierPathologiesFormulairePathologieListener());
			etatRDV();
			ongletPatient.getTableauRDVPatient().addMouseListener(new TableauRDVListener());
			etatConsultation();
			ongletPatient.getTableauConsultationPatient().addMouseListener(new TableauConsultationListener());
			etatOrdonnance();
			ongletPatient.getTableauOrdonnancePatient().addMouseListener(new TableauOrdonnanceListener());
			etatAnalyse();
			ongletPatient.getTableauAnalysePatient().addMouseListener(new TableauAnalyseListener());
			ongletPatient.getAjouterConsultation().addActionListener(new BouttonAjouterFormulaireConsultationListener());
			ongletPatient.getAjouterOrdonnance().addActionListener(new BouttonAjouterFormulaireOrdonnanceListener());
			ongletPatient.getAjouterRDVFormulaireNouveauRDV().addActionListener(new BouttonAjouterRDVFormulaireNouveauRDVListener());
			setListeMedecinFormulaireNouveauRDV();
			ongletPatient.getChoixMedecinFormulaireNouveauRDVComboBox().addActionListener(new ChoixMedecinFormulaireCreerRDVListener());
			ongletPatient.getChoixDateFormulaireNouveauRDVComboBox().addActionListener(new ChoixDateFormulaireCreerRDVListener());
			setDonneesPatientPanelBasDroitModifierPatient();
			ongletPatient.getModifierFormulaireModifierPatientButton().addActionListener(new BouttonModifierFormulaireModifierPatientListener());
			ongletPatient.getOuiSupprimer().addActionListener(new BouttonOuiFormulaireSupprimerPatientListener());
			
		}
		else if(profession.equalsIgnoreCase("infirmier")) {
			etatRDV();
			ongletPatient.getTableauRDVPatient().addMouseListener(new TableauRDVListener());
			etatOrdonnance();
			ongletPatient.getTableauOrdonnancePatient().addMouseListener(new TableauOrdonnanceListener());
			ongletPatient.getAjouterRDVFormulaireNouveauRDV().addActionListener(new BouttonAjouterRDVFormulaireNouveauRDVListener());
			setListeMedecinFormulaireNouveauRDV();
			ongletPatient.getChoixMedecinFormulaireNouveauRDVComboBox().addActionListener(new ChoixMedecinFormulaireCreerRDVListener());
			ongletPatient.getChoixDateFormulaireNouveauRDVComboBox().addActionListener(new ChoixDateFormulaireCreerRDVListener());
		}
		else if(profession.equalsIgnoreCase("pharmacien")) {
			etatOrdonnance();
			ongletPatient.getTableauOrdonnancePatient().addMouseListener(new TableauOrdonnanceListener());
		}
	}
	
	// M�thodes
	
	private void setCroixFermerOnglet(String nom, String prenom) {
		int index = fenetreAccueil.getConteneurPrincipal().indexOfTab(nom+" "+prenom);
		JPanel conteneurEnteteOnglet = new JPanel(new BorderLayout());
		conteneurEnteteOnglet.setOpaque(false);
		JPanel conteneurEnteteOngletCentre = new JPanel(new FlowLayout(FlowLayout.LEFT));
		conteneurEnteteOngletCentre.setOpaque(false);
		JLabel nomPrenomPatient = new JLabel(nom+" "+prenom);
		JSeparator separateur = new JSeparator();
		fermerOngletPatient = new JLabel("X");
		fermerOngletPatient.addMouseListener(new FermerOngletPatientListener());
		conteneurEnteteOngletCentre.add(nomPrenomPatient);
		conteneurEnteteOngletCentre.add(separateur);
		conteneurEnteteOnglet.add(conteneurEnteteOngletCentre, BorderLayout.CENTER);
		conteneurEnteteOnglet.add(fermerOngletPatient, BorderLayout.EAST);
		fenetreAccueil.getConteneurPrincipal().setTabComponentAt(index, conteneurEnteteOnglet);
	}
	
	private void setDonneesPatientPanelHautGauche() {
		DAOPatient connectionTablePatient = new DAOPatient();
		Patient patientEnCours = connectionTablePatient.rechercheParIDDAO(idPatientEnCours);
		DAOHospitalisation connectionTableHospitalisation = new DAOHospitalisation();
		String chambreHospitalise = connectionTableHospitalisation.estHospitaliseDAO(idPatientEnCours);
		ongletPatient.getIdFormulaireDonneesPersonnellesTextField().setText(""+idPatientEnCours);
		ongletPatient.getNomFormulaireDonneesPersonnellesTextField().setText(patientEnCours.getNom());
		ongletPatient.getPrenomFormulaireDonneesPersonnellesTextField().setText(patientEnCours.getPrenom());
		if(patientEnCours.getSexe() == 'm') ongletPatient.getMasculinFormulaireDonneesPersonnellesRadio().setSelected(true);
		else ongletPatient.getFemininFormulaireDonneesPersonnellesRadio().setSelected(true);
		ongletPatient.getDateDeNaissanceFormulaireDonneesPersonnellesTextField().setText(DonneesDates.formatDate(patientEnCours.getDateDeNaissance()));
		ongletPatient.getAdresseFormulaireDonneesPersonnellesTextField().setText(patientEnCours.getAdresse());
		ongletPatient.getMailFormulaireDonneesPersonnellesTextField().setText(patientEnCours.getMail());
		ongletPatient.getNumeroFormulaireDonneesPersonnellesTextField().setText("0"+patientEnCours.getNumero());
		if(chambreHospitalise != null) {
			ongletPatient.getOuiHospitaliseFormulaireDonneesPersonnellesRadio().setSelected(true);
			ongletPatient.getNumeroLitHospitaliserDonneesPersonnelles().setText(chambreHospitalise);
		}
		else ongletPatient.getNonHospitaliseFormulaireDonneesPersonnellesRadio().setSelected(true);
	}
	
	private void setPathologiesPanelBasGauche() {
		DAOPatient connectionTablePatient = new DAOPatient();
		Patient patientEnCours = connectionTablePatient.rechercheParIDDAO(idPatientEnCours);
		ongletPatient.getPathologieArea().setText(patientEnCours.getPathologies());
	}
	
	private void setDonneesPatientPanelBasDroitModifierPatient() {
		DAOPatient connectionTablePatient = new DAOPatient();
		Patient patientEnCours = connectionTablePatient.rechercheParIDDAO(idPatientEnCours);
		ongletPatient.getNomFormulaireModifierPatient().setText(patientEnCours.getNom());
		ongletPatient.getPrenomFormulaireModifierPatient().setText(patientEnCours.getPrenom());
		if(patientEnCours.getSexe() == 'm') ongletPatient.getMasculinFormulaireModifierPatient().setSelected(true);
		else ongletPatient.getFemininFormulaireModifierPatient().setSelected(true);
		ongletPatient.getDateDeNaissanceFormulaireModifierPatient().setText(DonneesDates.formatDate(patientEnCours.getDateDeNaissance()));
		ongletPatient.getAdresseFormulaireModifierPatient().setText(patientEnCours.getAdresse());
		ongletPatient.getMailFormulaireModifierPatient().setText(patientEnCours.getMail());
		ongletPatient.getNumeroFormulaireModifierPatient().setText("0"+patientEnCours.getNumero());
	}
	
	private void setListeMedecinFormulaireNouveauRDV() {
		DAOPersonnel connectionTablePersonnel = new DAOPersonnel();
		ArrayList<Medecin> arrayListMedecin = connectionTablePersonnel.rechercherMedecinDAO();
		String[] listeMedecin = new String[arrayListMedecin.size()+1];
		listeMedecin[0] = "";
		for(int i = 1 ; i < arrayListMedecin.size()+1 ; i++) {
			listeMedecin[i] = arrayListMedecin.get(i-1).getIDPersonnel()+" - "+arrayListMedecin.get(i-1).getNom()+" "+arrayListMedecin.get(i-1).getPrenom();
		}
		ongletPatient.setChoixMedecinFormulaireNouveauRDVComboBox(listeMedecin);
	}
	
	private void setDatesDebutFinFormulaireHospitalisation() {
		String[] moisFrancais = {"Janvier", "F�vrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Ao�t", "Septembre", "Octobre", "Novembre", "D�cembre"};
	
		ArrayList<String> listeDates = new ArrayList<String>();
		if(LocalDate.now().getYear() % 4 == 0) {
			for(int i = LocalDate.now().getDayOfMonth() ; i <= LocalDate.now().getMonth().length(true) ; i++) {
				listeDates.add(i+" "+moisFrancais[LocalDate.now().getMonthValue()-1]); 
			}
		}
		else {
			for(int i = LocalDate.now().getDayOfMonth() ; i <= LocalDate.now().getMonth().length(false) ; i++) {
				listeDates.add(i+" "+moisFrancais[LocalDate.now().getMonthValue()-1]); 
			}
		}
		
		String[] tableauDatesDebut = new String[listeDates.size()];
		for(int j = 0 ; j < tableauDatesDebut.length  ; j++) {
			tableauDatesDebut[j] = listeDates.get(j);
		}
		
		String[] tableauDatesFin = new String[listeDates.size()];
		tableauDatesFin[0] = "NC";
		for(int j = 1 ; j < tableauDatesDebut.length  ; j++) {
			tableauDatesFin[j] = listeDates.get(j);
		}
		ongletPatient.setDateDebutFormulaireHospitalisationComboBox(tableauDatesDebut);
		ongletPatient.setDateFinFormulaireHospitalisationComboBox(tableauDatesFin);
	}
	
	private void setListeAppareilFormulaireNouvelleAnalyse() {
		DAOAppareil connectionTableAppareil = new DAOAppareil();
		ArrayList<AppareilMedical> listeAppareilMedical = connectionTableAppareil.ensembleDesAppareilsDAO();
		String[] listeCodeAppareil = new String[listeAppareilMedical.size()];
		for(int i = 0 ; i < listeAppareilMedical.size() ; i++) {
			listeCodeAppareil[i] = listeAppareilMedical.get(i).getCodeAppareil();
		}
		ongletPatient.setChoixAppareilMedicalFormulaireNouvelleAnalyseComboBox(listeCodeAppareil);
	}
	
	private void setChambreIndividuelle() {
		DAOHospitalisation connectionTableHospitalisation = new DAOHospitalisation();
		ArrayList<String> listeChambreVide = connectionTableHospitalisation.listeChambreIndividuelleVideService(ongletPatient.getCodeServiceFormulaireHospitalisation().getText().charAt(0));
		String[] tableauChambreVide = new String[listeChambreVide.size()];
		for(int i = 0 ; i < tableauChambreVide.length ; i++) {
			tableauChambreVide[i] = listeChambreVide.get(i);
		}
		ongletPatient.setChambreIndividuelleFormulaireHospitalisationComboBox(tableauChambreVide);
	}
	
	private void setChambreDouble() {
		DAOHospitalisation connectionTableHospitalisation = new DAOHospitalisation();
		ArrayList<String> listeChambreVide = connectionTableHospitalisation.listeChambreDoubleVideService(ongletPatient.getCodeServiceFormulaireHospitalisation().getText().charAt(0));
		String[] tableauChambreVide = new String[listeChambreVide.size()];
		for(int i = 0 ; i < tableauChambreVide.length ; i++) {
			tableauChambreVide[i] = listeChambreVide.get(i);
		}
		ongletPatient.setChambreDoubleFormulaireHospitalisationComboBox(tableauChambreVide);
	}
	
	private void setChambreQuadruple() {
		DAOHospitalisation connectionTableHospitalisation = new DAOHospitalisation();
		ArrayList<String> listeChambreVide = connectionTableHospitalisation.listeChambreQuadrupleVideService(ongletPatient.getCodeServiceFormulaireHospitalisation().getText().charAt(0));
		String[] tableauChambreVide = new String[listeChambreVide.size()];
		for(int i = 0 ; i < tableauChambreVide.length ; i++) {
			tableauChambreVide[i] = listeChambreVide.get(i);
		}
		ongletPatient.setChambreQuadrupleFormulaireHospitalisationComboBox(tableauChambreVide);
	}
	
	private void etatRDV() {
		DAORDV connectionTableRDV = new DAORDV();
		DAOPatient connectionTablePatient = new DAOPatient();
		listeRDVPatientEnCours = connectionTableRDV.rechercheRDVParPatientDAO(connectionTablePatient.rechercheParIDDAO(idPatientEnCours));
		ongletPatient.getTableauRDVPatient().setModel(new RDVTableModel(listeRDVPatientEnCours.size()));
		for(int i = 0 ; i < listeRDVPatientEnCours.size() ; i++) {
			ongletPatient.getTableauRDVPatient().setValueAt(listeRDVPatientEnCours.get(i).getReferenceRDV(), i, 0);
			ongletPatient.getTableauRDVPatient().setValueAt(listeRDVPatientEnCours.get(i).getDocteur().getNom(), i, 1);
			ongletPatient.getTableauRDVPatient().setValueAt(listeRDVPatientEnCours.get(i).getSalle(), i, 2);
			ongletPatient.getTableauRDVPatient().setValueAt(DonneesDates.formatDate(listeRDVPatientEnCours.get(i).getDateHeure()), i, 3);
			ongletPatient.getTableauRDVPatient().setValueAt(DonneesDates.formatHeure(listeRDVPatientEnCours.get(i).getDateHeure()), i, 4);
		}
	}
	
	private void etatConsultation() {
		DAOConsultation connectionTableConsultation = new DAOConsultation();
		DAOPatient connectionTablePatient = new DAOPatient();
		listeConsultationPatientEnCours = connectionTableConsultation.afficherConsultationDAO(connectionTablePatient.rechercheParIDDAO(idPatientEnCours));
		ongletPatient.getTableauConsultationPatient().setModel(new ConsultationTableModel(listeConsultationPatientEnCours.size()));
		for(int i = 0 ; i < listeConsultationPatientEnCours.size() ; i++) {
			ongletPatient.getTableauConsultationPatient().setValueAt(listeConsultationPatientEnCours.get(i).getReferenceConsultation(), i, 0);
			ongletPatient.getTableauConsultationPatient().setValueAt(listeConsultationPatientEnCours.get(i).getMedecin().getNom(), i, 1);
			ongletPatient.getTableauConsultationPatient().setValueAt(listeConsultationPatientEnCours.get(i).getNotesConsultation(), i, 2);
			ongletPatient.getTableauConsultationPatient().setValueAt(DonneesDates.formatDate(listeConsultationPatientEnCours.get(i).getDate()), i, 3);
		}
	}
	
	private void etatOrdonnance() {
		DAOOrdonnance connectionTableOrdonnance = new DAOOrdonnance();
		DAOPatient connectionTablePatient = new DAOPatient();
		listeOrdonnancePatientEnCours = connectionTableOrdonnance.afficherOrdonnanceDAO(connectionTablePatient.rechercheParIDDAO(idPatientEnCours));
		ongletPatient.getTableauOrdonnancePatient().setModel(new ConsultationTableModel(listeOrdonnancePatientEnCours.size()));
		for(int i = 0 ; i < listeOrdonnancePatientEnCours.size() ; i++) {
			ongletPatient.getTableauOrdonnancePatient().setValueAt(listeOrdonnancePatientEnCours.get(i).getReferenceOrdonnance(), i, 0);
			ongletPatient.getTableauOrdonnancePatient().setValueAt(listeOrdonnancePatientEnCours.get(i).getMedecin().getNom(), i, 1);
			ongletPatient.getTableauOrdonnancePatient().setValueAt(listeOrdonnancePatientEnCours.get(i).getOrdonnance(), i, 2);
			ongletPatient.getTableauOrdonnancePatient().setValueAt(DonneesDates.formatDate(listeOrdonnancePatientEnCours.get(i).getDate()), i, 3);
		}
	}
	
	private void etatAnalyse() {
		DAOAnalyse connectionTableAnalyse = new DAOAnalyse();
		DAOPatient connectionTablePatient = new DAOPatient();
		listeAnalysePatientEnCours = connectionTableAnalyse.afficherAnalyseDAO(connectionTablePatient.rechercheParIDDAO(idPatientEnCours));
		ongletPatient.getTableauAnalysePatient().setModel(new AnalyseTableModel(listeAnalysePatientEnCours.size()));
		for(int i = 0 ; i < listeAnalysePatientEnCours.size() ; i++) {
			ongletPatient.getTableauAnalysePatient().setValueAt(listeAnalysePatientEnCours.get(i).getReference(), i, 0);
			ongletPatient.getTableauAnalysePatient().setValueAt(listeAnalysePatientEnCours.get(i).getTechnicien().getNom(), i, 1);
			ongletPatient.getTableauAnalysePatient().setValueAt(listeAnalysePatientEnCours.get(i).getAppareil().getCodeAppareil(), i, 2);
			ongletPatient.getTableauAnalysePatient().setValueAt(listeAnalysePatientEnCours.get(i).getContenuAnalyse(), i, 3);
			ongletPatient.getTableauAnalysePatient().setValueAt(DonneesDates.formatDate(listeAnalysePatientEnCours.get(i).getDateTime()), i, 4);
			ongletPatient.getTableauAnalysePatient().setValueAt(DonneesDates.formatHeure(listeAnalysePatientEnCours.get(i).getDateTime()), i, 5);
		}
	}
	
	private ArrayList<String> retourHorairesRestants(int idPersonnel, LocalDate date){
		DAORDV connectionTableRDV = new DAORDV();
		ArrayList<RDV> listeRDV = connectionTableRDV.rechercheRDVDUneDateParMedecinDAO(idPersonnel, date);
		int[][] listeHeuresInt = {{10,00}, {10,30}, {11,00}, {11,30}, {12,00}, {12,30}, {15,00}, {15,30}, {16,00}, {16,30}, {17,00}, {17,30},};
		ArrayList<String> horairesDejaPris = new ArrayList<String>();
		for(int j = 0 ; j < listeRDV.size() ; j++) {
			for(int k = 0 ; k < listeHeuresInt.length ; k++) {
				if(listeRDV.get(j).getDateHeure().getHour() == listeHeuresInt[k][0] && listeRDV.get(j).getDateHeure().getMinute() == listeHeuresInt[k][1]) {
					if(listeRDV.get(j).getDateHeure().getMinute() == 0) horairesDejaPris.add(listeRDV.get(j).getDateHeure().getHour()+"H"+listeRDV.get(j).getDateHeure().getMinute()+"0");
					else horairesDejaPris.add(listeRDV.get(j).getDateHeure().getHour()+"H"+listeRDV.get(j).getDateHeure().getMinute());
				}
			}
		}
		ArrayList<String> listesHoraires = new ArrayList<String>();
		listesHoraires.add("10H00");
		listesHoraires.add("10H30");
		listesHoraires.add("11H00");
		listesHoraires.add("11H30");
		listesHoraires.add("12H00");
		listesHoraires.add("12H30");
		listesHoraires.add("15H00");
		listesHoraires.add("15H30");
		listesHoraires.add("16H00");
		listesHoraires.add("16H30");
		listesHoraires.add("17H00");
		listesHoraires.add("17H30");
		ArrayList<String> listesHorairesRestants = new ArrayList<String>();
		int compteur = 0;
		for(int l = 0 ; l < listesHoraires.size() ; l++) {
			compteur = 0;
			for(int m = 0 ; m < horairesDejaPris.size() ; m++) {
				if(listesHoraires.get(l).equalsIgnoreCase(horairesDejaPris.get(m))) compteur++;
			}
			if(compteur == 0) listesHorairesRestants.add(listesHoraires.get(l));
		}
		return listesHorairesRestants;
	}
	
	private int extractionIDMedecinSelection() {
		String medecin = (String)ongletPatient.getChoixMedecinFormulaireNouveauRDVComboBox().getSelectedItem();
		int idMedecin = -1;
		for(int h = 0 ; h < medecin.length() ; h++) {
			if(medecin.charAt(h) == ' ') {
				idMedecin = Integer.parseInt(medecin.substring(0,h));
				break;
			}
		}
		return idMedecin;
	}
	
	private LocalDateTime extractionDateHeureSelection() {
		
		String date = (String)ongletPatient.getChoixDateFormulaireNouveauRDVComboBox().getSelectedItem();
		int jourDuMois = -1;
		for(int i = 0 ; i < date.length() ; i++) {
			if(date.charAt(i) == ' ') {
				jourDuMois = Integer.parseInt(date.substring(0,i));
				break;
			}
		}
		LocalDate dateRDV = LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonthValue(), jourDuMois);
		
		String heure = (String)ongletPatient.getChoixHeureFormulaireNouveauRDVComboBox().getSelectedItem();
		int valeurHeure = Integer.parseInt(heure.substring(0,2));
		int valeurMinutes = Integer.parseInt(heure.substring(3)); 
		
		LocalTime heureRDV = LocalTime.of(valeurHeure, valeurMinutes);
		
		return LocalDateTime.of(dateRDV, heureRDV);
	}
	
	private LocalDate extractionDateDebutSelectionFormulaireHospitalisation() {
		String date = (String)ongletPatient.getDateDebutFormulaireHospitalisationComboBox().getSelectedItem();
		int jourDuMois = -1;
		for(int i = 0 ; i < date.length() ; i++) {
			if(date.charAt(i) == ' ') {
				jourDuMois = Integer.parseInt(date.substring(0,i));
				break;
			}
		}
		return LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonthValue(), jourDuMois);
	}
	
	private LocalDate extractionDateFinSelectionFormulaireHospitalisation() {
		String date = (String)ongletPatient.getDateFinFormulaireHospitalisationComboBox().getSelectedItem();
		if(date.equalsIgnoreCase("NC")) return null;
		int jourDuMois = -1;
		for(int i = 0 ; i < date.length() ; i++) {
			if(date.charAt(i) == ' ') {
				jourDuMois = Integer.parseInt(date.substring(0,i));
				break;
			}
		}
		return LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonthValue(), jourDuMois);
	}
	
	// Listener
	
	class BouttonAjouterFormulaireConsultationListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			DAOConsultation connectionTableConsultation = new DAOConsultation();
			String notesConsultation = ongletPatient.getConsultationArea().getText();
			boolean retourAjoutConsultation = connectionTableConsultation.ajouterConsultationDAO(idPatientEnCours, idPersonnelEnCours, notesConsultation);
			if(retourAjoutConsultation) {
				JOptionPane.showMessageDialog(fenetreAccueil, "La consultation a �t� correctment ajout�e");
				ongletPatient.getConsultationArea().setText("");
				etatConsultation();
			}
			else {
				JOptionPane.showMessageDialog(fenetreAccueil, "Erreur lors de l'ajout de la consultation");
			}
		}
	} 
	
	class BouttonAjouterFormulaireOrdonnanceListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			DAOOrdonnance connectionTableOrdonnance = new DAOOrdonnance();
			String notesOrdonnance = ongletPatient.getOrdonnanceArea().getText();
			boolean retourAjoutOrdonnance = connectionTableOrdonnance.ajouterOrdonnanceDAO(idPatientEnCours, idPersonnelEnCours, notesOrdonnance);
			if(retourAjoutOrdonnance) {
				JOptionPane.showMessageDialog(fenetreAccueil, "L'ordonnance a �t� correctment ajout�e");
				ongletPatient.getOrdonnanceArea().setText("");
				etatOrdonnance();
			}
			else {
				JOptionPane.showMessageDialog(fenetreAccueil, "Erreur lors de l'ajout de l'ordonnance");
			}
		}
	}
	
	class BouttonAjouterFormulaireAnalyseListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			DAOAnalyse connectionTableAnalyse = new DAOAnalyse();
			String notesAnalyse = ongletPatient.getAnalyseArea().getText();
			String codeAppareil = (String)ongletPatient.getChoixAppareilMedicalFormulaireNouvelleAnalyseComboBox().getSelectedItem();
			boolean retourAjoutAnalyse = connectionTableAnalyse.ajouterAnalyseDAO(idPatientEnCours, idPersonnelEnCours, codeAppareil, notesAnalyse);
			if(retourAjoutAnalyse) {
				JOptionPane.showMessageDialog(fenetreAccueil, "L'analyse a �t� correctment ajout�e");
				ongletPatient.getAnalyseArea().setText("");
				etatAnalyse();
			}
			else {
				JOptionPane.showMessageDialog(fenetreAccueil, "Erreur lors de l'ajout de l'analyse");
			}
		}
	} 
	
	class BouttonOuiFormulaireSupprimerPatientListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			DAOPatient connectionTablePatient = new DAOPatient();
			boolean retourSupprimer = connectionTablePatient.supprimerPatientDAO(idPatientEnCours);
			if(retourSupprimer) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Le patient a bien �t� supprim�");
				fenetreAccueil.getConteneurPrincipal().remove(ongletPatient);
			}
			else {
				JOptionPane.showMessageDialog(fenetreAccueil, "Erreur lors de la suppression du patient");
			}
		}
	}
	
	class BouttonModifierFormulaireModifierPatientListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			boolean retourSQL;
			DAOPatient connectionTablePatient = new DAOPatient();
			String choixModifierPatient = ongletPatient.getChoixModificationFormulaireModifierPatient().getSelection().getActionCommand();
			if(choixModifierPatient == "nom") {
				String nouveauNom = ongletPatient.getNomFormulaireModifierPatient().getText();
				retourSQL = connectionTablePatient.modifierNomPatientDAO(idPatientEnCours, nouveauNom);
				if(retourSQL) {
					JOptionPane.showMessageDialog(fenetreAccueil, "Le nom du patient a bien �t� modifi�");
					setDonneesPatientPanelHautGauche();
				}
				else JOptionPane.showMessageDialog(fenetreAccueil, "Erreur lors de la modification du patient");
			}
			else if(choixModifierPatient == "prenom") {
				String nouveauPrenom = ongletPatient.getPrenomFormulaireModifierPatient().getText();
				retourSQL = connectionTablePatient.modifierPrenomPatientDAO(idPatientEnCours, nouveauPrenom);
				if(retourSQL) {
					JOptionPane.showMessageDialog(fenetreAccueil, "Le pr�nom du patient a bien �t� modifi�");
					setDonneesPatientPanelHautGauche();
				}
				else JOptionPane.showMessageDialog(fenetreAccueil, "Erreur lors de la modification du patient");
			}
			else if(choixModifierPatient == "sexe") {
				String nouveauSexe = ongletPatient.getButtonGroupSexeFormulaireModifierPatient().getSelection().getActionCommand();
				retourSQL = connectionTablePatient.modifierSexePatientDAO(idPatientEnCours, nouveauSexe);
				if(retourSQL) {
					JOptionPane.showMessageDialog(fenetreAccueil, "Le sexe du patient a bien �t� modifi�");
					setDonneesPatientPanelHautGauche();
				}
				else JOptionPane.showMessageDialog(fenetreAccueil, "Erreur lors de la modification du patient");
			}
			else if(choixModifierPatient == "dateDeNaissance") {
				LocalDate nouvelleDateDeNaissance = DonneesDates.fromStringToLocalDate(ongletPatient.getDateDeNaissanceFormulaireModifierPatient().getText()); 
				retourSQL = connectionTablePatient.modifierDateDeNaissancePatientDAO(idPatientEnCours, nouvelleDateDeNaissance);
				if(retourSQL) {
					JOptionPane.showMessageDialog(fenetreAccueil, "La date de naissance du patient a bien �t� modifi�");
					setDonneesPatientPanelHautGauche();
				}
				else JOptionPane.showMessageDialog(fenetreAccueil, "Erreur lors de la modification du patient");
			}
			else if(choixModifierPatient == "adresse") {
				String nouvelleAdresse = ongletPatient.getAdresseFormulaireModifierPatient().getText();
				retourSQL = connectionTablePatient.modifierAdressePatientDAO(idPatientEnCours, nouvelleAdresse);
				if(retourSQL) {
					JOptionPane.showMessageDialog(fenetreAccueil, "L'adresse du patient a bien �t� modifi�");
					setDonneesPatientPanelHautGauche();
				}
				else JOptionPane.showMessageDialog(fenetreAccueil, "Erreur lors de la modification du patient");
			}
			else if(choixModifierPatient == "mail") {
				String nouveauMail = ongletPatient.getMailFormulaireModifierPatient().getText();
				retourSQL = connectionTablePatient.modifierMailPatientDAO(idPatientEnCours, nouveauMail);
				if(retourSQL) {
					JOptionPane.showMessageDialog(fenetreAccueil, "Le mail du patient a bien �t� modifi�");
					setDonneesPatientPanelHautGauche();
				}
				else JOptionPane.showMessageDialog(fenetreAccueil, "Erreur lors de la modification du patient");
			}
			else if(choixModifierPatient == "numero") {
				long nouveauNumero = Long.parseLong(ongletPatient.getNumeroFormulaireModifierPatient().getText());
				retourSQL = connectionTablePatient.modifierNumeroTelephonePatientDAO(idPatientEnCours, nouveauNumero);
				if(retourSQL) {
					JOptionPane.showMessageDialog(fenetreAccueil, "Le num�ro de t�l�phone du patient a bien �t� modifi�");
					setDonneesPatientPanelHautGauche();
				}
				else JOptionPane.showMessageDialog(fenetreAccueil, "Erreur lors de la modification du patient");
			}
		}
	}
	
	class BouttonModifierPathologiesFormulairePathologieListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			DAOPatient connectionTablePatient = new DAOPatient();
			String nouvellesPathologies = ongletPatient.getPathologieArea().getText();
			boolean retourModifier = connectionTablePatient.modifierPathologiePatientDAO(idPatientEnCours, nouvellesPathologies);
			if(retourModifier) JOptionPane.showMessageDialog(fenetreAccueil, "La pathologie a bien �t� mise � jour");
			else JOptionPane.showMessageDialog(fenetreAccueil, "Erreur lors de la mise � jour de pathologie");
		}
	}
	
	class ChoixMedecinFormulaireCreerRDVListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			ArrayList<String> listeHorairesRestantes;
			LocalDate dateEnCours;
			String[] moisFrancais = {"Janvier", "F�vrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Ao�t", "Septembre", "Octobre", "Novembre", "D�cembre"};
			String[] tableauDatesRestantes;
			ArrayList<String> listeDates = new ArrayList<String>();
			int idMedecin = extractionIDMedecinSelection();
			if(LocalDate.now().getYear() % 4 == 0) {
				for(int i = LocalDate.now().getDayOfMonth() ; i <= LocalDate.now().getMonth().length(true) ; i++) {
					dateEnCours = LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonthValue(), i);
					listeHorairesRestantes = retourHorairesRestants(idMedecin, dateEnCours);
					if(!listeHorairesRestantes.isEmpty()) listeDates.add(dateEnCours.getDayOfMonth()+" "+moisFrancais[LocalDate.now().getMonthValue()-1]); 
				}
			}
			else {
				for(int i = LocalDate.now().getDayOfMonth() ; i <= LocalDate.now().getMonth().length(false) ; i++) {
					dateEnCours = LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonthValue(), i);
					listeHorairesRestantes = retourHorairesRestants(idMedecin, dateEnCours);
					if(!listeHorairesRestantes.isEmpty()) listeDates.add(dateEnCours.getDayOfMonth()+" "+moisFrancais[LocalDate.now().getMonthValue()-1]); 
				}
			}
			tableauDatesRestantes = new String[listeDates.size()];
			for(int j = 0 ; j < tableauDatesRestantes.length  ; j++) {
				tableauDatesRestantes[j] = listeDates.get(j);
			}
			ongletPatient.setChoixDateFormulaireNouveauRDVComboBox(tableauDatesRestantes);
			ongletPatient.setChoixHeureFormulaireNouveauRDVComboBox(new String[0]);
			ongletPatient.getChoixDateFormulaireNouveauRDVComboBox().setEnabled(true);
		}
	}
	
	class ChoixDateFormulaireCreerRDVListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			String dateEnCours = (String)ongletPatient.getChoixDateFormulaireNouveauRDVComboBox().getSelectedItem();
			int idMedecin = extractionIDMedecinSelection();
			int jourDuMois = 0;
			for(int i = 0 ; i < dateEnCours.length() ; i++) {
				if(dateEnCours.charAt(i) == ' ') {
					jourDuMois = Integer.parseInt(dateEnCours.substring(0,i));
					break;
				}
			}
			LocalDate dateRDV = LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonthValue(), jourDuMois);
			ArrayList<String> listesHorairesRestants = retourHorairesRestants(idMedecin, dateRDV);
			String[] listesHorairesRestantsPourComboBox = new String[listesHorairesRestants.size()];
			for(int n = 0 ; n < listesHorairesRestants.size() ; n++) {
				listesHorairesRestantsPourComboBox[n] = listesHorairesRestants.get(n);
			}
			ongletPatient.setChoixHeureFormulaireNouveauRDVComboBox(listesHorairesRestantsPourComboBox);
			ongletPatient.getChoixHeureFormulaireNouveauRDVComboBox().setEnabled(true);
		}
	}
	
	class BouttonAjouterRDVFormulaireNouveauRDVListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			DAORDV connectionTableRDV = new DAORDV();
			int idMedecinRDV = extractionIDMedecinSelection();
			LocalDateTime dateHeureRDV = extractionDateHeureSelection();
			int salleRDV = connectionTableRDV.recherchePremiereSalleLibreRDVDAO(dateHeureRDV);
			if(salleRDV == -1) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Aucune salle de disponible � ce cr�neau horaire-ci, veuillez changer d'horaire");
			}
			else {
				boolean retourSQL = connectionTableRDV.ajouterRDVDAO(idPatientEnCours, idMedecinRDV, dateHeureRDV, salleRDV);
				if(retourSQL) {
					JOptionPane.showMessageDialog(fenetreAccueil, "Le RDV a bien �t� ajout�");
				}
				else {
					JOptionPane.showMessageDialog(fenetreAccueil, "Erreur lors de l'ajout du patient");
				}
			}
		}
	}
	
	class ChoixAutomatiqueFormulaireHospitalisationListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			ongletPatient.getChoixChambreIndividuelleFormulaireHospitalisation().setEnabled(false);
			ongletPatient.getChambreIndividuelleFormulaireHospitalisationComboBox().setEnabled(false);
			ongletPatient.getChoixChambreDoubleFormulaireHospitalisation().setEnabled(false);
			ongletPatient.getChambreDoubleFormulaireHospitalisationComboBox().setEnabled(false);
			ongletPatient.getChoixChambreQuadrupleFormulaireHospitalisation().setEnabled(false);
			ongletPatient.getChambreQuadrupleFormulaireHospitalisationComboBox().setEnabled(false);
			ongletPatient.getChoixTypeChambreFormulaireHospitalisation().clearSelection();
		}
	}
	
	class ChoixManuelFormulaireHospitalisationListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			if(ongletPatient.getCodeServiceFormulaireHospitalisation().getText().isEmpty()) {
				JOptionPane.showMessageDialog(fenetreAccueil, "Veuillez renseigner le code service");
				ongletPatient.getChoixAutomatiqueFormulaireHospitalisation().setSelected(true);
			}
			else {
				setChambreIndividuelle();
				ongletPatient.getChoixChambreIndividuelleFormulaireHospitalisation().setEnabled(true);
				ongletPatient.getChoixChambreIndividuelleFormulaireHospitalisation().setSelected(true);
				ongletPatient.getChambreIndividuelleFormulaireHospitalisationComboBox().setEnabled(true);
				ongletPatient.getChoixChambreDoubleFormulaireHospitalisation().setEnabled(true);
				ongletPatient.getChoixChambreQuadrupleFormulaireHospitalisation().setEnabled(true);
			}
		}
	}
	
	class ChoixIndividuelFormulaireHospitalisationListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			setChambreIndividuelle();
			ongletPatient.getChambreIndividuelleFormulaireHospitalisationComboBox().setEnabled(true);
			ongletPatient.getChambreDoubleFormulaireHospitalisationComboBox().setEnabled(false);
			ongletPatient.getChambreQuadrupleFormulaireHospitalisationComboBox().setEnabled(false);
		}
	}
	
	class ChoixDoubleFormulaireHospitalisationListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			setChambreDouble();
			ongletPatient.getChambreIndividuelleFormulaireHospitalisationComboBox().setEnabled(false);
			ongletPatient.getChambreDoubleFormulaireHospitalisationComboBox().setEnabled(true);
			ongletPatient.getChambreQuadrupleFormulaireHospitalisationComboBox().setEnabled(false);
		}
	}

	class ChoixQuadrupleFormulaireHospitalisationListener implements ActionListener {
	
		public void actionPerformed(ActionEvent event) {
			setChambreQuadruple();
			ongletPatient.getChambreIndividuelleFormulaireHospitalisationComboBox().setEnabled(false);
			ongletPatient.getChambreDoubleFormulaireHospitalisationComboBox().setEnabled(false);
			ongletPatient.getChambreQuadrupleFormulaireHospitalisationComboBox().setEnabled(true);
		}
	}
	
	class BouttonHospitaliserFormulaireHospitalisationListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			DAOHospitalisation connectionTableHospitalisation = new DAOHospitalisation();
			if(ongletPatient.getChoixAutoManuelFormulaireHospitalisation().getSelection().getActionCommand() == "automatique") {
				String codeLit ="";
				if(!connectionTableHospitalisation.listeChambreIndividuelleVideService(ongletPatient.getCodeServiceFormulaireHospitalisation().getText().charAt(0)).isEmpty()) {
					codeLit = connectionTableHospitalisation.listeChambreIndividuelleVideService(ongletPatient.getCodeServiceFormulaireHospitalisation().getText().charAt(0)).get(0);
				}
				else if(!connectionTableHospitalisation.listeChambreDoubleVideService(ongletPatient.getCodeServiceFormulaireHospitalisation().getText().charAt(0)).isEmpty()) {
					codeLit = connectionTableHospitalisation.listeChambreDoubleVideService(ongletPatient.getCodeServiceFormulaireHospitalisation().getText().charAt(0)).get(0);
				}
				else if(!connectionTableHospitalisation.listeChambreQuadrupleVideService(ongletPatient.getCodeServiceFormulaireHospitalisation().getText().charAt(0)).isEmpty()) {
					codeLit = connectionTableHospitalisation.listeChambreQuadrupleVideService(ongletPatient.getCodeServiceFormulaireHospitalisation().getText().charAt(0)).get(0);
				}
				if(codeLit.equalsIgnoreCase("")) {
					JOptionPane.showMessageDialog(fenetreAccueil, "Il n'y a plus aucun lit libre dans ce service");
					return;
				}
				LocalDate dateDebut = extractionDateDebutSelectionFormulaireHospitalisation();
				LocalDate dateFin = extractionDateFinSelectionFormulaireHospitalisation();
				boolean retourSQL = connectionTableHospitalisation.ajouterHospitalisationDAO(idPatientEnCours, codeLit, dateDebut, dateFin);
				if(retourSQL) {
					JOptionPane.showMessageDialog(fenetreAccueil, "Hospitalisation du patient r�alis�");
					ongletPatient.getCodeServiceFormulaireHospitalisation().setText("");
					ongletPatient.getChoixAutomatiqueFormulaireHospitalisation().setSelected(true);
					ongletPatient.getOuiHospitaliseFormulaireDonneesPersonnellesRadio().setSelected(true);
				}
				else {
					JOptionPane.showMessageDialog(fenetreAccueil, "Erreur lors de l'hospitalisation du patient");
				}
			}
			else if(ongletPatient.getChoixAutoManuelFormulaireHospitalisation().getSelection().getActionCommand() == "manuel") {
				String codeLit = "";
				if(ongletPatient.getChoixTypeChambreFormulaireHospitalisation().getSelection().getActionCommand() == "individuel") {
					codeLit = (String)ongletPatient.getChambreIndividuelleFormulaireHospitalisationComboBox().getSelectedItem();
				}
				else if(ongletPatient.getChoixTypeChambreFormulaireHospitalisation().getSelection().getActionCommand() == "double") {
					codeLit = (String)ongletPatient.getChambreDoubleFormulaireHospitalisationComboBox().getSelectedItem();
				}
				else if(ongletPatient.getChoixTypeChambreFormulaireHospitalisation().getSelection().getActionCommand() == "quadruple") {
					codeLit = (String)ongletPatient.getChambreQuadrupleFormulaireHospitalisationComboBox().getSelectedItem();
				}
				LocalDate dateDebut = extractionDateDebutSelectionFormulaireHospitalisation();
				LocalDate dateFin = extractionDateFinSelectionFormulaireHospitalisation();
				boolean retourSQL = connectionTableHospitalisation.ajouterHospitalisationDAO(idPatientEnCours, codeLit, dateDebut, dateFin);
				if(retourSQL) {
					JOptionPane.showMessageDialog(fenetreAccueil, "Hospitalisation du patient r�alis�");
					ongletPatient.getCodeServiceFormulaireHospitalisation().setText("");
					ongletPatient.getChoixAutomatiqueFormulaireHospitalisation().setSelected(true);
					ongletPatient.getOuiHospitaliseFormulaireDonneesPersonnellesRadio().setSelected(true);
				}
				else {
					JOptionPane.showMessageDialog(fenetreAccueil, "Erreur lors de l'hospitalisation du patient");
				}
			}
		}
	}
	
	// MouseListener
	
	class FermerOngletPatientListener implements MouseListener {
		
		public void mouseClicked(MouseEvent e) {
			fenetreAccueil.getConteneurPrincipal().remove(ongletPatient);
		}
		public void mouseEntered(MouseEvent e) {
			fermerOngletPatient.setForeground(Color.RED);
		}
		public void mouseExited(MouseEvent e) {
			fermerOngletPatient.setForeground(Color.BLACK);
		}
		public void mousePressed(MouseEvent e) {}
		public void mouseReleased(MouseEvent e) {}
	}
	
	class TableauRDVListener implements MouseListener {
		public void mousePressed(MouseEvent e) {
			 JTable table =(JTable) e.getSource();
			 Point point = e.getPoint();
			 int row = table.rowAtPoint(point);
		     if (e.getClickCount() == 2) {
				int numeroReferenceEnCours = listeRDVPatientEnCours.get(row).getReferenceRDV();
				String nomDocteurEnCours = listeRDVPatientEnCours.get(row).getDocteur().getNom();
				String prenomDocteurEnCours = listeRDVPatientEnCours.get(row).getDocteur().getPrenom();
				int salleEnCours = listeRDVPatientEnCours.get(row).getSalle();
				LocalDateTime dateHeureAnalyseEnCours = listeRDVPatientEnCours.get(row).getDateHeure();
				ongletPatient.getNumeroReferenceFormulaireAfficherRDV().setText(""+numeroReferenceEnCours);
				ongletPatient.getNomPrenomMedecinFormulaireAfficherRDV().setText(nomDocteurEnCours+" "+prenomDocteurEnCours);
				ongletPatient.getSalleFormulaireAfficherRDV().setText(salleEnCours+"");
				ongletPatient.getDateRDVFormulaireAfficherRDV().setText(DonneesDates.formatDate(dateHeureAnalyseEnCours));
				ongletPatient.getHeureRDVFormulaireAfficherRDV().setText(DonneesDates.formatHeure(dateHeureAnalyseEnCours));
				ongletPatient.getLayoutMultiPanel().show(ongletPatient.getPanelBasDroit(), "panelAfficherRDV");
		     }
		}
		public void mouseClicked(MouseEvent e) {}
		public void mouseReleased(MouseEvent e) {}
		public void mouseEntered(MouseEvent e) {}
		public void mouseExited(MouseEvent e) {}
	}
	
	class TableauConsultationListener implements MouseListener {
		public void mousePressed(MouseEvent e) {
			 JTable table =(JTable) e.getSource();
			 Point point = e.getPoint();
			 int row = table.rowAtPoint(point);
		     if (e.getClickCount() == 2) {
				int numeroReferenceEnCours = listeConsultationPatientEnCours.get(row).getReferenceConsultation();
				String nomDocteurEnCours = listeConsultationPatientEnCours.get(row).getMedecin().getNom();
				String prenomDocteurEnCours = listeConsultationPatientEnCours.get(row).getMedecin().getPrenom();
				LocalDate dateConsultationEnCours = listeConsultationPatientEnCours.get(row).getDate();
				String contenuConsultation = listeConsultationPatientEnCours.get(row).getNotesConsultation();
				ongletPatient.getNumeroReferenceFormulaireAfficherConsultation().setText(""+numeroReferenceEnCours);
				ongletPatient.getNomPrenomDocteurFormulaireAfficherConsultation().setText(nomDocteurEnCours+" "+prenomDocteurEnCours);
				ongletPatient.getDateConsultationFormulaireAfficherConsultation().setText(DonneesDates.formatDate(dateConsultationEnCours));
				ongletPatient.getNotesConsultationFormulaireAfficherConsultationArea().setText(contenuConsultation);
				ongletPatient.getLayoutMultiPanel().show(ongletPatient.getPanelBasDroit(), "panelAfficherConsultation");
		     }
		}
		public void mouseClicked(MouseEvent e) {}
		public void mouseReleased(MouseEvent e) {}
		public void mouseEntered(MouseEvent e) {}
		public void mouseExited(MouseEvent e) {}
	}
	
	class TableauOrdonnanceListener implements MouseListener {
		public void mousePressed(MouseEvent e) {
			 JTable table =(JTable) e.getSource();
			 Point point = e.getPoint();
			 int row = table.rowAtPoint(point);
		     if (e.getClickCount() == 2) {
				int numeroReferenceEnCours = listeOrdonnancePatientEnCours.get(row).getReferenceOrdonnance();
				String nomDocteurEnCours = listeOrdonnancePatientEnCours.get(row).getMedecin().getNom();
				String prenomDocteurEnCours = listeOrdonnancePatientEnCours.get(row).getMedecin().getPrenom();
				LocalDate dateOrdonnanceEnCours = listeOrdonnancePatientEnCours.get(row).getDate();
				String contenuOrdonnance = listeOrdonnancePatientEnCours.get(row).getOrdonnance();
				ongletPatient.getNumeroReferenceFormulaireAfficherOrdonnance().setText(""+numeroReferenceEnCours);
				ongletPatient.getNomPrenomDocteurFormulaireAfficherOrdonnance().setText(nomDocteurEnCours+" "+prenomDocteurEnCours);
				ongletPatient.getDateOrdonnanceFormulaireAfficherOrdonnance().setText(DonneesDates.formatDate(dateOrdonnanceEnCours));
				ongletPatient.getNotesOrdonnanceFormulaireAfficherOrdonnanceArea().setText(contenuOrdonnance);
				ongletPatient.getLayoutMultiPanel().show(ongletPatient.getPanelBasDroit(), "panelAfficherOrdonnance");
		     }
		}
		public void mouseClicked(MouseEvent e) {}
		public void mouseReleased(MouseEvent e) {}
		public void mouseEntered(MouseEvent e) {}
		public void mouseExited(MouseEvent e) {}
	}
	
	class TableauAnalyseListener implements MouseListener {
		public void mousePressed(MouseEvent e) {
			 JTable table =(JTable) e.getSource();
			 Point point = e.getPoint();
			 int row = table.rowAtPoint(point);
		     if (e.getClickCount() == 2) {
				int numeroReferenceEnCours = listeAnalysePatientEnCours.get(row).getReference();
				String nomTechnicienEnCours = listeAnalysePatientEnCours.get(row).getTechnicien().getNom();
				String prenomTechnicienEnCours = listeAnalysePatientEnCours.get(row).getTechnicien().getPrenom();
				String appareilEnCours = listeAnalysePatientEnCours.get(row).getAppareil().getCodeAppareil();
				LocalDateTime dateHeureAnalyseEnCours = listeAnalysePatientEnCours.get(row).getDateTime();
				String contenuAnalyse = listeAnalysePatientEnCours.get(row).getContenuAnalyse();
				ongletPatient.getNumeroReferenceFormulaireAfficherAnalyse().setText(""+numeroReferenceEnCours);
				ongletPatient.getNomPrenomTechnicienFormulaireAfficherAnalyse().setText(nomTechnicienEnCours+" "+prenomTechnicienEnCours);
				ongletPatient.getAppareilFormulaireAfficherAnalyse().setText(appareilEnCours);
				ongletPatient.getDateAnalyseFormulaireAfficherAnalyse().setText(DonneesDates.formatDate(dateHeureAnalyseEnCours));
				ongletPatient.getHeureAnalyseFormulaireAfficherAnalyse().setText(DonneesDates.formatHeure(dateHeureAnalyseEnCours));
				ongletPatient.getNotesAnalyseFormulaireAfficherAnalyseArea().setText(contenuAnalyse);
				ongletPatient.getLayoutMultiPanel().show(ongletPatient.getPanelBasDroit(), "panelAfficherAnalyse");
		     }
		}
		public void mouseClicked(MouseEvent e) {}
		public void mouseReleased(MouseEvent e) {}
		public void mouseEntered(MouseEvent e) {}
		public void mouseExited(MouseEvent e) {}
	}
	
	
}