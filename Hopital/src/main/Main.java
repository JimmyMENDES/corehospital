package main;

import controller.LoginController;
import modele.jdbc.DAOIdentification;
import vues.Login;

/**
 * Classe Main du programme.
 * <p>Concernant l'organisation des classes, ce programme est compos� comme suit :
 * <ul><li>Package <b>hmi</b> qui regroupe l'ensemble des classes qui cr�ent l'interface.</li>
 * <li>Package <b>donnees</b> qui regoupe toutes les classes qui g�rent les m�thodes li�es � leur classes respectives : DonneesXXXX</li>
 * <li>Les packages <b>humain</b>, <b>immateriel</b>, <b>installations</b>, et <b>materiel</b> qui regroupent respectivement leur classes associ�es.</li>
 * <li>Pour finir, le package <b>main</b> contient la m�thode main</li></ul></p> 
 * @author Jimmy MENDES
 */
public class Main {

	/**
	 * M�thode Principale du projet Hopital.
	 * <p>La m�thode cr�e un hopital fictionnel appel� coreHospital.
	 * On utilise ensuite la m�thode addAll() pour ajouter des listes pr�configur�es de patients/medecins/Salles/RDV/Services/Appareils Medicaux.
	 * Enfin, la m�thode choixDebut() contenue dans la classe ChoixConsole lance l'interface.</p>
	 * @param args
	 */
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		LoginController main = new LoginController(new DAOIdentification(), new Login());
	}
	
}