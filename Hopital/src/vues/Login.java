package vues;

import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class Login extends JFrame {
	
	JPanel conteneurPrincipal;
	JPanel conteneurTexteLogin;
	JPanel conteneurLogin;
	JPanel conteneurTexteMDP;
	JPanel conteneurMDP;
	JPanel conteneurBoutton;
	
	JLabel loginTexte;
	JTextField loginUtilisateur;
	JLabel motDePasseTexte;
	JPasswordField motDePasse;
	JButton valider;
	
	public Login() {
		super();
		conteneurPrincipal = new JPanel();
		conteneurPrincipal.setLayout(new GridLayout(5,1));
		
		// Cr�ation/Ajout des diff�rents �l�ments au conteneurPrincipal
		this.setLoginText();
		this.setLogin();
		this.setMDPText();
		this.setMDP();
		this.setValiderButton();
		this.addElement();
		
		// Options Fenetre Login
		this.getContentPane().add(conteneurPrincipal);
		this.setTitle("Identification");
		this.setSize(400,200);
		this.setResizable(false);
		
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}
	
	public JButton getBouttonValider() {
		return this.valider;
	}
	
	public String getLoginUtilisateur() {
		return this.loginUtilisateur.getText();
	}
	
	public char[] getMotDePasse() {
		return this.motDePasse.getPassword();
	}
	
	public JPasswordField getMotDePassePasswordField() {
		return this.motDePasse;
	}
	
	public void clearLoginUtilisateur() {
		this.loginUtilisateur.setText("");
	}
	
	public void clearMotDePasse() {
		this.motDePasse.setText("");
	}
	
	private void setLoginText() {
		conteneurTexteLogin = new JPanel();
		loginTexte = new JLabel("Login :");
		conteneurTexteLogin.add(loginTexte);
	}
	
	private void setLogin() {
		conteneurLogin = new JPanel();
		loginUtilisateur = new JTextField();
		loginUtilisateur.setPreferredSize(new Dimension(150,25));
		conteneurLogin.add(loginUtilisateur);
	}
	
	private void setMDPText() {
		conteneurTexteMDP = new JPanel();
		motDePasseTexte = new JLabel("Mot de Passe :");
		conteneurTexteMDP.add(motDePasseTexte);
	}
	
	private void setMDP() {
		conteneurMDP = new JPanel();
		motDePasse = new JPasswordField();
		motDePasse.setPreferredSize(new Dimension(150,25));
		conteneurMDP.add(motDePasse);
	}
	
	private void setValiderButton() {
		conteneurBoutton = new JPanel();
		valider = new JButton("Valider");
		valider.setPreferredSize(new Dimension(80,22));
		conteneurBoutton.add(valider);
	}
	
	private void addElement() {
		conteneurPrincipal.add(conteneurTexteLogin);
		conteneurPrincipal.add(conteneurLogin);
		conteneurPrincipal.add(conteneurTexteMDP);
		conteneurPrincipal.add(conteneurMDP);
		conteneurPrincipal.add(conteneurBoutton);
	}
	
}