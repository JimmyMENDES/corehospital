package vues.tablemodel;

import javax.swing.table.AbstractTableModel;

@SuppressWarnings("serial")
public class MesRDVMedecinTableModel extends AbstractTableModel {
	private String[] columnNames = {"","Patient","Salle"};
	private Object[][] data;
	
	public MesRDVMedecinTableModel() {
		this.data = new Object[13][3];
		setValueAt("10H00",0,0);
		setValueAt("10H30",1,0);
		setValueAt("11H00",2,0);
		setValueAt("11H30",3,0);
		setValueAt("12H00",4,0);
		setValueAt("12H30",5,0);
		setValueAt("",6,0);
		setValueAt("",6,1);
		setValueAt("",6,2);
		setValueAt("15H00",7,0);
		setValueAt("15H30",8,0);
		setValueAt("16H00",9,0);
		setValueAt("16H30",10,0);
		setValueAt("17H00",11,0);
		setValueAt("17H30",12,0);
	}
	
	public int getColumnCount() {
		return columnNames.length;
	}
	
	public String getColumnName(int col) {
	    return columnNames[col];
	}
	
	public int getRowCount() {
		return data.length;
	}
	
	public void setRowCount(int nombreLignes) {
		data = new Object[nombreLignes][5];
	}
	
	public Object getValueAt(int row, int col) {
		return data[row][col];
	}
	
	public void setValueAt(Object value, int row, int col) {
	    data[row][col] = value;
	    fireTableCellUpdated(row, col);
	}
}
