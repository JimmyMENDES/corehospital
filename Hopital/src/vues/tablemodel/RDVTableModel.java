package vues.tablemodel;

import javax.swing.table.AbstractTableModel;

@SuppressWarnings("serial")
public class RDVTableModel extends AbstractTableModel {
	private String[] columnNames = {"R�f�rence","Docteur","Salle", "Date", "Heure"};
	private Object[][] data;
	
	public RDVTableModel(int nombreLignes) {
		this.data = new Object[nombreLignes][5];
	}
	
	public int getColumnCount() {
		return columnNames.length;
	}
	
	public String getColumnName(int col) {
	    return columnNames[col];
	}
	
	public int getRowCount() {
		return data.length;
	}
	
	public void setRowCount(int nombreLignes) {
		data = new Object[nombreLignes][5];
	}
	
	public Object getValueAt(int row, int col) {
		return data[row][col];
	}
	
	public void setValueAt(Object value, int row, int col) {
	    data[row][col] = value;
	    fireTableCellUpdated(row, col);
	}
}
