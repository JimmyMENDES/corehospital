package vues;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import vues.conteneurgraphiquelit.ConteneurGraphique1Lit;
import vues.conteneurgraphiquelit.ConteneurGraphique2Lit;
import vues.conteneurgraphiquelit.ConteneurGraphique4Lit;
import vues.tablemodel.MesRDVMedecinTableModel;
import vues.tablemodel.PharmacieTableModel;
import vues.tablemodel.RechercherPatientTableModel;

@SuppressWarnings("serial")
public class FenetreAccueil extends JFrame {

	JTabbedPane conteneurPrincipal;
	JPanel conteneurOngletAccueil;
	
	JPanel conteneurGauche;
	
	JPanel conteneurDroite;
	CardLayout layoutMultiPanelDroite;
	JPanel conteneurDroiteVide;
	JPanel conteneurDroiteCreerPatient;
	JPanel conteneurDroiteRechercherPatient;
	// Panel SuperAdmin
	JPanel conteneurDroiteAjoutPersonnel;
	JPanel conteneurDroiteSuppressionPersonnel;
	JPanel conteneurDroiteAjoutInstallations;
	JPanel conteneurDroiteSuppressionInstallations;
	JPanel conteneurDroiteAjoutAppareilMedical;
	JPanel conteneurDroiteSuppressionAppareilMedical;
	// Panel Medecin
	JPanel conteneurDroiteMesRDV;
	// Panel Infirmier
	JPanel conteneurDroiteEtatService;
	// Panel Pharmacien
	JPanel conteneurDroiteEtatPharmacie;
	JPanel conteneurDroiteAjoutRetraitArticle;
	JPanel conteneurDroiteCreerArticle;
	JPanel conteneurDroiteSuppressionArticle;
	
	// JComponent conteneurGauche
	JButton creerPatientConteneurGauche;
	JButton rechercherPatientConteneurGauche;
	JButton logoutConteneurGauche;
	// SuperAdmin
	JButton ajoutPersonnelConteneurGauche;
	JButton suppressionPersonnelConteneurGauche;
	JButton ajoutInstallationConteneurGauche;
	JButton suppressionInstallationConteneurGauche;
	JButton ajoutMaterielConteneurGauche;
	JButton suppressionMaterielConteneurGauche;
	// Medecin
	JButton mesRDVConteneurGauche;
	// Infirmier
	JButton etatServiceConteneurGauche;
	// Pharmacien
	JButton etatPharmacieConteneurGauche;
	JButton ajoutRetraitArticleConteneurGauche;
	JButton creerArticleConteneurGauche;
	JButton supprimerArticleConteneurGauche;
	
	// JComponent conteneurDroite
	
	// JComponent conteneurDroiteCreerPatient
	JTextField nomFormulaireCreerPatientTextField;
	JTextField prenomFormulaireCreerPatientTextField;
	ButtonGroup selectionSexeFormulaireCreerPatientButtonGroup;
	JTextField dateDeNaissanceFormulaireCreerPatientTextField;
	JTextArea adresseFormulaireCreerPatientTextArea;
	JTextField mailFormulaireCreerPatientTextField;
	JTextField telephoneFormulaireCreerPatientTextField;
	JTextArea pathologiesFormulaireCreerPatientTextArea;
	JButton validerFormulaireCreerPatient;
	JButton effacerFormulaireCreerPatient;
	
	// JComponent conteneurDroiteRechercherPatient
	JPanel conteneurDroiteRechercherPatientBas;
	CardLayout layoutMultiPanelconteneurDroiteRechercherPatientBas;
	ButtonGroup selectionMethodeRechercheFormulaireRechercherPatientButtonGroup;
	JRadioButton parIdentifiantFormulaireRechercherPatientRadio;
	JRadioButton parNomFormulaireRechercherPatientRadio;
	JRadioButton parDateDeNaissanceFormulaireRechercherPatientRadio;
	JTextField identifiantFormulaireRechercherPatientTextField;
	JTextField nomFormulaireRechercherPatientTextField;
	JTextField dateDeNaissanceFormulaireRechercherPatientTextField;
	JButton validerFormulaireRechercherPatient;
	JTable retourFormulaireRechercherPatient;
	
	// JComponent conteneurDroiteAjoutPersonnel
	JTextField nomFormulaireAjouterPersonnelTextField;
	JTextField prenomFormulaireAjouterPersonnelTextField;
	ButtonGroup selectionSexeFormulaireAjouterPersonnelButtonGroup;
	JTextField dateDeNaissanceFormulaireAjouterPersonnelTextField;
	JTextArea adresseFormulaireAjouterPersonnelTextArea;
	JTextField mailFormulaireAjouterPersonnelTextField;
	JTextField telephoneFormulaireAjouterPersonnelTextField;
	JComboBox<String> selectionProfessionFormulaireAjouterPersonnelComboBox;
	String[] choixProfession = {"medecin","infirmier","pharmacien","technicien","agent_administrateur","rh"};
	JTextField rppsFormulaireAjouterPersonnelTextField;
	JComboBox<String> selectionSpecialiteFormulaireAjouterPersonnelComboBox;
	String[] choixSpecialite = {"medecine_generale","cardiologie","chirurgie","pediatrie"};
	JComboBox<String> selectionServiceFormulaireAjouterPersonnelComboBox;
	String[] choixService = {"A","B","C","D","U"};
	JTextField salaireFormulaireAjouterPersonnelTextField;
	JButton validerFormulaireAjouterPersonnel;
	JButton effacerFormulaireAjoutPersonnel;
	
	// conteneurDroiteSuppressionPersonnel
	JTextField idFormulaireSuppressionPersonnelTextField;
	JButton supprimerFormulaireSuppressionPersonnel;
	
	// conteneurDroiteAjoutInstallations;
	JTextField codeServiceFormulaireAjoutInstallationsTextField;
	JTextField nomServiceFormulaireAjoutInstallationsTextField;
	ButtonGroup etatServiceFormulaireAjoutInstallationsTextFieldButtonGroup;
	JRadioButton serviceVideRadio;
	JRadioButton serviceRempliRadio;
	JCheckBox chambreIndividuelleFormulaireAjoutInstallationsCheckBox;
	JSpinner chambreIndividuelleFormulaireAjoutInstallationsSpinner;
	JCheckBox chambreDoubleFormulaireAjoutInstallationsCheckBox;
	JSpinner chambreDoubleFormulaireAjoutInstallationsSpinner;
	JCheckBox chambreQuadrupleFormulaireAjoutInstallationsCheckBox;
	JSpinner chambreQuadrupleFormulaireAjoutInstallationsSpinner;
	JButton ajouterFormulaireAjoutInstallations;
	
	// conteneurDroiteSuppressionInstallations;
	ButtonGroup choixTypeInstallationFormulaireSuppressionInstallations;
	JRadioButton choixServiceFormulaireSuppressionInstallations;
	JRadioButton choixChambreFormulaireSuppressionInstallations;
	JRadioButton choixLitFormulaireSuppressionInstallations;
	JTextField codeServiceFormulaireSuppressionInstallations;
	JTextField codeChambreFormulaireSuppressionInstallations;
	JTextField codeLitFormulaireSuppressionInstallations;
	JButton supprimerFormulaireSuppressionInstallations;
	
	// conteneurDroiteAjoutAppareilMedical;
	JTextField codeAppareilFormulaireAjoutAppareilMedicalTextField;
	JTextField typeAppareilFormulaireAjoutAppareilMedicalTextField;
	JButton ajouterAppareilFormulaireAjoutAppareilMedical;
	
	// conteneurDroiteSuppressionAppareilMedical;
	JTextField codeAppareilFormulaireSuppressionAppareilMedical;
	JButton supprimerAppareilFormulaireSuppressionAppareilMedical;
	
	// conteneurDroiteMesRDV
	JTable mesRDVMedecin;
	
	// conteneurDroiteEtatService
	ConteneurGraphique1Lit conteneurDroiteEtatServiceLigne1Cellule1;
	ConteneurGraphique1Lit conteneurDroiteEtatServiceLigne1Cellule2;
	ConteneurGraphique1Lit conteneurDroiteEtatServiceLigne1Cellule3;
	ConteneurGraphique1Lit conteneurDroiteEtatServiceLigne1Cellule4;
	ConteneurGraphique1Lit conteneurDroiteEtatServiceLigne2Cellule1;
	ConteneurGraphique1Lit conteneurDroiteEtatServiceLigne2Cellule2;
	ConteneurGraphique1Lit conteneurDroiteEtatServiceLigne2Cellule3;
	ConteneurGraphique1Lit conteneurDroiteEtatServiceLigne2Cellule4;
	ConteneurGraphique2Lit conteneurDroiteEtatServiceLigne3Cellule1;
	ConteneurGraphique2Lit conteneurDroiteEtatServiceLigne3Cellule2;
	ConteneurGraphique2Lit conteneurDroiteEtatServiceLigne4Cellule1;
	ConteneurGraphique2Lit conteneurDroiteEtatServiceLigne4Cellule2;
	ConteneurGraphique2Lit conteneurDroiteEtatServiceLigne5Cellule1;
	ConteneurGraphique2Lit conteneurDroiteEtatServiceLigne5Cellule2;
	ConteneurGraphique2Lit conteneurDroiteEtatServiceLigne6Cellule1;
	ConteneurGraphique2Lit conteneurDroiteEtatServiceLigne6Cellule2;
	ConteneurGraphique4Lit conteneurDroiteEtatServiceLigne7;
	ConteneurGraphique4Lit conteneurDroiteEtatServiceLigne8;
	ConteneurGraphique4Lit conteneurDroiteEtatServiceLigne9;
	ConteneurGraphique4Lit conteneurDroiteEtatServiceLigne10;
	
	// conteneurDroiteEtatPharmacie;
	JTable etatPharmacieTable;
	
	// conteneurDroiteAjoutRetraitArticle;
	JTextField referenceFormulaireAjoutRetraitArticleTextField;
	JSpinner quantiteFormulaireAjoutRetraitArticleTextField;
	JButton ajouterArticleFormulaireAjoutRetraitArticle;
	JButton retraitArticleFormulaireAjoutRetraitArticle;
	
	// conteneurDroiteCreerArticle;
	JTextField referenceFormulaireCreerArticleTextField;
	JTextField nomFormulaireCreerArticleTextField;
	JComboBox<String> selectionTypeArticleFormulaireCreerArticleComboBox;
	String[] choixTypeArticle = {"article_general","medicament","equipement"};
	JButton validerFormulaireCreerArticle;
	JButton effacerFormulaireCreerArticle;
	
	// conteneurDroiteSuppressionArticle;
	JTextField referenceFormulaireSupprimerArticleTextField;
	JButton supprimerArticleFormulaire;
	
	public FenetreAccueil(String profession) {
		
		conteneurPrincipal = new JTabbedPane();
		
		conteneurOngletAccueil = new JPanel(new GridLayout(1,2));
		
		conteneurGauche = new JPanel();
		conteneurDroite = new JPanel();
		layoutMultiPanelDroite = new CardLayout();

		if(profession.equalsIgnoreCase("agent_administrateur")) {
			this.setPartieGaucheAgentAdministrateur();
			this.setPartieDroiteAgentAdministrateur();
		}
		else if(profession.equalsIgnoreCase("rh")) {
			this.setPartieGaucheRH();
			this.setPartieDroiteRH();
		}
		else if(profession.equalsIgnoreCase("technicien")) {
			this.setPartieGaucheTechnicien();
			this.setPartieDroiteTechnicien();
		}
		else if(profession.equalsIgnoreCase("medecin")) {
			this.setPartieGaucheMedecin();
			this.setPartieDroiteMedecin();
		}
		else if(profession.equalsIgnoreCase("infirmier")) {
			this.setPartieGaucheInfirmier();
			this.setPartieDroiteInfirmier();
		}
		else if(profession.equalsIgnoreCase("pharmacien")) {
			this.setPartieGauchePharmacien();
			this.setPartieDroitePharmacien();
		}
		else if(profession.equalsIgnoreCase("superadmin")) {
			this.setPartieGaucheSuperAdmin();
			this.setPartieDroiteSuperAdmin();
		}
		
		conteneurPrincipal.add("Accueil", conteneurOngletAccueil);
		
		this.getContentPane().add(conteneurPrincipal);
		this.setTitle("Core Hospital Administrator");
		this.setSize(1024,720);
		this.setMinimumSize(new Dimension(800,600));
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}
	
	// M�thodes private appel�es par le constructeur --------------------------------------------------------------------------------------------------------------------------------------
	
	// Construction partie gauche des interfaces ---------------------------------------------------------------------
	
	private void setPartieGaucheSuperAdmin() {
		conteneurGauche.setLayout(new GridLayout(4,1,100,60));
		conteneurGauche.setBorder(BorderFactory.createEmptyBorder(60, 50, 60, 50));
		this.setBouttonsAddSupprPersonnel();
		this.setBouttonsAddSupprInstallation();
		this.setBouttonsAddSupprMateriel();
		this.setBouttonLogout();
		conteneurOngletAccueil.add(conteneurGauche);
	}
	
	private void setPartieGaucheAgentAdministrateur() {
		conteneurGauche.setLayout(new GridLayout(3,1,100,100));
		conteneurGauche.setBorder(BorderFactory.createEmptyBorder(100, 100, 100, 100));
		this.setBouttonCreer();
		this.setBouttonRechercher();
		this.setBouttonLogout();
		conteneurOngletAccueil.add(conteneurGauche);
	}
	
	private void setPartieGaucheRH() {

	}
	
	private void setPartieGaucheTechnicien() {
		conteneurGauche.setLayout(new GridLayout(2,1,100,160));
		conteneurGauche.setBorder(BorderFactory.createEmptyBorder(160, 100, 160, 100));
		this.setBouttonRechercher();
		this.setBouttonLogout();
		conteneurOngletAccueil.add(conteneurGauche);
	}
	
	private void setPartieGaucheMedecin() {
		conteneurGauche.setLayout(new GridLayout(4,1,100,60));
		conteneurGauche.setBorder(BorderFactory.createEmptyBorder(60, 100, 60, 100));
		this.setBouttonCreer();
		this.setBouttonRechercher();
		this.setBouttonMesRDV();
		this.setBouttonLogout();
		conteneurOngletAccueil.add(conteneurGauche);
	}
	
	private void setPartieGaucheInfirmier() {
		conteneurGauche.setLayout(new GridLayout(3,1,100,100));
		conteneurGauche.setBorder(BorderFactory.createEmptyBorder(100, 100, 100, 100));
		this.setBouttonRechercher();
		this.setBouttonEtatService();
		this.setBouttonLogout();
		conteneurOngletAccueil.add(conteneurGauche);
	}
	
	private void setPartieGauchePharmacien() {
		conteneurGauche.setLayout(new GridLayout(5,1,100,60));
		conteneurGauche.setBorder(BorderFactory.createEmptyBorder(60, 50, 60, 50));
		this.setBouttonEtatPharmacie();
		this.setBouttonRechercher();
		this.setBouttonsAjouterRetirerArticle();
		this.setBouttonsCreerSupprimerArticle();
		this.setBouttonLogout();
		conteneurOngletAccueil.add(conteneurGauche);
	}
	
	// M�thodes mod�lisant les bouttons utilis�s par la partie gauche de l'interface-------------------------------------------------------
	
	private void setBouttonCreer() {
		creerPatientConteneurGauche = new JButton("Cr�er un patient");
		conteneurGauche.add(creerPatientConteneurGauche);
	}
	
	private void setBouttonRechercher() {
		rechercherPatientConteneurGauche = new JButton("Rechercher un patient");
		conteneurGauche.add(rechercherPatientConteneurGauche);
	}
	
	private void setBouttonMesRDV() {
		mesRDVConteneurGauche = new JButton("Mes Rendez-vous");
		conteneurGauche.add(mesRDVConteneurGauche);
	}
	
	private void setBouttonEtatService() {
		etatServiceConteneurGauche = new JButton("�tat du service");
		conteneurGauche.add(etatServiceConteneurGauche);
	}
	
	private void setBouttonsAddSupprPersonnel() {
		JPanel conteneurGaucheSuperAdminLigne1 = new JPanel(new GridLayout(1,2,30,0));
		ajoutPersonnelConteneurGauche = new JButton("Ajouter un employ�");
		suppressionPersonnelConteneurGauche = new JButton("Supprimer un employ�");
		conteneurGaucheSuperAdminLigne1.add(ajoutPersonnelConteneurGauche);
		conteneurGaucheSuperAdminLigne1.add(suppressionPersonnelConteneurGauche);
		conteneurGauche.add(conteneurGaucheSuperAdminLigne1);
	}
	
	private void setBouttonsAddSupprInstallation() {
		JPanel conteneurGaucheSuperAdminLigne2 = new JPanel(new GridLayout(1,2,30,0));
		ajoutInstallationConteneurGauche = new JButton("Ajouter une installation");
		suppressionInstallationConteneurGauche = new JButton("Supprimer une installation");
		conteneurGaucheSuperAdminLigne2.add(ajoutInstallationConteneurGauche);
		conteneurGaucheSuperAdminLigne2.add(suppressionInstallationConteneurGauche);
		conteneurGauche.add(conteneurGaucheSuperAdminLigne2);
	}
	
	private void setBouttonsAddSupprMateriel() {
		JPanel conteneurGaucheSuperAdminLigne3 = new JPanel(new GridLayout(1,2,30,0));
		ajoutMaterielConteneurGauche = new JButton("Nouvel appareil m�dical");
		suppressionMaterielConteneurGauche = new JButton("Suppr. appareil m�dical");
		conteneurGaucheSuperAdminLigne3.add(ajoutMaterielConteneurGauche);
		conteneurGaucheSuperAdminLigne3.add(suppressionMaterielConteneurGauche);
		conteneurGauche.add(conteneurGaucheSuperAdminLigne3);
	}
	
	private void setBouttonEtatPharmacie() {
		etatPharmacieConteneurGauche = new JButton("�tat de la pharmacie");
		conteneurGauche.add(etatPharmacieConteneurGauche);
	}
	
	private void setBouttonsAjouterRetirerArticle() {
		ajoutRetraitArticleConteneurGauche = new JButton("Ajouter ou retirer un article");
		conteneurGauche.add(ajoutRetraitArticleConteneurGauche);
	}
	
	private void setBouttonsCreerSupprimerArticle() {
		JPanel conteneurGauchePharmacienLigne1 = new JPanel(new GridLayout(1,2,30,0));
		creerArticleConteneurGauche = new JButton("Cr�er un nouvel article");
		supprimerArticleConteneurGauche = new JButton("Supprimer un article");
		conteneurGauchePharmacienLigne1.add(creerArticleConteneurGauche);
		conteneurGauchePharmacienLigne1.add(supprimerArticleConteneurGauche);
		conteneurGauche.add(conteneurGauchePharmacienLigne1);
	}
	
	private void setBouttonLogout() {
		logoutConteneurGauche = new JButton("Logout");
		conteneurGauche.add(logoutConteneurGauche);
	}
	
	// Construction partie droite des interfaces ---------------------------------------------------------------------
	
	private void setPartieDroiteSuperAdmin() {
		conteneurDroite.setLayout(layoutMultiPanelDroite);
		conteneurDroite.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		
		conteneurDroiteVide = new JPanel();
		conteneurDroite.add(conteneurDroiteVide,"panelVide");
		
		this.setPanelDroitAjoutPersonnel();
		conteneurDroite.add(conteneurDroiteAjoutPersonnel,"panelAjoutPersonnel");
		
		this.setPanelDroitSuppressionPersonnel();
		conteneurDroite.add(conteneurDroiteSuppressionPersonnel,"panelSuppressionPersonnel");
		
		this.setPanelDroitAjoutInstallations();
		conteneurDroite.add(conteneurDroiteAjoutInstallations,"panelAjoutInstallations");
		
		this.setPanelDroitSuppressionInstallations();
		conteneurDroite.add(conteneurDroiteSuppressionInstallations,"panelSuppressionInstallations");
		
		this.setPanelDroitAjoutAppareilMedical();
		conteneurDroite.add(conteneurDroiteAjoutAppareilMedical,"panelAjoutAppareilMedical");
		
		this.setPanelDroitSupprimerAppareilMedical();
		conteneurDroite.add(conteneurDroiteSuppressionAppareilMedical,"panelSuppressionAppareilMedical");
		
		conteneurOngletAccueil.add(conteneurDroite);
	}
	
	private void setPartieDroiteAgentAdministrateur() {
		conteneurDroite.setLayout(layoutMultiPanelDroite);
		conteneurDroite.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		
		conteneurDroiteVide = new JPanel();
		conteneurDroite.add(conteneurDroiteVide,"panelVide");
		
		this.setPanelDroitCreer();
		conteneurDroite.add(conteneurDroiteCreerPatient,"panelCreer");
		
		this.setPanelDroitRechercher();
		conteneurDroite.add(conteneurDroiteRechercherPatient,"panelRechercher");	 
		
		conteneurOngletAccueil.add(conteneurDroite);
	}
	
	private void setPartieDroiteTechnicien() {
		conteneurDroite.setLayout(layoutMultiPanelDroite);
		conteneurDroite.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		
		conteneurDroiteVide = new JPanel();
		conteneurDroite.add(conteneurDroiteVide,"panelVide");
		
		this.setPanelDroitRechercher();
		conteneurDroite.add(conteneurDroiteRechercherPatient,"panelRechercher");	 
		
		conteneurOngletAccueil.add(conteneurDroite);
	}
	
	private void setPartieDroiteRH() {

	}
	
	private void setPartieDroiteMedecin() {
		conteneurDroite.setLayout(layoutMultiPanelDroite);
		conteneurDroite.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		
		conteneurDroiteVide = new JPanel();
		conteneurDroite.add(conteneurDroiteVide,"panelVide");
		
		this.setPanelDroitCreer();
		conteneurDroite.add(conteneurDroiteCreerPatient,"panelCreer");
		
		this.setPanelDroitRechercher();
		conteneurDroite.add(conteneurDroiteRechercherPatient,"panelRechercher");
		
		this.setPanelDroitMesRDV();
		conteneurDroite.add(conteneurDroiteMesRDV,"panelMesRDV");
		
		conteneurOngletAccueil.add(conteneurDroite);
	}
	
	private void setPartieDroiteInfirmier() {
		conteneurDroite.setLayout(layoutMultiPanelDroite);
		conteneurDroite.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		
		conteneurDroiteVide = new JPanel();
		conteneurDroite.add(conteneurDroiteVide,"panelVide");
		
		this.setPanelDroitRechercher();
		conteneurDroite.add(conteneurDroiteRechercherPatient,"panelRechercher");
		
		this.setPanelDroitEtatService();
		conteneurDroite.add(conteneurDroiteEtatService,"panelEtatService");
		
		conteneurOngletAccueil.add(conteneurDroite);
	}
	
	private void setPartieDroitePharmacien() {
		conteneurDroite.setLayout(layoutMultiPanelDroite);
		conteneurDroite.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		
		conteneurDroiteVide = new JPanel();
		conteneurDroite.add(conteneurDroiteVide,"panelVide");
		
		this.setPanelDroitEtatPharmacie();
		conteneurDroite.add(conteneurDroiteEtatPharmacie,"panelEtatPharmacie");
		
		this.setPanelDroitRechercher();
		conteneurDroite.add(conteneurDroiteRechercherPatient,"panelRechercher");
		
		this.setPanelDroitAjoutRetraitArticle();
		conteneurDroite.add(conteneurDroiteAjoutRetraitArticle,"panelAjoutRetraitArticle");
		
		this.setPanelDroitCreerArticle();
		conteneurDroite.add(conteneurDroiteCreerArticle,"panelCreerArticle");
		
		this.setPanelDroitSuppressionArticle();
		conteneurDroite.add(conteneurDroiteSuppressionArticle,"panelSuppressionArticle");
		
		conteneurOngletAccueil.add(conteneurDroite);
	}
	
	// Construction des Panel Droits � ajouter dans le cardLayout ------------------------------------------------------------------------------------------------------------------
	
	private void setPanelDroitCreer() {
		conteneurDroiteCreerPatient = new JPanel(new GridLayout(9,1));
	
		JPanel conteneurDroiteCreerLigne1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel nom = new JLabel("Nom :");
		JSeparator separateurCreerLigne1 = new JSeparator(); 
		separateurCreerLigne1.setPreferredSize(new Dimension(29,0));
		nomFormulaireCreerPatientTextField = new JTextField();
		nomFormulaireCreerPatientTextField.setPreferredSize(new Dimension(200,20));
		conteneurDroiteCreerLigne1.add(nom);
		conteneurDroiteCreerLigne1.add(separateurCreerLigne1);
		conteneurDroiteCreerLigne1.add(nomFormulaireCreerPatientTextField);
		
		JPanel conteneurDroiteCreerLigne2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel prenom = new JLabel("Prenom :");
		JSeparator separateurCreerLigne2 = new JSeparator(); 
		separateurCreerLigne2.setPreferredSize(new Dimension(10,0));
		prenomFormulaireCreerPatientTextField = new JTextField();
		prenomFormulaireCreerPatientTextField.setPreferredSize(new Dimension(200,20));
		conteneurDroiteCreerLigne2.add(prenom);
		conteneurDroiteCreerLigne2.add(separateurCreerLigne2);
		conteneurDroiteCreerLigne2.add(prenomFormulaireCreerPatientTextField);
		
		JPanel conteneurDroiteCreerLigne3 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		selectionSexeFormulaireCreerPatientButtonGroup = new ButtonGroup();
		JLabel sexe = new JLabel("Sexe :");
		JRadioButton masculin = new JRadioButton();
		masculin.setSelected(true);
		masculin.setActionCommand("masculin");
		JLabel masculinText = new JLabel("Masculin");
		JRadioButton feminin = new JRadioButton();
		feminin.setActionCommand("feminin");
		JLabel femininText = new JLabel("Feminin");
		selectionSexeFormulaireCreerPatientButtonGroup.add(masculin);
		selectionSexeFormulaireCreerPatientButtonGroup.add(feminin);
		conteneurDroiteCreerLigne3.add(sexe);
		conteneurDroiteCreerLigne3.add(masculin);
		conteneurDroiteCreerLigne3.add(masculinText);
		conteneurDroiteCreerLigne3.add(feminin);
		conteneurDroiteCreerLigne3.add(femininText);
		
		JPanel conteneurDroiteCreerLigne4 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel dateDeNaissance = new JLabel("Date de Naissance :");
		dateDeNaissanceFormulaireCreerPatientTextField = new JTextField();
		dateDeNaissanceFormulaireCreerPatientTextField.setPreferredSize(new Dimension(150,20));
		JLabel datePlaceholder = new JLabel("(jj/mm/aaaa)");
		conteneurDroiteCreerLigne4.add(dateDeNaissance);
		conteneurDroiteCreerLigne4.add(dateDeNaissanceFormulaireCreerPatientTextField);
		conteneurDroiteCreerLigne4.add(datePlaceholder);
		
		JPanel conteneurDroiteCreerLigne5 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel adresse = new JLabel("Adresse :");
		adresseFormulaireCreerPatientTextArea = new JTextArea();
		adresseFormulaireCreerPatientTextArea.setPreferredSize(new Dimension(489,50));
		conteneurDroiteCreerLigne5.add(adresse);
		conteneurDroiteCreerLigne5.add(adresseFormulaireCreerPatientTextArea);
		
		JPanel conteneurDroiteCreerLigne6 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel mail = new JLabel("E-Mail :");
		JSeparator separateurCreerLigne6 = new JSeparator(); 
		separateurCreerLigne6.setPreferredSize(new Dimension(19,0));
		mailFormulaireCreerPatientTextField = new JTextField();
		mailFormulaireCreerPatientTextField.setPreferredSize(new Dimension(200,20));
		conteneurDroiteCreerLigne6.add(mail);
		conteneurDroiteCreerLigne6.add(separateurCreerLigne6);
		conteneurDroiteCreerLigne6.add(mailFormulaireCreerPatientTextField);
		
		JPanel conteneurDroiteCreerLigne7 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel numeroTelephone = new JLabel("Telephone :");
		telephoneFormulaireCreerPatientTextField = new JTextField();
		telephoneFormulaireCreerPatientTextField.setPreferredSize(new Dimension(200,20));
		conteneurDroiteCreerLigne7.add(numeroTelephone);
		conteneurDroiteCreerLigne7.add(telephoneFormulaireCreerPatientTextField);
		
		JPanel conteneurDroiteCreerLigne8 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel pathologies = new JLabel("Pathologies :");
		pathologiesFormulaireCreerPatientTextArea = new JTextArea();
		pathologiesFormulaireCreerPatientTextArea.setPreferredSize(new Dimension(489,50));
		conteneurDroiteCreerLigne8.add(pathologies);
		conteneurDroiteCreerLigne8.add(pathologiesFormulaireCreerPatientTextArea);
		
		JPanel conteneurDroiteCreerLigne9 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		validerFormulaireCreerPatient = new JButton("Valider");
		JSeparator separateurBouttonFormulaire = new JSeparator();
		separateurBouttonFormulaire.setPreferredSize(new Dimension(50,0));
		effacerFormulaireCreerPatient = new JButton("Effacer");
		conteneurDroiteCreerLigne9.add(validerFormulaireCreerPatient);
		conteneurDroiteCreerLigne9.add(separateurBouttonFormulaire);
		conteneurDroiteCreerLigne9.add(effacerFormulaireCreerPatient);
		
		conteneurDroiteCreerPatient.add(conteneurDroiteCreerLigne1);
		conteneurDroiteCreerPatient.add(conteneurDroiteCreerLigne2);
		conteneurDroiteCreerPatient.add(conteneurDroiteCreerLigne3);
		conteneurDroiteCreerPatient.add(conteneurDroiteCreerLigne4);
		conteneurDroiteCreerPatient.add(conteneurDroiteCreerLigne5);
		conteneurDroiteCreerPatient.add(conteneurDroiteCreerLigne6);
		conteneurDroiteCreerPatient.add(conteneurDroiteCreerLigne7);
		conteneurDroiteCreerPatient.add(conteneurDroiteCreerLigne8);
		conteneurDroiteCreerPatient.add(conteneurDroiteCreerLigne9);
		
	}
	
	private void setPanelDroitRechercher() {
		
		conteneurDroiteRechercherPatient = new JPanel(new GridLayout(2,1));
		JPanel conteneurDroiteRechercherHaut = new JPanel(new GridLayout(5,1));

		JPanel conteneurDroiteRechercherHautLigne1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel methodeRechercheTexte = new JLabel("M�thode de recherche :");
		conteneurDroiteRechercherHautLigne1.add(methodeRechercheTexte);
		
		selectionMethodeRechercheFormulaireRechercherPatientButtonGroup = new ButtonGroup();
		
		JPanel conteneurDroiteRechercherHautLigne2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		parIdentifiantFormulaireRechercherPatientRadio = new JRadioButton();
		parIdentifiantFormulaireRechercherPatientRadio.setActionCommand("identifiant");
		parIdentifiantFormulaireRechercherPatientRadio.setSelected(true);
		JLabel methodeRechercheIdentifiant = new JLabel("N�Identifiant :");
		identifiantFormulaireRechercherPatientTextField = new JTextField();
		identifiantFormulaireRechercherPatientTextField.setPreferredSize(new Dimension(105,20));
		conteneurDroiteRechercherHautLigne2.add(parIdentifiantFormulaireRechercherPatientRadio);
		conteneurDroiteRechercherHautLigne2.add(methodeRechercheIdentifiant);
		conteneurDroiteRechercherHautLigne2.add(identifiantFormulaireRechercherPatientTextField);
		
		JPanel conteneurDroiteRechercherHautLigne3 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		parNomFormulaireRechercherPatientRadio = new JRadioButton();
		parNomFormulaireRechercherPatientRadio.setActionCommand("nom");
		JLabel methodeRechercheNom = new JLabel("Nom :");
		nomFormulaireRechercherPatientTextField = new JTextField();
		nomFormulaireRechercherPatientTextField.setPreferredSize(new Dimension(105,20));
		nomFormulaireRechercherPatientTextField.setEnabled(false);
		conteneurDroiteRechercherHautLigne3.add(parNomFormulaireRechercherPatientRadio);
		conteneurDroiteRechercherHautLigne3.add(methodeRechercheNom);
		conteneurDroiteRechercherHautLigne3.add(nomFormulaireRechercherPatientTextField);
		
		JPanel conteneurDroiteRechercherHautLigne4 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		parDateDeNaissanceFormulaireRechercherPatientRadio = new JRadioButton();
		parDateDeNaissanceFormulaireRechercherPatientRadio.setActionCommand("dateDeNaissance");
		JLabel methodeRechercheDate = new JLabel("Date de naissance :");
		dateDeNaissanceFormulaireRechercherPatientTextField = new JTextField();
		dateDeNaissanceFormulaireRechercherPatientTextField.setPreferredSize(new Dimension(105,20));
		dateDeNaissanceFormulaireRechercherPatientTextField.setEnabled(false);
		conteneurDroiteRechercherHautLigne4.add(parDateDeNaissanceFormulaireRechercherPatientRadio);
		conteneurDroiteRechercherHautLigne4.add(methodeRechercheDate);
		conteneurDroiteRechercherHautLigne4.add(dateDeNaissanceFormulaireRechercherPatientTextField);
		
		JPanel conteneurDroiteRechercherHautLigne5 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		validerFormulaireRechercherPatient = new JButton("Valider");
		conteneurDroiteRechercherHautLigne5.add(validerFormulaireRechercherPatient);

		selectionMethodeRechercheFormulaireRechercherPatientButtonGroup.add(parIdentifiantFormulaireRechercherPatientRadio);
		selectionMethodeRechercheFormulaireRechercherPatientButtonGroup.add(parNomFormulaireRechercherPatientRadio);
		selectionMethodeRechercheFormulaireRechercherPatientButtonGroup.add(parDateDeNaissanceFormulaireRechercherPatientRadio);
		
		conteneurDroiteRechercherHaut.add(conteneurDroiteRechercherHautLigne1);
		conteneurDroiteRechercherHaut.add(conteneurDroiteRechercherHautLigne2);
		conteneurDroiteRechercherHaut.add(conteneurDroiteRechercherHautLigne3);
		conteneurDroiteRechercherHaut.add(conteneurDroiteRechercherHautLigne4);
		conteneurDroiteRechercherHaut.add(conteneurDroiteRechercherHautLigne5);
		
		
		conteneurDroiteRechercherPatientBas = new JPanel();
		//JPanel conteneurDroiteRechercherBas = new JPanel(new GridLayout(1,1,40,40));
		layoutMultiPanelconteneurDroiteRechercherPatientBas = new CardLayout();
		conteneurDroiteRechercherPatientBas.setLayout(layoutMultiPanelconteneurDroiteRechercherPatientBas);
		
		JPanel conteneurDroiteRechercherBasVide = new JPanel();
		conteneurDroiteRechercherPatientBas.add(conteneurDroiteRechercherBasVide,"panelVide");
		
		JScrollPane conteneurAscenseurRechercherBas = new JScrollPane();
		retourFormulaireRechercherPatient = new JTable();
		retourFormulaireRechercherPatient.setModel(new RechercherPatientTableModel(0));
		conteneurAscenseurRechercherBas.getViewport().add(retourFormulaireRechercherPatient);
		conteneurDroiteRechercherPatientBas.add(conteneurAscenseurRechercherBas,"panelRecherche");	
		
		conteneurDroiteRechercherPatient.add(conteneurDroiteRechercherHaut);
		conteneurDroiteRechercherPatient.add(conteneurDroiteRechercherPatientBas);
	}
	
	private void setPanelDroitEtatService(){
		conteneurDroiteEtatService = new JPanel(new GridLayout(11,1));
		//Ligne0
		JPanel conteneurDroiteEtatServiceLigne0 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel etatServiceTexte = new JLabel("�tat du service XXX : ");
		conteneurDroiteEtatServiceLigne0.add(etatServiceTexte);
		
		//Ligne1
		JPanel conteneurDroiteEtatServiceLigne1 = new JPanel(new GridLayout(1,4));
		conteneurDroiteEtatServiceLigne1Cellule1 = new ConteneurGraphique1Lit();
		conteneurDroiteEtatServiceLigne1Cellule1.setBorder(BorderFactory.createTitledBorder("Chambre 1"));
		conteneurDroiteEtatServiceLigne1Cellule2 = new ConteneurGraphique1Lit();
		conteneurDroiteEtatServiceLigne1Cellule2.setBorder(BorderFactory.createTitledBorder("Chambre 2"));
		conteneurDroiteEtatServiceLigne1Cellule3 = new ConteneurGraphique1Lit();
		conteneurDroiteEtatServiceLigne1Cellule3.setBorder(BorderFactory.createTitledBorder("Chambre 3"));
		conteneurDroiteEtatServiceLigne1Cellule4 = new ConteneurGraphique1Lit();
		conteneurDroiteEtatServiceLigne1Cellule4.setBorder(BorderFactory.createTitledBorder("Chambre 4"));
		conteneurDroiteEtatServiceLigne1.add(conteneurDroiteEtatServiceLigne1Cellule1);
		conteneurDroiteEtatServiceLigne1.add(conteneurDroiteEtatServiceLigne1Cellule2);
		conteneurDroiteEtatServiceLigne1.add(conteneurDroiteEtatServiceLigne1Cellule3);
		conteneurDroiteEtatServiceLigne1.add(conteneurDroiteEtatServiceLigne1Cellule4);
		
		//Ligne2
		JPanel conteneurDroiteEtatServiceLigne2 = new JPanel(new GridLayout(1,4));
		conteneurDroiteEtatServiceLigne2Cellule1 = new ConteneurGraphique1Lit();
		conteneurDroiteEtatServiceLigne2Cellule1.setBorder(BorderFactory.createTitledBorder("Chambre 5"));
		conteneurDroiteEtatServiceLigne2Cellule2 = new ConteneurGraphique1Lit();
		conteneurDroiteEtatServiceLigne2Cellule2.setBorder(BorderFactory.createTitledBorder("Chambre 6"));
		conteneurDroiteEtatServiceLigne2Cellule3 = new ConteneurGraphique1Lit();
		conteneurDroiteEtatServiceLigne2Cellule3.setBorder(BorderFactory.createTitledBorder("Chambre 7"));
		conteneurDroiteEtatServiceLigne2Cellule4 = new ConteneurGraphique1Lit();
		conteneurDroiteEtatServiceLigne2Cellule4.setBorder(BorderFactory.createTitledBorder("Chambre 8"));
		conteneurDroiteEtatServiceLigne2.add(conteneurDroiteEtatServiceLigne2Cellule1);
		conteneurDroiteEtatServiceLigne2.add(conteneurDroiteEtatServiceLigne2Cellule2);
		conteneurDroiteEtatServiceLigne2.add(conteneurDroiteEtatServiceLigne2Cellule3);
		conteneurDroiteEtatServiceLigne2.add(conteneurDroiteEtatServiceLigne2Cellule4);
		
		//Ligne3
		JPanel conteneurDroiteEtatServiceLigne3 = new JPanel(new GridLayout(1,2));
		conteneurDroiteEtatServiceLigne3Cellule1 = new ConteneurGraphique2Lit();
		conteneurDroiteEtatServiceLigne3Cellule1.setBorder(BorderFactory.createTitledBorder("Chambre 9"));
		conteneurDroiteEtatServiceLigne3Cellule2 = new ConteneurGraphique2Lit();
		conteneurDroiteEtatServiceLigne3Cellule2.setBorder(BorderFactory.createTitledBorder("Chambre 10"));
		conteneurDroiteEtatServiceLigne3.add(conteneurDroiteEtatServiceLigne3Cellule1);
		conteneurDroiteEtatServiceLigne3.add(conteneurDroiteEtatServiceLigne3Cellule2);
		
		//Ligne4
		JPanel conteneurDroiteEtatServiceLigne4 = new JPanel(new GridLayout(1,2));
		conteneurDroiteEtatServiceLigne4Cellule1 = new ConteneurGraphique2Lit();
		conteneurDroiteEtatServiceLigne4Cellule1.setBorder(BorderFactory.createTitledBorder("Chambre 11"));
		conteneurDroiteEtatServiceLigne4Cellule2 = new ConteneurGraphique2Lit();
		conteneurDroiteEtatServiceLigne4Cellule2.setBorder(BorderFactory.createTitledBorder("Chambre 12"));
		conteneurDroiteEtatServiceLigne4.add(conteneurDroiteEtatServiceLigne4Cellule1);
		conteneurDroiteEtatServiceLigne4.add(conteneurDroiteEtatServiceLigne4Cellule2);
		
		//Ligne5
		JPanel conteneurDroiteEtatServiceLigne5 = new JPanel(new GridLayout(1,2));
		conteneurDroiteEtatServiceLigne5Cellule1 = new ConteneurGraphique2Lit();
		conteneurDroiteEtatServiceLigne5Cellule1.setBorder(BorderFactory.createTitledBorder("Chambre 13"));
		conteneurDroiteEtatServiceLigne5Cellule2 = new ConteneurGraphique2Lit();
		conteneurDroiteEtatServiceLigne5Cellule2.setBorder(BorderFactory.createTitledBorder("Chambre 14"));
		conteneurDroiteEtatServiceLigne5.add(conteneurDroiteEtatServiceLigne5Cellule1);
		conteneurDroiteEtatServiceLigne5.add(conteneurDroiteEtatServiceLigne5Cellule2);
		
		//Ligne6
		JPanel conteneurDroiteEtatServiceLigne6 = new JPanel(new GridLayout(1,2));
		conteneurDroiteEtatServiceLigne6Cellule1 = new ConteneurGraphique2Lit();
		conteneurDroiteEtatServiceLigne6Cellule1.setBorder(BorderFactory.createTitledBorder("Chambre 15"));
		conteneurDroiteEtatServiceLigne6Cellule2 = new ConteneurGraphique2Lit();
		conteneurDroiteEtatServiceLigne6Cellule2.setBorder(BorderFactory.createTitledBorder("Chambre 16"));
		conteneurDroiteEtatServiceLigne6.add(conteneurDroiteEtatServiceLigne6Cellule1);
		conteneurDroiteEtatServiceLigne6.add(conteneurDroiteEtatServiceLigne6Cellule2);
		
		//Ligne7
		conteneurDroiteEtatServiceLigne7 = new ConteneurGraphique4Lit();
		conteneurDroiteEtatServiceLigne7.setBorder(BorderFactory.createTitledBorder("Chambre 17"));
		
		//Ligne8
		conteneurDroiteEtatServiceLigne8 = new ConteneurGraphique4Lit();
		conteneurDroiteEtatServiceLigne8.setBorder(BorderFactory.createTitledBorder("Chambre 18"));
		
		//Ligne9
		conteneurDroiteEtatServiceLigne9 = new ConteneurGraphique4Lit();
		conteneurDroiteEtatServiceLigne9.setBorder(BorderFactory.createTitledBorder("Chambre 19"));
		
		//Ligne10
		conteneurDroiteEtatServiceLigne10 = new ConteneurGraphique4Lit();
		conteneurDroiteEtatServiceLigne10.setBorder(BorderFactory.createTitledBorder("Chambre 20"));

		conteneurDroiteEtatService.add(conteneurDroiteEtatServiceLigne0);
		conteneurDroiteEtatService.add(conteneurDroiteEtatServiceLigne1);
		conteneurDroiteEtatService.add(conteneurDroiteEtatServiceLigne2);
		conteneurDroiteEtatService.add(conteneurDroiteEtatServiceLigne3);
		conteneurDroiteEtatService.add(conteneurDroiteEtatServiceLigne4);
		conteneurDroiteEtatService.add(conteneurDroiteEtatServiceLigne5);
		conteneurDroiteEtatService.add(conteneurDroiteEtatServiceLigne6);
		conteneurDroiteEtatService.add(conteneurDroiteEtatServiceLigne7);
		conteneurDroiteEtatService.add(conteneurDroiteEtatServiceLigne8);
		conteneurDroiteEtatService.add(conteneurDroiteEtatServiceLigne9);
		conteneurDroiteEtatService.add(conteneurDroiteEtatServiceLigne10);
	}
	
	private void setPanelDroitMesRDV() {
		conteneurDroiteMesRDV = new JPanel(new GridLayout(1,1));
		JScrollPane conteneurAscenseurMesRDV = new JScrollPane();
		mesRDVMedecin = new JTable(new MesRDVMedecinTableModel());
		mesRDVMedecin.setRowHeight(48);
		conteneurAscenseurMesRDV.getViewport().add(mesRDVMedecin);
		conteneurDroiteMesRDV.add(conteneurAscenseurMesRDV);
	}
	
	private void setPanelDroitAjoutPersonnel() {
		conteneurDroiteAjoutPersonnel = new JPanel(new GridLayout(13,1));
		
		JPanel conteneurDroiteAjoutPersonnelLigne1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel nom = new JLabel("Nom :");
		JSeparator separateurAjoutPersonnelLigne1 = new JSeparator(); 
		separateurAjoutPersonnelLigne1.setPreferredSize(new Dimension(29,0));
		nomFormulaireAjouterPersonnelTextField = new JTextField();
		nomFormulaireAjouterPersonnelTextField.setPreferredSize(new Dimension(200,20));
		conteneurDroiteAjoutPersonnelLigne1.add(nom);
		conteneurDroiteAjoutPersonnelLigne1.add(separateurAjoutPersonnelLigne1);
		conteneurDroiteAjoutPersonnelLigne1.add(nomFormulaireAjouterPersonnelTextField);
		
		JPanel conteneurDroiteAjoutPersonnelLigne2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel prenom = new JLabel("Prenom :");
		JSeparator separateurAjoutPersonnelLigne2 = new JSeparator(); 
		separateurAjoutPersonnelLigne2.setPreferredSize(new Dimension(10,0));
		prenomFormulaireAjouterPersonnelTextField = new JTextField();
		prenomFormulaireAjouterPersonnelTextField.setPreferredSize(new Dimension(200,20));
		conteneurDroiteAjoutPersonnelLigne2.add(prenom);
		conteneurDroiteAjoutPersonnelLigne2.add(separateurAjoutPersonnelLigne2);
		conteneurDroiteAjoutPersonnelLigne2.add(prenomFormulaireAjouterPersonnelTextField);
		
		JPanel conteneurDroiteAjoutPersonnelLigne3 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		selectionSexeFormulaireAjouterPersonnelButtonGroup = new ButtonGroup();
		JLabel sexe = new JLabel("Sexe :");
		JRadioButton masculin = new JRadioButton();
		masculin.setSelected(true);
		masculin.setActionCommand("masculin");
		JLabel masculinText = new JLabel("Masculin");
		JRadioButton feminin = new JRadioButton();
		feminin.setActionCommand("feminin");
		JLabel femininText = new JLabel("Feminin");
		selectionSexeFormulaireAjouterPersonnelButtonGroup.add(masculin);
		selectionSexeFormulaireAjouterPersonnelButtonGroup.add(feminin);
		conteneurDroiteAjoutPersonnelLigne3.add(sexe);
		conteneurDroiteAjoutPersonnelLigne3.add(masculin);
		conteneurDroiteAjoutPersonnelLigne3.add(masculinText);
		conteneurDroiteAjoutPersonnelLigne3.add(feminin);
		conteneurDroiteAjoutPersonnelLigne3.add(femininText);
		
		JPanel conteneurDroiteAjoutPersonnelLigne4 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel dateDeNaissance = new JLabel("Date de Naissance :");
		dateDeNaissanceFormulaireAjouterPersonnelTextField = new JTextField();
		dateDeNaissanceFormulaireAjouterPersonnelTextField.setPreferredSize(new Dimension(150,20));
		JLabel datePlaceholder = new JLabel("(jjmmaaaa)");
		conteneurDroiteAjoutPersonnelLigne4.add(dateDeNaissance);
		conteneurDroiteAjoutPersonnelLigne4.add(dateDeNaissanceFormulaireAjouterPersonnelTextField);
		conteneurDroiteAjoutPersonnelLigne4.add(datePlaceholder);
		
		JPanel conteneurDroiteAjoutPersonnelLigne5 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel adresse = new JLabel("Adresse :");
		adresseFormulaireAjouterPersonnelTextArea = new JTextArea();
		adresseFormulaireAjouterPersonnelTextArea.setPreferredSize(new Dimension(489,50));
		conteneurDroiteAjoutPersonnelLigne5.add(adresse);
		conteneurDroiteAjoutPersonnelLigne5.add(adresseFormulaireAjouterPersonnelTextArea);
		
		JPanel conteneurDroiteAjoutPersonnelLigne6 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel mail = new JLabel("E-Mail :");
		JSeparator separateurAjoutPersonnelLigne6 = new JSeparator(); 
		separateurAjoutPersonnelLigne6.setPreferredSize(new Dimension(19,0));
		mailFormulaireAjouterPersonnelTextField = new JTextField();
		mailFormulaireAjouterPersonnelTextField.setPreferredSize(new Dimension(200,20));
		conteneurDroiteAjoutPersonnelLigne6.add(mail);
		conteneurDroiteAjoutPersonnelLigne6.add(separateurAjoutPersonnelLigne6);
		conteneurDroiteAjoutPersonnelLigne6.add(mailFormulaireAjouterPersonnelTextField);
		
		JPanel conteneurDroiteAjoutPersonnelLigne7 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel numeroTelephone = new JLabel("Telephone :");
		telephoneFormulaireAjouterPersonnelTextField = new JTextField();
		telephoneFormulaireAjouterPersonnelTextField.setPreferredSize(new Dimension(200,20));
		conteneurDroiteAjoutPersonnelLigne7.add(numeroTelephone);
		conteneurDroiteAjoutPersonnelLigne7.add(telephoneFormulaireAjouterPersonnelTextField);
		
		JPanel conteneurDroiteAjoutPersonnelLigne8 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel profession = new JLabel("Profession :");
		selectionProfessionFormulaireAjouterPersonnelComboBox = new JComboBox<>(choixProfession);
		conteneurDroiteAjoutPersonnelLigne8.add(profession);
		conteneurDroiteAjoutPersonnelLigne8.add(selectionProfessionFormulaireAjouterPersonnelComboBox);
		
		JPanel conteneurDroiteAjoutPersonnelLigne9 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel rpps = new JLabel("N�RPPS :");
		rppsFormulaireAjouterPersonnelTextField = new JTextField();
		rppsFormulaireAjouterPersonnelTextField.setPreferredSize(new Dimension(200,20));
		rppsFormulaireAjouterPersonnelTextField.setEnabled(false);
		conteneurDroiteAjoutPersonnelLigne9.add(rpps);
		conteneurDroiteAjoutPersonnelLigne9.add(rppsFormulaireAjouterPersonnelTextField);
		
		JPanel conteneurDroiteAjoutPersonnelLigne10 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel specialite = new JLabel("Sp�cialit� :");
		selectionSpecialiteFormulaireAjouterPersonnelComboBox = new JComboBox<>(choixSpecialite);
		selectionSpecialiteFormulaireAjouterPersonnelComboBox.setEnabled(false);
		conteneurDroiteAjoutPersonnelLigne10.add(specialite);
		conteneurDroiteAjoutPersonnelLigne10.add(selectionSpecialiteFormulaireAjouterPersonnelComboBox);
		
		JPanel conteneurDroiteAjoutPersonnelLigne11 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel service = new JLabel("Service :");
		selectionServiceFormulaireAjouterPersonnelComboBox = new JComboBox<>(choixService);
		selectionServiceFormulaireAjouterPersonnelComboBox.setEnabled(false);
		conteneurDroiteAjoutPersonnelLigne11.add(service);
		conteneurDroiteAjoutPersonnelLigne11.add(selectionServiceFormulaireAjouterPersonnelComboBox);
		
		JPanel conteneurDroiteAjoutPersonnelLigne12 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel salaire = new JLabel("Salaire :");
		salaireFormulaireAjouterPersonnelTextField = new JTextField();
		salaireFormulaireAjouterPersonnelTextField.setPreferredSize(new Dimension(200,20));
		conteneurDroiteAjoutPersonnelLigne12.add(salaire);
		conteneurDroiteAjoutPersonnelLigne12.add(salaireFormulaireAjouterPersonnelTextField);
		
		// A modif
		JPanel conteneurDroiteAjoutPersonnelLigne13 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		validerFormulaireAjouterPersonnel = new JButton("Valider");
		JSeparator separateurBouttonFormulaire = new JSeparator();
		separateurBouttonFormulaire.setPreferredSize(new Dimension(50,0));
		effacerFormulaireAjoutPersonnel = new JButton("Effacer");
		conteneurDroiteAjoutPersonnelLigne13.add(validerFormulaireAjouterPersonnel);
		conteneurDroiteAjoutPersonnelLigne13.add(separateurBouttonFormulaire);
		conteneurDroiteAjoutPersonnelLigne13.add(effacerFormulaireAjoutPersonnel);
		
		conteneurDroiteAjoutPersonnel.add(conteneurDroiteAjoutPersonnelLigne1);
		conteneurDroiteAjoutPersonnel.add(conteneurDroiteAjoutPersonnelLigne2);
		conteneurDroiteAjoutPersonnel.add(conteneurDroiteAjoutPersonnelLigne3);
		conteneurDroiteAjoutPersonnel.add(conteneurDroiteAjoutPersonnelLigne4);
		conteneurDroiteAjoutPersonnel.add(conteneurDroiteAjoutPersonnelLigne5);
		conteneurDroiteAjoutPersonnel.add(conteneurDroiteAjoutPersonnelLigne6);
		conteneurDroiteAjoutPersonnel.add(conteneurDroiteAjoutPersonnelLigne7);
		conteneurDroiteAjoutPersonnel.add(conteneurDroiteAjoutPersonnelLigne8);
		conteneurDroiteAjoutPersonnel.add(conteneurDroiteAjoutPersonnelLigne9);
		conteneurDroiteAjoutPersonnel.add(conteneurDroiteAjoutPersonnelLigne10);
		conteneurDroiteAjoutPersonnel.add(conteneurDroiteAjoutPersonnelLigne11);
		conteneurDroiteAjoutPersonnel.add(conteneurDroiteAjoutPersonnelLigne12);
		conteneurDroiteAjoutPersonnel.add(conteneurDroiteAjoutPersonnelLigne13);
	}
	
	private void setPanelDroitSuppressionPersonnel() {
		conteneurDroiteSuppressionPersonnel = new JPanel(new BorderLayout());
		
		JPanel conteneurDroiteSuppressionPersonnelCenter = new JPanel(new GridLayout(3,1));
		
		JPanel conteneurDroiteSuppressionPersonnelLigne1 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		JLabel id = new JLabel("N�Identification :");
		conteneurDroiteSuppressionPersonnelLigne1.add(id);
		
		JPanel conteneurDroiteSuppressionPersonnelLigne2 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		idFormulaireSuppressionPersonnelTextField = new JTextField();
		idFormulaireSuppressionPersonnelTextField.setPreferredSize(new Dimension(100,20));
		conteneurDroiteSuppressionPersonnelLigne2.add(idFormulaireSuppressionPersonnelTextField);
		
		JPanel conteneurDroiteSuppressionPersonnelLigne3 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		supprimerFormulaireSuppressionPersonnel = new JButton("Supprimer");
		conteneurDroiteSuppressionPersonnelLigne3.add(supprimerFormulaireSuppressionPersonnel);
		
		conteneurDroiteSuppressionPersonnelCenter.add(conteneurDroiteSuppressionPersonnelLigne1);
		conteneurDroiteSuppressionPersonnelCenter.add(conteneurDroiteSuppressionPersonnelLigne2);
		conteneurDroiteSuppressionPersonnelCenter.add(conteneurDroiteSuppressionPersonnelLigne3);
		
		JPanel conteneurDroiteSuppressionPersonnelMargeNord = new JPanel();
		conteneurDroiteSuppressionPersonnelMargeNord.setPreferredSize(new Dimension(0, 250));
		JPanel conteneurDroiteSuppressionPersonnelMargeSud = new JPanel();
		conteneurDroiteSuppressionPersonnelMargeSud.setPreferredSize(new Dimension(0, 250));
		
		conteneurDroiteSuppressionPersonnel.add(conteneurDroiteSuppressionPersonnelMargeNord, BorderLayout.NORTH);
		conteneurDroiteSuppressionPersonnel.add(conteneurDroiteSuppressionPersonnelCenter, BorderLayout.CENTER);
		conteneurDroiteSuppressionPersonnel.add(conteneurDroiteSuppressionPersonnelMargeSud, BorderLayout.SOUTH);
	}
	
	private void setPanelDroitAjoutInstallations() {
		conteneurDroiteAjoutInstallations = new JPanel(new GridLayout(8,1));
		
		JPanel conteneurDroiteAjoutInstallationsLigne1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel codeService = new JLabel("Code du Service :");
		codeServiceFormulaireAjoutInstallationsTextField = new JTextField();
		codeServiceFormulaireAjoutInstallationsTextField.setPreferredSize(new Dimension(200,20));
		conteneurDroiteAjoutInstallationsLigne1.add(codeService);
		conteneurDroiteAjoutInstallationsLigne1.add(codeServiceFormulaireAjoutInstallationsTextField);
		
		JPanel conteneurDroiteAjoutInstallationsLigne2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel nomService = new JLabel("Nom du Service :");
		nomServiceFormulaireAjoutInstallationsTextField = new JTextField();
		nomServiceFormulaireAjoutInstallationsTextField.setPreferredSize(new Dimension(200,20));
		conteneurDroiteAjoutInstallationsLigne2.add(nomService);
		conteneurDroiteAjoutInstallationsLigne2.add(nomServiceFormulaireAjoutInstallationsTextField);
		
		etatServiceFormulaireAjoutInstallationsTextFieldButtonGroup = new ButtonGroup();
		
		JPanel conteneurDroiteAjoutInstallationsLigne3 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		serviceVideRadio = new JRadioButton();
		etatServiceFormulaireAjoutInstallationsTextFieldButtonGroup.add(serviceVideRadio);
		serviceVideRadio.setSelected(true);
		JLabel serviceVide = new JLabel("Vide");
		conteneurDroiteAjoutInstallationsLigne3.add(serviceVideRadio);
		conteneurDroiteAjoutInstallationsLigne3.add(serviceVide);
		
		JPanel conteneurDroiteAjoutInstallationsLigne4 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		serviceRempliRadio = new JRadioButton();
		etatServiceFormulaireAjoutInstallationsTextFieldButtonGroup.add(serviceRempliRadio);
		JLabel serviceRempli = new JLabel("Rempli");
		conteneurDroiteAjoutInstallationsLigne4.add(serviceRempliRadio);
		conteneurDroiteAjoutInstallationsLigne4.add(serviceRempli);
		
		JPanel conteneurDroiteAjoutInstallationsLigne5 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JSeparator alineaLigne5 = new JSeparator();
		alineaLigne5.setPreferredSize(new Dimension(50,0));
		chambreIndividuelleFormulaireAjoutInstallationsCheckBox = new JCheckBox();   
		chambreIndividuelleFormulaireAjoutInstallationsCheckBox.setEnabled(false);
		JLabel chambreIndividuelle = new JLabel("Chambre individuelle");
		chambreIndividuelleFormulaireAjoutInstallationsSpinner = new JSpinner();
		chambreIndividuelleFormulaireAjoutInstallationsSpinner.setPreferredSize(new Dimension(35,20));
		chambreIndividuelleFormulaireAjoutInstallationsSpinner.setEnabled(false);
		conteneurDroiteAjoutInstallationsLigne5.add(alineaLigne5);
		conteneurDroiteAjoutInstallationsLigne5.add(chambreIndividuelleFormulaireAjoutInstallationsCheckBox);
		conteneurDroiteAjoutInstallationsLigne5.add(chambreIndividuelle);
		conteneurDroiteAjoutInstallationsLigne5.add(chambreIndividuelleFormulaireAjoutInstallationsSpinner);
		
		JPanel conteneurDroiteAjoutInstallationsLigne6 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JSeparator alineaLigne6 = new JSeparator();
		alineaLigne6.setPreferredSize(new Dimension(50,0));
		chambreDoubleFormulaireAjoutInstallationsCheckBox = new JCheckBox();
		chambreDoubleFormulaireAjoutInstallationsCheckBox.setEnabled(false);
		JLabel chambreDouble = new JLabel("Chambre double");
		JSeparator espaceLigne6 = new JSeparator();
		espaceLigne6.setPreferredSize(new Dimension(19,0));
		chambreDoubleFormulaireAjoutInstallationsSpinner = new JSpinner();
		chambreDoubleFormulaireAjoutInstallationsSpinner.setPreferredSize(new Dimension(35,20));
		chambreDoubleFormulaireAjoutInstallationsSpinner.setEnabled(false);
		conteneurDroiteAjoutInstallationsLigne6.add(alineaLigne6);
		conteneurDroiteAjoutInstallationsLigne6.add(chambreDoubleFormulaireAjoutInstallationsCheckBox);
		conteneurDroiteAjoutInstallationsLigne6.add(chambreDouble);
		conteneurDroiteAjoutInstallationsLigne6.add(espaceLigne6);
		conteneurDroiteAjoutInstallationsLigne6.add(chambreDoubleFormulaireAjoutInstallationsSpinner);
		
		JPanel conteneurDroiteAjoutInstallationsLigne7 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JSeparator alineaLigne7 = new JSeparator();
		alineaLigne7.setPreferredSize(new Dimension(50,0));
		chambreQuadrupleFormulaireAjoutInstallationsCheckBox = new JCheckBox();
		chambreQuadrupleFormulaireAjoutInstallationsCheckBox.setEnabled(false);
		JLabel chambreQuadruple = new JLabel("Chambre quadruple");
		JSeparator espaceLigne7 = new JSeparator();
		espaceLigne7.setPreferredSize(new Dimension(2,0));
		chambreQuadrupleFormulaireAjoutInstallationsSpinner = new JSpinner();
		chambreQuadrupleFormulaireAjoutInstallationsSpinner.setPreferredSize(new Dimension(35,20));
		chambreQuadrupleFormulaireAjoutInstallationsSpinner.setEnabled(false);
		conteneurDroiteAjoutInstallationsLigne7.add(alineaLigne7);
		conteneurDroiteAjoutInstallationsLigne7.add(chambreQuadrupleFormulaireAjoutInstallationsCheckBox);
		conteneurDroiteAjoutInstallationsLigne7.add(chambreQuadruple);
		conteneurDroiteAjoutInstallationsLigne7.add(espaceLigne7);
		conteneurDroiteAjoutInstallationsLigne7.add(chambreQuadrupleFormulaireAjoutInstallationsSpinner);
		
		JPanel conteneurDroiteAjoutInstallationsLigne8 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		ajouterFormulaireAjoutInstallations = new JButton("Ajouter");
		conteneurDroiteAjoutInstallationsLigne8.add(ajouterFormulaireAjoutInstallations);
		
		conteneurDroiteAjoutInstallations.add(conteneurDroiteAjoutInstallationsLigne1);
		conteneurDroiteAjoutInstallations.add(conteneurDroiteAjoutInstallationsLigne2);
		conteneurDroiteAjoutInstallations.add(conteneurDroiteAjoutInstallationsLigne3);
		conteneurDroiteAjoutInstallations.add(conteneurDroiteAjoutInstallationsLigne4);
		conteneurDroiteAjoutInstallations.add(conteneurDroiteAjoutInstallationsLigne5);
		conteneurDroiteAjoutInstallations.add(conteneurDroiteAjoutInstallationsLigne6);
		conteneurDroiteAjoutInstallations.add(conteneurDroiteAjoutInstallationsLigne7);
		conteneurDroiteAjoutInstallations.add(conteneurDroiteAjoutInstallationsLigne8);
	}
	
	private void setPanelDroitSuppressionInstallations() {
		conteneurDroiteSuppressionInstallations = new JPanel(new BorderLayout());
		choixTypeInstallationFormulaireSuppressionInstallations = new ButtonGroup();

		JPanel conteneurDroiteSuppressionInstallationsCenter = new JPanel(new GridLayout(8,1));
		
		JPanel conteneurDroiteSuppressionInstallationsLigne1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel suppressionLabel = new JLabel("Supprimer :");
		conteneurDroiteSuppressionInstallationsLigne1.add(suppressionLabel);
		
		JPanel conteneurDroiteSuppressionInstallationsLigne2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		choixServiceFormulaireSuppressionInstallations = new JRadioButton();
		choixServiceFormulaireSuppressionInstallations.setActionCommand("service"); 
		choixTypeInstallationFormulaireSuppressionInstallations.add(choixServiceFormulaireSuppressionInstallations);
		choixServiceFormulaireSuppressionInstallations.setSelected(true);
		JLabel choixServiceLabel = new JLabel("Service");
		conteneurDroiteSuppressionInstallationsLigne2.add(choixServiceFormulaireSuppressionInstallations);
		conteneurDroiteSuppressionInstallationsLigne2.add(choixServiceLabel);
		
		JPanel conteneurDroiteSuppressionInstallationsLigne3 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JSeparator alineaLigne3 = new JSeparator();
		alineaLigne3.setPreferredSize(new Dimension(50,0));
		JLabel codeServiceLabel = new JLabel("Code Service :");
		JSeparator serviceSeparator = new JSeparator();
		serviceSeparator.setPreferredSize(new Dimension(4,0));
		codeServiceFormulaireSuppressionInstallations = new JTextField();
		codeServiceFormulaireSuppressionInstallations.setPreferredSize(new Dimension(37,20));
		conteneurDroiteSuppressionInstallationsLigne3.add(alineaLigne3);
		conteneurDroiteSuppressionInstallationsLigne3.add(codeServiceLabel);
		conteneurDroiteSuppressionInstallationsLigne3.add(serviceSeparator);
		conteneurDroiteSuppressionInstallationsLigne3.add(codeServiceFormulaireSuppressionInstallations);
		
		JPanel conteneurDroiteSuppressionInstallationsLigne4 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		choixChambreFormulaireSuppressionInstallations = new JRadioButton();
		choixChambreFormulaireSuppressionInstallations.setActionCommand("chambre"); 
		choixTypeInstallationFormulaireSuppressionInstallations.add(choixChambreFormulaireSuppressionInstallations);
		JLabel choixChambreLabel = new JLabel("Chambre");
		conteneurDroiteSuppressionInstallationsLigne4.add(choixChambreFormulaireSuppressionInstallations);
		conteneurDroiteSuppressionInstallationsLigne4.add(choixChambreLabel);
		
		JPanel conteneurDroiteSuppressionInstallationsLigne5 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JSeparator alineaLigne5 = new JSeparator();
		alineaLigne5.setPreferredSize(new Dimension(50,0));
		JLabel codeChambreLabel = new JLabel("Code Chambre :");
		codeChambreFormulaireSuppressionInstallations = new JTextField();
		codeChambreFormulaireSuppressionInstallations.setPreferredSize(new Dimension(37,20));
		codeChambreFormulaireSuppressionInstallations.setEnabled(false);
		conteneurDroiteSuppressionInstallationsLigne5.add(alineaLigne5);
		conteneurDroiteSuppressionInstallationsLigne5.add(codeChambreLabel);
		conteneurDroiteSuppressionInstallationsLigne5.add(codeChambreFormulaireSuppressionInstallations);
		
		JPanel conteneurDroiteSuppressionInstallationsLigne6 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		choixLitFormulaireSuppressionInstallations = new JRadioButton();
		choixLitFormulaireSuppressionInstallations.setActionCommand("lit");
		choixTypeInstallationFormulaireSuppressionInstallations.add(choixLitFormulaireSuppressionInstallations);
		JLabel choixLitLabel = new JLabel("Lit");
		conteneurDroiteSuppressionInstallationsLigne6.add(choixLitFormulaireSuppressionInstallations);
		conteneurDroiteSuppressionInstallationsLigne6.add(choixLitLabel);
		
		JPanel conteneurDroiteSuppressionInstallationsLigne7 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JSeparator alineaLigne7 = new JSeparator();
		alineaLigne7.setPreferredSize(new Dimension(50,0));
		JLabel codeLitLabel = new JLabel("Code Lit :");
		JSeparator litSeparator = new JSeparator();
		litSeparator.setPreferredSize(new Dimension(33,0));
		codeLitFormulaireSuppressionInstallations = new JTextField();
		codeLitFormulaireSuppressionInstallations.setPreferredSize(new Dimension(37,20));
		codeLitFormulaireSuppressionInstallations.setEnabled(false);
		conteneurDroiteSuppressionInstallationsLigne7.add(alineaLigne7);
		conteneurDroiteSuppressionInstallationsLigne7.add(codeLitLabel);
		conteneurDroiteSuppressionInstallationsLigne7.add(litSeparator);
		conteneurDroiteSuppressionInstallationsLigne7.add(codeLitFormulaireSuppressionInstallations);
		
		JPanel conteneurDroiteSuppressionInstallationsLigne8 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		supprimerFormulaireSuppressionInstallations = new JButton("Supprimer");
		conteneurDroiteSuppressionInstallationsLigne8.add(supprimerFormulaireSuppressionInstallations);
		
		conteneurDroiteSuppressionInstallationsCenter.add(conteneurDroiteSuppressionInstallationsLigne1);
		conteneurDroiteSuppressionInstallationsCenter.add(conteneurDroiteSuppressionInstallationsLigne2);
		conteneurDroiteSuppressionInstallationsCenter.add(conteneurDroiteSuppressionInstallationsLigne3);
		conteneurDroiteSuppressionInstallationsCenter.add(conteneurDroiteSuppressionInstallationsLigne4);
		conteneurDroiteSuppressionInstallationsCenter.add(conteneurDroiteSuppressionInstallationsLigne5);
		conteneurDroiteSuppressionInstallationsCenter.add(conteneurDroiteSuppressionInstallationsLigne6);
		conteneurDroiteSuppressionInstallationsCenter.add(conteneurDroiteSuppressionInstallationsLigne7);
		conteneurDroiteSuppressionInstallationsCenter.add(conteneurDroiteSuppressionInstallationsLigne8);
		
		JPanel margeNord = new JPanel();
		margeNord.setPreferredSize(new Dimension(0,150));
		JPanel margeSud = new JPanel();
		margeSud.setPreferredSize(new Dimension(0,150));
		
		conteneurDroiteSuppressionInstallations.add(margeNord, BorderLayout.NORTH);
		conteneurDroiteSuppressionInstallations.add(conteneurDroiteSuppressionInstallationsCenter, BorderLayout.CENTER);
		conteneurDroiteSuppressionInstallations.add(margeSud, BorderLayout.SOUTH);
	}
	
	private void setPanelDroitAjoutAppareilMedical() {
		conteneurDroiteAjoutAppareilMedical = new JPanel(new BorderLayout());
		
		JPanel conteneurCentre = new JPanel(new GridLayout(5,1));
		
		JPanel conteneurCentreligne1 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		JLabel codeAppareil = new JLabel("Code appareil :");
		conteneurCentreligne1.add(codeAppareil);
		
		JPanel conteneurCentreligne2 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		codeAppareilFormulaireAjoutAppareilMedicalTextField = new JTextField();
		codeAppareilFormulaireAjoutAppareilMedicalTextField.setPreferredSize(new Dimension(75,20));
		conteneurCentreligne2.add(codeAppareilFormulaireAjoutAppareilMedicalTextField);
		
		JPanel conteneurCentreligne3 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		JLabel typeAppareil = new JLabel("Type d'appareil :");
		conteneurCentreligne3.add(typeAppareil);
		
		JPanel conteneurCentreligne4 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		typeAppareilFormulaireAjoutAppareilMedicalTextField = new JTextField();
		typeAppareilFormulaireAjoutAppareilMedicalTextField.setPreferredSize(new Dimension(200,20));
		conteneurCentreligne4.add(typeAppareilFormulaireAjoutAppareilMedicalTextField);
		
		JPanel conteneurCentreligne5 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		ajouterAppareilFormulaireAjoutAppareilMedical = new JButton("Ajouter");
		conteneurCentreligne5.add(ajouterAppareilFormulaireAjoutAppareilMedical);
		
		conteneurCentre.add(conteneurCentreligne1);
		conteneurCentre.add(conteneurCentreligne2);
		conteneurCentre.add(conteneurCentreligne3);
		conteneurCentre.add(conteneurCentreligne4);
		conteneurCentre.add(conteneurCentreligne5);
		
		JPanel margeHaute = new JPanel();
		margeHaute.setPreferredSize(new Dimension(0,200));
		JPanel margeBasse = new JPanel();
		margeBasse.setPreferredSize(new Dimension(0,200));
		conteneurDroiteAjoutAppareilMedical.add(margeHaute, BorderLayout.NORTH);
		conteneurDroiteAjoutAppareilMedical.add(conteneurCentre, BorderLayout.CENTER);
		conteneurDroiteAjoutAppareilMedical.add(margeBasse, BorderLayout.SOUTH);
	}
	
	private void setPanelDroitSupprimerAppareilMedical() {
		conteneurDroiteSuppressionAppareilMedical = new JPanel(new BorderLayout());
		
		JPanel conteneurCentre = new JPanel(new GridLayout(3,1));
		
		JPanel conteneurCentreligne1 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		JLabel codeAppareil = new JLabel("Code appareil :");
		conteneurCentreligne1.add(codeAppareil);
	
		JPanel conteneurCentreligne2 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		codeAppareilFormulaireSuppressionAppareilMedical = new JTextField();
		codeAppareilFormulaireSuppressionAppareilMedical.setPreferredSize(new Dimension(75,20));
		conteneurCentreligne2.add(codeAppareilFormulaireSuppressionAppareilMedical);
		
		JPanel conteneurCentreligne3 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		supprimerAppareilFormulaireSuppressionAppareilMedical = new JButton("Supprimer");
		conteneurCentreligne3.add(supprimerAppareilFormulaireSuppressionAppareilMedical);
		
		conteneurCentre.add(conteneurCentreligne1);
		conteneurCentre.add(conteneurCentreligne2);
		conteneurCentre.add(conteneurCentreligne3);

		JPanel margeHaute = new JPanel();
		margeHaute.setPreferredSize(new Dimension(0,250));
		JPanel margeBasse = new JPanel();
		margeBasse.setPreferredSize(new Dimension(0,250));
		conteneurDroiteSuppressionAppareilMedical.add(margeHaute, BorderLayout.NORTH);
		conteneurDroiteSuppressionAppareilMedical.add(conteneurCentre, BorderLayout.CENTER);
		conteneurDroiteSuppressionAppareilMedical.add(margeBasse, BorderLayout.SOUTH);
	}
	
	private void setPanelDroitEtatPharmacie() {
		conteneurDroiteEtatPharmacie = new JPanel(new GridLayout(1,1));
		JScrollPane conteneurAscenseurEtatPharmacie = new JScrollPane();
		etatPharmacieTable = new JTable(new PharmacieTableModel(0));
		conteneurAscenseurEtatPharmacie.getViewport().add(etatPharmacieTable);
		conteneurDroiteEtatPharmacie.add(conteneurAscenseurEtatPharmacie);
	}
	
	private void setPanelDroitAjoutRetraitArticle() {
		conteneurDroiteAjoutRetraitArticle = new JPanel(new BorderLayout());
		
		JPanel conteneurCentre = new JPanel(new GridLayout(4,1));
		
		JPanel conteneurCentreligne1 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		JLabel numeroID = new JLabel("R�f�rence :");
		conteneurCentreligne1.add(numeroID);
		
		JPanel conteneurCentreligne2 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		referenceFormulaireAjoutRetraitArticleTextField = new JTextField();
		referenceFormulaireAjoutRetraitArticleTextField.setPreferredSize(new Dimension(50,20));
		conteneurCentreligne2.add(referenceFormulaireAjoutRetraitArticleTextField);
		
		JPanel conteneurCentreligne3 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		JLabel quantite = new JLabel("Quantit� :");
		quantiteFormulaireAjoutRetraitArticleTextField = new JSpinner();
		quantiteFormulaireAjoutRetraitArticleTextField.setPreferredSize(new Dimension(50,20));
		conteneurCentreligne3.add(quantite);
		conteneurCentreligne3.add(quantiteFormulaireAjoutRetraitArticleTextField);
		
		JPanel conteneurCentreligne4 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		ajouterArticleFormulaireAjoutRetraitArticle = new JButton("Ajouter");
		JSeparator separateurBouttonFormulaireAjoutRetrait = new JSeparator();
		separateurBouttonFormulaireAjoutRetrait.setPreferredSize(new Dimension(50,0));
		retraitArticleFormulaireAjoutRetraitArticle = new JButton("Retirer");
		conteneurCentreligne4.add(ajouterArticleFormulaireAjoutRetraitArticle);
		conteneurCentreligne4.add(separateurBouttonFormulaireAjoutRetrait);
		conteneurCentreligne4.add(retraitArticleFormulaireAjoutRetraitArticle);
		
		conteneurCentre.add(conteneurCentreligne1);
		conteneurCentre.add(conteneurCentreligne2);
		conteneurCentre.add(conteneurCentreligne3);
		conteneurCentre.add(conteneurCentreligne4);
		
		JPanel margeHaute = new JPanel();
		margeHaute.setPreferredSize(new Dimension(0,250));
		JPanel margeBasse = new JPanel();
		margeBasse.setPreferredSize(new Dimension(0,250));
		conteneurDroiteAjoutRetraitArticle.add(margeHaute, BorderLayout.NORTH);
		conteneurDroiteAjoutRetraitArticle.add(conteneurCentre, BorderLayout.CENTER);
		conteneurDroiteAjoutRetraitArticle.add(margeBasse, BorderLayout.SOUTH);
	}
	
	private void setPanelDroitCreerArticle() {
		conteneurDroiteCreerArticle = new JPanel(new GridLayout(5,1));
		
		JPanel conteneurDroiteCreerArticleLigne0 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel titre = new JLabel("Veuillez entrer les sp�cifications du nouvel article :");
		conteneurDroiteCreerArticleLigne0.add(titre);
		
		JPanel conteneurDroiteCreerArticleLigne1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel reference = new JLabel("R�f�rence :");
		referenceFormulaireCreerArticleTextField = new JTextField();
		referenceFormulaireCreerArticleTextField.setPreferredSize(new Dimension(200,20));
		conteneurDroiteCreerArticleLigne1.add(reference);
		conteneurDroiteCreerArticleLigne1.add(referenceFormulaireCreerArticleTextField);
		
		JPanel conteneurDroiteCreerArticleLigne2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel nom = new JLabel("Nom :");
		JSeparator separateurCreerArticleLigne2 = new JSeparator(); 
		separateurCreerArticleLigne2.setPreferredSize(new Dimension(27,0));
		nomFormulaireCreerArticleTextField = new JTextField();
		nomFormulaireCreerArticleTextField.setPreferredSize(new Dimension(200,20));
		conteneurDroiteCreerArticleLigne2.add(nom);
		conteneurDroiteCreerArticleLigne2.add(separateurCreerArticleLigne2);
		conteneurDroiteCreerArticleLigne2.add(nomFormulaireCreerArticleTextField);
		
		JPanel conteneurDroiteCreerArticleLigne3 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel type = new JLabel("Type :");
		selectionTypeArticleFormulaireCreerArticleComboBox = new JComboBox<>(choixTypeArticle);
		conteneurDroiteCreerArticleLigne3.add(type);
		conteneurDroiteCreerArticleLigne3.add(selectionTypeArticleFormulaireCreerArticleComboBox);
		
		JPanel conteneurDroiteCreerArticleLigne4 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		validerFormulaireCreerArticle = new JButton("Valider");
		JSeparator separateurBouttonFormulaireCreerArticle = new JSeparator();
		separateurBouttonFormulaireCreerArticle.setPreferredSize(new Dimension(50,0));
		effacerFormulaireCreerArticle = new JButton("Effacer");
		conteneurDroiteCreerArticleLigne4.add(validerFormulaireCreerArticle);
		conteneurDroiteCreerArticleLigne4.add(separateurBouttonFormulaireCreerArticle);
		conteneurDroiteCreerArticleLigne4.add(effacerFormulaireCreerArticle);
		
		conteneurDroiteCreerArticle.add(conteneurDroiteCreerArticleLigne0);
		conteneurDroiteCreerArticle.add(conteneurDroiteCreerArticleLigne1);
		conteneurDroiteCreerArticle.add(conteneurDroiteCreerArticleLigne2);
		conteneurDroiteCreerArticle.add(conteneurDroiteCreerArticleLigne3);
		conteneurDroiteCreerArticle.add(conteneurDroiteCreerArticleLigne4);
	}
	
	private void setPanelDroitSuppressionArticle() {
		conteneurDroiteSuppressionArticle = new JPanel(new BorderLayout());
		
		JPanel conteneurCentre = new JPanel(new GridLayout(3,1));
		
		JPanel conteneurCentreligne1 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		JLabel reference = new JLabel("Reference :");
		conteneurCentreligne1.add(reference);
		
		JPanel conteneurCentreligne2 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		referenceFormulaireSupprimerArticleTextField = new JTextField();
		referenceFormulaireSupprimerArticleTextField.setPreferredSize(new Dimension(90,20));
		conteneurCentreligne2.add(referenceFormulaireSupprimerArticleTextField);
		
		JPanel conteneurCentreligne3 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		supprimerArticleFormulaire = new JButton("Supprimer");
		conteneurCentreligne3.add(supprimerArticleFormulaire);
		
		conteneurCentre.add(conteneurCentreligne1);
		conteneurCentre.add(conteneurCentreligne2);
		conteneurCentre.add(conteneurCentreligne3);
		
		JPanel margeHaute = new JPanel();
		margeHaute.setPreferredSize(new Dimension(0,250));
		JPanel margeBasse = new JPanel();
		margeBasse.setPreferredSize(new Dimension(0,250));
		conteneurDroiteSuppressionArticle.add(margeHaute, BorderLayout.NORTH);
		conteneurDroiteSuppressionArticle.add(conteneurCentre, BorderLayout.CENTER);
		conteneurDroiteSuppressionArticle.add(margeBasse, BorderLayout.SOUTH);
	}
	
	// Getters
	
	public JTabbedPane getConteneurPrincipal() {
		return this.conteneurPrincipal;
	}
	
	public JPanel getConteneurDroite() {
		return this.conteneurDroite;
	}
	
	public CardLayout getLayoutMultiPanelDroite() {
		return this.layoutMultiPanelDroite;
	}
	
	// Getters ConteneurGauche
	
	public JButton getCreerPatientConteneurGauche() {
		return this.creerPatientConteneurGauche;
	}
	
	public JButton getRechercherPatientConteneurGauche() {
		return this.rechercherPatientConteneurGauche;
	}
	
	public JButton getLogoutConteneurGauche() {
		return this.logoutConteneurGauche;
	}
	
	// SuperAdmin 
	
	public JButton getAjoutPersonnelConteneurGauche() {
		return this.ajoutPersonnelConteneurGauche;
	}
	
	public JButton getSuppressionPersonnelConteneurGauche() {
		return this.suppressionPersonnelConteneurGauche;
	}
	
	public JButton getAjoutInstallationConteneurGauche() {
		return this.ajoutInstallationConteneurGauche;
	}
	
	public JButton getSuppressionInstallationConteneurGauche() {
		return this.suppressionInstallationConteneurGauche;
	}
	
	public JButton getAjoutMaterielConteneurGauche() {
		return this.ajoutMaterielConteneurGauche;
	}
	
	public JButton getSuppressionMaterielConteneurGauche() {
		return this.suppressionMaterielConteneurGauche;
	}
	
	// Medecin
	
	public JButton getMesRDVConteneurGauche() {
		return this.mesRDVConteneurGauche;
	}
	
	// Infirmier
	
	public JButton getEtatServiceConteneurGauche() {
		return this.etatServiceConteneurGauche;
	}
	
	// Pharmacien

	public JButton getEtatPharmacieConteneurGauche() {
		return this.etatPharmacieConteneurGauche;
	}
	
	public JButton getAjoutRetraitArticleConteneurGauche() {
		return this.ajoutRetraitArticleConteneurGauche;
	}
	
	public JButton getCreerArticleConteneurGauche() {
		return this.creerArticleConteneurGauche;
	}
	
	public JButton getSupprimerArticleConteneurGauche() {
		return this.supprimerArticleConteneurGauche;
	}
	
	// Getters FormulaireCreerPatient

	public JTextField getNomFormulaireCreerPatientTextField() {
		return this.nomFormulaireCreerPatientTextField;
	}
	
	public JTextField getPrenomFormulaireCreerPatientTextField() {
		return this.prenomFormulaireCreerPatientTextField;
	}
	
	public ButtonGroup getSelectionSexeFormulaireCreerPatientButtonGroup() {
		return this.selectionSexeFormulaireCreerPatientButtonGroup;
	}
	
	public JTextField getDateDeNaissanceFormulaireCreerPatientTextField() {
		return this.dateDeNaissanceFormulaireCreerPatientTextField;
	}
	
	public JTextArea getAdresseFormulaireCreerPatientTextArea() {
		return this.adresseFormulaireCreerPatientTextArea;
	}
	
	public JTextField getMailFormulaireCreerPatientTextField() {
		return this.mailFormulaireCreerPatientTextField;
	}
	
	public JTextField getTelephoneFormulaireCreerPatientTextField() {
		return this.telephoneFormulaireCreerPatientTextField;
	}
	
	public JTextArea getPathologiesFormulaireCreerPatientTextArea() {
		return this.pathologiesFormulaireCreerPatientTextArea;
	}
	
	public JButton getValiderFormulaireCreerPatient() {
		return this.validerFormulaireCreerPatient;
	}
	
	public JButton getEffacerFormulaireCreerPatient() {
		return this.effacerFormulaireCreerPatient;
	}
	
	// Getters FormulaireRechercherPatient
	
	public JPanel getConteneurDroiteRechercherPatientBas() {
		return this.conteneurDroiteRechercherPatientBas;
	}
	
	public CardLayout getLayoutMultiPanelconteneurDroiteRechercherPatientBas() {
		return this.layoutMultiPanelconteneurDroiteRechercherPatientBas;
	}
	
	public ButtonGroup getSelectionMethodeRechercheFormulaireRechercherPatientButtonGroup() {
		return this.selectionMethodeRechercheFormulaireRechercherPatientButtonGroup;
	}
	
	public JRadioButton getParIdentifiantFormulaireRechercherPatientRadio() {
		return this.parIdentifiantFormulaireRechercherPatientRadio;
	}
	
	public JRadioButton getParNomFormulaireRechercherPatientRadio() {
		return this.parNomFormulaireRechercherPatientRadio;
	}
	
	public JRadioButton getParDateDeNaissanceFormulaireRechercherPatientRadio() {
		return this.parDateDeNaissanceFormulaireRechercherPatientRadio;
	}
	
	public JTextField getIdentifiantFormulaireRechercherPatientTextField() {
		return this.identifiantFormulaireRechercherPatientTextField;
	}
	
	public JTextField getNomFormulaireRechercherPatientTextField() {
		return this.nomFormulaireRechercherPatientTextField;
	}
	
	public JTextField getDateDeNaissanceFormulaireRechercherPatientTextField() {
		return this.dateDeNaissanceFormulaireRechercherPatientTextField;
	}
	
	public JButton getValiderFormulaireRechercherPatient() {
		return this.validerFormulaireRechercherPatient;
	}
	
	public JTable getRetourFormulaireRechercherPatient() {
		return this.retourFormulaireRechercherPatient;
	}
	
	// Getters FormulaireAjouterPersonnel
	
	public JTextField getNomFormulaireAjouterPersonnelTextField() {
		return this.nomFormulaireAjouterPersonnelTextField;
	}
	
	public JTextField getPrenomFormulaireAjouterPersonnelTextField() {
		return this.prenomFormulaireAjouterPersonnelTextField;
	}
	
	public ButtonGroup getSelectionSexeFormulaireAjouterPersonnelButtonGroup() {
		return this.selectionSexeFormulaireAjouterPersonnelButtonGroup;
	}
	
	public JTextField getDateDeNaissanceFormulaireAjouterPersonnelTextField() {
		return this.dateDeNaissanceFormulaireAjouterPersonnelTextField;
	}
	
	public JTextArea getAdresseFormulaireAjouterPersonnelTextArea() {
		return this.adresseFormulaireAjouterPersonnelTextArea;
	}
	
	public JTextField getMailFormulaireAjouterPersonnelTextField() {
		return this.mailFormulaireAjouterPersonnelTextField;
	}
	
	public JTextField getTelephoneFormulaireAjouterPersonnelTextField() {
		return this.telephoneFormulaireAjouterPersonnelTextField;
	}
	
	public JComboBox<String> getSelectionProfessionFormulaireAjouterPersonnelComboBox() {
		return this.selectionProfessionFormulaireAjouterPersonnelComboBox;
	}
	
	public JTextField getRppsFormulaireAjouterPersonnelTextField() {
		return this.rppsFormulaireAjouterPersonnelTextField;
	}
	
	public JComboBox<String> getSelectionSpecialiteFormulaireAjouterPersonnelComboBox() {
		return this.selectionSpecialiteFormulaireAjouterPersonnelComboBox;
	}
	
	public JComboBox<String> getSelectionServiceFormulaireAjouterPersonnelComboBox() {
		return this.selectionServiceFormulaireAjouterPersonnelComboBox;
	}
	
	public JTextField getSalaireFormulaireAjouterPersonnelTextField() {
		return this.salaireFormulaireAjouterPersonnelTextField;
	}
	
	public JButton getValiderFormulaireAjouterPersonnel() {
		return this.validerFormulaireAjouterPersonnel;
	}
	
	public JButton getEffacerFormulaireAjoutPersonnel() {
		return this.effacerFormulaireAjoutPersonnel;
	}
	
	// Getters FormulaireSuppressionPersonnel
	
	public JTextField getIdFormulaireSuppressionPersonnelTextField() {
		return this.idFormulaireSuppressionPersonnelTextField;
	}
	
	public JButton getSupprimerFormulaireSuppressionPersonnel() {
		return this.supprimerFormulaireSuppressionPersonnel;
	}
	
	// Getters FormulaireAjoutInstallations
	
	public JTextField getCodeServiceFormulaireAjoutInstallationsTextField() {
		return this.codeServiceFormulaireAjoutInstallationsTextField;
	}
	
	public JTextField getNomServiceFormulaireAjoutInstallationsTextField() {
		return this.nomServiceFormulaireAjoutInstallationsTextField;
	}
	
	public ButtonGroup getEtatServiceFormulaireAjoutInstallationsTextFieldButtonGroup() {
		return this.etatServiceFormulaireAjoutInstallationsTextFieldButtonGroup;
	}
	
	public JRadioButton getServiceVideRadio() {
		return this.serviceVideRadio;
	}
	
	public JRadioButton getServiceRempliRadio() {
		return this.serviceRempliRadio;
	}
	
	public JCheckBox getChambreIndividuelleFormulaireAjoutInstallationsCheckBox() {
		return this.chambreIndividuelleFormulaireAjoutInstallationsCheckBox;
	}
	
	public JSpinner getChambreIndividuelleFormulaireAjoutInstallationsSpinner() {
		return this.chambreIndividuelleFormulaireAjoutInstallationsSpinner;
	}
	
	public JCheckBox getChambreDoubleFormulaireAjoutInstallationsCheckBox() {
		return this.chambreDoubleFormulaireAjoutInstallationsCheckBox;
	}
	
	public JSpinner getChambreDoubleFormulaireAjoutInstallationsSpinner() {
		return this.chambreDoubleFormulaireAjoutInstallationsSpinner;
	}
	
	public JCheckBox getChambreQuadrupleFormulaireAjoutInstallationsCheckBox() {
		return this.chambreQuadrupleFormulaireAjoutInstallationsCheckBox;
	}
	
	public JSpinner getChambreQuadrupleFormulaireAjoutInstallationsSpinner() {
		return this.chambreQuadrupleFormulaireAjoutInstallationsSpinner;
	}
	
	public JButton getAjouterFormulaireAjoutInstallations() {
		return this.ajouterFormulaireAjoutInstallations;
	}
	
	// Getters FormulaireSuppressionInstallations
	
	public ButtonGroup getChoixTypeInstallationFormulaireSuppressionInstallations() {
		return this.choixTypeInstallationFormulaireSuppressionInstallations;
	}
	
	public JRadioButton getChoixServiceFormulaireSuppressionInstallations() {
		return this.choixServiceFormulaireSuppressionInstallations;
	}
	
	public JRadioButton getChoixChambreFormulaireSuppressionInstallations() {
		return this.choixChambreFormulaireSuppressionInstallations;
	}
	
	public JRadioButton getChoixLitFormulaireSuppressionInstallations() {
		return this.choixLitFormulaireSuppressionInstallations;
	}
	
	public JTextField getCodeServiceFormulaireSuppressionInstallations() {
		return this.codeServiceFormulaireSuppressionInstallations;
	}
	
	public JTextField getCodeChambreFormulaireSuppressionInstallations() {
		return this.codeChambreFormulaireSuppressionInstallations;
	}
	
	public JTextField getCodeLitFormulaireSuppressionInstallations() {
		return this.codeLitFormulaireSuppressionInstallations;
	}
	
	public JButton getSupprimerFormulaireSuppressionInstallations() {
		return this.supprimerFormulaireSuppressionInstallations;
	}
	
	// Getters FormulaireAjoutAppareilMedical
	
	public JTextField getCodeAppareilFormulaireAjoutAppareilMedicalTextField() {
		return this.codeAppareilFormulaireAjoutAppareilMedicalTextField;
	}
		
	public JTextField getTypeAppareilFormulaireAjoutAppareilMedicalTextField() {
		return this.typeAppareilFormulaireAjoutAppareilMedicalTextField;
	}
		
	public JButton getAjouterAppareilFormulaireAjoutAppareilMedical() {
		return this.ajouterAppareilFormulaireAjoutAppareilMedical;
	}
		
	// Getters FormulaireSuppressionAppareilMedical
		
	public JButton getSupprimerAppareilFormulaireSuppressionAppareilMedical() {
		return this.supprimerAppareilFormulaireSuppressionAppareilMedical;
	}
		
	public JTextField getCodeAppareilFormulaireSuppressionAppareilMedical() {
		return this.codeAppareilFormulaireSuppressionAppareilMedical;
	 }
	
	// Getter contconteneurDroiteMesRDV
	
	public JTable getMesRDVMedecin() {
		return this.mesRDVMedecin;
	}
	
	// Getter
	
	public ConteneurGraphique1Lit getConteneurDroiteEtatServiceLigne1Cellule1() {
		return this.conteneurDroiteEtatServiceLigne1Cellule1;
	}
	
	public ConteneurGraphique1Lit getConteneurDroiteEtatServiceLigne1Cellule2() {
		return this.conteneurDroiteEtatServiceLigne1Cellule2;
	}
	
	public ConteneurGraphique1Lit getConteneurDroiteEtatServiceLigne1Cellule3() {
		return this.conteneurDroiteEtatServiceLigne1Cellule3;
	}
	
	public ConteneurGraphique1Lit getConteneurDroiteEtatServiceLigne1Cellule4() {
		return this.conteneurDroiteEtatServiceLigne1Cellule4;
	}
	
	public ConteneurGraphique1Lit getConteneurDroiteEtatServiceLigne2Cellule1() {
		return this.conteneurDroiteEtatServiceLigne2Cellule1;
	}
	
	public ConteneurGraphique1Lit getConteneurDroiteEtatServiceLigne2Cellule2() {
		return this.conteneurDroiteEtatServiceLigne2Cellule2;
	}
	
	public ConteneurGraphique1Lit getConteneurDroiteEtatServiceLigne2Cellule3() {
		return this.conteneurDroiteEtatServiceLigne2Cellule3;
	}
	
	public ConteneurGraphique1Lit getConteneurDroiteEtatServiceLigne2Cellule4() {
		return this.conteneurDroiteEtatServiceLigne2Cellule4;
	}
	
	public ConteneurGraphique2Lit getConteneurDroiteEtatServiceLigne3Cellule1() {
		return this.conteneurDroiteEtatServiceLigne3Cellule1;
	}
	
	public ConteneurGraphique2Lit getConteneurDroiteEtatServiceLigne3Cellule2() {
		return this.conteneurDroiteEtatServiceLigne3Cellule2;
	}
	
	public ConteneurGraphique2Lit getConteneurDroiteEtatServiceLigne4Cellule1() {
		return this.conteneurDroiteEtatServiceLigne4Cellule1;
	}
	
	public ConteneurGraphique2Lit getConteneurDroiteEtatServiceLigne4Cellule2() {
		return this.conteneurDroiteEtatServiceLigne4Cellule2;
	}
	
	public ConteneurGraphique2Lit getConteneurDroiteEtatServiceLigne5Cellule1() {
		return this.conteneurDroiteEtatServiceLigne5Cellule1;
	}
	
	public ConteneurGraphique2Lit getConteneurDroiteEtatServiceLigne5Cellule2() {
		return this.conteneurDroiteEtatServiceLigne5Cellule2;
	}
	
	public ConteneurGraphique2Lit getConteneurDroiteEtatServiceLigne6Cellule1() {
		return this.conteneurDroiteEtatServiceLigne6Cellule1;
	}
	
	public ConteneurGraphique2Lit getConteneurDroiteEtatServiceLigne6Cellule2() {
		return this.conteneurDroiteEtatServiceLigne6Cellule2;
	}
	
	public ConteneurGraphique4Lit getConteneurDroiteEtatServiceLigne7() {
		return this.conteneurDroiteEtatServiceLigne7;
	}
	
	public ConteneurGraphique4Lit getConteneurDroiteEtatServiceLigne8() {
		return this.conteneurDroiteEtatServiceLigne8;
	}
	
	public ConteneurGraphique4Lit getConteneurDroiteEtatServiceLigne9() {
		return this.conteneurDroiteEtatServiceLigne9;
	}
	
	public ConteneurGraphique4Lit getConteneurDroiteEtatServiceLigne10() {
		return this.conteneurDroiteEtatServiceLigne10;
	}
	
	// Getter etatPharmacie
	
	public JTable getEtatPharmacieTable() {
		return this.etatPharmacieTable;
	}
	
	// Getters FormulaireAjoutRetraitArticle
	
	public JTextField getReferenceFormulaireAjoutRetraitArticleTextField() {
		return this.referenceFormulaireAjoutRetraitArticleTextField;
	}
		
	public JSpinner getQuantiteFormulaireAjoutRetraitArticleTextField() {
		return this.quantiteFormulaireAjoutRetraitArticleTextField;
	}
	
	public JButton getAjouterArticleFormulaireAjoutRetraitArticle() {
		return this.ajouterArticleFormulaireAjoutRetraitArticle;
	}
	
	public JButton getRetraitArticleFormulaireAjoutRetraitArticle() {
		return this.retraitArticleFormulaireAjoutRetraitArticle;
	}
	
	// Getters FormulaireCreerArticle
	
	public JTextField getReferenceFormulaireCreerArticleTextField() {
		return this.referenceFormulaireCreerArticleTextField;
	}
		
	public JTextField getNomFormulaireCreerArticleTextField() {
		return this.nomFormulaireCreerArticleTextField;
	}
		
	public JComboBox<String> getSelectionTypeArticleFormulaireCreerArticleComboBox() {
		return this.selectionTypeArticleFormulaireCreerArticleComboBox;
	}
		
	public JButton getValiderFormulaireCreerArticle() {
		return this.validerFormulaireCreerArticle;
	}
	
	public JButton getEffacerFormulaireCreerArticle() {
		return this.effacerFormulaireCreerArticle;
	}
	
	// FormulaireSupprimerArticle
	
	public JTextField getReferenceFormulaireSupprimerArticleTextField() {
		return this.referenceFormulaireSupprimerArticleTextField;
	}
	
	public JButton getSupprimerArticleFormulaire() {
		return this.supprimerArticleFormulaire;
	}

}