package vues;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import vues.tablemodel.AnalyseTableModel;
import vues.tablemodel.ConsultationTableModel;
import vues.tablemodel.OrdonnanceTableModel;
import vues.tablemodel.RDVTableModel;

@SuppressWarnings("serial")
public class OngletPatient extends JPanel  {
	
	JPanel panelHautGauche;
	JPanel panelHautDroit;
	JTabbedPane ongletsBasGauche;
	JPanel panelBasDroit;
	
	JPanel conteneurBasDroiteVide;
	JPanel conteneurBasDroiteNouveauRDV;
	JPanel conteneurBasDroiteAfficherRDV;
	JPanel conteneurBasDroiteNouvelleConsultation;
	JPanel conteneurBasDroiteAfficherConsultation;
	JPanel conteneurBasDroiteNouvelleOrdonnance;
	JPanel conteneurBasDroiteAfficherOrdonnance;
	JPanel conteneurBasDroiteNouvelleAnalyse;
	JPanel conteneurBasDroiteAfficherAnalyse;
	JPanel conteneurBasDroiteHospitalisation;
	JPanel conteneurBasDroiteModifierPatient;
	JPanel conteneurBasDroiteSupprimerPatient;
	
	JPanel pathologie;
	JPanel rdv;
	JPanel consultation;
	JPanel ordonnance;
	JPanel analyse;
	
	CardLayout layoutMultiPanel;
	
	// JComponent
	
	// Donn�es Personnelles
	JTextField idFormulaireDonneesPersonnellesTextField;
	JTextField nomFormulaireDonneesPersonnellesTextField;
	JTextField prenomFormulaireDonneesPersonnellesTextField;
	JRadioButton masculinFormulaireDonneesPersonnellesRadio;
	JRadioButton femininFormulaireDonneesPersonnellesRadio;
	JTextField dateDeNaissanceFormulaireDonneesPersonnellesTextField;
	JTextField adresseFormulaireDonneesPersonnellesTextField;
	JTextField mailFormulaireDonneesPersonnellesTextField;
	JTextField numeroFormulaireDonneesPersonnellesTextField;
	JRadioButton ouiHospitaliseFormulaireDonneesPersonnellesRadio;
	JRadioButton nonHospitaliseFormulaireDonneesPersonnellesRadio;
	JTextField numeroLitHospitaliserDonneesPersonnelles;
	
	// BAS GAUCHE PATHOS
	JTextArea pathologieArea;
	JButton modifierPathologiesFormulairePathologie;
	
	// PAS GAUCHE CONSULT
	JTable tableauRDVPatient;
	JTable tableauConsultationPatient;
	JTable tableauOrdonnancePatient;
	JTable tableauAnalysePatient;
	
	// Nouveau RDV
	JComboBox<String> choixMedecinFormulaireNouveauRDVComboBox;
	JComboBox<String> choixDateFormulaireNouveauRDVComboBox;
	JComboBox<String> choixHeureFormulaireNouveauRDVComboBox;
	JButton ajouterRDVFormulaireNouveauRDV;
	
	// Npuvelle Consultation
	JTextArea consultationArea;
	JButton ajouterConsultation;
	
	// Nouvelle Ordonnance
	JTextArea ordonnanceArea;
	JButton ajouterOrdonnance;
	
	// Nouvelle Analyse
	JComboBox<String> choixAppareilMedicalFormulaireNouvelleAnalyseComboBox;
	JTextArea analyseArea;
	JButton ajouterAnalyse;
	
	// Supprimer Patient
	JButton ouiSupprimer;
	
	// AFFICHER RDV
	JTextField numeroReferenceFormulaireAfficherRDV;
	JTextField nomPrenomMedecinFormulaireAfficherRDV;
	JTextField salleFormulaireAfficherRDV;
	JTextField dateRDVFormulaireAfficherRDV;
	JTextField heureRDVFormulaireAfficherRDV;
	
	// AFFICHER CONSULTATION
	
	JTextField numeroReferenceFormulaireAfficherConsultation;
	JTextField nomPrenomDocteurFormulaireAfficherConsultation;
	JTextField dateConsultationFormulaireAfficherConsultation;
	JTextArea notesConsultationFormulaireAfficherConsultationArea;
	
	// AFFICHER ORDONNANCE
	
	JTextField numeroReferenceFormulaireAfficherOrdonnance;
	JTextField nomPrenomDocteurFormulaireAfficherOrdonnance;
	JTextField dateOrdonnanceFormulaireAfficherOrdonnance;
	JTextArea notesOrdonnanceFormulaireAfficherOrdonnanceArea;
	
	// AFFICHER ANALYSE
	
	JTextField numeroReferenceFormulaireAfficherAnalyse;
	JTextField nomPrenomTechnicienFormulaireAfficherAnalyse;
	JTextField appareilFormulaireAfficherAnalyse;
	JTextField dateAnalyseFormulaireAfficherAnalyse;
	JTextField heureAnalyseFormulaireAfficherAnalyse;
	JTextArea notesAnalyseFormulaireAfficherAnalyseArea;
	
	//MODIFIER PATIENT
	ButtonGroup choixModificationFormulaireModifierPatient;
	JTextField nomFormulaireModifierPatient;
	JTextField prenomFormulaireModifierPatient;
	ButtonGroup buttonGroupSexeFormulaireModifierPatient;
	JRadioButton masculinFormulaireModifierPatient;
	JRadioButton femininFormulaireModifierPatient;
	JTextField dateDeNaissanceFormulaireModifierPatient;
	JTextArea adresseFormulaireModifierPatient;
	JTextField mailFormulaireModifierPatient;
	JTextField numeroFormulaireModifierPatient;
	JButton modifierFormulaireModifierPatientButton;
	
	// Hospitaliser
	ButtonGroup choixAutoManuelFormulaireHospitalisation;
	ButtonGroup choixTypeChambreFormulaireHospitalisation;
	JTextField codeServiceFormulaireHospitalisation;
	JRadioButton choixAutomatiqueFormulaireHospitalisation;
	JRadioButton choixManuelFormulaireHospitalisation;
	JComboBox<String> dateDebutFormulaireHospitalisationComboBox;
	JComboBox<String> dateFinFormulaireHospitalisationComboBox;
	JRadioButton choixChambreIndividuelleFormulaireHospitalisation;
	JComboBox<String> chambreIndividuelleFormulaireHospitalisationComboBox;
	JRadioButton choixChambreDoubleFormulaireHospitalisation;
	JComboBox<String> chambreDoubleFormulaireHospitalisationComboBox;
	JRadioButton choixChambreQuadrupleFormulaireHospitalisation;
	JComboBox<String> chambreQuadrupleFormulaireHospitalisationComboBox;
	JButton hospitaliserFormulaireHospitalisation;
	
	public OngletPatient(String profession, int idPatient){
		super();
		
		this.setLayout(new GridLayout(2,2));
		this.setPanelHautGauche();
		this.setPanelHautDroite(profession);
		this.setPanelBasGauche(profession);
		this.setPanelBasDroit(profession);

		this.add(panelHautGauche);
		this.add(panelHautDroit);
		this.add(ongletsBasGauche);
		this.add(panelBasDroit);
	}
	
	private void setPanelHautGauche() {
		
		panelHautGauche = new JPanel(new GridLayout(8,1));
		panelHautGauche.setBorder(BorderFactory.createTitledBorder("Donn�es personelles du patient"));
		
		JPanel ligne1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel id = new JLabel("N�Identifiant :");
		idFormulaireDonneesPersonnellesTextField = new JTextField();
		idFormulaireDonneesPersonnellesTextField.setPreferredSize(new Dimension(105,20));
		idFormulaireDonneesPersonnellesTextField.setEditable(false);
		ligne1.add(id);
		ligne1.add(idFormulaireDonneesPersonnellesTextField);
			
		JPanel ligne2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel nom = new JLabel("Nom :");
		nomFormulaireDonneesPersonnellesTextField = new JTextField();
		nomFormulaireDonneesPersonnellesTextField.setPreferredSize(new Dimension(105,20));
		nomFormulaireDonneesPersonnellesTextField.setEditable(false);
		JLabel prenom = new JLabel("Prenom :");
		prenomFormulaireDonneesPersonnellesTextField = new JTextField();
		prenomFormulaireDonneesPersonnellesTextField.setPreferredSize(new Dimension(105,20));
		prenomFormulaireDonneesPersonnellesTextField.setEditable(false);
		ligne2.add(nom);
		JSeparator separateurLigne2 = new JSeparator();
		separateurLigne2.setPreferredSize(new Dimension(38,0));
		ligne2.add(separateurLigne2);
		ligne2.add(nomFormulaireDonneesPersonnellesTextField);
		JSeparator separateurLigne2bis = new JSeparator();
		separateurLigne2bis.setPreferredSize(new Dimension(25,0));
		ligne2.add(separateurLigne2bis);
		ligne2.add(prenom);
		JSeparator separateurLigne2ter = new JSeparator();
		separateurLigne2ter.setPreferredSize(new Dimension(25,0));
		ligne2.add(separateurLigne2ter);
		ligne2.add(prenomFormulaireDonneesPersonnellesTextField);

		JPanel ligne3 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel sexe = new JLabel("Sexe :");
		ButtonGroup sexeGroup = new ButtonGroup();
		masculinFormulaireDonneesPersonnellesRadio = new JRadioButton();
		sexeGroup.add(masculinFormulaireDonneesPersonnellesRadio);
		masculinFormulaireDonneesPersonnellesRadio.setEnabled(false);
		JLabel masculinText = new JLabel("Masculin");
		femininFormulaireDonneesPersonnellesRadio = new JRadioButton();
		sexeGroup.add(femininFormulaireDonneesPersonnellesRadio);
		femininFormulaireDonneesPersonnellesRadio.setEnabled(false);
		JLabel femininText = new JLabel("Feminin");
		ligne3.add(sexe);
		ligne3.add(masculinFormulaireDonneesPersonnellesRadio);
		ligne3.add(masculinText);
		ligne3.add(femininFormulaireDonneesPersonnellesRadio);
		ligne3.add(femininText);
			
		JPanel ligne4 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel dateDeNaissance = new JLabel("Date de Naissance :");
		dateDeNaissanceFormulaireDonneesPersonnellesTextField = new JTextField();
		dateDeNaissanceFormulaireDonneesPersonnellesTextField.setPreferredSize(new Dimension(67,20));
		dateDeNaissanceFormulaireDonneesPersonnellesTextField.setEditable(false);
		ligne4.add(dateDeNaissance);
		ligne4.add(dateDeNaissanceFormulaireDonneesPersonnellesTextField);

		JPanel ligne5 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel adresse = new JLabel("Adresse :");
		adresseFormulaireDonneesPersonnellesTextField = new JTextField();
		adresseFormulaireDonneesPersonnellesTextField.setPreferredSize(new Dimension(332,20));
		adresseFormulaireDonneesPersonnellesTextField.setEditable(false);
		ligne5.add(adresse);
		JSeparator separateurLigne5 = new JSeparator();
		separateurLigne5.setPreferredSize(new Dimension(16,0));
		ligne5.add(separateurLigne5);
		ligne5.add(adresseFormulaireDonneesPersonnellesTextField);

		JPanel ligne6 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel mail = new JLabel("Mail :");
		mailFormulaireDonneesPersonnellesTextField = new JTextField();
		mailFormulaireDonneesPersonnellesTextField.setPreferredSize(new Dimension(200,20));
		mailFormulaireDonneesPersonnellesTextField.setEditable(false);
		ligne6.add(mail);
		JSeparator separateurLigne6 = new JSeparator();
		separateurLigne6.setPreferredSize(new Dimension(41,0));
		ligne6.add(separateurLigne6);
		ligne6.add(mailFormulaireDonneesPersonnellesTextField);
			
		JPanel ligne7 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel numero = new JLabel("Numero :");
		numeroFormulaireDonneesPersonnellesTextField = new JTextField();
		numeroFormulaireDonneesPersonnellesTextField.setPreferredSize(new Dimension(105,20));
		numeroFormulaireDonneesPersonnellesTextField.setEditable(false);
		ligne7.add(numero);
		JSeparator separateurLigne7 = new JSeparator();
		separateurLigne7.setPreferredSize(new Dimension(19,0));
		ligne7.add(separateurLigne7);
		ligne7.add(numeroFormulaireDonneesPersonnellesTextField);

		JPanel ligne8 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		ButtonGroup ouiNon = new ButtonGroup();
		JLabel hospitalise = new JLabel("Hospitalis� :");
		ouiHospitaliseFormulaireDonneesPersonnellesRadio = new JRadioButton();
		ouiNon.add(ouiHospitaliseFormulaireDonneesPersonnellesRadio);
		ouiHospitaliseFormulaireDonneesPersonnellesRadio.setEnabled(false);
		JLabel ouiText = new JLabel("Oui");
		nonHospitaliseFormulaireDonneesPersonnellesRadio = new JRadioButton();
		ouiNon.add(nonHospitaliseFormulaireDonneesPersonnellesRadio);
		nonHospitaliseFormulaireDonneesPersonnellesRadio.setEnabled(false);
		JLabel nonText = new JLabel("Non");
		JSeparator espaceNumeroLit = new JSeparator();
		espaceNumeroLit.setPreferredSize(new Dimension(5,0));
		numeroLitHospitaliserDonneesPersonnelles = new JTextField();
		numeroLitHospitaliserDonneesPersonnelles.setPreferredSize(new Dimension(105,20));
		numeroLitHospitaliserDonneesPersonnelles.setEditable(false);
		JSeparator espaceBoutton = new JSeparator();
		espaceBoutton.setPreferredSize(new Dimension(60,0));
		JButton sortieHopitalFormulaireDonneesPersonnelles = new JButton("Sortie d'h�pital");
		ligne8.add(hospitalise);
		ligne8.add(nonHospitaliseFormulaireDonneesPersonnellesRadio);
		ligne8.add(nonText);
		ligne8.add(ouiHospitaliseFormulaireDonneesPersonnellesRadio);
		ligne8.add(ouiText);
		ligne8.add(espaceNumeroLit);
		ligne8.add(numeroLitHospitaliserDonneesPersonnelles);
		ligne8.add(espaceBoutton);
		ligne8.add(sortieHopitalFormulaireDonneesPersonnelles);
	
		panelHautGauche.add(ligne1);
		panelHautGauche.add(ligne2);
		panelHautGauche.add(ligne3);
		panelHautGauche.add(ligne4);
		panelHautGauche.add(ligne5);
		panelHautGauche.add(ligne6);
		panelHautGauche.add(ligne7);
		panelHautGauche.add(ligne8);
	}
	
	private void setPanelHautDroite(String profession) {
		
		if(profession.equalsIgnoreCase("agent_administrateur")) {
			panelHautDroit = new JPanel(new GridLayout(2,2,40,40));
			panelHautDroit.setBorder(BorderFactory.createEmptyBorder(80, 40, 100, 40));
			this.setBouttonNouveauRDV();
			this.setBouttonHospitalisation();
			this.setBouttonModifierPatient();
			this.setBouttonSupprimerPatient();
		}
		else if(profession.equalsIgnoreCase("technicien")) {
			panelHautDroit = new JPanel(new GridLayout(1,1));
			panelHautDroit.setBorder(BorderFactory.createEmptyBorder(125, 125, 125, 125));
			this.setBouttonNouvelleAnalyse();
		}
		else if(profession.equalsIgnoreCase("medecin")) {
			panelHautDroit = new JPanel(new GridLayout(3,1,40,40));
			panelHautDroit.setBorder(BorderFactory.createEmptyBorder(60, 60, 60, 60));
			this.setBouttonNouvelleConsultation();
			this.setBouttonNouvelleOrdonnance();
			this.setBouttonNouveauRDV();
			this.setBouttonModifierPatient();
			this.setBouttonSupprimerPatient();
		}
		else if(profession.equalsIgnoreCase("infirmier")) {
			panelHautDroit = new JPanel(new GridLayout(1,1));
			panelHautDroit.setBorder(BorderFactory.createEmptyBorder(125, 125, 125, 125));
			this.setBouttonNouveauRDV();
		}
		else if(profession.equalsIgnoreCase("pharmacien")) {
			panelHautDroit = new JPanel();
			// Logo !
		}
	}
	
	private void setBouttonNouveauRDV() {
		JButton nouveauRDVBoutton = new JButton("Nouveau RDV");
		nouveauRDVBoutton.addActionListener(new BouttonNouveauRDVListener());
		panelHautDroit.add(nouveauRDVBoutton);
	}
	
	private void setBouttonNouvelleConsultation() {
		JButton nouvelleConsultationBoutton = new JButton("Nouvelle consultation");
		nouvelleConsultationBoutton.addActionListener(new BouttonNouvelleConsultationListener()); 
		panelHautDroit.add(nouvelleConsultationBoutton);
	}
	
	private void setBouttonNouvelleOrdonnance() {
		JButton nouvlleOrdonnanceBoutton = new JButton("Nouvelle ordonnance");
		nouvlleOrdonnanceBoutton.addActionListener(new BouttonNouvelleOrdonnanceListener()); 
		panelHautDroit.add(nouvlleOrdonnanceBoutton);
	}
	
	private void setBouttonNouvelleAnalyse() {
		JButton nouvelleAnalyseBoutton = new JButton("Nouvelle analyse");
		nouvelleAnalyseBoutton.addActionListener(new BouttonNouvelleAnalyseListener()); 
		panelHautDroit.add(nouvelleAnalyseBoutton);
	}
	
	private void setBouttonHospitalisation() {
		JButton hospitalisationBoutton = new JButton("Hospitaliser");
		hospitalisationBoutton.addActionListener(new BouttonHospitalisationListener()); 
		panelHautDroit.add(hospitalisationBoutton);
	}
	
	private void setBouttonModifierPatient() {
		JButton modifierPatientBoutton = new JButton("Modifier les donn�es");
		modifierPatientBoutton.addActionListener(new BouttonModifierPatientListener()); 
		panelHautDroit.add(modifierPatientBoutton);
	}
	
	private void setBouttonSupprimerPatient() {
		JButton supprimerPatientBoutton = new JButton("Supprimer le Patient");
		supprimerPatientBoutton.addActionListener(new BouttonSupprimerPatientListener()); 
		panelHautDroit.add(supprimerPatientBoutton);
	}
	
	private void setPanelBasGauche(String profession) {
		
		ongletsBasGauche = new JTabbedPane();
		
		if(profession.equalsIgnoreCase("agent_administrateur")) {
			setPanelBasGaucheRDV();
		}
		else if(profession.equalsIgnoreCase("technicien")) {
			setPanelBasGaucheOrdonnance();
			setPanelBasGaucheAnalyse();
		}
		else if(profession.equalsIgnoreCase("medecin")) {
			setPanelBasGauchePathologie();
			setPanelBasGaucheRDV();
			setPanelBasGaucheConsultation();
			setPanelBasGaucheOrdonnance();
			setPanelBasGaucheAnalyse();
		}
		else if(profession.equalsIgnoreCase("infirmier")) {
			setPanelBasGaucheRDV();
			setPanelBasGaucheOrdonnance();
		}
		else if(profession.equalsIgnoreCase("pharmacien")) {
			setPanelBasGaucheOrdonnance();
		}
	}
	
	private void setPanelBasGauchePathologie() {
		pathologie = new JPanel(new BorderLayout());
		JScrollPane pathologieAscenseur = new JScrollPane();
		pathologieArea = new JTextArea();
		pathologieAscenseur.getViewport().add(pathologieArea);
		
		JPanel conteneurPanelBasGaucheBoutton = new JPanel(new FlowLayout(FlowLayout.CENTER));
		conteneurPanelBasGaucheBoutton.setPreferredSize(new Dimension(0,35));
		modifierPathologiesFormulairePathologie = new JButton("Modifier");
		conteneurPanelBasGaucheBoutton.add(modifierPathologiesFormulairePathologie);
		
		pathologie.add(pathologieAscenseur, BorderLayout.CENTER);
		pathologie.add(conteneurPanelBasGaucheBoutton, BorderLayout.SOUTH);
		ongletsBasGauche.addTab("Pathologies", pathologie);
	}
	
	private void setPanelBasGaucheRDV() {
		rdv = new JPanel(new GridLayout(1,1));
		JScrollPane rdvAscenseur = new JScrollPane();
		tableauRDVPatient = new JTable(new RDVTableModel(0));
		rdvAscenseur.getViewport().add(tableauRDVPatient);
		rdv.add(rdvAscenseur);
		ongletsBasGauche.addTab("RDV", rdv);
	}
	
	private void setPanelBasGaucheConsultation() {
		consultation = new JPanel(new GridLayout(1,1));
		JScrollPane consultationAscenseur = new JScrollPane();
		tableauConsultationPatient = new JTable(new ConsultationTableModel(0));
		consultationAscenseur.getViewport().add(tableauConsultationPatient);
		consultation.add(consultationAscenseur);
		ongletsBasGauche.addTab("Consultation", consultation);
	}
	
	private void setPanelBasGaucheOrdonnance() {
		ordonnance = new JPanel(new GridLayout(1,1));
		JScrollPane ordonnanceAscenseur = new JScrollPane();
		tableauOrdonnancePatient = new JTable(new OrdonnanceTableModel(0));
		ordonnanceAscenseur.getViewport().add(tableauOrdonnancePatient);
		ordonnance.add(ordonnanceAscenseur);
		ongletsBasGauche.addTab("Ordonnance", ordonnance);
	}
	
	private void setPanelBasGaucheAnalyse() {
		analyse = new JPanel(new GridLayout(1,1));
		JScrollPane analyseAscenseur = new JScrollPane();
		tableauAnalysePatient = new JTable(new AnalyseTableModel(0));
		analyseAscenseur.getViewport().add(tableauAnalysePatient);
		analyse.add(analyseAscenseur);
		ongletsBasGauche.addTab("Analyse", analyse);
	}
	
	private void setPanelBasDroit(String profession) {	
		
		panelBasDroit = new JPanel();
		layoutMultiPanel = new CardLayout();
		panelBasDroit.setLayout(layoutMultiPanel);
		panelBasDroit.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		
		conteneurBasDroiteVide = new JPanel();
		panelBasDroit.add(conteneurBasDroiteVide,"panelVide");
		
		if(profession.equalsIgnoreCase("agent_administrateur")) {
			this.setPanelBasDroitNouveauRDV();
			panelBasDroit.add(conteneurBasDroiteNouveauRDV,"panelNouveauRDV");
			this.setPanelBasDroitHospitalisation();
			panelBasDroit.add(conteneurBasDroiteHospitalisation,"panelHospitalisation");
			this.setPanelBasDroitModifierPatient();
			panelBasDroit.add(conteneurBasDroiteModifierPatient,"panelModifierPatient");
			this.setPanelBasDroitSupprimerPatient();
			panelBasDroit.add(conteneurBasDroiteSupprimerPatient,"panelSupprimerPatient");
			this.setPanelBasDroitAfficherRDV();
			panelBasDroit.add(conteneurBasDroiteAfficherRDV,"panelAfficherRDV");
		}
		else if(profession.equalsIgnoreCase("technicien")) {
			this.setPanelBasDroitNouvelleAnalyse();
			panelBasDroit.add(conteneurBasDroiteNouvelleAnalyse,"panelNouvelleAnalyse");
			this.setPanelBasDroitAfficherAnalyse();
			panelBasDroit.add(conteneurBasDroiteAfficherAnalyse,"panelAfficherAnalyse");
			this.setPanelBasDroitAfficherOrdonnance();
			panelBasDroit.add(conteneurBasDroiteAfficherOrdonnance,"panelAfficherOrdonnance");
		}
		else if(profession.equalsIgnoreCase("medecin")) {
			this.setPanelBasDroitNouvelleConsultation();
			panelBasDroit.add(conteneurBasDroiteNouvelleConsultation,"panelNouvelleConsultation");
			this.setPanelBasDroitAfficherConsultation();
			panelBasDroit.add(conteneurBasDroiteAfficherConsultation,"panelAfficherConsultation");
			this.setPanelBasDroitNouvelleOrdonnance();
			panelBasDroit.add(conteneurBasDroiteNouvelleOrdonnance,"panelNouvelleOrdonnance");
			this.setPanelBasDroitAfficherOrdonnance();
			panelBasDroit.add(conteneurBasDroiteAfficherOrdonnance,"panelAfficherOrdonnance");
			this.setPanelBasDroitAfficherAnalyse();
			panelBasDroit.add(conteneurBasDroiteAfficherAnalyse,"panelAfficherAnalyse");
			this.setPanelBasDroitNouveauRDV();
			panelBasDroit.add(conteneurBasDroiteNouveauRDV,"panelNouveauRDV");
			this.setPanelBasDroitAfficherRDV();
			panelBasDroit.add(conteneurBasDroiteAfficherRDV,"panelAfficherRDV");
			this.setPanelBasDroitModifierPatient();
			panelBasDroit.add(conteneurBasDroiteModifierPatient,"panelModifierPatient");
			this.setPanelBasDroitSupprimerPatient();
			panelBasDroit.add(conteneurBasDroiteSupprimerPatient,"panelSupprimerPatient");
		}
		else if(profession.equalsIgnoreCase("infirmier")) {
			this.setPanelBasDroitNouveauRDV();
			panelBasDroit.add(conteneurBasDroiteNouveauRDV,"panelNouveauRDV");
			this.setPanelBasDroitAfficherRDV();
			panelBasDroit.add(conteneurBasDroiteAfficherRDV,"panelAfficherRDV");
			this.setPanelBasDroitAfficherOrdonnance();
			panelBasDroit.add(conteneurBasDroiteAfficherOrdonnance,"panelAfficherOrdonnance");
		}
		else if(profession.equalsIgnoreCase("pharmacien")) {
			this.setPanelBasDroitAfficherOrdonnance();
			panelBasDroit.add(conteneurBasDroiteAfficherOrdonnance,"panelAfficherOrdonnance");
		}
	}
	
	private void setPanelBasDroitNouveauRDV() {
		conteneurBasDroiteNouveauRDV = new JPanel(new GridLayout(4,1));
		
		JPanel panelNouveauRDVLigne1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel choixMedecin =  new JLabel("Docteur :");
		choixMedecinFormulaireNouveauRDVComboBox = new JComboBox<>();
		panelNouveauRDVLigne1.add(choixMedecin);
		panelNouveauRDVLigne1.add(choixMedecinFormulaireNouveauRDVComboBox);
		
		JPanel panelNouveauRDVLigne2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel choixDate =  new JLabel("Date :");
		choixDateFormulaireNouveauRDVComboBox = new JComboBox<>();
		choixDateFormulaireNouveauRDVComboBox.setEnabled(false);
		panelNouveauRDVLigne2.add(choixDate);
		panelNouveauRDVLigne2.add(choixDateFormulaireNouveauRDVComboBox);
		
		JPanel panelNouveauRDVLigne3 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel choixHeure =  new JLabel("Heure :");
		choixHeureFormulaireNouveauRDVComboBox = new JComboBox<>();
		choixHeureFormulaireNouveauRDVComboBox.setEnabled(false);
		panelNouveauRDVLigne3.add(choixHeure);
		panelNouveauRDVLigne3.add(choixHeureFormulaireNouveauRDVComboBox);
		
		JPanel panelNouveauRDVLigne4 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		ajouterRDVFormulaireNouveauRDV = new JButton("Ajouter");
		panelNouveauRDVLigne4.add(ajouterRDVFormulaireNouveauRDV);
		
		conteneurBasDroiteNouveauRDV.add(panelNouveauRDVLigne1);
		conteneurBasDroiteNouveauRDV.add(panelNouveauRDVLigne2);
		conteneurBasDroiteNouveauRDV.add(panelNouveauRDVLigne3);
		conteneurBasDroiteNouveauRDV.add(panelNouveauRDVLigne4);
	}
	
	private void setPanelBasDroitAfficherRDV() {
		conteneurBasDroiteAfficherRDV = new JPanel(new BorderLayout());
		
		JPanel panelAfficherRDVCentre = new JPanel(new GridLayout(4,1));
		
		JPanel panelAfficherRDVCentreLigne1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel rdvLabel =  new JLabel("RDV :");
		panelAfficherRDVCentreLigne1.add(rdvLabel);
		
		JPanel panelAfficherRDVCentreLigne2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel numeroReferenceLabel =  new JLabel("N�r�f�rence :");
		numeroReferenceFormulaireAfficherRDV = new JTextField();
		numeroReferenceFormulaireAfficherRDV.setPreferredSize(new Dimension(100,20));
		numeroReferenceFormulaireAfficherRDV.setEditable(false);
		panelAfficherRDVCentreLigne2.add(numeroReferenceLabel);
		panelAfficherRDVCentreLigne2.add(numeroReferenceFormulaireAfficherRDV);
		
		JPanel panelAfficherRDVCentreLigne3 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel nomDocteurLabel =  new JLabel("Docteur :");
		JSeparator separateurNom = new JSeparator();
		separateurNom.setPreferredSize(new Dimension(19,0));
		nomPrenomMedecinFormulaireAfficherRDV = new JTextField();
		nomPrenomMedecinFormulaireAfficherRDV.setPreferredSize(new Dimension(150,20));
		nomPrenomMedecinFormulaireAfficherRDV.setEditable(false);
		JSeparator separateurSalle1 = new JSeparator();
		separateurSalle1.setPreferredSize(new Dimension(5,0));
		JLabel numSalleLabel = new JLabel("Salle :");
		JSeparator separateurSalle2 = new JSeparator();
		separateurSalle2.setPreferredSize(new Dimension(5,0));
		salleFormulaireAfficherRDV = new JTextField();
		salleFormulaireAfficherRDV.setPreferredSize(new Dimension(50,20));
		salleFormulaireAfficherRDV.setEditable(false);
		panelAfficherRDVCentreLigne3.add(nomDocteurLabel);
		panelAfficherRDVCentreLigne3.add(separateurNom);
		panelAfficherRDVCentreLigne3.add(nomPrenomMedecinFormulaireAfficherRDV);
		panelAfficherRDVCentreLigne3.add(separateurSalle1);
		panelAfficherRDVCentreLigne3.add(numSalleLabel);
		panelAfficherRDVCentreLigne3.add(separateurSalle2);
		panelAfficherRDVCentreLigne3.add(salleFormulaireAfficherRDV);
		
		JPanel panelAfficherRDVCentreLigne4 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel dateRDVLabel =  new JLabel("Date :");
		JSeparator separateurDate1 = new JSeparator();
		separateurDate1.setPreferredSize(new Dimension(38,0));
		dateRDVFormulaireAfficherRDV = new JTextField();
		dateRDVFormulaireAfficherRDV.setPreferredSize(new Dimension(100,20));
		dateRDVFormulaireAfficherRDV.setEditable(false);
		JSeparator separateurDate2 = new JSeparator();
		separateurDate2.setPreferredSize(new Dimension(49,0));
		JLabel heureRDVLabel = new JLabel("Heure : ");
		JSeparator separateurDate3 = new JSeparator();
		separateurDate3.setPreferredSize(new Dimension(2,0));
		heureRDVFormulaireAfficherRDV = new JTextField();
		heureRDVFormulaireAfficherRDV.setPreferredSize(new Dimension(50,20));
		heureRDVFormulaireAfficherRDV.setEditable(false);
		panelAfficherRDVCentreLigne4.add(dateRDVLabel);
		panelAfficherRDVCentreLigne4.add(separateurDate1);
		panelAfficherRDVCentreLigne4.add(dateRDVFormulaireAfficherRDV);
		panelAfficherRDVCentreLigne4.add(separateurDate2);
		panelAfficherRDVCentreLigne4.add(heureRDVLabel);
		panelAfficherRDVCentreLigne4.add(separateurDate3);
		panelAfficherRDVCentreLigne4.add(heureRDVFormulaireAfficherRDV);
		
		panelAfficherRDVCentre.add(panelAfficherRDVCentreLigne1);
		panelAfficherRDVCentre.add(panelAfficherRDVCentreLigne2);
		panelAfficherRDVCentre.add(panelAfficherRDVCentreLigne3);
		panelAfficherRDVCentre.add(panelAfficherRDVCentreLigne4);
		
		JPanel margeHauteAfficherRDVNord = new JPanel();
		margeHauteAfficherRDVNord.setPreferredSize(new Dimension(0,50));
		JPanel margeHauteAfficherRDVSud = new JPanel();
		margeHauteAfficherRDVSud.setPreferredSize(new Dimension(0,75));
		
		conteneurBasDroiteAfficherRDV.add(margeHauteAfficherRDVNord,BorderLayout.NORTH);
		conteneurBasDroiteAfficherRDV.add(panelAfficherRDVCentre,BorderLayout.CENTER);
		conteneurBasDroiteAfficherRDV.add(margeHauteAfficherRDVSud,BorderLayout.SOUTH);
	}
	
	private void setPanelBasDroitNouvelleConsultation() {
		conteneurBasDroiteNouvelleConsultation = new JPanel(new BorderLayout());
		
		JPanel panelNouvelleConsultationNord = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel consultationLabel =  new JLabel("Consultation :");
		panelNouvelleConsultationNord.add(consultationLabel);
		
		JPanel panelNouvelleConsultationCenter = new JPanel(new GridLayout(1,1,20,20));
		consultationArea = new JTextArea();
		panelNouvelleConsultationCenter.add(consultationArea);
		
		JPanel panelNouvelleConsultationSud = new JPanel(new FlowLayout(FlowLayout.CENTER));
		ajouterConsultation = new JButton("Ajouter");
		panelNouvelleConsultationSud.add(ajouterConsultation);
		
		conteneurBasDroiteNouvelleConsultation.add(panelNouvelleConsultationNord,BorderLayout.NORTH);
		conteneurBasDroiteNouvelleConsultation.add(panelNouvelleConsultationCenter,BorderLayout.CENTER);
		conteneurBasDroiteNouvelleConsultation.add(panelNouvelleConsultationSud,BorderLayout.SOUTH);
	}
	
	private void setPanelBasDroitAfficherConsultation() {
		conteneurBasDroiteAfficherConsultation = new JPanel(new BorderLayout());
		
		JPanel panelAfficherConsultationNord = new JPanel(new GridLayout(4,1));
		
		JPanel panelAfficherConsultationNordLigne1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel consultationLabel =  new JLabel("Consultation :");
		panelAfficherConsultationNordLigne1.add(consultationLabel);
		
		JPanel panelAfficherConsultationNordLigne2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel numeroReferenceLabel =  new JLabel("N�r�f�rence :");
		numeroReferenceFormulaireAfficherConsultation = new JTextField();
		numeroReferenceFormulaireAfficherConsultation.setPreferredSize(new Dimension(100,20));
		numeroReferenceFormulaireAfficherConsultation.setEditable(false);
		panelAfficherConsultationNordLigne2.add(numeroReferenceLabel);
		panelAfficherConsultationNordLigne2.add(numeroReferenceFormulaireAfficherConsultation);
		
		JPanel panelAfficherConsultationNordLigne3 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel nomDocteurLabel =  new JLabel("Docteur :");
		JSeparator separateurNom = new JSeparator();
		separateurNom.setPreferredSize(new Dimension(19,0));
		nomPrenomDocteurFormulaireAfficherConsultation = new JTextField();
		nomPrenomDocteurFormulaireAfficherConsultation.setPreferredSize(new Dimension(150,20));
		nomPrenomDocteurFormulaireAfficherConsultation.setEditable(false);
		panelAfficherConsultationNordLigne3.add(nomDocteurLabel);
		panelAfficherConsultationNordLigne3.add(separateurNom);
		panelAfficherConsultationNordLigne3.add(nomPrenomDocteurFormulaireAfficherConsultation);
		
		JPanel panelAfficherConsultationNordLigne4 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel dateConsultationLabel =  new JLabel("Date :");
		JSeparator separateurDate = new JSeparator();
		separateurDate.setPreferredSize(new Dimension(38,0));
		dateConsultationFormulaireAfficherConsultation = new JTextField();
		dateConsultationFormulaireAfficherConsultation.setPreferredSize(new Dimension(100,20));
		dateConsultationFormulaireAfficherConsultation.setEditable(false);
		panelAfficherConsultationNordLigne4.add(dateConsultationLabel);
		panelAfficherConsultationNordLigne4.add(separateurDate);
		panelAfficherConsultationNordLigne4.add(dateConsultationFormulaireAfficherConsultation);
		
		panelAfficherConsultationNord.add(panelAfficherConsultationNordLigne1);
		panelAfficherConsultationNord.add(panelAfficherConsultationNordLigne2);
		panelAfficherConsultationNord.add(panelAfficherConsultationNordLigne3);
		panelAfficherConsultationNord.add(panelAfficherConsultationNordLigne4);
		
		JPanel panelAfficherConsultationCenter = new JPanel(new GridLayout(1,1,20,20));
		notesConsultationFormulaireAfficherConsultationArea = new JTextArea();
		notesConsultationFormulaireAfficherConsultationArea.setEditable(false);
		panelAfficherConsultationCenter.add(notesConsultationFormulaireAfficherConsultationArea);
		
		conteneurBasDroiteAfficherConsultation.add(panelAfficherConsultationNord,BorderLayout.NORTH);
		conteneurBasDroiteAfficherConsultation.add(panelAfficherConsultationCenter,BorderLayout.CENTER);
	}
	
	private void setPanelBasDroitNouvelleOrdonnance() {
		conteneurBasDroiteNouvelleOrdonnance = new JPanel(new BorderLayout());
		
		JPanel panelNouvelleOrdonnanceNord = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel ordonnanceLabel =  new JLabel("Ordonnance :");
		panelNouvelleOrdonnanceNord.add(ordonnanceLabel);

		
		JPanel panelNouvelleOrdonnanceCenter = new JPanel(new GridLayout(1,1,20,20));
		ordonnanceArea = new JTextArea();
		panelNouvelleOrdonnanceCenter.add(ordonnanceArea);
		
		
		JPanel panelNouvelleOrdonnanceSud = new JPanel(new FlowLayout(FlowLayout.CENTER));
		ajouterOrdonnance = new JButton("Ajouter");
		panelNouvelleOrdonnanceSud.add(ajouterOrdonnance);
		
		conteneurBasDroiteNouvelleOrdonnance.add(panelNouvelleOrdonnanceNord,BorderLayout.NORTH);
		conteneurBasDroiteNouvelleOrdonnance.add(panelNouvelleOrdonnanceCenter,BorderLayout.CENTER);
		conteneurBasDroiteNouvelleOrdonnance.add(panelNouvelleOrdonnanceSud,BorderLayout.SOUTH);
	}
	
	private void setPanelBasDroitAfficherOrdonnance() {
		conteneurBasDroiteAfficherOrdonnance = new JPanel(new BorderLayout());
		
		JPanel panelAfficherOrdonnanceNord = new JPanel(new GridLayout(4,1));
		
		JPanel panelAfficherOrdonnanceNordLigne1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel ordonnanceLabel =  new JLabel("Ordonnance :");
		panelAfficherOrdonnanceNordLigne1.add(ordonnanceLabel);
		
		JPanel panelAfficherOrdonnanceNordLigne2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel numeroReferenceLabel =  new JLabel("N�r�f�rence :");
		numeroReferenceFormulaireAfficherOrdonnance = new JTextField();
		numeroReferenceFormulaireAfficherOrdonnance.setPreferredSize(new Dimension(100,20));
		numeroReferenceFormulaireAfficherOrdonnance.setEditable(false);
		panelAfficherOrdonnanceNordLigne2.add(numeroReferenceLabel);
		panelAfficherOrdonnanceNordLigne2.add(numeroReferenceFormulaireAfficherOrdonnance);
		
		JPanel panelAfficherOrdonnanceNordLigne3 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel nomDocteurLabel =  new JLabel("Docteur :");
		JSeparator separateurNom = new JSeparator();
		separateurNom.setPreferredSize(new Dimension(19,0));
		nomPrenomDocteurFormulaireAfficherOrdonnance = new JTextField();
		nomPrenomDocteurFormulaireAfficherOrdonnance.setPreferredSize(new Dimension(150,20));
		nomPrenomDocteurFormulaireAfficherOrdonnance.setEditable(false);
		panelAfficherOrdonnanceNordLigne3.add(nomDocteurLabel);
		panelAfficherOrdonnanceNordLigne3.add(separateurNom);
		panelAfficherOrdonnanceNordLigne3.add(nomPrenomDocteurFormulaireAfficherOrdonnance);
		
		JPanel panelAfficherOrdonnanceNordLigne4 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel dateOrdonnanceLabel =  new JLabel("Date :");
		JSeparator separateurDate = new JSeparator();
		separateurDate.setPreferredSize(new Dimension(38,0));
		dateOrdonnanceFormulaireAfficherOrdonnance = new JTextField();
		dateOrdonnanceFormulaireAfficherOrdonnance.setPreferredSize(new Dimension(100,20));
		dateOrdonnanceFormulaireAfficherOrdonnance.setEditable(false);
		panelAfficherOrdonnanceNordLigne4.add(dateOrdonnanceLabel);
		panelAfficherOrdonnanceNordLigne4.add(separateurDate);
		panelAfficherOrdonnanceNordLigne4.add(dateOrdonnanceFormulaireAfficherOrdonnance);
		
		panelAfficherOrdonnanceNord.add(panelAfficherOrdonnanceNordLigne1);
		panelAfficherOrdonnanceNord.add(panelAfficherOrdonnanceNordLigne2);
		panelAfficherOrdonnanceNord.add(panelAfficherOrdonnanceNordLigne3);
		panelAfficherOrdonnanceNord.add(panelAfficherOrdonnanceNordLigne4);
		
		JPanel panelAfficherOrdonnanceCenter = new JPanel(new GridLayout(1,1,20,20));
		notesOrdonnanceFormulaireAfficherOrdonnanceArea = new JTextArea();
		notesOrdonnanceFormulaireAfficherOrdonnanceArea.setEditable(false);
		panelAfficherOrdonnanceCenter.add(notesOrdonnanceFormulaireAfficherOrdonnanceArea);
		
		conteneurBasDroiteAfficherOrdonnance.add(panelAfficherOrdonnanceNord,BorderLayout.NORTH);
		conteneurBasDroiteAfficherOrdonnance.add(panelAfficherOrdonnanceCenter,BorderLayout.CENTER);
	}
	
	private void setPanelBasDroitNouvelleAnalyse() {
		conteneurBasDroiteNouvelleAnalyse = new JPanel(new BorderLayout());
		
		JPanel panelNouvelleAnalyseLigne1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel codeAppareilLabel =  new JLabel("Code Appareil :");
		choixAppareilMedicalFormulaireNouvelleAnalyseComboBox = new JComboBox<>();
		panelNouvelleAnalyseLigne1.add(codeAppareilLabel);
		panelNouvelleAnalyseLigne1.add(choixAppareilMedicalFormulaireNouvelleAnalyseComboBox);
		
		JPanel panelNouvelleAnalyseLigne2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel analyseLabel =  new JLabel("Analyse :");
		analyseArea = new JTextArea();
		analyseArea.setPreferredSize(new Dimension(400,300));
		panelNouvelleAnalyseLigne2.add(analyseLabel);
		panelNouvelleAnalyseLigne2.add(analyseArea);
		
		JPanel panelNouvelleAnalyseLigne3 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		ajouterAnalyse = new JButton("Ajouter");
		panelNouvelleAnalyseLigne3.add(ajouterAnalyse);
		
		conteneurBasDroiteNouvelleAnalyse.add(panelNouvelleAnalyseLigne1,BorderLayout.NORTH);
		conteneurBasDroiteNouvelleAnalyse.add(panelNouvelleAnalyseLigne2,BorderLayout.CENTER);
		conteneurBasDroiteNouvelleAnalyse.add(panelNouvelleAnalyseLigne3,BorderLayout.SOUTH);
	}
	
	private void setPanelBasDroitAfficherAnalyse() {
		conteneurBasDroiteAfficherAnalyse = new JPanel(new BorderLayout());
		
		JPanel panelAfficherAnalyseNord = new JPanel(new GridLayout(4,1));
		
		JPanel panelAfficherAnalyseNordLigne1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel analyseLabel =  new JLabel("Analyse :");
		panelAfficherAnalyseNordLigne1.add(analyseLabel);
		
		JPanel panelAfficherAnalyseNordLigne2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel numeroReferenceLabel =  new JLabel("N�r�f�rence :");
		numeroReferenceFormulaireAfficherAnalyse = new JTextField();
		numeroReferenceFormulaireAfficherAnalyse.setPreferredSize(new Dimension(100,20));
		numeroReferenceFormulaireAfficherAnalyse.setEditable(false);
		panelAfficherAnalyseNordLigne2.add(numeroReferenceLabel);
		panelAfficherAnalyseNordLigne2.add(numeroReferenceFormulaireAfficherAnalyse);
		
		JPanel panelAfficherAnalyseNordLigne3 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel nomDocteurLabel =  new JLabel("Technicien :");
		JSeparator separateurNom = new JSeparator();
		separateurNom.setPreferredSize(new Dimension(2,0));
		nomPrenomTechnicienFormulaireAfficherAnalyse = new JTextField();
		nomPrenomTechnicienFormulaireAfficherAnalyse.setPreferredSize(new Dimension(150,20));
		nomPrenomTechnicienFormulaireAfficherAnalyse.setEditable(false);
		JSeparator separateurAppareil1 = new JSeparator();
		separateurAppareil1.setPreferredSize(new Dimension(5,0));
		JLabel codeAppareilLabel = new JLabel("Appareil :");
		JSeparator separateurAppareil2 = new JSeparator();
		separateurAppareil2.setPreferredSize(new Dimension(5,0));
		appareilFormulaireAfficherAnalyse = new JTextField();
		appareilFormulaireAfficherAnalyse.setPreferredSize(new Dimension(50,20));
		appareilFormulaireAfficherAnalyse.setEditable(false);
		panelAfficherAnalyseNordLigne3.add(nomDocteurLabel);
		panelAfficherAnalyseNordLigne3.add(separateurNom);
		panelAfficherAnalyseNordLigne3.add(nomPrenomTechnicienFormulaireAfficherAnalyse);
		panelAfficherAnalyseNordLigne3.add(separateurAppareil1);
		panelAfficherAnalyseNordLigne3.add(codeAppareilLabel);
		panelAfficherAnalyseNordLigne3.add(separateurAppareil2);
		panelAfficherAnalyseNordLigne3.add(appareilFormulaireAfficherAnalyse);
		
		JPanel panelAfficherAnalyseNordLigne4 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel dateAnalyseLabel =  new JLabel("Date :");
		JSeparator separateurDate1 = new JSeparator();
		separateurDate1.setPreferredSize(new Dimension(38,0));
		dateAnalyseFormulaireAfficherAnalyse = new JTextField();
		dateAnalyseFormulaireAfficherAnalyse.setPreferredSize(new Dimension(100,20));
		dateAnalyseFormulaireAfficherAnalyse.setEditable(false);
		JSeparator separateurDate2 = new JSeparator();
		separateurDate2.setPreferredSize(new Dimension(65,0));
		JLabel heureAnalyseLabel = new JLabel("Heure : ");
		JSeparator separateurDate3 = new JSeparator();
		separateurDate3.setPreferredSize(new Dimension(5,0));
		heureAnalyseFormulaireAfficherAnalyse = new JTextField();
		heureAnalyseFormulaireAfficherAnalyse.setPreferredSize(new Dimension(50,20));
		heureAnalyseFormulaireAfficherAnalyse.setEditable(false);
		panelAfficherAnalyseNordLigne4.add(dateAnalyseLabel);
		panelAfficherAnalyseNordLigne4.add(separateurDate1);
		panelAfficherAnalyseNordLigne4.add(dateAnalyseFormulaireAfficherAnalyse);
		panelAfficherAnalyseNordLigne4.add(separateurDate2);
		panelAfficherAnalyseNordLigne4.add(heureAnalyseLabel);
		panelAfficherAnalyseNordLigne4.add(separateurDate3);
		panelAfficherAnalyseNordLigne4.add(heureAnalyseFormulaireAfficherAnalyse);
		
		panelAfficherAnalyseNord.add(panelAfficherAnalyseNordLigne1);
		panelAfficherAnalyseNord.add(panelAfficherAnalyseNordLigne2);
		panelAfficherAnalyseNord.add(panelAfficherAnalyseNordLigne3);
		panelAfficherAnalyseNord.add(panelAfficherAnalyseNordLigne4);
		
		JPanel panelAfficherAnalyseCenter = new JPanel(new GridLayout(1,1,20,20));
		notesAnalyseFormulaireAfficherAnalyseArea = new JTextArea();
		notesAnalyseFormulaireAfficherAnalyseArea.setEditable(false);
		panelAfficherAnalyseCenter.add(notesAnalyseFormulaireAfficherAnalyseArea);
		
		conteneurBasDroiteAfficherAnalyse.add(panelAfficherAnalyseNord,BorderLayout.NORTH);
		conteneurBasDroiteAfficherAnalyse.add(panelAfficherAnalyseCenter,BorderLayout.CENTER);
	}
	
	private void setPanelBasDroitHospitalisation() {
		conteneurBasDroiteHospitalisation = new JPanel(new GridLayout(7,1));
		choixAutoManuelFormulaireHospitalisation = new ButtonGroup();
		choixTypeChambreFormulaireHospitalisation = new ButtonGroup();
		
		JPanel conteneurBasDroiteHospitalisationLigne1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel codeServiceLabel = new JLabel("Code Service : ");
		codeServiceFormulaireHospitalisation = new JTextField();
		codeServiceFormulaireHospitalisation.setPreferredSize(new Dimension(15,20));
		JSeparator espaceElement1 = new JSeparator();
		espaceElement1.setPreferredSize(new Dimension(10,0));
		JLabel dateDebutHospitalisation = new JLabel("Date d�but :");
		JSeparator espaceElement2 = new JSeparator();
		espaceElement2.setPreferredSize(new Dimension(5,0));
		dateDebutFormulaireHospitalisationComboBox = new JComboBox<String>();
		JSeparator espaceElement3 = new JSeparator();
		espaceElement3.setPreferredSize(new Dimension(10,0));
		JLabel dateFinHospitalisation = new JLabel("Date fin :");
		JSeparator espaceElement4 = new JSeparator();
		espaceElement4.setPreferredSize(new Dimension(5,0));
		dateFinFormulaireHospitalisationComboBox = new JComboBox<String>();
		conteneurBasDroiteHospitalisationLigne1.add(codeServiceLabel);
		conteneurBasDroiteHospitalisationLigne1.add(codeServiceFormulaireHospitalisation);
		conteneurBasDroiteHospitalisationLigne1.add(espaceElement1);
		conteneurBasDroiteHospitalisationLigne1.add(dateDebutHospitalisation);
		conteneurBasDroiteHospitalisationLigne1.add(espaceElement2);
		conteneurBasDroiteHospitalisationLigne1.add(dateDebutFormulaireHospitalisationComboBox);
		conteneurBasDroiteHospitalisationLigne1.add(espaceElement3);
		conteneurBasDroiteHospitalisationLigne1.add(dateFinHospitalisation);
		conteneurBasDroiteHospitalisationLigne1.add(espaceElement4);
		conteneurBasDroiteHospitalisationLigne1.add(dateFinFormulaireHospitalisationComboBox);
		
		JPanel conteneurBasDroiteHospitalisationLigne2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		choixAutomatiqueFormulaireHospitalisation = new JRadioButton();
		choixAutomatiqueFormulaireHospitalisation.setActionCommand("automatique");
		choixAutoManuelFormulaireHospitalisation.add(choixAutomatiqueFormulaireHospitalisation);
		choixAutomatiqueFormulaireHospitalisation.setSelected(true);
		JLabel choixAutoLabel = new JLabel("Automatique");
		conteneurBasDroiteHospitalisationLigne2.add(choixAutomatiqueFormulaireHospitalisation);
		conteneurBasDroiteHospitalisationLigne2.add(choixAutoLabel);
		
		JPanel conteneurBasDroiteHospitalisationLigne3 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		choixManuelFormulaireHospitalisation = new JRadioButton();
		choixManuelFormulaireHospitalisation.setActionCommand("manuel");
		choixAutoManuelFormulaireHospitalisation.add(choixManuelFormulaireHospitalisation);
		JLabel choixManuelLabel = new JLabel("Manuel");
		conteneurBasDroiteHospitalisationLigne3.add(choixManuelFormulaireHospitalisation);
		conteneurBasDroiteHospitalisationLigne3.add(choixManuelLabel);
		
		JPanel conteneurBasDroiteHospitalisationLigne4 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JSeparator alineaLigne4 = new JSeparator();
		alineaLigne4.setPreferredSize(new Dimension(50,0));
		choixChambreIndividuelleFormulaireHospitalisation = new JRadioButton();
		choixChambreIndividuelleFormulaireHospitalisation.setActionCommand("individuel");
		choixTypeChambreFormulaireHospitalisation.add(choixChambreIndividuelleFormulaireHospitalisation);
		choixChambreIndividuelleFormulaireHospitalisation.setEnabled(false);
		JLabel choixIndividuelLabel = new JLabel("Individuel");
		chambreIndividuelleFormulaireHospitalisationComboBox = new JComboBox<String>();
		chambreIndividuelleFormulaireHospitalisationComboBox.setEnabled(false);
		conteneurBasDroiteHospitalisationLigne4.add(alineaLigne4);
		conteneurBasDroiteHospitalisationLigne4.add(choixChambreIndividuelleFormulaireHospitalisation);
		conteneurBasDroiteHospitalisationLigne4.add(choixIndividuelLabel);
		conteneurBasDroiteHospitalisationLigne4.add(chambreIndividuelleFormulaireHospitalisationComboBox);
		
		JPanel conteneurBasDroiteHospitalisationLigne5 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JSeparator alineaLigne5 = new JSeparator();
		alineaLigne5.setPreferredSize(new Dimension(50,0));
		choixChambreDoubleFormulaireHospitalisation = new JRadioButton();
		choixChambreDoubleFormulaireHospitalisation.setActionCommand("double");
		choixTypeChambreFormulaireHospitalisation.add(choixChambreDoubleFormulaireHospitalisation);
		choixChambreDoubleFormulaireHospitalisation.setEnabled(false);
		JLabel choixDoubleLabel = new JLabel("Double");
		chambreDoubleFormulaireHospitalisationComboBox = new JComboBox<String>();
		chambreDoubleFormulaireHospitalisationComboBox.setEnabled(false);
		conteneurBasDroiteHospitalisationLigne5.add(alineaLigne5);
		conteneurBasDroiteHospitalisationLigne5.add(choixChambreDoubleFormulaireHospitalisation);
		conteneurBasDroiteHospitalisationLigne5.add(choixDoubleLabel);
		conteneurBasDroiteHospitalisationLigne5.add(chambreDoubleFormulaireHospitalisationComboBox);
		
		JPanel conteneurBasDroiteHospitalisationLigne6 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JSeparator alineaLigne6 = new JSeparator();
		alineaLigne6.setPreferredSize(new Dimension(50,0));
		choixChambreQuadrupleFormulaireHospitalisation = new JRadioButton();
		choixChambreQuadrupleFormulaireHospitalisation.setActionCommand("quadruple");
		choixTypeChambreFormulaireHospitalisation.add(choixChambreQuadrupleFormulaireHospitalisation);
		choixChambreQuadrupleFormulaireHospitalisation.setEnabled(false);
		JLabel choixQuadrupleLabel = new JLabel("Quadruple");
		chambreQuadrupleFormulaireHospitalisationComboBox = new JComboBox<String>();
		chambreQuadrupleFormulaireHospitalisationComboBox.setEnabled(false);
		conteneurBasDroiteHospitalisationLigne6.add(alineaLigne6);
		conteneurBasDroiteHospitalisationLigne6.add(choixChambreQuadrupleFormulaireHospitalisation);
		conteneurBasDroiteHospitalisationLigne6.add(choixQuadrupleLabel);
		conteneurBasDroiteHospitalisationLigne6.add(chambreQuadrupleFormulaireHospitalisationComboBox);
		
		JPanel conteneurBasDroiteHospitalisationLigne7 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		hospitaliserFormulaireHospitalisation = new JButton("Hospitaliser");
		conteneurBasDroiteHospitalisationLigne7.add(hospitaliserFormulaireHospitalisation);
		
		conteneurBasDroiteHospitalisation.add(conteneurBasDroiteHospitalisationLigne1);
		conteneurBasDroiteHospitalisation.add(conteneurBasDroiteHospitalisationLigne2);
		conteneurBasDroiteHospitalisation.add(conteneurBasDroiteHospitalisationLigne3);
		conteneurBasDroiteHospitalisation.add(conteneurBasDroiteHospitalisationLigne4);
		conteneurBasDroiteHospitalisation.add(conteneurBasDroiteHospitalisationLigne5);
		conteneurBasDroiteHospitalisation.add(conteneurBasDroiteHospitalisationLigne6);
		conteneurBasDroiteHospitalisation.add(conteneurBasDroiteHospitalisationLigne7);
	}
	
	private void setPanelBasDroitModifierPatient() {
		conteneurBasDroiteModifierPatient = new JPanel(new GridLayout(8,1));
		
		choixModificationFormulaireModifierPatient = new ButtonGroup();
			
		JPanel conteneurBasDroiteModifierPatientLigne1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JRadioButton choixNomFormulaireModifierPatient = new JRadioButton();
		choixModificationFormulaireModifierPatient.add(choixNomFormulaireModifierPatient);
		choixNomFormulaireModifierPatient.setActionCommand("nom");
		choixNomFormulaireModifierPatient.addActionListener(new choixNomFormulaireModifierPatientListener());
		choixNomFormulaireModifierPatient.setSelected(true);
		JLabel nom = new JLabel("Nom :");
		nomFormulaireModifierPatient = new JTextField();
		nomFormulaireModifierPatient.setPreferredSize(new Dimension(150,20));
		conteneurBasDroiteModifierPatientLigne1.add(choixNomFormulaireModifierPatient);
		conteneurBasDroiteModifierPatientLigne1.add(nom);
		conteneurBasDroiteModifierPatientLigne1.add(nomFormulaireModifierPatient);
		
		JPanel conteneurBasDroiteModifierPatientLigne2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JRadioButton choixPrenomFormulaireModifierPatient = new JRadioButton();
		choixModificationFormulaireModifierPatient.add(choixPrenomFormulaireModifierPatient);
		choixPrenomFormulaireModifierPatient.setActionCommand("prenom");
		choixPrenomFormulaireModifierPatient.addActionListener(new choixPrenomFormulaireModifierPatientListener());
		JLabel prenom = new JLabel("Pr�nom :");
		prenomFormulaireModifierPatient = new JTextField();
		prenomFormulaireModifierPatient.setPreferredSize(new Dimension(150,20));
		prenomFormulaireModifierPatient.setEnabled(false);
		conteneurBasDroiteModifierPatientLigne2.add(choixPrenomFormulaireModifierPatient);
		conteneurBasDroiteModifierPatientLigne2.add(prenom);
		conteneurBasDroiteModifierPatientLigne2.add(prenomFormulaireModifierPatient);
		
		JPanel conteneurBasDroiteModifierPatientLigne3 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JRadioButton choixSexeFormulaireModifierPatient = new JRadioButton();
		choixModificationFormulaireModifierPatient.add(choixSexeFormulaireModifierPatient);
		choixSexeFormulaireModifierPatient.setActionCommand("sexe");
		choixSexeFormulaireModifierPatient.addActionListener(new choixSexeFormulaireModifierPatientListener());
		JLabel sexe = new JLabel("Sexe :");
		buttonGroupSexeFormulaireModifierPatient = new ButtonGroup();
		masculinFormulaireModifierPatient = new JRadioButton();
		buttonGroupSexeFormulaireModifierPatient.add(masculinFormulaireModifierPatient);
		masculinFormulaireModifierPatient.setActionCommand("masculin");
		masculinFormulaireModifierPatient.setEnabled(false);
		JLabel masculin = new JLabel("Masculin");
		femininFormulaireModifierPatient = new JRadioButton();
		buttonGroupSexeFormulaireModifierPatient.add(femininFormulaireModifierPatient);
		femininFormulaireModifierPatient.setActionCommand("feminin");
		femininFormulaireModifierPatient.setEnabled(false);
		JLabel feminin = new JLabel("Feminin");
		conteneurBasDroiteModifierPatientLigne3.add(choixSexeFormulaireModifierPatient);
		conteneurBasDroiteModifierPatientLigne3.add(sexe);
		conteneurBasDroiteModifierPatientLigne3.add(masculinFormulaireModifierPatient);
		conteneurBasDroiteModifierPatientLigne3.add(masculin);
		conteneurBasDroiteModifierPatientLigne3.add(femininFormulaireModifierPatient);
		conteneurBasDroiteModifierPatientLigne3.add(feminin);
		
		JPanel conteneurBasDroiteModifierPatientLigne4 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JRadioButton choixDateDeNaissanceFormulaireModifierPatient = new JRadioButton();
		choixModificationFormulaireModifierPatient.add(choixDateDeNaissanceFormulaireModifierPatient);
		choixDateDeNaissanceFormulaireModifierPatient.setActionCommand("dateDeNaissance");
		choixDateDeNaissanceFormulaireModifierPatient.addActionListener(new choixDateDeNaissanceFormulaireModifierPatientListener());
		JLabel dateDeNaissance = new JLabel("Date de Naissance :");
		dateDeNaissanceFormulaireModifierPatient = new JTextField();
		dateDeNaissanceFormulaireModifierPatient.setPreferredSize(new Dimension(68,20));
		dateDeNaissanceFormulaireModifierPatient.setEnabled(false);
		conteneurBasDroiteModifierPatientLigne4.add(choixDateDeNaissanceFormulaireModifierPatient);
		conteneurBasDroiteModifierPatientLigne4.add(dateDeNaissance);
		conteneurBasDroiteModifierPatientLigne4.add(dateDeNaissanceFormulaireModifierPatient);
		
		JPanel conteneurBasDroiteModifierPatientLigne5 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JRadioButton choixAdresseFormulaireModifierPatient = new JRadioButton();
		choixModificationFormulaireModifierPatient.add(choixAdresseFormulaireModifierPatient);
		choixAdresseFormulaireModifierPatient.setActionCommand("adresse");
		choixAdresseFormulaireModifierPatient.addActionListener(new choixAdresseFormulaireModifierPatientListener());
		JLabel adresse = new JLabel("Adresse :");
		adresseFormulaireModifierPatient = new JTextArea();
		adresseFormulaireModifierPatient.setPreferredSize(new Dimension(300,40));
		adresseFormulaireModifierPatient.setEnabled(false);
		conteneurBasDroiteModifierPatientLigne5.add(choixAdresseFormulaireModifierPatient);
		conteneurBasDroiteModifierPatientLigne5.add(adresse);
		conteneurBasDroiteModifierPatientLigne5.add(adresseFormulaireModifierPatient);
		
		JPanel conteneurBasDroiteModifierPatientLigne6 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JRadioButton choixMailFormulaireModifierPatient = new JRadioButton();
		choixModificationFormulaireModifierPatient.add(choixMailFormulaireModifierPatient);
		choixMailFormulaireModifierPatient.setActionCommand("mail");
		choixMailFormulaireModifierPatient.addActionListener(new choixMailFormulaireModifierPatientListener());
		JLabel mail = new JLabel("Adresse Mail :");
		mailFormulaireModifierPatient = new JTextField();
		mailFormulaireModifierPatient.setPreferredSize(new Dimension(275,20));
		mailFormulaireModifierPatient.setEnabled(false);
		conteneurBasDroiteModifierPatientLigne6.add(choixMailFormulaireModifierPatient);
		conteneurBasDroiteModifierPatientLigne6.add(mail);
		conteneurBasDroiteModifierPatientLigne6.add(mailFormulaireModifierPatient);
		
		JPanel conteneurBasDroiteModifierPatientLigne7 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JRadioButton choixNumeroFormulaireModifierPatient = new JRadioButton();
		choixModificationFormulaireModifierPatient.add(choixNumeroFormulaireModifierPatient);
		choixNumeroFormulaireModifierPatient.setActionCommand("numero");
		choixNumeroFormulaireModifierPatient.addActionListener(new choixNumeroFormulaireModifierPatientListener());
		JLabel numero = new JLabel("Num�ro de t�l�phone :");
		numeroFormulaireModifierPatient = new JTextField();
		numeroFormulaireModifierPatient.setPreferredSize(new Dimension(77,20));
		numeroFormulaireModifierPatient.setEnabled(false);
		conteneurBasDroiteModifierPatientLigne7.add(choixNumeroFormulaireModifierPatient);
		conteneurBasDroiteModifierPatientLigne7.add(numero);
		conteneurBasDroiteModifierPatientLigne7.add(numeroFormulaireModifierPatient);
		
		JPanel conteneurBasDroiteModifierPatientLigne8 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		modifierFormulaireModifierPatientButton = new JButton("Modifier");
		conteneurBasDroiteModifierPatientLigne8.add(modifierFormulaireModifierPatientButton);
		
		conteneurBasDroiteModifierPatient.add(conteneurBasDroiteModifierPatientLigne1);
		conteneurBasDroiteModifierPatient.add(conteneurBasDroiteModifierPatientLigne2);
		conteneurBasDroiteModifierPatient.add(conteneurBasDroiteModifierPatientLigne3);
		conteneurBasDroiteModifierPatient.add(conteneurBasDroiteModifierPatientLigne4);
		conteneurBasDroiteModifierPatient.add(conteneurBasDroiteModifierPatientLigne5);
		conteneurBasDroiteModifierPatient.add(conteneurBasDroiteModifierPatientLigne6);
		conteneurBasDroiteModifierPatient.add(conteneurBasDroiteModifierPatientLigne7);
		conteneurBasDroiteModifierPatient.add(conteneurBasDroiteModifierPatientLigne8);
	}
	
	private void setPanelBasDroitSupprimerPatient() {
		conteneurBasDroiteSupprimerPatient = new JPanel(new BorderLayout());
		
		JPanel conteneurBasDroiteSupprimerPatientCenter = new JPanel(new GridLayout(2,1));
		
		JPanel conteneurBasDroiteSupprimerPatientCenterLigne1 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		JLabel avertissement = new JLabel("�tes-vous s�rs de vouloir supprimer ce patient ?");
		conteneurBasDroiteSupprimerPatientCenterLigne1.add(avertissement);
		
		JPanel conteneurBasDroiteSupprimerPatientCenterLigne2 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		ouiSupprimer = new JButton("Oui");
		JSeparator separateurBouttonFormulaireSupprimerPatient = new JSeparator();
		separateurBouttonFormulaireSupprimerPatient.setPreferredSize(new Dimension(20,0));
		JButton nonSupprimer = new JButton("Non");
		nonSupprimer.addActionListener(new BouttonNonFormulaireSupprimerPatientListener());
		conteneurBasDroiteSupprimerPatientCenterLigne2.add(ouiSupprimer);
		conteneurBasDroiteSupprimerPatientCenterLigne2.add(separateurBouttonFormulaireSupprimerPatient);
		conteneurBasDroiteSupprimerPatientCenterLigne2.add(nonSupprimer);
		
		conteneurBasDroiteSupprimerPatientCenter.add(conteneurBasDroiteSupprimerPatientCenterLigne1);
		conteneurBasDroiteSupprimerPatientCenter.add(conteneurBasDroiteSupprimerPatientCenterLigne2);
		
		JPanel conteneurBasDroiteSupprimerPatientNord = new JPanel();
		conteneurBasDroiteSupprimerPatientNord.setPreferredSize(new Dimension(0,125));
		JPanel conteneurBasDroiteSupprimerPatientSud = new JPanel();
		conteneurBasDroiteSupprimerPatientSud.setPreferredSize(new Dimension(0,100));
		
		conteneurBasDroiteSupprimerPatient.add(conteneurBasDroiteSupprimerPatientNord,BorderLayout.NORTH);
		conteneurBasDroiteSupprimerPatient.add(conteneurBasDroiteSupprimerPatientCenter,BorderLayout.CENTER);
		conteneurBasDroiteSupprimerPatient.add(conteneurBasDroiteSupprimerPatientSud,BorderLayout.SOUTH);
	}

	// BouttonsListener 
	
	class BouttonNouveauRDVListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			layoutMultiPanel.show(panelBasDroit, "panelNouveauRDV");
		}
	}
	
	class BouttonNouvelleConsultationListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			layoutMultiPanel.show(panelBasDroit, "panelNouvelleConsultation");
		}
	}
	
	class BouttonNouvelleOrdonnanceListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			layoutMultiPanel.show(panelBasDroit, "panelNouvelleOrdonnance");
		}
	}
	
	class BouttonNouvelleAnalyseListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			layoutMultiPanel.show(panelBasDroit, "panelNouvelleAnalyse");
		}
	}
	
	class BouttonHospitalisationListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			layoutMultiPanel.show(panelBasDroit, "panelHospitalisation");
		}
	}
	
	class BouttonModifierPatientListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			layoutMultiPanel.show(panelBasDroit, "panelModifierPatient");
		}
	}
	
	class BouttonSupprimerPatientListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			layoutMultiPanel.show(panelBasDroit, "panelSupprimerPatient");
		}
	}
	
	class BouttonNonFormulaireSupprimerPatientListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			layoutMultiPanel.show(panelBasDroit, "panelVide");
		}
	}
	
	class choixNomFormulaireModifierPatientListener implements ActionListener {
		
		public void actionPerformed(ActionEvent event) {
			nomFormulaireModifierPatient.setEnabled(true);
			prenomFormulaireModifierPatient.setEnabled(false);
			masculinFormulaireModifierPatient.setEnabled(false);
			femininFormulaireModifierPatient.setEnabled(false);
			dateDeNaissanceFormulaireModifierPatient.setEnabled(false);
			adresseFormulaireModifierPatient.setEnabled(false);
			mailFormulaireModifierPatient.setEnabled(false);
			numeroFormulaireModifierPatient.setEnabled(false);
		}
	}
	
	class choixPrenomFormulaireModifierPatientListener implements ActionListener {

		public void actionPerformed(ActionEvent event) {
			nomFormulaireModifierPatient.setEnabled(false);
			prenomFormulaireModifierPatient.setEnabled(true);
			masculinFormulaireModifierPatient.setEnabled(false);
			femininFormulaireModifierPatient.setEnabled(false);
			dateDeNaissanceFormulaireModifierPatient.setEnabled(false);
			adresseFormulaireModifierPatient.setEnabled(false);
			mailFormulaireModifierPatient.setEnabled(false);
			numeroFormulaireModifierPatient.setEnabled(false);
		}
	}
	
	class choixSexeFormulaireModifierPatientListener implements ActionListener {

		public void actionPerformed(ActionEvent event) {
			nomFormulaireModifierPatient.setEnabled(false);
			prenomFormulaireModifierPatient.setEnabled(false);
			masculinFormulaireModifierPatient.setEnabled(true);
			femininFormulaireModifierPatient.setEnabled(true);
			dateDeNaissanceFormulaireModifierPatient.setEnabled(false);
			adresseFormulaireModifierPatient.setEnabled(false);
			mailFormulaireModifierPatient.setEnabled(false);
			numeroFormulaireModifierPatient.setEnabled(false);
		}
	}
	
	class choixDateDeNaissanceFormulaireModifierPatientListener implements ActionListener {

		public void actionPerformed(ActionEvent event) {
			nomFormulaireModifierPatient.setEnabled(false);
			prenomFormulaireModifierPatient.setEnabled(false);
			masculinFormulaireModifierPatient.setEnabled(false);
			femininFormulaireModifierPatient.setEnabled(false);
			dateDeNaissanceFormulaireModifierPatient.setEnabled(true);
			adresseFormulaireModifierPatient.setEnabled(false);
			mailFormulaireModifierPatient.setEnabled(false);
			numeroFormulaireModifierPatient.setEnabled(false);
		}
	}
	
	class choixAdresseFormulaireModifierPatientListener implements ActionListener {

		public void actionPerformed(ActionEvent event) {
			nomFormulaireModifierPatient.setEnabled(false);
			prenomFormulaireModifierPatient.setEnabled(false);
			masculinFormulaireModifierPatient.setEnabled(false);
			femininFormulaireModifierPatient.setEnabled(false);
			dateDeNaissanceFormulaireModifierPatient.setEnabled(false);
			adresseFormulaireModifierPatient.setEnabled(true);
			mailFormulaireModifierPatient.setEnabled(false);
			numeroFormulaireModifierPatient.setEnabled(false);
		}
	}
	
	class choixMailFormulaireModifierPatientListener implements ActionListener {

		public void actionPerformed(ActionEvent event) {
			nomFormulaireModifierPatient.setEnabled(false);
			prenomFormulaireModifierPatient.setEnabled(false);
			masculinFormulaireModifierPatient.setEnabled(false);
			femininFormulaireModifierPatient.setEnabled(false);
			dateDeNaissanceFormulaireModifierPatient.setEnabled(false);
			adresseFormulaireModifierPatient.setEnabled(false);
			mailFormulaireModifierPatient.setEnabled(true);
			numeroFormulaireModifierPatient.setEnabled(false);
		}
	}
	
	class choixNumeroFormulaireModifierPatientListener implements ActionListener {

		public void actionPerformed(ActionEvent event) {
			nomFormulaireModifierPatient.setEnabled(false);
			prenomFormulaireModifierPatient.setEnabled(false);
			masculinFormulaireModifierPatient.setEnabled(false);
			femininFormulaireModifierPatient.setEnabled(false);
			dateDeNaissanceFormulaireModifierPatient.setEnabled(false);
			adresseFormulaireModifierPatient.setEnabled(false);
			mailFormulaireModifierPatient.setEnabled(false);
			numeroFormulaireModifierPatient.setEnabled(true);
		}
	}
	
	// Getters
	// Donn�es Personnelles 
	public JTextField getIdFormulaireDonneesPersonnellesTextField() {
		return this.idFormulaireDonneesPersonnellesTextField;
	}
	
	public JTextField getNomFormulaireDonneesPersonnellesTextField() {
		return this.nomFormulaireDonneesPersonnellesTextField;
	}
	
	public JTextField getPrenomFormulaireDonneesPersonnellesTextField() {
		return this.prenomFormulaireDonneesPersonnellesTextField;
	}
	
	public JRadioButton getMasculinFormulaireDonneesPersonnellesRadio() {
		return this.masculinFormulaireDonneesPersonnellesRadio;
	}
	
	public JRadioButton getFemininFormulaireDonneesPersonnellesRadio() {
		return this.femininFormulaireDonneesPersonnellesRadio;
	}
	
	public JTextField getDateDeNaissanceFormulaireDonneesPersonnellesTextField() {
		return this.dateDeNaissanceFormulaireDonneesPersonnellesTextField;
	}
	
	public JTextField getAdresseFormulaireDonneesPersonnellesTextField() {
		return this.adresseFormulaireDonneesPersonnellesTextField;
	}
	
	public JTextField getMailFormulaireDonneesPersonnellesTextField() {
		return this.mailFormulaireDonneesPersonnellesTextField;
	}
	
	public JTextField getNumeroFormulaireDonneesPersonnellesTextField() {
		return this.numeroFormulaireDonneesPersonnellesTextField;
	}
	
	public JRadioButton getOuiHospitaliseFormulaireDonneesPersonnellesRadio() {
		return this.ouiHospitaliseFormulaireDonneesPersonnellesRadio;
	}
	
	public JRadioButton getNonHospitaliseFormulaireDonneesPersonnellesRadio() {
		return this.nonHospitaliseFormulaireDonneesPersonnellesRadio;
	}
	
	public JTextField getNumeroLitHospitaliserDonneesPersonnelles() {
		return this.numeroLitHospitaliserDonneesPersonnelles;
	}
	
	// BAS GAUCHE PATHOS
	public JTextArea getPathologieArea() {
		return this.pathologieArea;
	}
	
	public JButton getModifierPathologiesFormulairePathologie() {
		return this.modifierPathologiesFormulairePathologie;
	}
	
	// Nouveau RDV
	public JComboBox<String> getChoixMedecinFormulaireNouveauRDVComboBox(){
		return this.choixMedecinFormulaireNouveauRDVComboBox;
	}
	
	public JComboBox<String> getChoixDateFormulaireNouveauRDVComboBox() {
		return this.choixDateFormulaireNouveauRDVComboBox;
	}
	
	public JComboBox<String> getChoixHeureFormulaireNouveauRDVComboBox(){
		return this.choixHeureFormulaireNouveauRDVComboBox;
	}
	
	public JButton getAjouterRDVFormulaireNouveauRDV() {
		return this.ajouterRDVFormulaireNouveauRDV;
	}
	
	// Nouvelle Consultation
	public JTextArea getConsultationArea() {
		return this.consultationArea;
	}
	
	public JButton getAjouterConsultation() {
		return this.ajouterConsultation;
	}
	
	// Nouvelle Ordonnance
	public JTextArea getOrdonnanceArea() {
		return this.ordonnanceArea;
	}
	public JButton getAjouterOrdonnance() {
		return this.ajouterOrdonnance;
	}
	// tableaux
	public JTable getTableauRDVPatient() {
		return this.tableauRDVPatient;
	}
	
	public JTable getTableauConsultationPatient() {
		return this.tableauConsultationPatient;
	}
	
	public JTable getTableauOrdonnancePatient() {
		return this.tableauOrdonnancePatient;
	}
	
	public JTable getTableauAnalysePatient() {
		return this.tableauAnalysePatient;
	}
	// Nouvelle Analyse
	
	public JComboBox<String> getChoixAppareilMedicalFormulaireNouvelleAnalyseComboBox(){
		return this.choixAppareilMedicalFormulaireNouvelleAnalyseComboBox;
	}
	
	public JTextArea getAnalyseArea() {
		return this.analyseArea;
	}
	
	public JButton getAjouterAnalyse() {
		return this.ajouterAnalyse;
	}
	
	// SUPPR PATIENT
	
	public JButton getOuiSupprimer() {
		return this.ouiSupprimer;
	}
	
	// AFFICHER RDV
	
	public JTextField getNumeroReferenceFormulaireAfficherRDV() {
		return this.numeroReferenceFormulaireAfficherRDV;
	}
	
	public JTextField getNomPrenomMedecinFormulaireAfficherRDV() {
		return this.nomPrenomMedecinFormulaireAfficherRDV;
	}
	
	public JTextField getSalleFormulaireAfficherRDV() {
		return this.salleFormulaireAfficherRDV;
	}
	
	public JTextField getDateRDVFormulaireAfficherRDV() {
		return this.dateRDVFormulaireAfficherRDV;
	}
	
	public JTextField getHeureRDVFormulaireAfficherRDV() {
		return this.heureRDVFormulaireAfficherRDV;
	}
	
	// AFFIHCER CONSULT
	
	public JTextField getNumeroReferenceFormulaireAfficherConsultation() {
		return this.numeroReferenceFormulaireAfficherConsultation;
	}
	
	public JTextField getNomPrenomDocteurFormulaireAfficherConsultation() {
		return this.nomPrenomDocteurFormulaireAfficherConsultation;
	}
	
	public JTextField getDateConsultationFormulaireAfficherConsultation() {
		return this.dateConsultationFormulaireAfficherConsultation;
	}
	
	public JTextArea getNotesConsultationFormulaireAfficherConsultationArea() {
		return this.notesConsultationFormulaireAfficherConsultationArea;
	}
	
	// AFFICHER ORDONNANCE
	
	public JTextField getNumeroReferenceFormulaireAfficherOrdonnance() {
		return this.numeroReferenceFormulaireAfficherOrdonnance;
	}
	
	public JTextField getNomPrenomDocteurFormulaireAfficherOrdonnance() {
		return this.nomPrenomDocteurFormulaireAfficherOrdonnance;
	}
	
	public JTextField getDateOrdonnanceFormulaireAfficherOrdonnance() {
		return this.dateOrdonnanceFormulaireAfficherOrdonnance;
	}
	
	public JTextArea getNotesOrdonnanceFormulaireAfficherOrdonnanceArea() {
		return this.notesOrdonnanceFormulaireAfficherOrdonnanceArea;
	}
	
	// AFFICHER ANALYSE
	
	public JTextField getNumeroReferenceFormulaireAfficherAnalyse() {
		return this.numeroReferenceFormulaireAfficherAnalyse;
	}
	
	public JTextField getNomPrenomTechnicienFormulaireAfficherAnalyse() {
		return this.nomPrenomTechnicienFormulaireAfficherAnalyse;
	}
	
	public JTextField getAppareilFormulaireAfficherAnalyse() {
		return this.appareilFormulaireAfficherAnalyse;
	}
	
	public JTextField getDateAnalyseFormulaireAfficherAnalyse() {
		return this.dateAnalyseFormulaireAfficherAnalyse;
	}
	
	public JTextField getHeureAnalyseFormulaireAfficherAnalyse() {
		return this.heureAnalyseFormulaireAfficherAnalyse;
	}
	
	public JTextArea getNotesAnalyseFormulaireAfficherAnalyseArea() {
		return this.notesAnalyseFormulaireAfficherAnalyseArea;
	}
	
	// MODIFIER PATIENT
	
	public ButtonGroup getChoixModificationFormulaireModifierPatient() {
		return this.choixModificationFormulaireModifierPatient;
	}
	
	public JTextField getNomFormulaireModifierPatient() {
		return this.nomFormulaireModifierPatient;
	}
	
	public JTextField getPrenomFormulaireModifierPatient() {
		return this.prenomFormulaireModifierPatient;
	}
	
	public ButtonGroup getButtonGroupSexeFormulaireModifierPatient() {
		return this.buttonGroupSexeFormulaireModifierPatient;
	}
	
	public JRadioButton getMasculinFormulaireModifierPatient() {
		return this.masculinFormulaireModifierPatient;
	}
	
	public JRadioButton getFemininFormulaireModifierPatient() {
		return this.femininFormulaireModifierPatient;
	}
	
	public JTextField getDateDeNaissanceFormulaireModifierPatient() {
		return this.dateDeNaissanceFormulaireModifierPatient;
	}
	
	public JTextArea getAdresseFormulaireModifierPatient() {
		return this.adresseFormulaireModifierPatient;
	}
	
	public JTextField getMailFormulaireModifierPatient() {
		return this.mailFormulaireModifierPatient;
	}
	
	public JTextField getNumeroFormulaireModifierPatient() {
		return this.numeroFormulaireModifierPatient;
	}
	
	public JButton getModifierFormulaireModifierPatientButton() {
		return this.modifierFormulaireModifierPatientButton;
	}
	
	// Hospitaliser
	
	public ButtonGroup getChoixAutoManuelFormulaireHospitalisation() {
		return this.choixAutoManuelFormulaireHospitalisation;
	}
	
	public ButtonGroup getChoixTypeChambreFormulaireHospitalisation() {
		return this.choixTypeChambreFormulaireHospitalisation;
	}
	
	public JTextField getCodeServiceFormulaireHospitalisation() {
		return this.codeServiceFormulaireHospitalisation;
	}
	
	public JRadioButton getChoixAutomatiqueFormulaireHospitalisation() {
		return this.choixAutomatiqueFormulaireHospitalisation;
	}
	
	public JRadioButton getChoixManuelFormulaireHospitalisation() {
		return this.choixManuelFormulaireHospitalisation;
	}
	
	public JComboBox<String> getDateDebutFormulaireHospitalisationComboBox(){
		return this.dateDebutFormulaireHospitalisationComboBox;
	}
	
	public JComboBox<String> getDateFinFormulaireHospitalisationComboBox(){
		return this.dateFinFormulaireHospitalisationComboBox;
	}
	
	public JRadioButton getChoixChambreIndividuelleFormulaireHospitalisation() {
		return this.choixChambreIndividuelleFormulaireHospitalisation;
	}
	
	public JComboBox<String> getChambreIndividuelleFormulaireHospitalisationComboBox(){
		return this.chambreIndividuelleFormulaireHospitalisationComboBox;
	}
	
	public JRadioButton getChoixChambreDoubleFormulaireHospitalisation() {
		return this.choixChambreDoubleFormulaireHospitalisation;
	}
	
	public JComboBox<String> getChambreDoubleFormulaireHospitalisationComboBox(){
		return this.chambreDoubleFormulaireHospitalisationComboBox;
	}
	
	public JRadioButton getChoixChambreQuadrupleFormulaireHospitalisation() {
		return this.choixChambreQuadrupleFormulaireHospitalisation;
	}
	
	public JComboBox<String> getChambreQuadrupleFormulaireHospitalisationComboBox(){
		return this.chambreQuadrupleFormulaireHospitalisationComboBox;
	}
	
	public JButton getHospitaliserFormulaireHospitalisation() {
		return this.hospitaliserFormulaireHospitalisation;
	}
	
	//
	
	public CardLayout getLayoutMultiPanel() {
		return this.layoutMultiPanel;
	}
	
	public JPanel getPanelBasDroit() {
		return this.panelBasDroit;
	}
	
	//
	
	
	// Setters
	
	public void setChoixMedecinFormulaireNouveauRDVComboBox(String[] listeMedecin){
		this.choixMedecinFormulaireNouveauRDVComboBox.setModel(new DefaultComboBoxModel<>(listeMedecin));
	}
	
	public void setChoixDateFormulaireNouveauRDVComboBox(String[] listeDates){
		this.choixDateFormulaireNouveauRDVComboBox.setModel(new DefaultComboBoxModel<>(listeDates));
	}
	
	public void setChoixHeureFormulaireNouveauRDVComboBox(String[] listeHeures){
		this.choixHeureFormulaireNouveauRDVComboBox.setModel(new DefaultComboBoxModel<>(listeHeures));
	}
	
	public void setChoixAppareilMedicalFormulaireNouvelleAnalyseComboBox(String[] listeCodeAppareil){
		this.choixAppareilMedicalFormulaireNouvelleAnalyseComboBox.setModel(new DefaultComboBoxModel<>(listeCodeAppareil));
	}
	
	public void setDateDebutFormulaireHospitalisationComboBox(String[] listeDateDebut) {
		this.dateDebutFormulaireHospitalisationComboBox.setModel(new DefaultComboBoxModel<>(listeDateDebut));
	}
	
	public void setDateFinFormulaireHospitalisationComboBox(String[] listeDateFin) {
		this.dateFinFormulaireHospitalisationComboBox.setModel(new DefaultComboBoxModel<>(listeDateFin));
	}
	
	public void setChambreIndividuelleFormulaireHospitalisationComboBox(String[] listeChambreIndividuelleRestantes) {
		this.chambreIndividuelleFormulaireHospitalisationComboBox.setModel(new DefaultComboBoxModel<>(listeChambreIndividuelleRestantes));
	}
	
	public void setChambreDoubleFormulaireHospitalisationComboBox(String[] listeChambreDoubleRestantes) {
		this.chambreDoubleFormulaireHospitalisationComboBox.setModel(new DefaultComboBoxModel<>(listeChambreDoubleRestantes));
	}
	
	public void setChambreQuadrupleFormulaireHospitalisationComboBox(String[] listeChambreQuadrupleRestantes) {
		this.chambreQuadrupleFormulaireHospitalisationComboBox.setModel(new DefaultComboBoxModel<>(listeChambreQuadrupleRestantes));
	}
	
}