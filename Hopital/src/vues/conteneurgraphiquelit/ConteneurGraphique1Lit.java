package vues.conteneurgraphiquelit;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class ConteneurGraphique1Lit extends JPanel {
	
	private BufferedImage img;
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(img, 5, 14, this);
	}
	
	public BufferedImage getImg() {
		return this.img;
	}
	
	public void setImg(BufferedImage img) {
		this.img = img;
	}
	
}