package vues.conteneurgraphiquelit;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class ConteneurGraphique4Lit extends JPanel {
	
	private BufferedImage img1;
	private BufferedImage img2;
	private BufferedImage img3;
	private BufferedImage img4;

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(img1, 5, 14, this);
		g.drawImage(img2, 130, 14, this);
		g.drawImage(img3, 255, 14, this);
		g.drawImage(img4, 380, 14, this);
	}
	
	public BufferedImage getImg1() {
		return this.img1;
	}
	
	public BufferedImage getImg2() {
		return this.img2;
	}
	
	public BufferedImage getImg3() {
		return this.img3;
	}
	
	public BufferedImage getImg4() {
		return this.img4;
	}
	
	public void setImg1(BufferedImage img) {
		this.img1 = img;
	}
	
	public void setImg2(BufferedImage img) {
		this.img2 = img;
	}
	
	public void setImg3(BufferedImage img) {
		this.img3 = img;
	}
	
	public void setImg4(BufferedImage img) {
		this.img4 = img;
	}
}