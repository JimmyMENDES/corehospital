package modele.materiel;

/**
 * Classe représentant un appareil médical.
 * @author Jimmy MENDES
 */
public class AppareilMedical {
	
	private String codeAppareil;
	private String typeAppareil;
	
    public AppareilMedical(String code, String nom) {
    	this.codeAppareil = code;
    	this.typeAppareil = nom;
    }
    
    // Getters
    
    public String getTypeAppareil() {
    	return this.typeAppareil;
    }
    
    public String getCodeAppareil() {
    	return this.codeAppareil;
    }
    
    // Méthodes
    
    public String toString() {
    	return this.codeAppareil + " " + this.typeAppareil;
    }

}