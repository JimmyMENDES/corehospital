package modele.materiel;

/**
 * Classe r�pr�sentant un �quipement m�dical.
 * @author Jimmy MENDES
 */
public class EquipementMedical extends Article {

    public EquipementMedical(String reference, String nom, int quantite) {
    	super(reference, nom, quantite);
    }
    
    // M�thodes
    
    public String toString() {
    	return "Equipement M�dical : "+this.nom+", Quantit� : "+this.quantite+", Ref�rence : "+this.reference;
    }

}