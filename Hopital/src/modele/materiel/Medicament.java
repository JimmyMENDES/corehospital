package modele.materiel;

/**
 * Classe repr�sentant un m�dicament.
 * @author Jimmy MENDES
 */
public class Medicament extends Article {

    public Medicament(String reference, String nom, int quantite) {
    	super(reference, nom, quantite);
    }

    // M�thodes
    
    public String toString() {
    	return "M�dicament : "+this.nom+", Quantit� : "+this.quantite+", Ref�rence : "+this.reference;
    }
    
}