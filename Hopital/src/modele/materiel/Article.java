package modele.materiel;

/**
 * Classe r�pr�sentant un article quelconque de la pharmacie.
 * @author Jimmy MENDES
 */
public class Article {

	protected String reference;
	protected String nom;
	protected int quantite;

    public Article(String reference, String nom, int quantite) {
    	this.reference = reference;
    	this.nom = nom;
    	this.quantite = quantite;
    }
    
    // Getters et Setters
    
    /**
     * Accesseur du nom d'un article.
     * @return nom
     */
    public String getReference() {
    	return this.reference;
    }
    
    /**
     * Accesseur du nom d'un article.
     * @return nom
     */
    public String getNom() {
    	return this.nom;
    }
    
    /**
     * Accesseur de la quantit� d'un article.
     * @return quantite
     */
    public int getQuantite() {
    	return this.quantite;
    }
    
    /**
     * Mutateur de la quantit� d'un article.
     * @param nombre
     */
    public void setQuantite(int nombre) {
    	this.quantite = nombre;
    }
    
    // M�thodes
    
    public String toString() {
    	return "Article : "+this.nom+", Quantit� : "+this.quantite+", Ref�rence : "+this.reference;
    }
    
    public boolean equals(Article article) {
    	if(this.reference.equalsIgnoreCase(article.reference) && this.nom.equalsIgnoreCase(article.nom)) return true;
    	else return false;
    }
}