package modele.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DAOIdentification {
	private String url="jdbc:mysql://localhost/hopital";
	private String loginUtilisateur = "root";
	private String motDePasse = "";
	private Connection connectionBDD;
	
	public DAOIdentification() {
		this.connectionBDD = SingleConnection.getInstance(url, loginUtilisateur, motDePasse);
	}
	
	/**
	 * 
	 * @param login
	 * @param motDePasse
	 * @return -1 si pas de r�sultat, 0 si superAdmin et le num�ro d'id si membre du personnel
	 */
	public int identification(String login, char[] motDePasse) {
		Statement requeteSQL = null;
		ResultSet resultatRequete = null;
		String mdp = "";
		for(int i = 0 ; i < motDePasse.length ; i++) {
			mdp += motDePasse[i];
		}
		try {
			requeteSQL = connectionBDD.createStatement();
			String identificationSQL ="SELECT * FROM `identification` WHERE `login_utilisateur` = '"+login+"'";
			resultatRequete = requeteSQL.executeQuery(identificationSQL);
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return -1;
		}
		try {
			while(resultatRequete.next()) {
				if(resultatRequete.getString("mdp_utilisateur").equals(mdp)) {
					return resultatRequete.getInt("id_personnel");
				}
			}
			return -1;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return -1;
		}
	}
	
}
