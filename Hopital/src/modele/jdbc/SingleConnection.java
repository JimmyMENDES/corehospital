package modele.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SingleConnection {
	
	private static Connection connectionBDD;
	
	private SingleConnection(String url, String loginUtilisateur, String motDePasse ) {
		try {
			connectionBDD = DriverManager.getConnection(url, loginUtilisateur, motDePasse);
			System.out.println("Connexion Effectu�e");
		}
		catch(SQLException e) {
			System.err.println("Erreu chargement driver");
			e.printStackTrace();
		}
	}
	
	public static Connection getInstance(String url, String loginUtilisateur, String motDePasse) {
		if(connectionBDD != null) return connectionBDD;
		else {
			new SingleConnection(url, loginUtilisateur, motDePasse);
			return connectionBDD;
		}
	}

}
