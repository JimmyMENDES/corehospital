package modele.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;

import modele.donnees.DonneesDates;
import modele.humain.AgentAdministrateur;
import modele.humain.Infirmier;
import modele.humain.Medecin;
import modele.humain.Personnel;
import modele.humain.Pharmacien;
import modele.humain.TechnicienLaboratoire;

public class DAOPersonnel {
	
	private String url="jdbc:mysql://localhost/hopital";
	private String loginUtilisateur = "root";
	private String motDePasse = "";
	private Connection connectionBDD;
	
	public DAOPersonnel() {
		this.connectionBDD = SingleConnection.getInstance(url, loginUtilisateur, motDePasse);
	}
	
	/**
	 * M�thode DAO permettant de rechercher un membre du personnel � partir de son id.
	 * <p>La requete SQL utilis�e est [SELECT * FROM `personnel` WHERE `id_personnel` = 'idPersonnel']</p> 
	 * @param idPersonnel
	 * @return le membre du personnel en question, sinon null
	 */
	public Personnel rechercheParIDDAO(int idPersonnel) {
		// Ajouter un throw illegal si reference n'existe pas
		Statement requeteSQL = null;
		ResultSet resultatRequete = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			//SELECT * FROM `personnel` WHERE `id_personnel` = '1'
			String rechercheSQL ="SELECT * FROM `personnel` WHERE `id_personnel` = '"+idPersonnel+"'";
			resultatRequete = requeteSQL.executeQuery(rechercheSQL);
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
		try {
			while(resultatRequete.next()) {
				String sexeRecherche = resultatRequete.getString("sexe");
				char sexe;
				if(sexeRecherche.equalsIgnoreCase("masculin")) sexe = 'm';
				else sexe = 'f';
				LocalDate dateNaissanceEnCours = resultatRequete.getDate("date_de_naissance").toLocalDate();
				LocalDate dateAncienneteEnCours = resultatRequete.getDate("date_anciennete").toLocalDate();
				String profession = resultatRequete.getString("profession");
				if(profession.equalsIgnoreCase("rh")) {
					// � ajouter 
				}
				else if(profession.equalsIgnoreCase("agent_administrateur")) {
					return new AgentAdministrateur(resultatRequete.getString("nom"), resultatRequete.getString("prenom"), sexe, dateNaissanceEnCours, resultatRequete.getString("adresse"), resultatRequete.getString("adresse_mail"), resultatRequete.getLong("numero_telephone"), idPersonnel, resultatRequete.getDouble("salaire"), dateAncienneteEnCours); 
				}
				else if(profession.equalsIgnoreCase("technicien")) {
					return new TechnicienLaboratoire(resultatRequete.getString("nom"), resultatRequete.getString("prenom"), sexe, dateNaissanceEnCours, resultatRequete.getString("adresse"), resultatRequete.getString("adresse_mail"), resultatRequete.getLong("numero_telephone"), idPersonnel, resultatRequete.getDouble("salaire"), dateAncienneteEnCours); 
				}
				else if(profession.equalsIgnoreCase("pharmacien")) {
					return new Pharmacien(resultatRequete.getString("nom"), resultatRequete.getString("prenom"), sexe, dateNaissanceEnCours, resultatRequete.getString("adresse"), resultatRequete.getString("adresse_mail"), resultatRequete.getLong("numero_telephone"), idPersonnel, resultatRequete.getDouble("salaire"), dateAncienneteEnCours, resultatRequete.getLong("rpps")); 
				}
				else if(profession.equalsIgnoreCase("infirmier")) {
					return new Infirmier(resultatRequete.getString("nom"), resultatRequete.getString("prenom"), sexe, dateNaissanceEnCours, resultatRequete.getString("adresse"), resultatRequete.getString("adresse_mail"), resultatRequete.getLong("numero_telephone"), idPersonnel, resultatRequete.getDouble("salaire"), dateAncienneteEnCours, resultatRequete.getLong("rpps"), resultatRequete.getString("code_service").charAt(0)); 
				}
				else if(profession.equalsIgnoreCase("medecin")) {
					return new Medecin(resultatRequete.getString("nom"), resultatRequete.getString("prenom"), sexe, dateNaissanceEnCours, resultatRequete.getString("adresse"), resultatRequete.getString("adresse_mail"), resultatRequete.getLong("numero_telephone"), idPersonnel, resultatRequete.getDouble("salaire"), dateAncienneteEnCours, resultatRequete.getLong("rpps"), resultatRequete.getString("specialite"), resultatRequete.getString("code_service").charAt(0)); 
				}
			}
			return null;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
	}

	/**
	 * M�thode DAO permettant de rechercher un membre du personnel � partir de son nom.
	 * <p>La requete SQL utilis�e est [SELECT * FROM `personnel` WHERE `nom` = `nom`]</p> 
	 * @param nom
	 * @return un ArrayList contenant l'ensemble des membre du Personnel dont le nom est pass� en argument, sinon null
	 */
	public ArrayList<Personnel> recherchePersonnelParNomDAO(String nom) {
		Statement requeteSQL = null;
		ResultSet resultatRequete = null;
		ArrayList<Personnel> listePersonnel = new ArrayList<Personnel>();
		try {
			requeteSQL = connectionBDD.createStatement();
			String rechercheSQL ="SELECT * FROM `personnel` WHERE `nom` = `"+nom+"`";
			resultatRequete = requeteSQL.executeQuery(rechercheSQL);
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
		try {
			while(resultatRequete.next()) {
				int id = resultatRequete.getInt("id_patient");
				String nomRecherche = resultatRequete.getString("nom");
				String prenom = resultatRequete.getString("prenom");
				char sexe = resultatRequete.getString("sexe").charAt(0);
				LocalDate dateNaissance = resultatRequete.getDate("date_de_naissance").toLocalDate();
				String adresse = resultatRequete.getString("adresse");
				String mail = resultatRequete.getString("adresse_mail");
				long telephone = resultatRequete.getLong("numero_telephone");
				LocalDate dateAnciennete = resultatRequete.getDate("date_anciennete").toLocalDate();
				double salaire = resultatRequete.getDouble("salaire");
				String profession = resultatRequete.getString("profession");
				long rpps = resultatRequete.getLong("rpps");
				String specialite = resultatRequete.getString("specialite");
				char service = resultatRequete.getString("service").charAt(0);
				if(profession.equalsIgnoreCase("agent_administrateur")) {
					listePersonnel.add(new AgentAdministrateur(nomRecherche, prenom, sexe, dateNaissance, adresse, mail, telephone, id, salaire, dateAnciennete));
				}
				else if(profession.equalsIgnoreCase("rh")) {
					// Ajouter plus tard
				}
				else if(profession.equalsIgnoreCase("technicien")) {
					listePersonnel.add(new TechnicienLaboratoire(nomRecherche, prenom, sexe, dateNaissance, adresse, mail, telephone, id, salaire, dateAnciennete));
				}
				else if(profession.equalsIgnoreCase("medecin")) {
					listePersonnel.add(new Medecin(nomRecherche, prenom, sexe, dateNaissance, adresse, mail, telephone, id, salaire, dateAnciennete, rpps, specialite, service));
				}
				else if(profession.equalsIgnoreCase("infirmier")) {
					listePersonnel.add(new Infirmier(nomRecherche, prenom, sexe, dateNaissance, adresse, mail, telephone, id, salaire, dateAnciennete, rpps, service));
				}
				else if(profession.equalsIgnoreCase("pharmacien")) {
					listePersonnel.add(new Pharmacien(nomRecherche, prenom, sexe, dateNaissance, adresse, mail, telephone, id, salaire, dateAnciennete, rpps));
				}
				
			}
			return listePersonnel;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
	}
	
	/**
	 * M�thode DAO permettant de rechercher un medecin � partir de son nom.
	 * <p>La requete SQL utilis�e est [SELECT * FROM `personnel` WHERE `profession` = 'medecin' AND `nom` = 'nom']</p> 
	 * @param nom
	 * @return un ArrayList contenant l'ensemble des medecins dont le nom est pass� en argument, sinon null
	 */
	public ArrayList<Medecin> rechercheMedecinParNomDAO(String nom) {
		Statement requeteSQL = null;
		ResultSet resultatRequete = null;
		ArrayList<Medecin> listeMedecin = new ArrayList<Medecin>();
		try {
			requeteSQL = connectionBDD.createStatement();
			String rechercheSQL ="SELECT * FROM `personnel` WHERE `profession` = 'medecin' AND `nom` = '"+nom+"'";
			resultatRequete = requeteSQL.executeQuery(rechercheSQL);
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
		try {
			while(resultatRequete.next()) {
				int id = resultatRequete.getInt("id_personnel");
				String nomRecherche = resultatRequete.getString("nom");
				String prenom = resultatRequete.getString("prenom");
				char sexe = resultatRequete.getString("sexe").charAt(0);
				LocalDate dateNaissance = resultatRequete.getDate("date_de_naissance").toLocalDate();
				String adresse = resultatRequete.getString("adresse");
				String mail = resultatRequete.getString("adresse_mail");
				long telephone = resultatRequete.getLong("numero_telephone");
				LocalDate dateAnciennete = resultatRequete.getDate("date_anciennete").toLocalDate();
				double salaire = resultatRequete.getDouble("salaire");
				long rpps = resultatRequete.getLong("rpps");
				String specialite = resultatRequete.getString("specialite");
				char service = resultatRequete.getString("code_service").charAt(0);
				
				listeMedecin.add(new Medecin(nomRecherche, prenom, sexe, dateNaissance, adresse, mail, telephone, id, salaire, dateAnciennete, rpps, specialite, service));
				
			}
			return listeMedecin;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
	}
	
	/**
	 * M�thode DAO permettant de rajouter une ligne dans la table personnel.
	 * <p>La requete SQL utilis�e est [INSERT INTO `personnel` (`nom`, `prenom`, `sexe`, `date_de_naissance`, `adresse`, `adresse_mail`, `numero_telephone`, `date_anciennete`, `salaire`, `profession`) VALUES ('nom', 'prenom', 'sexe', 'dateDeNaissance', 'adresse', 'adresseMail', 'numero', 'dateAnciennete', 'salaire', 'profession')]</p>
	 * @param personnel
	 * @return true si le membre du personnel a bien �t� ajout� � la table, false sinon
	 */
	public boolean ajouterPersonnel(String nom, String prenom, String sexe, LocalDate dateDeNaissance, String adresse, String mail, long telephone, double salaire, String profession, long rpps, String specialite, String codeService) {
		Statement requeteSQL = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String dateDeNaissanceSQL = DonneesDates.formatDateSQL(dateDeNaissance);
			String dateAncienneteSQL = DonneesDates.formatDateSQL(LocalDate.now());
			String ajoutPersonnelSQL = "";
			if(rpps == -1) {
				ajoutPersonnelSQL ="INSERT INTO `personnel` (`nom`, `prenom`, `sexe`, `date_de_naissance`, `adresse`, `adresse_mail`, `numero_telephone`, `date_anciennete`, `salaire`, `profession`) VALUES ('"+nom+"', '"+prenom+"', '"+sexe+"', '"+dateDeNaissanceSQL+"', '"+adresse+"', '"+mail+"', '"+telephone+"', '"+dateAncienneteSQL+"', '"+salaire+"', '"+profession+"')";
			}
			else {
				if(profession.equalsIgnoreCase("medecin")) {
					ajoutPersonnelSQL ="INSERT INTO `personnel` (`nom`, `prenom`, `sexe`, `date_de_naissance`, `adresse`, `adresse_mail`, `numero_telephone`, `date_anciennete`, `salaire`, `profession`, `rpps`, `specialite`, `code_service`) VALUES ('"+nom+"', '"+prenom+"', '"+sexe+"', '"+dateDeNaissanceSQL+"', '"+adresse+"', '"+mail+"', '"+telephone+"', '"+dateAncienneteSQL+"', '"+salaire+"', '"+profession+"', '"+rpps+"', '"+specialite+"', '"+codeService+"')";

				}
				else if(profession.equalsIgnoreCase("infirmier")) {
					ajoutPersonnelSQL ="INSERT INTO `personnel` (`nom`, `prenom`, `sexe`, `date_de_naissance`, `adresse`, `adresse_mail`, `numero_telephone`, `date_anciennete`, `salaire`, `profession`, `rpps`, `code_service`) VALUES ('"+nom+"', '"+prenom+"', '"+sexe+"', '"+dateDeNaissanceSQL+"', '"+adresse+"', '"+mail+"', '"+telephone+"', '"+dateAncienneteSQL+"', '"+salaire+"', '"+profession+"', '"+rpps+"', '"+codeService+"')";
				}
				else if(profession.equalsIgnoreCase("pharmacien")) {
					ajoutPersonnelSQL ="INSERT INTO `personnel` (`nom`, `prenom`, `sexe`, `date_de_naissance`, `adresse`, `adresse_mail`, `numero_telephone`, `date_anciennete`, `salaire`, `profession`, `rpps`) VALUES ('"+nom+"', '"+prenom+"', '"+sexe+"', '"+dateDeNaissanceSQL+"', '"+adresse+"', '"+mail+"', '"+telephone+"', '"+dateAncienneteSQL+"', '"+salaire+"', '"+profession+"', '"+rpps+"')";
				}
			}
			requeteSQL.executeUpdate(ajoutPersonnelSQL);
			System.out.println("Personnel ajout�e");
			return true;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return false;
		}
	}
	
	/**
	 * M�thode DAO permettant de supprimer une ligne dans la table personnel.
	 * <p>La requete SQL utilis�e est [DELETE FROM `personnel` WHERE `personnel`.`id_personnel` = personnel.getIDPersonnel()]</p>
	 * @param personnel
	 * @return true si le membre du personnel a bien �t� supprim� � la table, false sinon
	 */
	public boolean supprimerPersonnel(int idPersonnel) {
		Statement requeteSQL = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String suppressionPersonnelSQL ="DELETE FROM `personnel` WHERE `personnel`.`id_personnel` = "+idPersonnel;
			requeteSQL.executeUpdate(suppressionPersonnelSQL);
			System.out.println("Personnel supprim�");
			return true;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return false;
		}
	}
	
	public String professionPersonnel(int idPersonnel) {
			Statement requeteSQL = null;
			ResultSet resultatRequete = null;
			try {
				requeteSQL = connectionBDD.createStatement();
				String rechercheSQL ="SELECT * FROM `personnel` WHERE `id_personnel` = '"+idPersonnel+"'";
				resultatRequete = requeteSQL.executeQuery(rechercheSQL);
			}
			catch(SQLException e) {
				System.out.println("Erreur SQL");
				return null;
			}
			try {
				while(resultatRequete.next()) {
					return  resultatRequete.getString("profession");
					
				}
				return null;
			}
			catch(SQLException e) {
				System.out.println("Erreur SQL");
				return null;
			}
	}
	
	public String servicePersonnel(int idPersonnel) {
		Statement requeteSQL = null;
		ResultSet resultatRequete = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String rechercheSQL ="SELECT * FROM `personnel` WHERE `id_personnel` = '"+idPersonnel+"'";
			resultatRequete = requeteSQL.executeQuery(rechercheSQL);
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
		try {
			while(resultatRequete.next()) {
				return  resultatRequete.getString("code_service");
				
			}
			return null;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
}
	
	public ArrayList<Medecin> rechercherMedecinDAO(){
		Statement requeteSQL = null;
		ResultSet resultatRequete = null;
		ArrayList<Medecin> listeMedecin = new ArrayList<Medecin>();
		try {
			requeteSQL = connectionBDD.createStatement();
			String rechercheSQL ="SELECT * FROM `personnel` WHERE `profession` = 'medecin'";
			resultatRequete = requeteSQL.executeQuery(rechercheSQL);
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
		try {
			while(resultatRequete.next()) {
				int id = resultatRequete.getInt("id_personnel");
				String nomRecherche = resultatRequete.getString("nom");
				String prenom = resultatRequete.getString("prenom");
				char sexe = resultatRequete.getString("sexe").charAt(0);
				LocalDate dateNaissance = resultatRequete.getDate("date_de_naissance").toLocalDate();
				String adresse = resultatRequete.getString("adresse");
				String mail = resultatRequete.getString("adresse_mail");
				long telephone = resultatRequete.getLong("numero_telephone");
				LocalDate dateAnciennete = resultatRequete.getDate("date_anciennete").toLocalDate();
				double salaire = resultatRequete.getDouble("salaire");
				long rpps = resultatRequete.getLong("rpps");
				String specialite = resultatRequete.getString("specialite");
				char service = resultatRequete.getString("code_service").charAt(0);
				listeMedecin.add(new Medecin(nomRecherche, prenom, sexe, dateNaissance, adresse, mail, telephone, id, salaire, dateAnciennete, rpps, specialite, service));
			}
			return listeMedecin;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
	}
	
	public Medecin rechercherMedecinParIDDAO(int idPersonnel){
		Statement requeteSQL = null;
		ResultSet resultatRequete = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String rechercheSQL ="SELECT * FROM `personnel` WHERE `id_personnel` = '"+idPersonnel+"'";
			resultatRequete = requeteSQL.executeQuery(rechercheSQL);
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
		try {
			while(resultatRequete.next()) {
				String nomRecherche = resultatRequete.getString("nom");
				String prenom = resultatRequete.getString("prenom");
				char sexe = resultatRequete.getString("sexe").charAt(0);
				LocalDate dateNaissance = resultatRequete.getDate("date_de_naissance").toLocalDate();
				String adresse = resultatRequete.getString("adresse");
				String mail = resultatRequete.getString("adresse_mail");
				long telephone = resultatRequete.getLong("numero_telephone");
				LocalDate dateAnciennete = resultatRequete.getDate("date_anciennete").toLocalDate();
				double salaire = resultatRequete.getDouble("salaire");
				long rpps = resultatRequete.getLong("rpps");
				String specialite = resultatRequete.getString("specialite");
				char service = resultatRequete.getString("code_service").charAt(0);
				return new Medecin(nomRecherche, prenom, sexe, dateNaissance, adresse, mail, telephone, idPersonnel, salaire, dateAnciennete, rpps, specialite, service);
			}
			return null;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
	}
	
}
