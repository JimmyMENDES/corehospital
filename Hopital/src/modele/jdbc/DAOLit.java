package modele.jdbc;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class DAOLit {
	private String url="jdbc:mysql://localhost/hopital";
	private String loginUtilisateur = "root";
	private String motDePasse = "";
	private Connection connectionBDD;
	
	public DAOLit() {
		this.connectionBDD = SingleConnection.getInstance(url, loginUtilisateur, motDePasse);
	}
	
	public boolean supprimerLitDAO(String codeLit) {
		Statement requeteSQL = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String ligneCodeSQL ="DELETE FROM `lit` WHERE `code_lit` = '"+codeLit+"'";
			requeteSQL.executeUpdate(ligneCodeSQL);
			System.out.println("Lit supprim�");
			return true;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return false;
		}
	}
}
