package modele.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;

import modele.donnees.DonneesDates;
import modele.humain.Medecin;
import modele.humain.Patient;
import modele.immateriel.RDV;

/**
 * Classe regroupant l'ensemble des m�thodes DAO utilis�es pour la table rdv.
 * @author Jimmy MENDES
 */
public class DAORDV {
	
	private String url="jdbc:mysql://localhost/hopital";
	private String loginUtilisateur = "root";
	private String motDePasse = "";
	private Connection connectionBDD;
	
	public DAORDV() {
		this.connectionBDD = SingleConnection.getInstance(url, loginUtilisateur, motDePasse);
	}
	
	/**
	 * M�thode DAO permettant de rajouter une ligne dans la table rdv.
	 * <p>La requete SQL utilis�e est [INSERT INTO `rdv` (`id_patient`, `id_personnel`, `date_heure_rdv`, `num_salle`) VALUES ('patient.getIDPatient()', 'medecin.getIDPersonnel()', 'DonneesDates.formatDateTimeSQL(dateHeure)', 'numeroSalle')]</p>
	 * @param patient
	 * @param technicien
	 * @param appareil
	 * @param contenuAnalyse
	 * @return true si l'analyse a bien �t� ajout�e � la table, false sinon
	 */
	public boolean ajouterRDVDAO(int idPatient, int idMedecin, LocalDateTime dateHeure, int numeroSalle) {
		Statement requeteSQL = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String ajoutAnalyseSQL ="INSERT INTO `rdv` (`id_patient`, `id_personnel`, `date_heure_rdv`, `num_salle`) VALUES ('"+idPatient+"', '"+idMedecin+"', '"+DonneesDates.formatDateTimeSQL(dateHeure)+"', '"+numeroSalle+"');";
			requeteSQL.executeUpdate(ajoutAnalyseSQL);
			System.out.println("RDV ajout�e");
			return true;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return false;
		}
	}
	
	/**
	 * M�thode DAO permettant de rechercher un RDV � partir d'un patient.
	 * <p>La requete SQL utilis�e est [SELECT * FROM `rdv` WHERE `id_patient` = 'patient.getIDPatient()']</p> 
	 * @param patient
	 * @return une arrayList contenant les rdv du patient, null sinon
	 */
	public ArrayList<RDV> rechercheRDVParPatientDAO(Patient patient) {
		Statement requeteSQL = null;
		ResultSet resultatRequete = null;
		ArrayList<RDV> listeRDV = new ArrayList<RDV>();
		try {
			requeteSQL = connectionBDD.createStatement();
			String rechercheSQL ="SELECT * FROM `rdv` WHERE `id_patient` = '"+patient.getIDPatient()+"'";
			resultatRequete = requeteSQL.executeQuery(rechercheSQL);
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
		try {
			while(resultatRequete.next()) {
				int reference = resultatRequete.getInt("reference_rdv");
				int idMedecin = resultatRequete.getInt("id_personnel");
				DAOPersonnel connectionTablePersonnel = new DAOPersonnel();
				Medecin rechercheMedecin = (Medecin) connectionTablePersonnel.rechercheParIDDAO(idMedecin);
				int numeroSalle = resultatRequete.getInt("num_salle");
				LocalDate dateEncours = resultatRequete.getDate("date_heure_rdv").toLocalDate();
				LocalTime heureEncours = resultatRequete.getTime("date_heure_rdv").toLocalTime();
				LocalDateTime dateHeureEnCours = LocalDateTime.of(dateEncours, heureEncours);
				listeRDV.add(new RDV(reference,patient, rechercheMedecin, numeroSalle, dateHeureEnCours));
			}
			return listeRDV;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
	}
	
	/**
	 * M�thode DAO permettant de rechercher un RDV � partir d'un medecin.
	 * <p>La requete SQL utilis�e est [SELECT * FROM `rdv` WHERE `id_personnel` = 'medecin.getIDPersonnel()']</p> 
	 * @param medecin
	 * @return une arrayList contenant les rdv du jour du medecin, null sinon
	 */
	public ArrayList<RDV> rechercheRDVDuJourParMedecinDAO(int idMedecin) {
		Statement requeteSQL = null;
		ResultSet resultatRequete = null;
		ArrayList<RDV> listeRDV = new ArrayList<RDV>();
		try {
			requeteSQL = connectionBDD.createStatement();
			String rechercheSQL ="SELECT * FROM `rdv` WHERE `id_personnel` = '"+idMedecin+"'";
			resultatRequete = requeteSQL.executeQuery(rechercheSQL);
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
		try {
			while(resultatRequete.next()) {
				LocalDate dateEnCours = resultatRequete.getDate("date_heure_rdv").toLocalDate();
				if(dateEnCours.isEqual(LocalDate.now())) {
					int reference = resultatRequete.getInt("reference_rdv");
					int idPatient = resultatRequete.getInt("id_patient");
					DAOPatient connectionTablePatient = new DAOPatient();
					Patient recherchePatient = connectionTablePatient.rechercheParIDDAO(idPatient);
					int numeroSalle = resultatRequete.getInt("num_salle");
					LocalTime heureEncours = resultatRequete.getTime("date_heure_rdv").toLocalTime();
					LocalDateTime dateHeureEnCours = LocalDateTime.of(dateEnCours, heureEncours);
					DAOPersonnel connectionTablePersonnel = new DAOPersonnel();
					listeRDV.add(new RDV(reference, recherchePatient, connectionTablePersonnel.rechercherMedecinParIDDAO(idMedecin) , numeroSalle, dateHeureEnCours));
				}
			}
			return listeRDV;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
	}
	
	/**
	 * M�thode DAO permettant de rechercher un RDV � partir d'un medecin.
	 * <p>La requete SQL utilis�e est [SELECT * FROM `rdv` WHERE `id_personnel` = 'medecin.getIDPersonnel()']</p> 
	 * @param medecin
	 * @return une arrayList contenant les rdv du jour du medecin, null sinon
	 */
	public ArrayList<RDV> rechercheRDVDUneDateParMedecinDAO(int idMedecin, LocalDate date) {
		Statement requeteSQL = null;
		ResultSet resultatRequete = null;
		ArrayList<RDV> listeRDV = new ArrayList<RDV>();
		try {
			requeteSQL = connectionBDD.createStatement();
			String rechercheSQL ="SELECT * FROM `rdv` WHERE `id_personnel` = '"+idMedecin+"'";
			resultatRequete = requeteSQL.executeQuery(rechercheSQL);
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
		try {
			while(resultatRequete.next()) {
				LocalDate dateEnCours = resultatRequete.getDate("date_heure_rdv").toLocalDate();
				if(dateEnCours.isEqual(date)) {
					int reference = resultatRequete.getInt("reference_rdv");
					int idPatient = resultatRequete.getInt("id_patient");
					DAOPatient connectionTablePatient = new DAOPatient();
					Patient recherchePatient = connectionTablePatient.rechercheParIDDAO(idPatient);
					int numeroSalle = resultatRequete.getInt("num_salle");
					LocalTime heureEncours = resultatRequete.getTime("date_heure_rdv").toLocalTime();
					LocalDateTime dateHeureEnCours = LocalDateTime.of(dateEnCours, heureEncours);
					DAOPersonnel connectionTablePersonnel = new DAOPersonnel();
					listeRDV.add(new RDV(reference, recherchePatient, connectionTablePersonnel.rechercherMedecinParIDDAO(idMedecin) , numeroSalle, dateHeureEnCours));
				}
			}
			return listeRDV;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
	}
	
	/**
	 * M�thode DAO permettant de rechercher un RDV � partir d'un medecin.
	 * <p>La requete SQL utilis�e est [SELECT * FROM `rdv` WHERE `id_personnel` = 'medecin.getIDPersonnel()']</p> 
	 * @param medecin
	 * @return une arrayList contenant les rdv du jour du medecin, null sinon
	 */
	public ArrayList<RDV> rechercheRDVDuJourEtPlusParMedecinDAO(int idMedecin) {
		Statement requeteSQL = null;
		ResultSet resultatRequete = null;
		ArrayList<RDV> listeRDV = new ArrayList<RDV>();
		try {
			requeteSQL = connectionBDD.createStatement();
			String rechercheSQL ="SELECT * FROM `rdv` WHERE `id_personnel` = '"+idMedecin+"'";
			resultatRequete = requeteSQL.executeQuery(rechercheSQL);
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
		try {
			while(resultatRequete.next()) {
				LocalDate dateEnCours = resultatRequete.getDate("date_heure_rdv").toLocalDate();
				if(dateEnCours.isEqual(LocalDate.now()) || dateEnCours.isAfter(LocalDate.now())) {
					int reference = resultatRequete.getInt("reference_rdv");
					int idPatient = resultatRequete.getInt("id_patient");
					DAOPatient connectionTablePatient = new DAOPatient();
					Patient recherchePatient = connectionTablePatient.rechercheParIDDAO(idPatient);
					int numeroSalle = resultatRequete.getInt("num_salle");
					LocalTime heureEncours = resultatRequete.getTime("date_heure_rdv").toLocalTime();
					LocalDateTime dateHeureEnCours = LocalDateTime.of(dateEnCours, heureEncours);
					DAOPersonnel connectionTablePersonnel = new DAOPersonnel();
					listeRDV.add(new RDV(reference, recherchePatient, connectionTablePersonnel.rechercherMedecinParIDDAO(idMedecin) , numeroSalle, dateHeureEnCours));
				}
			}
			return listeRDV;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
	}
	
	/**
	 * M�thode DAO permettant de rechercher un RDV � partir d'un medecin.
	 * <p>La requete SQL utilis�e est [SELECT * FROM `rdv` WHERE `id_personnel` = 'medecin.getIDPersonnel()']</p> 
	 * @param medecin
	 * @return une arrayList contenant les rdv du jour du medecin, null sinon
	 */
	public int recherchePremiereSalleLibreRDVDAO(LocalDateTime dateHeure) {
		Statement requeteSQL = null;
		ResultSet resultatRequete = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String rechercheSQL ="SELECT * FROM `rdv` WHERE `date_heure_rdv` = '"+DonneesDates.formatDateTimeSQL(dateHeure)+"'";
			resultatRequete = requeteSQL.executeQuery(rechercheSQL);
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return -1;
		}
		try {
			ArrayList<Integer> listeSallePrises = new ArrayList<Integer>();
			while(resultatRequete.next()) {
				listeSallePrises.add(resultatRequete.getInt("num_salle"));
			}
			int compteur;
			for(int i = 1 ; i <= 10 ; i++) {
				compteur = 0;
				for(int j = 0 ; j < listeSallePrises.size() ; j++) {
					if(listeSallePrises.get(j) == i) compteur++;
				}
				if(compteur == 0) return i;
			}
			return -1;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return -1;
		}
	}

	/**
	 * M�thode DAO permettant de supprimer une ligne de la table rdv.
	 * <p>La requete SQL utilis�e est [DELETE FROM `rdv` WHERE `reference_rdv` = 'rdv.getReferenceRDV()']</p> 
	 * @param rdv
	 * @return true si la requ�te est r�ussie, false sinon
	 */
	public boolean supprimerRDVDAO(RDV rdv) {
		// IllegalArgument si le patient n'existe pas dans la bdd
		Statement requeteSQL = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String ligneCodeSQL ="DELETE FROM `rdv` WHERE `reference_rdv` = '"+rdv.getReferenceRDV()+"'";
			requeteSQL.executeUpdate(ligneCodeSQL);
			System.out.println("RDV supprim�");
			return true;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return false;
		}
	}
	
	/**
	 * M�thode DAO permettant de modifier le medecin d'une ligne de la table rdv.
	 * <p>La requete SQL utilis�e est [UPDATE `rdv` SET `id_personnel` = 'medecin.getIDPersonnel()' WHERE `rdv`.`reference_rdv` = rdv.getReferenceRDV();]</p> 
	 * @param rdv
	 * @param medecin
	 * @return true si la requ�te est r�ussie, false sinon
	 */
	public boolean modifierMedecinRDVDAO(RDV rdv, Medecin medecin) {
		Statement requeteSQL = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String ligneCodeSQL ="UPDATE `rdv` SET `id_personnel` = '"+medecin.getIDPersonnel()+"' WHERE `rdv`.`reference_rdv` = "+rdv.getReferenceRDV()+";";
			requeteSQL.executeUpdate(ligneCodeSQL);
			System.out.println("Medecin du RDV modifi�");
			return true;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return false;
		}
	}
	
	/**
	 * M�thode DAO permettant de modifier la date d'une ligne de la table rdv.
	 * <p>La requete SQL utilis�e est [UPDATE `rdv` SET `date_heure_rdv` = '2021-05-29 15:00:00' WHERE `rdv`.`reference_rdv` = rdv.getReferenceRDV();]</p> 
	 * @param rdv
	 * @param nouvelleDate
	 * @return true si la requ�te est r�ussie, false sinon
	 */
	public boolean modifierDateRDVDAO(RDV rdv, LocalDateTime nouvelleDate) {
		Statement requeteSQL = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String ligneCodeSQL ="UPDATE `rdv` SET `date_heure_rdv` = '"+DonneesDates.formatDateTimeSQL(nouvelleDate)+"' WHERE `rdv`.`reference_rdv` = "+rdv.getReferenceRDV()+";";
			requeteSQL.executeUpdate(ligneCodeSQL);
			System.out.println("RDV modifi�");
			return true;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return false;
		}
	}
	
	

}
