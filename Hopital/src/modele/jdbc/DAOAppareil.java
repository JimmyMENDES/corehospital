package modele.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;
import java.util.ArrayList;

import modele.materiel.AppareilMedical;

public class DAOAppareil {

	private String url="jdbc:mysql://localhost/hopital";
	private String loginUtilisateur = "root";
	private String motDePasse = "";
	private Connection connectionBDD;
	
	public DAOAppareil() {
		this.connectionBDD = SingleConnection.getInstance(url, loginUtilisateur, motDePasse);
	}
	
	/**
	 * M�thode DAO permettant de rechercher un appareil � partir de sa r�f�rence.
	 * <p>La requete SQL utilis�e est [SELECT * FROM `appareil` WHERE `code_appareil` = 'reference']</p>
	 * @param reference
	 * @return l'appareil en question sinon null
	 */
	public AppareilMedical rechercheParReferenceDAO(String reference) {
		// Ajouter un throw illegal si reference n'existe pas
		Statement requeteSQL = null;
		ResultSet resultatRequete = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String rechercheSQL ="SELECT * FROM `appareil` WHERE `code_appareil` = '"+reference+"'";
			resultatRequete = requeteSQL.executeQuery(rechercheSQL);
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
		try {
			while(resultatRequete.next()) {
				return new AppareilMedical(resultatRequete.getString("code_appareil"), resultatRequete.getString("type_appareil"));
			}
			return null;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
	}
	
	public ArrayList<AppareilMedical> ensembleDesAppareilsDAO(){
		Statement requeteSQL = null;
		ResultSet resultatRequete = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String rechercheSQL ="SELECT * FROM `appareil`";
			resultatRequete = requeteSQL.executeQuery(rechercheSQL);
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
		try {
			ArrayList<AppareilMedical> listeAppareilMedical = new ArrayList<AppareilMedical>();
			while(resultatRequete.next()) {
				String codeAppareil = resultatRequete.getString("code_appareil");
				String nomAppareil = resultatRequete.getString("type_appareil");
				listeAppareilMedical.add(new AppareilMedical(codeAppareil, nomAppareil));
			}
			return listeAppareilMedical;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
	}
	
	public boolean ajouterAppareilMedical(String codeAppareil, String nomAppareil) {
		Statement requeteSQL = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String ajoutAppareilSQL ="INSERT INTO `appareil` (`code_appareil`, `type_appareil`) VALUES ('"+codeAppareil+"', '"+nomAppareil+"');";
			requeteSQL.executeUpdate(ajoutAppareilSQL);
			System.out.println("Appareil cr��");
			return true;
		}
		catch(SQLIntegrityConstraintViolationException e) {
			System.out.println("L'appareil existe d�j�");
			return false;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return false;
		}
	}
	
	public boolean supprimerAppareilDAO(String codeAppareil) {
		// IllegalArgument si l'article n'existe pas dans la bdd
		Statement requeteSQL = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String supprimerAppareilSQL ="DELETE FROM `appareil` WHERE `code_appareil` = '"+codeAppareil+"'";
			requeteSQL.executeUpdate(supprimerAppareilSQL);
			System.out.println("Appareil supprim�");
			return true;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return false;
		}
	}
}
