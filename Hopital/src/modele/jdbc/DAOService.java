package modele.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import modele.installations.Chambre;
import modele.installations.Service;

public class DAOService {

	private String url="jdbc:mysql://localhost/hopital";
	private String loginUtilisateur = "root";
	private String motDePasse = "";
	private Connection connectionBDD;
	
	public DAOService() {
		this.connectionBDD = SingleConnection.getInstance(url, loginUtilisateur, motDePasse);
	}
	
	/**
	 * M�thode DAO permettant de rechercher un service � partir de son code.
	 * <p>La requete SQL utilis�e est [SELECT * FROM `service` WHERE `code_service` = 'code']</p> 
	 * @param code
	 * @return le service en question sinon null
	 */
	public Service rechercheParReferenceDAO(char code) {
		// Ajouter un throw illegal si reference n'existe pas
		Statement requeteSQL = null;
		ResultSet resultatRequete = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String rechercheSQL ="SELECT * FROM `service` WHERE `code_service` = '"+code+"'";
			resultatRequete = requeteSQL.executeQuery(rechercheSQL);
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
		try {
			while(resultatRequete.next()) {
				return new Service(resultatRequete.getString("nom_service"), resultatRequete.getString("code_service").charAt(0), new ArrayList<Chambre>());
			}
			return null;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
	}
	
	public boolean ajouterInstallationDAO(String codeService, String nomService, int nombreChambreIndividuelle, int nombreChambreDouble, int nombreChambreQuadruple) {
		Statement requeteSQL = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String ajoutServiceSQL ="INSERT INTO `service` (`code_service`, `nom_service`) VALUES ('"+codeService+"', '"+nomService+"')";
			requeteSQL.executeUpdate(ajoutServiceSQL);
			System.out.println("Service ajout�");
			if(nombreChambreIndividuelle >= 1) {
				for(int i = 1 ; i <= nombreChambreIndividuelle  ; i++) {
					String ajoutChambreIndividuelleSQL ="INSERT INTO `chambre` (`code_chambre`, `code_service`, `num_chambre`, `capacite`) VALUES ('"+codeService+i+"', '"+codeService+"', '"+i+"', '1')";
					requeteSQL.executeUpdate(ajoutChambreIndividuelleSQL);
					String ajoutLitIndividuelSQL ="INSERT INTO `lit` (`code_lit`, `code_service`, `code_chambre`, `num_lit`, `est_libre`) VALUES ('"+codeService+i+1+"', '"+codeService+"', '"+codeService+i+"', '1', '1')";
					requeteSQL.executeUpdate(ajoutLitIndividuelSQL);
				}
			}
			if(nombreChambreDouble >= 1) {
				for(int i = (nombreChambreIndividuelle+1) ; i <= (nombreChambreIndividuelle+nombreChambreDouble)  ; i++) {
					String ajoutChambreDoubleSQL ="INSERT INTO `chambre` (`code_chambre`, `code_service`, `num_chambre`, `capacite`) VALUES ('"+codeService+i+"', '"+codeService+"', '"+i+"', '2')";
					requeteSQL.executeUpdate(ajoutChambreDoubleSQL);
					for(int j = 1 ; j < 3 ; j++) {
						String ajoutLitDoubleSQL ="INSERT INTO `lit` (`code_lit`, `code_service`, `code_chambre`, `num_lit`, `est_libre`) VALUES ('"+codeService+i+j+"', '"+codeService+"', '"+codeService+i+"', '"+j+"', '1')";
						requeteSQL.executeUpdate(ajoutLitDoubleSQL);
					}
				}
			}
			if(nombreChambreQuadruple >= 1) {
				for(int i = (nombreChambreIndividuelle+nombreChambreDouble+1) ; i <= (nombreChambreIndividuelle+nombreChambreDouble+nombreChambreQuadruple)  ; i++) {
					String ajoutChambreDoubleSQL ="INSERT INTO `chambre` (`code_chambre`, `code_service`, `num_chambre`, `capacite`) VALUES ('"+codeService+i+"', '"+codeService+"', '"+i+"', '4')";
					requeteSQL.executeUpdate(ajoutChambreDoubleSQL);
					for(int j = 1 ; j < 5 ; j++) {
						String ajoutLitQuadrupleSQL ="INSERT INTO `lit` (`code_lit`, `code_service`, `code_chambre`, `num_lit`, `est_libre`) VALUES ('"+codeService+i+j+"', '"+codeService+"', '"+codeService+i+"', '"+j+"', '1')";
						requeteSQL.executeUpdate(ajoutLitQuadrupleSQL);
					}
				}
			}
			return true;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return false;
		}
	}
	
	public boolean supprimerServiceDAO(char codeService) {
		Statement requeteSQL = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String ligneCodeSQL ="DELETE FROM `service` WHERE `code_service` = '"+codeService+"'";
			requeteSQL.executeUpdate(ligneCodeSQL);
			System.out.println("Service supprim�");
			return true;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return false;
		}
	}
}
