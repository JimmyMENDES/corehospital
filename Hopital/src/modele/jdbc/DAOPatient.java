package modele.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;

import modele.donnees.DonneesDates;
import modele.humain.Patient;

public class DAOPatient {

	private String url="jdbc:mysql://localhost/hopital";
	private String loginUtilisateur = "root";
	private String motDePasse = "";
	private Connection connectionBDD;
	
	public DAOPatient() {
		this.connectionBDD = SingleConnection.getInstance(url, loginUtilisateur, motDePasse);
	}
	
	/**
	 * M�thode DAO permettant de rechercher un patient � partir de son ID.
	 * <p>La requete SQL utilis�e est [SELECT * FROM `patient` WHERE `id_patient` = 'id']</p> 
	 * @param id
	 * @return le patient en question sinon null
	 */
	public Patient rechercheParIDDAO(int id) {
		Statement requeteSQL = null;
		ResultSet resultatRequete = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String rechercheSQL ="SELECT * FROM `patient` WHERE `id_patient` = '"+id+"'";
			resultatRequete = requeteSQL.executeQuery(rechercheSQL);
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
		try {
			while(resultatRequete.next()) {
				String nom = resultatRequete.getString("nom");
				String prenom = resultatRequete.getString("prenom");
				char sexe = resultatRequete.getString("sexe").charAt(0);
				LocalDate dateNaissance = resultatRequete.getDate("date_de_naissance").toLocalDate();
				String adresse = resultatRequete.getString("adresse");
				String mail = resultatRequete.getString("adresse_mail");
				long telephone = resultatRequete.getLong("numero_telephone");
				String pathologie = resultatRequete.getString("pathologies");
				return new Patient(id, nom, prenom, sexe, dateNaissance, adresse, mail, telephone, pathologie);
			}
			return null;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
	}
	
	/**
	 * M�thode DAO permettant de rechercher un patient � partir de son nom.
	 * <p>La requete SQL utilis�e est [SELECT * FROM `patient` WHERE `nom` = 'nom']</p> 
	 * @param nom
	 * @return le patient en question sinon null
	 */
	public ArrayList<Patient> rechercheParNomDAO(String nom) {
		Statement requeteSQL = null;
		ResultSet resultatRequete = null;
		ArrayList<Patient> listePatient = new ArrayList<Patient>();
		try {
			requeteSQL = connectionBDD.createStatement();
			String rechercheSQL ="SELECT * FROM `patient` WHERE `nom` = '"+nom+"'";
			resultatRequete = requeteSQL.executeQuery(rechercheSQL);
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
		try {
			while(resultatRequete.next()) {
				int id = resultatRequete.getInt("id_patient");
				String nomRecherche = resultatRequete.getString("nom");
				String prenom = resultatRequete.getString("prenom");
				char sexe = resultatRequete.getString("sexe").charAt(0);
				LocalDate dateNaissance = resultatRequete.getDate("date_de_naissance").toLocalDate();
				String adresse = resultatRequete.getString("adresse");
				String mail = resultatRequete.getString("adresse_mail");
				long telephone = resultatRequete.getLong("numero_telephone");
				String pathologie = resultatRequete.getString("pathologies");
				listePatient.add(new Patient(id, nomRecherche, prenom, sexe, dateNaissance, adresse, mail, telephone, pathologie));
			}
			return listePatient;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
	}
	
	/**
	 * M�thode DAO permettant de rechercher un patient � partir de sa date de naissance.
	 * <p>La requete SQL utilis�e est [SELECT * FROM `patient` WHERE `date_de_naissance` = 'DonneesDates.formatDateSQL(date)']</p> 
	 * @param dateDeNaissance
	 * @return le patient en question sinon null
	 */
	public ArrayList<Patient> rechercheParDateDAO(LocalDate date) {
		Statement requeteSQL = null;
		ResultSet resultatRequete = null;
		ArrayList<Patient> listePatient = new ArrayList<Patient>();
		try {
			requeteSQL = connectionBDD.createStatement();
			String rechercheSQL ="SELECT * FROM `patient` WHERE `date_de_naissance` = '"+DonneesDates.formatDateSQL(date)+"'";
			resultatRequete = requeteSQL.executeQuery(rechercheSQL);
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
		try {
			while(resultatRequete.next()) {
				int id = resultatRequete.getInt("id_patient");
				String nom = resultatRequete.getString("nom");
				String prenom = resultatRequete.getString("prenom");
				char sexe = resultatRequete.getString("sexe").charAt(0);
				LocalDate dateNaissance = resultatRequete.getDate("date_de_naissance").toLocalDate();
				String adresse = resultatRequete.getString("adresse");
				String mail = resultatRequete.getString("adresse_mail");
				long telephone = resultatRequete.getLong("numero_telephone");
				String pathologie = resultatRequete.getString("pathologies");
				listePatient.add(new Patient(id, nom, prenom, sexe, dateNaissance, adresse, mail, telephone, pathologie));
			}
			return listePatient;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
	}
	
	/**
	 * M�thode DAO permettant de cr�er une nouvelle ligne dans la table Patient.
	 * <p>La requete SQL utilis�e est [INSERT INTO `article` (`nom`, `prenom`, `sexe`, `date_de_naissance`, `adresse`, `adresse_mail`, `numero_telephone`, `pathologies`) VALUES ('nom', 'prenom', 'sexe', 'DonneesDates.formatDateSQL(dateDeNaissance)', 'adresse', 'mail', 'telephone', 'pathologies']</p> 
	 * @param reference
	 * @param nom
	 * @param type
	 * @return le num�ro d'id du patient si la requ�te est r�ussie, -1 sinon
	 */
	public int creerPatientDAO(String nom, String prenom, String sexe, LocalDate dateDeNaissance, String adresse, String mail, long telephone, String pathologies) {
		Statement requeteSQL = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String ligneCodeSQL ="INSERT INTO `patient` (`nom`, `prenom`, `sexe`, `date_de_naissance`, `adresse`, `adresse_mail`, `numero_telephone`, `pathologies`) VALUES ('"+nom+"', '"+prenom+"', '"+sexe+"', '"+DonneesDates.formatDateSQL(dateDeNaissance)+"', '"+adresse+"', '"+mail+"', '"+telephone+"', '"+pathologies+"')";
			requeteSQL.executeUpdate(ligneCodeSQL);
			System.out.println("Patient cr��");
			return obtenirIDPatientDAO(nom, prenom, dateDeNaissance, adresse, mail);
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return -1;
		}
	}
	
	private int obtenirIDPatientDAO(String nom, String prenom, LocalDate dateDeNaissance, String adresse, String mail) {
		Statement requeteSQL = null;
		ResultSet resultatRequete = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String rechercheSQL ="SELECT * FROM `patient` WHERE `nom` = '"+nom+"' AND `prenom` = '"+prenom+"' AND `date_de_naissance` = '"+DonneesDates.formatDateSQL(dateDeNaissance)+"' AND `adresse` = '"+adresse+"' AND `adresse_mail` = '"+mail+"'";
			resultatRequete = requeteSQL.executeQuery(rechercheSQL);
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return -1;
		}
		try {
			while(resultatRequete.next()) {
				return resultatRequete.getInt("id_patient");
			}
			return -1;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return -1;
		}
	}
	
	/**
	 * M�thode DAO permettant de supprimer une ligne de la table Patient.
	 * <p>La requete SQL utilis�e est [DELETE FROM `patient` WHERE `id_patient` = 'id']</p> 
	 * @param id
	 * @return true si la requ�te est r�ussie, false sinon
	 */
	public boolean supprimerPatientDAO(int id) {
		// IllegalArgument si le patient n'existe pas dans la bdd
		Statement requeteSQL = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String ligneCodeSQL ="DELETE FROM `patient` WHERE `id_patient` = '"+id+"'";
			requeteSQL.executeUpdate(ligneCodeSQL);
			System.out.println("Patient supprim�");
			return true;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return false;
		}
	}
	
	/**
	 * M�thode DAO permettant de modifier le nom d'une ligne de la table patient.
	 * <p>La requete SQL utilis�e est [UPDATE `patient` SET `nom` = 'nouveauNom' WHERE `patient`.`id_patient` = patient.getIDPatient();]</p> 
	 * @param patient
	 * @param nouveauNom
	 * @return true si la requ�te est r�ussie, false sinon
	 */
	public boolean modifierNomPatientDAO(int idPatient, String nouveauNom) {
		Statement requeteSQL = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String ligneCodeSQL ="UPDATE `patient` SET `nom` = '"+nouveauNom+"' WHERE `patient`.`id_patient` = "+idPatient+";";
			requeteSQL.executeUpdate(ligneCodeSQL);
			System.out.println("Nom du patient modifi�");
			return true;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return false;
		}
	}
	
	/**
	 * M�thode DAO permettant de modifier le prenom d'une ligne de la table patient.
	 * <p>La requete SQL utilis�e est [UPDATE `patient` SET `prenom` = 'nouveauPrenom' WHERE `patient`.`id_patient` = patient.getIDPatient();]</p> 
	 * @param patient
	 * @param nouveauPrenom
	 * @return true si la requ�te est r�ussie, false sinon
	 */
	public boolean modifierPrenomPatientDAO(int idPatient, String nouveauPrenom) {
		Statement requeteSQL = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String ligneCodeSQL ="UPDATE `patient` SET `prenom` = '"+nouveauPrenom+"' WHERE `patient`.`id_patient` = "+idPatient+";";
			requeteSQL.executeUpdate(ligneCodeSQL);
			System.out.println("Pr�nom du patient modifi�");
			return true;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return false;
		}
	}
	
	/**
	 * M�thode DAO permettant de modifier le sexe d'une ligne de la table patient.
	 * <p>La requete SQL utilis�e est [UPDATE `patient` SET `sexe` = 'nouveauSexe' WHERE `patient`.`id_patient` = patient.getIDPatient();]</p> 
	 * @param patient
	 * @param nouveauSexe
	 * @return true si la requ�te est r�ussie, false sinon
	 */
	public boolean modifierSexePatientDAO(int idPatient, String nouveauSexe) {
		Statement requeteSQL = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String ligneCodeSQL ="UPDATE `patient` SET `sexe` = '"+nouveauSexe+"' WHERE `patient`.`id_patient` = "+idPatient+";";
			requeteSQL.executeUpdate(ligneCodeSQL);
			System.out.println("Sexe du patient modifi�");
			return true;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return false;
		}
	}
	
	/**
	 * M�thode DAO permettant de modifier la date de naissance d'une ligne de la table patient.
	 * <p>La requete SQL utilis�e est [UPDATE `patient` SET `date_de_naissance` = 'DonneesDates.formatDateSQL(nouvelleDate)' WHERE `patient`.`id_patient` = patient.getIDPatient();]</p> 
	 * @param patient
	 * @param nouvelleDate
	 * @return true si la requ�te est r�ussie, false sinon
	 */
	public boolean modifierDateDeNaissancePatientDAO(int idPatient, LocalDate nouvelleDate) {
		Statement requeteSQL = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String ligneCodeSQL ="UPDATE `patient` SET `date_de_naissance` = '"+DonneesDates.formatDateSQL(nouvelleDate)+"' WHERE `patient`.`id_patient` = "+idPatient+";";
			requeteSQL.executeUpdate(ligneCodeSQL);
			System.out.println("Date de naissance du patient modifi�");
			return true;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return false;
		}
	}
	
	/**
	 * M�thode DAO permettant de modifier l'adresse d'une ligne de la table patient.
	 * <p>La requete SQL utilis�e est [UPDATE `patient` SET `adresse` = 'nouvelleAdresse' WHERE `patient`.`id_patient` = patient.getIDPatient();]</p> 
	 * @param patient
	 * @param nouvelleAdresse
	 * @return true si la requ�te est r�ussie, false sinon
	 */
	public boolean modifierAdressePatientDAO(int idPatient, String nouvelleAdresse) {
		Statement requeteSQL = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String ligneCodeSQL ="UPDATE `patient` SET `adresse` = '"+nouvelleAdresse+"' WHERE `patient`.`id_patient` = "+idPatient+";";
			requeteSQL.executeUpdate(ligneCodeSQL);
			System.out.println("Adresse du patient modifi�");
			return true;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return false;
		}
	}
	
	/**
	 * M�thode DAO permettant de modifier l'adresse mail d'une ligne de la table patient.
	 * <p>La requete SQL utilis�e est [UPDATE `patient` SET `adresse_mail` = 'nouvelleAdresseMail' WHERE `patient`.`id_patient` = patient.getIDPatient();]</p> 
	 * @param patient
	 * @param nouvelleAdresseMail
	 * @return true si la requ�te est r�ussie, false sinon
	 */
	public boolean modifierMailPatientDAO(int idPatient, String nouvelleAdresseMail) {
		Statement requeteSQL = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String ligneCodeSQL ="UPDATE `patient` SET `adresse_mail` = '"+nouvelleAdresseMail+"' WHERE `patient`.`id_patient` = "+idPatient+";";
			requeteSQL.executeUpdate(ligneCodeSQL);
			System.out.println("Mail du patient modifi�");
			return true;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return false;
		}
	}
	
	/**
	 * M�thode DAO permettant de modifier le num�ro de t�l�phone d'une ligne de la table patient.
	 * <p>La requete SQL utilis�e est [UPDATE `patient` SET `numero_telephone` = 'nouveauNumero' WHERE `patient`.`id_patient` = patient.getIDPatient();]</p> 
	 * @param patient
	 * @param nouveauNumero
	 * @return true si la requ�te est r�ussie, false sinon
	 */
	public boolean modifierNumeroTelephonePatientDAO(int idPatient, long nouveauNumero) {
		Statement requeteSQL = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String ligneCodeSQL ="UPDATE `patient` SET `numero_telephone` = '"+nouveauNumero+"' WHERE `patient`.`id_patient` = "+idPatient+";";
			requeteSQL.executeUpdate(ligneCodeSQL);
			System.out.println("Num�ro de t�l�phone du patient modifi�");
			return true;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return false;
		}
	}
	
	/**
	 * M�thode DAO permettant de modifier la pathologie d'une ligne de la table patient.
	 * <p>La requete SQL utilis�e est [UPDATE `patient` SET `pathologies` = 'nouvellePathologie' WHERE `patient`.`id_patient` = patient.getIDPatient();]</p> 
	 * @param patient
	 * @param nouvellePathologie
	 * @return true si la requ�te est r�ussie, false sinon
	 */
	public boolean modifierPathologiePatientDAO(int idPatient, String nouvellePathologie) {
		Statement requeteSQL = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String ligneCodeSQL ="UPDATE `patient` SET `pathologies` = '"+nouvellePathologie+"' WHERE `patient`.`id_patient` = "+idPatient+";";
			requeteSQL.executeUpdate(ligneCodeSQL);
			System.out.println("Pathologie du patient modifi�");
			return true;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return false;
		}
	}
	
}