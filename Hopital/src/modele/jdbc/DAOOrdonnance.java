package modele.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;

import modele.donnees.DonneesDates;
import modele.humain.Medecin;
import modele.humain.Patient;
import modele.immateriel.Ordonnance;

/**
 * Classe regroupant l'ensemble des m�thodes DAO utilis�es pour la table Ordonnance.
 * @author Jimmy MENDES
 */
public class DAOOrdonnance {
	private String url="jdbc:mysql://localhost/hopital";
	private String loginUtilisateur = "root";
	private String motDePasse = "";
	private Connection connectionBDD;
	
	public DAOOrdonnance() {
		this.connectionBDD = SingleConnection.getInstance(url, loginUtilisateur, motDePasse);
	}
	
	/**
	 * M�thode DAO qui renvoie l'ensemble des ordonnances d'un patient.
	 * <p>La requete SQL utilis�e est [SELECT * FROM `ordonnance` WHERE `id_patient` = 'patient.getIDPatient()']</p>
	 * @param patient
	 * @return ArrayList<Ordonnance> avec l'ensemble des ordonnances d'un patient, null si erreur SQL
	 */
	public ArrayList<Ordonnance> afficherOrdonnanceDAO(Patient patient){
		Statement requeteSQL = null;
		ResultSet resultatRequete = null;
		ArrayList<Ordonnance> listeOrdonnance = new ArrayList<Ordonnance>();
		try {
			requeteSQL = connectionBDD.createStatement();
			String ordonnancePatientSQL ="SELECT * FROM `ordonnance` WHERE `id_patient` = '"+patient.getIDPatient()+"'";
			resultatRequete = requeteSQL.executeQuery(ordonnancePatientSQL);
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
		try {
			while(resultatRequete.next()) {
				int referenceOrdonnance = resultatRequete.getInt("reference_ordonnance");
				DAOPersonnel connectionTablePersonnel = new DAOPersonnel();
				int idPersonnel = resultatRequete.getInt("id_personnel");
				Medecin medecinOrdonnance = (Medecin)connectionTablePersonnel.rechercheParIDDAO(idPersonnel);
				LocalDate dateOrdonnance = resultatRequete.getDate("date_ordonnance").toLocalDate();
				String notesOrdonnance = resultatRequete.getString("contenu_ordonnance");
				listeOrdonnance.add(new Ordonnance(referenceOrdonnance,patient,medecinOrdonnance,dateOrdonnance,notesOrdonnance));
			}
			return listeOrdonnance;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
	}
	
	/**
	 * M�thode DAO permettant de rajouter une ligne dans la table Ordonnance.
	 * <p>La requete SQL utilis�e est [INSERT INTO `ordonnance` (`id_patient`, `id_personnel`, `date_ordonnance`, `contenu_ordonnance`) VALUES ('patient.getIDPatient()', 'medecin.getIDPersonnel()', 'DonneesDates.formatDateSQL(LocalDate.now())', 'contenuOrdonnance')]</p>
	 * @param patient
	 * @param medecin
	 * @param contenuOrdonnance
	 * @return true si l'ordonnance a bien �t� ajout�e � la table, false sinon
	 */
	public boolean ajouterOrdonnanceDAO(int idPatient, int idMedecin, String contenuOrdonnance) {
		Statement requeteSQL = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String ajoutOrdonnanceSQL ="INSERT INTO `ordonnance` (`id_patient`, `id_personnel`, `date_ordonnance`, `contenu_ordonnance`) VALUES ('"+idPatient+"', '"+idMedecin+"', '"+DonneesDates.formatDateSQL(LocalDate.now())+"', '"+contenuOrdonnance+"')";
			requeteSQL.executeUpdate(ajoutOrdonnanceSQL);
			System.out.println("Ordonnance ajout�e");
			return true;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return false;
		}
	}

}
