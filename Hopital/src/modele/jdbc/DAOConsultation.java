package modele.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;

import modele.donnees.DonneesDates;
import modele.humain.Medecin;
import modele.humain.Patient;
import modele.immateriel.Consultation;

/**
 * Classe regroupant l'ensemble des m�thodes DAO utilis�es pour la table Consultation.
 * @author Jimmy MENDES
 */
public class DAOConsultation {
	private String url="jdbc:mysql://localhost/hopital";
	private String loginUtilisateur = "root";
	private String motDePasse = "";
	private Connection connectionBDD;
	
	public DAOConsultation() {
		this.connectionBDD = SingleConnection.getInstance(url, loginUtilisateur, motDePasse);
	}
	
	/**
	 * M�thode DAO qui renvoie l'ensemble des consultations d'un patient.
	 * <p>La requete SQL utilis�e est [SELECT * FROM `consultation` WHERE `id_patient` = 'patient.getIDPatient()']</p>
	 * @param patient
	 * @return ArrayList<Consultation> avec l'ensemble des consultations d'un patient, null si erreur SQL
	 */
	public ArrayList<Consultation> afficherConsultationDAO(Patient patient){
		Statement requeteSQL = null;
		ResultSet resultatRequete = null;
		ArrayList<Consultation> listeConsultation = new ArrayList<Consultation>();
		try {
			requeteSQL = connectionBDD.createStatement();
			String consultationPatientSQL ="SELECT * FROM `consultation` WHERE `id_patient` = '"+patient.getIDPatient()+"'";
			resultatRequete = requeteSQL.executeQuery(consultationPatientSQL);
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
		try {
			while(resultatRequete.next()) {
				int referenceConsultation = resultatRequete.getInt("reference_consultation");
				DAOPersonnel connectionTablePersonnel = new DAOPersonnel();
				int idPersonnel = resultatRequete.getInt("id_personnel");
				Medecin medecinConsultation = (Medecin)connectionTablePersonnel.rechercheParIDDAO(idPersonnel);
				LocalDate dateConsultation = resultatRequete.getDate("date_consultation").toLocalDate();
				String notesConsultation = resultatRequete.getString("contenu_consultation");
				listeConsultation.add(new Consultation(referenceConsultation,patient,medecinConsultation,dateConsultation,notesConsultation));
			}
			return listeConsultation;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
	}
	
	/**
	 * M�thode DAO permettant de rajouter une ligne dans la table consultation.
	 * <p>La requete SQL utilis�e est [INSERT INTO `consultation` (`id_patient`, `id_personnel`, `date_consultation`, `contenu_consultation`) VALUES ('patient.getIDPatient()', 'medecin.getIDPersonnel()', 'DonneesDates.formatDateSQL(LocalDate.now())', 'contenuConsultation')]</p>
	 * @param patient
	 * @param medecin
	 * @param contenuConsultation
	 * @return true si la consultation a bien �t� ajout�e � la table, false sinon
	 */
	public boolean ajouterConsultationDAO(int idPatient, int idMedecin, String contenuConsultation) {
		Statement requeteSQL = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String ajoutConsultationSQL ="INSERT INTO `consultation` (`id_patient`, `id_personnel`, `date_consultation`, `contenu_consultation`) VALUES ('"+idPatient+"', '"+idMedecin+"', '"+DonneesDates.formatDateSQL(LocalDate.now())+"', '"+contenuConsultation+"')";
			requeteSQL.executeUpdate(ajoutConsultationSQL);
			System.out.println("Consultation ajout�e");
			return true;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return false;
		}
	}
}
