package modele.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;
import java.util.ArrayList;

import modele.materiel.Article;
import modele.materiel.EquipementMedical;
import modele.materiel.Medicament;

/**
 * Classe regroupant l'ensemble des m�thodes DAO utilis�es pour la table Article.
 * @author Jimmy MENDES
 */
public class DAOArticle {
	
	private String url="jdbc:mysql://localhost/hopital";
	private String loginUtilisateur = "root";
	private String motDePasse = "";
	private Connection connectionBDD;
	
	public DAOArticle() {
		this.connectionBDD = SingleConnection.getInstance(url, loginUtilisateur, motDePasse);
	}
	
	/**
	 * M�thode DAO qui renvoie l'integralit� de la table Article.
	 * <p>La requete SQL utilis�e est [SELECT * FROM `article`]</p>
	 * @return ArrayList<Article> avec l'ensemble des �l�ments de la table Article, null si erreur SQL
	 */
	public ArrayList<Article> inventaireArticleDAO() {
		Statement requeteSQL = null;
		ResultSet resultatRequete = null;
		ArrayList<Article> listeArticle = new ArrayList<Article>();
		try {
			requeteSQL = connectionBDD.createStatement();
			String inventaireSQL ="SELECT * FROM `article`";
			resultatRequete = requeteSQL.executeQuery(inventaireSQL);
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
		
		try {
			while(resultatRequete.next()) {
				String typeArticle = resultatRequete.getString("type");
				if(typeArticle.equalsIgnoreCase("article_general")) {
					listeArticle.add(new Article(resultatRequete.getString("reference_article"), resultatRequete.getString("nom"), resultatRequete.getInt("quantite")));
				}
				else if(typeArticle.equalsIgnoreCase("equipement")) {
					listeArticle.add(new EquipementMedical(resultatRequete.getString("reference_article"), resultatRequete.getString("nom"), resultatRequete.getInt("quantite")));
				}
				else {
					listeArticle.add(new Medicament(resultatRequete.getString("reference_article"), resultatRequete.getString("nom"), resultatRequete.getInt("quantite")));
				}
			}
			return listeArticle;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
	}
	
	/**
	 * M�thode DAO permettant de rechercher un article � partir de son nom.
	 * <p>La requete SQL utilis�e est [SELECT * FROM `article` WHERE `nom` = 'nom']</p>
	 * @param nom
	 * @return l'article en question sinon null
	 */
	public ArrayList<Article> rechercheParNomDAO(String nom) {
		Statement requeteSQL = null;
		ResultSet resultatRequete = null;
		ArrayList<Article> listeArticle = new ArrayList<Article>();
		try {
			requeteSQL = connectionBDD.createStatement();
			String rechercheSQL ="SELECT * FROM `article` WHERE `nom` = '"+nom+"'";
			resultatRequete = requeteSQL.executeQuery(rechercheSQL);
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
		try {
			while(resultatRequete.next()) {
				String typeArticle = resultatRequete.getString("type");
				if(typeArticle.equalsIgnoreCase("article_general")) {
					listeArticle.add(new Article(resultatRequete.getString("reference_article"), resultatRequete.getString("nom"), resultatRequete.getInt("quantite")));
				}
				else if(typeArticle.equalsIgnoreCase("equipement")) {
					listeArticle.add(new EquipementMedical(resultatRequete.getString("reference_article"), resultatRequete.getString("nom"), resultatRequete.getInt("quantite")));
				}
				else {
					listeArticle.add(new Medicament(resultatRequete.getString("reference_article"), resultatRequete.getString("nom"), resultatRequete.getInt("quantite")));
				}
			}
			return listeArticle;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
	}
	
	/**
	 * M�thode DAO permettant de rechercher un article � partir de sa r�f�rence.
	 * <p>La requete SQL utilis�e est [SELECT * FROM `article` WHERE `reference_article` = 'reference']</p>
	 * @param reference
	 * @return l'article en question sinon null
	 */
	public Article rechercheParReferenceDAO(String reference) {
		Statement requeteSQL = null;
		ResultSet resultatRequete = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String rechercheSQL ="SELECT * FROM `article` WHERE `reference_article` = '"+reference+"'";
			resultatRequete = requeteSQL.executeQuery(rechercheSQL);
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
		try {
			while(resultatRequete.next()) {
				String typeArticle = resultatRequete.getString("type");
				if(typeArticle.equalsIgnoreCase("article_general")) {
					return new Article(resultatRequete.getString("reference_article"), resultatRequete.getString("nom"), resultatRequete.getInt("quantite"));
				}
				else if(typeArticle.equalsIgnoreCase("equipement")) {
					return new EquipementMedical(resultatRequete.getString("reference_article"), resultatRequete.getString("nom"), resultatRequete.getInt("quantite"));
				}
				else {
					return new Medicament(resultatRequete.getString("reference_article"), resultatRequete.getString("nom"), resultatRequete.getInt("quantite"));
				}
			}
			return null;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
	}
	
	/**
	 * M�thode DAO permettant de cr�er une nouvelle ligne dans la table Article.
	 * <p>La requete SQL utilis�e est [INSERT INTO `article` (`reference_article`, `nom`, `quantite`, `type`) VALUES ('reference', 'nom', '0', 'type']</p>
	 * @param reference
	 * @param nom
	 * @param type
	 * @return true si la requ�te est r�ussie, false sinon
	 */
	public boolean creerArticleDAO(String reference, String nom, String type) {
		Statement requeteSQL = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String inventaireSQL ="INSERT INTO `article` (`reference_article`, `nom`, `quantite`, `type`) VALUES ('"+reference+"', '"+nom+"', '0', '"+type+"');";
			requeteSQL.executeUpdate(inventaireSQL);
			System.out.println("Article cr��");
			return true;
		}
		catch(SQLIntegrityConstraintViolationException e) {
			System.out.println("L'article existe d�j�");
			return false;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return false;
		}
	}
	
	/**
	 * M�thode DAO permettant de supprimer une ligne de la table Article.
	 * <p>La requete SQL utilis�e est [DELETE FROM `article` WHERE `reference_article` = 'article.getReference()']</p>
	 * @param article
	 * @return true si la requ�te est r�ussie, false sinon
	 */
	public boolean supprimerArticleDAO(Article article) {
		// IllegalArgument si l'article n'existe pas dans la bdd
		Statement requeteSQL = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String inventaireSQL ="DELETE FROM `article` WHERE `reference_article` = '"+article.getReference()+"'";
			requeteSQL.executeUpdate(inventaireSQL);
			System.out.println("Article supprim�");
			return true;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return false;
		}
	}
	
	/**
	 * M�thode DAO permettant d'ajouter une quantit� � un �l�ment de la table Article.
	 * <p>La requete SQL utilis�e est [UPDATE `article` SET `quantite` = `quantite` + 'nombre' WHERE `article`.`reference_article` = 'reference']</p>
	 * @param reference
	 * @param nombre
	 * @return true si la requ�te est r�ussie, false sinon
	 * @throws IllegalArgumentException si nombre <= 0
	 */
	public boolean ajouterArticleDAO(String reference, int nombre) {
		// Ajouter un throw illegal si reference n'existe pas
		if(nombre <= 0) throw new IllegalArgumentException();
		DAOArticle connectionTableArticle = new DAOArticle();
		Article articleEnCours = connectionTableArticle.rechercheParReferenceDAO(reference);
		if(articleEnCours == null) return false;
		Statement requeteSQL = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String inventaireSQL ="UPDATE `article` SET `quantite` = `quantite` + '"+nombre+"' WHERE `article`.`reference_article` = '"+reference+"'";
			requeteSQL.executeUpdate(inventaireSQL);
			if(nombre == 1) System.out.println(nombre + " unit� ajout�e");
			else System.out.println(nombre + " unit�s ajout�es");
			return true;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return false;
		}
	}
	
	/**
	 * M�thode DAO permettant de retirer une quantit� � un �l�ment de la table Article.
	 * <p>La requete SQL utilis�e est [UPDATE `article` SET `quantite` = `quantite` - 'nombre' WHERE `article`.`reference_article` = 'reference']</p>
	 * @param reference
	 * @param nombre
	 * @return true si la requ�te est r�ussie, false sinon
	 * @throws IllegalArgumentException si nombre <= 0
	 */
	public boolean retirerArticleDAO(String reference, int nombre) {
		// Ajouter un throw illegal si reference n'existe pas + voir si retrait supp�rieur � la quantit� pr�sente.
		if(nombre <= 0) throw new IllegalArgumentException();
		Statement requeteSQL = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String inventaireSQL ="UPDATE `article` SET `quantite` = `quantite` - '"+nombre+"' WHERE `article`.`reference_article` = '"+reference+"'";
			requeteSQL.executeUpdate(inventaireSQL);
			if(nombre == 1) System.out.println(nombre + " unit� retir�e");
			else System.out.println(nombre + " unit�s retir�es");
			return true;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return false;
		}
	}
	

}