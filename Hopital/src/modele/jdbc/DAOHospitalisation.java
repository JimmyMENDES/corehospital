package modele.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;

import modele.donnees.DonneesDates;

public class DAOHospitalisation {

	private String url="jdbc:mysql://localhost/hopital";
	private String loginUtilisateur = "root";
	private String motDePasse = "";
	private Connection connectionBDD;
	
	public DAOHospitalisation() {
		this.connectionBDD = SingleConnection.getInstance(url, loginUtilisateur, motDePasse);
	}
	
	/**
	 * 
	 * @param idPatient
	 * @return null si non hospitalis�, le code chambre sinon
	 */
	public String estHospitaliseDAO(int idPatient) {
		Statement requeteSQL = null;
		ResultSet resultatRequete = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String rechercheSQL ="SELECT * FROM `hospitalisation` WHERE `id_patient` = '"+idPatient+"'";
			resultatRequete = requeteSQL.executeQuery(rechercheSQL);
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
		try {
			while(resultatRequete.next()) {
				LocalDate dateEntree = resultatRequete.getDate("date_entree").toLocalDate();
				LocalDate dateSortie;
				if(resultatRequete.getDate("date_sortie") != null) {
					dateSortie = resultatRequete.getDate("date_sortie").toLocalDate();
				}
				else dateSortie = null;
				if(dateEntree.isBefore(LocalDate.now()) || dateEntree.isEqual(LocalDate.now())) {
					if(dateSortie == null || dateSortie.isAfter(LocalDate.now())) {
						return resultatRequete.getString("code_lit");
					}
				}
			}
			return null;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
	}
	
	public boolean ajouterHospitalisationDAO(int idPatient, String codeLit, LocalDate dateDebut, LocalDate dateFin) {
		Statement requeteSQL = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String ajoutHospitalisationSQL = "";
			if(dateFin != null) {
				ajoutHospitalisationSQL ="INSERT INTO `hospitalisation` (`id_patient`, `code_lit`, `date_entree`, `date_sortie`) VALUES ('"+idPatient+"', '"+codeLit+"', '"+DonneesDates.formatDateSQL(dateDebut)+"', '"+DonneesDates.formatDateSQL(dateFin)+"');";
			}
			else {
				ajoutHospitalisationSQL ="INSERT INTO `hospitalisation` (`id_patient`, `code_lit`, `date_entree`) VALUES ('"+idPatient+"', '"+codeLit+"', '"+DonneesDates.formatDateSQL(dateDebut)+"');";
			}
			requeteSQL.executeUpdate(ajoutHospitalisationSQL);
			System.out.println("Hospitalisation ajout�e");
			return true;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return false;
		}
	}
	
	public ArrayList<String> listeChambreIndividuelleVideService(char codeService) {
		Statement requeteSQL = null;
		ResultSet resultatRequete = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String rechercheSQL ="SELECT * FROM `hospitalisation`";
			resultatRequete = requeteSQL.executeQuery(rechercheSQL);
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
		try {
			ArrayList<String> listeChambrePrise = new ArrayList<String>();
			while(resultatRequete.next()) {
				if(resultatRequete.getString("code_lit").charAt(0) == codeService) {
					if(resultatRequete.getString("code_lit").length() == 3 && Integer.parseInt(resultatRequete.getString("code_lit").charAt(1)+"") < 9) {
						if(resultatRequete.getString("date_sortie") == null || resultatRequete.getDate("date_sortie").toLocalDate().isAfter(LocalDate.now())) {
							if(resultatRequete.getDate("date_entree").toLocalDate().isBefore(LocalDate.now()) || resultatRequete.getDate("date_entree").toLocalDate().isEqual(LocalDate.now())) {
								listeChambrePrise.add(resultatRequete.getString("code_lit"));
							}
						}
					}
				}	
			}
			int compteur;
			ArrayList<String> listeChambreIndividuelleVide = new ArrayList<String>();
			String[] listeChambreIndividuelle = {codeService+"11",codeService+"21",codeService+"31",codeService+"41",codeService+"51",codeService+"61",codeService+"71",codeService+"81"};
			for(int i = 0 ; i < listeChambreIndividuelle.length  ; i++) {
				compteur = 0;
				for(int j = 0 ; j < listeChambrePrise.size() ; j++) {
					if(listeChambrePrise.get(j).equalsIgnoreCase(listeChambreIndividuelle[i])) compteur++;
				}
				if(compteur == 0) listeChambreIndividuelleVide.add(listeChambreIndividuelle[i]);
			}
			return listeChambreIndividuelleVide;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
	}
	
	public ArrayList<String> listeChambreDoubleVideService(char codeService) {
		Statement requeteSQL = null;
		ResultSet resultatRequete = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String rechercheSQL ="SELECT * FROM `hospitalisation`";
			resultatRequete = requeteSQL.executeQuery(rechercheSQL);
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
		try {
			ArrayList<String> listeChambrePrise = new ArrayList<String>();
			while(resultatRequete.next()) {
				if(resultatRequete.getString("code_lit").charAt(0) == codeService) {
					int tailleCodeLit = resultatRequete.getString("code_lit").length();
					if((tailleCodeLit == 3 && Integer.parseInt(resultatRequete.getString("code_lit").charAt(1)+"") < 9 ) || (tailleCodeLit == 4 && Integer.parseInt(resultatRequete.getString("code_lit").substring(1,3)) < 17)) {
						if(resultatRequete.getString("date_sortie") == null || resultatRequete.getDate("date_sortie").toLocalDate().isAfter(LocalDate.now())) {
							if(resultatRequete.getDate("date_entree").toLocalDate().isBefore(LocalDate.now()) || resultatRequete.getDate("date_entree").toLocalDate().isEqual(LocalDate.now())) {
								listeChambrePrise.add(resultatRequete.getString("code_lit"));
							}
						}
					}
				}	
			}
			int compteur;
			ArrayList<String> listeChambreIndividuelleVide = new ArrayList<String>();
			String[] listeChambreIndividuelle = {codeService+"91",codeService+"92",codeService+"101",codeService+"102",codeService+"111",codeService+"112",
			codeService+"121",codeService+"122",codeService+"131",codeService+"132",codeService+"141",codeService+"142",codeService+"151",codeService+"152",codeService+"161",codeService+"162"};
			for(int i = 0 ; i < listeChambreIndividuelle.length  ; i++) {
				compteur = 0;
				for(int j = 0 ; j < listeChambrePrise.size() ; j++) {
					if(listeChambrePrise.get(j).equalsIgnoreCase(listeChambreIndividuelle[i])) compteur++;
				}
				if(compteur == 0) listeChambreIndividuelleVide.add(listeChambreIndividuelle[i]);
			}
			return listeChambreIndividuelleVide;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
	}
	
	public ArrayList<String> listeChambreQuadrupleVideService(char codeService) {
		Statement requeteSQL = null;
		ResultSet resultatRequete = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String rechercheSQL ="SELECT * FROM `hospitalisation`";
			resultatRequete = requeteSQL.executeQuery(rechercheSQL);
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
		try {
			ArrayList<String> listeChambrePrise = new ArrayList<String>();
			while(resultatRequete.next()) {
				if(resultatRequete.getString("code_lit").charAt(0) == codeService) {
					int tailleCodeLit = resultatRequete.getString("code_lit").length();
					if(tailleCodeLit == 4 && Integer.parseInt(resultatRequete.getString("code_lit").substring(1,3)) > 16) {
						if(resultatRequete.getString("date_sortie") == null || resultatRequete.getDate("date_sortie").toLocalDate().isAfter(LocalDate.now())) {
							if(resultatRequete.getDate("date_entree").toLocalDate().isBefore(LocalDate.now()) || resultatRequete.getDate("date_entree").toLocalDate().isEqual(LocalDate.now())) {
								listeChambrePrise.add(resultatRequete.getString("code_lit"));
							}
						}
					}
				}	
			}
			int compteur;
			ArrayList<String> listeChambreIndividuelleVide = new ArrayList<String>();
			String[] listeChambreIndividuelle = {codeService+"171",codeService+"172",codeService+"173",codeService+"174",codeService+"181",codeService+"182",
			codeService+"183",codeService+"184",codeService+"191",codeService+"192",codeService+"193",codeService+"194",codeService+"201",codeService+"202",codeService+"203",codeService+"204"};
			for(int i = 0 ; i < listeChambreIndividuelle.length  ; i++) {
				compteur = 0;
				for(int j = 0 ; j < listeChambrePrise.size() ; j++) {
					if(listeChambrePrise.get(j).equalsIgnoreCase(listeChambreIndividuelle[i])) compteur++;
				}
				if(compteur == 0) listeChambreIndividuelleVide.add(listeChambreIndividuelle[i]);
			}
			return listeChambreIndividuelleVide;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
	}
	
}