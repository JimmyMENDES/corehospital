package modele.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import modele.humain.Patient;
import modele.humain.Personnel;
import modele.immateriel.Analyse;
import modele.materiel.AppareilMedical;

/**
 * Classe regroupant l'ensemble des m�thodes DAO utilis�es pour la table Analyse.
 * @author Jimmy MENDES
 */
public class DAOAnalyse {
	
	private String url="jdbc:mysql://localhost/hopital";
	private String loginUtilisateur = "root";
	private String motDePasse = "";
	private Connection connectionBDD;
	
	public DAOAnalyse() {
		this.connectionBDD = SingleConnection.getInstance(url, loginUtilisateur, motDePasse);
	}
	
	/**
	 * M�thode DAO permettant de rajouter une ligne dans la table analyse.
	 * <p>La requete SQL utilis�e est [INSERT INTO `analyse` (`id_patient`, `id_personnel`, `code_appareil`, `date_heure_analyse`, `contenu_analyse`) VALUES ('patient.getIDPatient()', 'technicien.getIDPersonnel()', 'appareil.getCodeAppareil()', 'LocalDateTime.now().format(formatageSQL)', 'contenuAnalyse')]</p>
	 * @param patient
	 * @param technicien
	 * @param appareil
	 * @param contenuAnalyse
	 * @return true si l'analyse a bien �t� ajout�e � la table, false sinon
	 */
	public boolean ajouterAnalyseDAO(int idPatient, int idTechnicien, String codeAppareil, String contenuAnalyse) {
		DateTimeFormatter formatageSQL = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		Statement requeteSQL = null;
		try {
			requeteSQL = connectionBDD.createStatement();
			String ajoutAnalyseSQL ="INSERT INTO `analyse` (`id_patient`, `id_personnel`, `code_appareil`, `date_heure_analyse`, `contenu_analyse`) VALUES ('"+idPatient+"', '"+idTechnicien+"', '"+codeAppareil+"', '"+LocalDateTime.now().format(formatageSQL)+"', '"+contenuAnalyse+"')";
			requeteSQL.executeUpdate(ajoutAnalyseSQL);
			System.out.println("Analyse ajout�e");
			return true;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return false;
		}
	}
	
	/**
	 * M�thode DAO qui renvoie l'ensemble des analyses d'un patient.
	 * <p>La requete SQL utilis�e est [SELECT * FROM `analyse` WHERE `id_patient` = 'patient.getIDPatient()']</p>
	 * @param patient
	 * @return ArrayList<Analyse> avec l'ensemble des analyse d'un patient, null si erreur SQL
	 */
	public ArrayList<Analyse> afficherAnalyseDAO(Patient patient){
		Statement requeteSQL = null;
		ResultSet resultatRequete = null;
		ArrayList<Analyse> listeAnalyse = new ArrayList<Analyse>();
		try {
			requeteSQL = connectionBDD.createStatement();
			String analysePatientSQL ="SELECT * FROM `analyse` WHERE `id_patient` = '"+patient.getIDPatient()+"'";
			resultatRequete = requeteSQL.executeQuery(analysePatientSQL);
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
		try {
			while(resultatRequete.next()) {
				DAOPersonnel connectionTablePersonnel = new DAOPersonnel();
				DAOAppareil connectionTableAppareil = new DAOAppareil();
				int idPersonnel = resultatRequete.getInt("id_personnel");
				Personnel technicienEnCours = connectionTablePersonnel.rechercheParIDDAO(idPersonnel);
				LocalDate dateEncours = resultatRequete.getDate("date_heure_analyse").toLocalDate();
				LocalTime heureEncours = resultatRequete.getTime("date_heure_analyse").toLocalTime();
				LocalDateTime dateHeureEnCours = LocalDateTime.of(dateEncours, heureEncours);
				AppareilMedical appareilEnCours = connectionTableAppareil.rechercheParReferenceDAO(resultatRequete.getString("code_appareil"));
				listeAnalyse.add(new Analyse(resultatRequete.getInt("reference_analyse"), patient, technicienEnCours, dateHeureEnCours, appareilEnCours, resultatRequete.getString("contenu_analyse")));
			}
			return listeAnalyse;
		}
		catch(SQLException e) {
			System.out.println("Erreur SQL");
			return null;
		}
	}
	
}