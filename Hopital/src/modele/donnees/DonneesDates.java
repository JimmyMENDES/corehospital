package modele.donnees;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Classe regroupant l'ensemble des m�thodes utilis�es pour g�rer les donn�es li�es aux dates.
 * @author Jimmy MENDES
 */
public abstract class DonneesDates {

	/**
	 * M�thode permettant de d�couper un String en trois int pour une LocalDate.
	 * @param date
	 * @return LocalDate.of(le String en entr�e d�coup� en trois int)
	 * @throws DateTimeException si le format d'entr� est incorrect
	 */
	public static LocalDate fromStringToLocalDate(String date) {
		int dateJour = 0, dateMois = 1, dateAnnee = 1;
		if(date.length() == 8) {
			String choixJourString = Character.toString(date.charAt(0)) + Character.toString(date.charAt(1)) ;
			dateJour = Integer.parseInt(choixJourString);
			String choixMoisString = Character.toString(date.charAt(2)) + Character.toString(date.charAt(3)) ;
			dateMois = Integer.parseInt(choixMoisString);
			String choixAnneeString = Character.toString(date.charAt(4)) + Character.toString(date.charAt(5)) + Character.toString(date.charAt(6)) + Character.toString(date.charAt(7));
			dateAnnee = Integer.parseInt(choixAnneeString);
		}
		else if(date.length() == 7) {
			String choixJourString = Character.toString(date.charAt(0)) ;
			dateJour = Integer.parseInt(choixJourString);
			String choixMoisString = Character.toString(date.charAt(1)) + Character.toString(date.charAt(2)) ;
			dateMois = Integer.parseInt(choixMoisString);
			String choixAnneeString = Character.toString(date.charAt(3)) + Character.toString(date.charAt(4)) + Character.toString(date.charAt(5)) + Character.toString(date.charAt(6));
			dateAnnee = Integer.parseInt(choixAnneeString);
		}
		return LocalDate.of(dateAnnee, dateMois, dateJour);
	}
	
	/**
	 * M�thode permettant de fortmater une date au format jj/mm/aaaa.
	 * @param date
	 * @return la date format�e
	 */
	 public static String formatDate(LocalDate date) {
	    	DateTimeFormatter formatage = DateTimeFormatter.ofPattern("dd/LL/yyyy");
	    	return date.format(formatage);
	 }
	 
	/**
	 * M�thode permettant de fortmater une dateTime au format jj/mm/aaaa.
	 * @param date
	 * @return la date format�e
	 */
	 public static String formatDate(LocalDateTime dateHeure) {
		 	DateTimeFormatter formatage = DateTimeFormatter.ofPattern("dd/LL/yyyy");
		 	return dateHeure.format(formatage);
	 }
	 
	/**
	 * M�thode permettant de fortmater une dateTime au format hh:mm.
	 * @param date
	 * @return la date format�e
	 */
	 public static String formatHeure(LocalDateTime dateHeure) {
		 	DateTimeFormatter formatage = DateTimeFormatter.ofPattern("HH:mm");
		 	return dateHeure.format(formatage);
	 }
	 
	/**
	 * M�thode permettant de fortmater une dateTime au format hh:mm.
	 * @param date
	 * @return la date format�e
	 */
	 public static String formatHeureComparaison(LocalDateTime dateHeure) {
		 DateTimeFormatter formatage = DateTimeFormatter.ofPattern("HHmm");
		 return dateHeure.format(formatage);
	}
	 
	 /**
	  * M�thode permettant de fortmater une date au format aaaa-mm-jj.
	  * @param date
	  * @return la date format�e
	  */
	  public static String formatDateSQL(LocalDate date) {
		  	DateTimeFormatter formatage = DateTimeFormatter.ofPattern("yyyy-LL-dd");
		    return date.format(formatage);
	  }
	 
	  
	/**
	 * M�thode permettant de fortmater une dateTime au format jj/mm/aaaa hh:mm.
	 * @param dateTime
	 * @return la dateTime format�e
	 */
	  public static String formatDateHeure(LocalDateTime dateTime) {
		  	DateTimeFormatter formatage = DateTimeFormatter.ofPattern("dd/LL/yyyy HH:mm");
			return dateTime.format(formatage);
	  }
		 
	/**
	 * M�thode permettant de fortmater une dateTime au format jj/mm/aaaa hh:mm:ss.
	 * @param dateTime
	 * @return la dateTime format�e
	 */
	 public static String formatDateTime(LocalDateTime dateTime) {
		    DateTimeFormatter formatage = DateTimeFormatter.ofPattern("dd/LL/yyyy HH:mm:ss");
		    return dateTime.format(formatage);
	 }
	 
	 /**
	  * M�thode permettant de fortmater une date au format aaaa-mm-jj HH:mm:ss.
	  * @param dateTime
	  * @return la date format�e
	  */
	  public static String formatDateTimeSQL(LocalDateTime dateHeure) {
		  	DateTimeFormatter formatage = DateTimeFormatter.ofPattern("yyyy-LL-dd HH:mm:ss");
		    return dateHeure.format(formatage);
	  }
}
