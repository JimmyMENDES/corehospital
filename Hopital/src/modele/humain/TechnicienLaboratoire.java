package modele.humain;

import java.time.LocalDate;

/**
 * Classe représentant un technicien de laboratoire.
 * @author Jimmy MENDES
 */
public class TechnicienLaboratoire extends Personnel {

    public TechnicienLaboratoire(String nom, String prenom, char sexe, LocalDate date, String adresse, String mail, long numero, int id, double salaire, LocalDate dateAnciennete) {
    	super(nom,prenom,sexe,date,adresse,mail,numero,id,salaire,dateAnciennete);
    }

}