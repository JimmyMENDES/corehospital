package modele.humain;

import java.time.LocalDate;

/**
 * Classe repr�sentant un personnel medical de l'h�pital.
 */
public abstract class PersonnelMedical extends Personnel {

    protected long rPPS;
    
    public PersonnelMedical(String nom, String prenom, char sexe, LocalDate date, String adresse, String mail, long numero, int id, double salaire, LocalDate dateAnciennete, long rPPS) {
    	super(nom,prenom,sexe,date,adresse,mail,numero,id,salaire,dateAnciennete);
    	this.rPPS = rPPS;
    }

}