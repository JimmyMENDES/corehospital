package modele.humain;

import java.time.LocalDate;
import java.util.ArrayList;

import modele.immateriel.Analyse;
import modele.immateriel.Consultation;
import modele.immateriel.Ordonnance;

/**
 * Classe repr�sentant un Patient.
 */
public class Patient extends Personne {
	
	private int iDPatient;
	private String pathologies;
	private ArrayList<Consultation> listeConsultation;
	private ArrayList<Ordonnance> listeOrdonnance;
	private ArrayList<Analyse> listeAnalyse;

    public Patient(int iD, String nom, String prenom, char sexe, LocalDate date, String adresse, String mail, long numero, String pathologies) {
    	super(nom,prenom,sexe,date,adresse,mail,numero);
    	this.iDPatient = iD;
    	this.pathologies = pathologies;
    	listeConsultation = new ArrayList<Consultation>();
    	listeOrdonnance = new ArrayList<Ordonnance>();
    	listeAnalyse = new ArrayList<Analyse>();
    }
    
    /**
     * Accesseur de iDPatient.
     * @return iDPatient
     */
    public int getIDPatient() {
    	return this.iDPatient;
    }
    
    /**
     * Mutateur de iDPatient.
     * <p>Cette fontion n'est utilis�e que lors de la cr�ation d'un objet Patient.
     * Le numero en param�tre est le num�ro index + 1 de l'ArrayList contenant l'ensemble des Patients.</p>
     * @param numero
     */
    public void setIDPatient(int numero) {
    	this.iDPatient = numero;
    }
    
    /**
     * Accesseur de pathologies.
     * @return pathologies
     */
    public String getPathologies() {
    	return this.pathologies;
    }
    
    /**
     * Mutateur de pathologies.
     * @param pathologie
     */
    public void setPathologiePatient(String pathologie) {
    	this.pathologies = pathologie;
    }
    
    /**
     * Accesseur de listeConsultation.
     * @return listeConsultation
     */
    public ArrayList<Consultation> getListeConsultation(){
    	return this.listeConsultation;
    }
    
    /**
     * Accesseur de listeOrdonnance.
     * @return listeOrdonnance
     */
    public ArrayList<Ordonnance> getListeOrdonnance(){
    	return this.listeOrdonnance;
    }
    
    /**
     * Accesseur de listeAnalyse.
     * @return listeAnalyse
     */
    public ArrayList<Analyse> getListeAnalyse(){
    	return this.listeAnalyse;
    }
    
}