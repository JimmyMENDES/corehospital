package modele.humain;

import java.time.LocalDate;

/**
 * Classe repr�sentant un medecin de l'h�pital.
 */
public class Medecin extends PersonnelMedical  {

	private String specialite;
	private char codeService;

    public Medecin(String nom, String prenom, char sexe, LocalDate date, String adresse, String mail, long numero, int id, double salaire, LocalDate dateAnciennete, long rPPS, String specialite, char codeService) {
    	super(nom,prenom,sexe,date,adresse,mail,numero,id,salaire,dateAnciennete,rPPS);
    	this.specialite = specialite;
    	this.codeService = codeService;
    }
    
    public String getSpecialite() {
		return this.specialite;
	}
    
    public char getCodeService() {
    	return this.codeService;
    }

}