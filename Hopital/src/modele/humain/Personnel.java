package modele.humain;

import java.time.LocalDate;;

/**
 * Classe repr�sentant le personnel de l'h�pital.
 */
public abstract class Personnel extends Personne {

    protected int iDPersonnel;
    protected double salaire;
    protected LocalDate dateAnciennete;

    
    public Personnel(String nom, String prenom, char sexe, LocalDate date, String adresse, String mail, long numero, int id, double salaire, LocalDate dateAnciennete) {
    	super(nom,prenom,sexe,date,adresse,mail,numero);
    	this.iDPersonnel = id;
    	this.salaire = salaire;
    	this.dateAnciennete = dateAnciennete;
    }
    
    public int getIDPersonnel() {
    	return this.iDPersonnel;
    }
    
    public double getSalaire() {
    	return this.salaire;
    }
    
    public LocalDate getDateAnciennete() {
    	return this.dateAnciennete;
    }
    
}