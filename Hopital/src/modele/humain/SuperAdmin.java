package modele.humain;

import java.time.LocalDate;

/**
 * Classe représentant le SuperAdmin
 * @author Jimmy MENDES
 */
public class SuperAdmin extends Personne {

    public SuperAdmin(String nom, String prenom, char sexe, LocalDate date, String adresse, String mail, long numero) {
    	super(nom,prenom,sexe,date,adresse,mail,numero);
    }

}