package modele.humain;

import java.time.LocalDate;

/**
 * Classe représentant un infirmier.
 * @author Jimmy MENDES
 */
public class Infirmier extends PersonnelMedical {

	private char codeService;
	
    public Infirmier(String nom, String prenom, char sexe, LocalDate date, String adresse, String mail, long numero, int id, double salaire, LocalDate dateAnciennete, long rPPS, char codeService) {
    	super(nom,prenom,sexe,date,adresse,mail,numero,id,salaire,dateAnciennete,rPPS);
    	this.codeService = codeService;
    }
    
    public char getService() {
    	return this.codeService;
    }
    
    public void setService(char nouveauService) {
    	this.codeService = nouveauService;
    }

}