package modele.humain;

import java.time.LocalDate;

/**
 * Classe repr�sentant un agent d'administration de l'h�pital.
 */
public class AgentAdministrateur extends Personnel {

    public AgentAdministrateur(String nom, String prenom, char sexe, LocalDate date, String adresse, String mail, long numero, int id, double salaire, LocalDate dateAnciennete) {
    	super(nom,prenom,sexe,date,adresse,mail,numero,id,salaire,dateAnciennete);
    }
    
}