package modele.humain;

import java.time.LocalDate;

/**
 * Classe repr�sentant une Personne.
 */
public abstract class Personne {

	protected String nom;
    protected String prenom;
    protected char sexe;
    protected LocalDate dateDeNaissance; // Dates � g�rer
    protected String adresse;
    protected String adresseMail;
    protected long numeroDeTelephone;
    
    /**
     * Constructeur de la classe Personne.
     * <p>Constructeur utilis� par les constructeurs des classes filles</p>
     * @param nom
     * @param prenom
     * @param sexe
     * @param date
     * @param adresse
     * @param mail
     * @param numero
     */
    public Personne(String nom, String prenom, char sexe, LocalDate date, String adresse, String mail, long numero) {
    	this.nom = nom;
    	this.prenom = prenom;
    	this.sexe = sexe;
    	this.dateDeNaissance = date;
    	this.adresse = adresse;
    	this.adresseMail = mail;
    	this.numeroDeTelephone = numero;
    }
    
    /*
     * Ensemble des Accesseurs et Mutateurs de tout les param�tres de la classe Personne.
     */
    
    /**
     * Accesseur de nom.
     * @return nom
     */
    public String getNom() {
    	return this.nom;
    }
    
    /**
     * Accesseur de prenom.
     * @return prenom
     */
    public String getPrenom() {
    	return this.prenom;
    }
    
    /**
     * Accesseur de sexe.
     * @return sexe
     */
    public char getSexe() {
    	return this.sexe;
    }
    
    /**
     * Accesseur de dateDeNaissance.
     * @return dateDeNaissance
     */
    public LocalDate getDateDeNaissance() {
    	return this.dateDeNaissance;
    }
    
    /**
     * Accesseur de adresse.
     * @return adresse
     */
    public String getAdresse() {
    	return this.adresse;
    }
    
    /**
     * Accesseur de adresseMail.
     * @return adresseMail
     */
    public String getMail() {
    	return this.adresseMail;
    }
    
    /**
     * Accesseur de numeroDeTelephone.
     * @return numeroDeTelephone
     */
    public long getNumero() {
    	return this.numeroDeTelephone;
    }
    
    /**
     * Mutateur de nom.
     * @param nouveauNom
     */
    public void setNom(String nouveauNom) {
    	this.nom = nouveauNom;
    }
    
    /**
     * Mutateur de prenom.
     * @param nouveauPrenom
     */
    public void setPrenom(String nouveauPrenom) {
    	this.prenom = nouveauPrenom;
    }
    
    /**
     * Mutateur de sexe.
     * @param nouveauSexe
     */
    public void setSexe(char nouveauSexe) {
    	this.sexe = nouveauSexe;
    }
    
    /**
     * Mutateur de date de naissance.
     * @param nouvelleDate
     */
    public void setDateDeNaissance(LocalDate nouvelleDate) {
    	this.dateDeNaissance = nouvelleDate;
    }
    
    /**
     * Mutateur de adresse.
     * @param nouvelleAdresse
     */
    public void setAdresse(String nouvelleAdresse) {
    	this.adresse = nouvelleAdresse;
    }
    
    /**
     * Mutateur de adresseMail.
     * @param nouveauMail
     */
    public void setMail(String nouveauMail) {
    	this.adresseMail = nouveauMail;
    }
    
    /**
     * Mutateur de numeroDeTelephone.
     * @param nouveauNumero
     */
    public void setNumeroTelephone(long nouveauNumero) {
    	this.numeroDeTelephone = nouveauNumero;
    }

}