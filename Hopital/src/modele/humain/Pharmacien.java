package modele.humain;

import java.time.LocalDate;

/**
 * Classe représentant un pharmacien.
 * @author Jimmy MENDES
 */
public class Pharmacien extends PersonnelMedical {

    public Pharmacien(String nom, String prenom, char sexe, LocalDate date, String adresse, String mail, long numero, int id, double salaire, LocalDate dateAnciennete, long rPPS) {
    	super(nom,prenom,sexe,date,adresse,mail,numero,id,salaire,dateAnciennete,rPPS);
    }


}