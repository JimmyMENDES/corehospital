package modele.installations;

import java.util.ArrayList;

/**
 * Classe repr�sentant les urgences d'un h�pital.
 * <p>Cette classe est une sous-classe de Service. 
 * La diff�rence est que l'ArrayList<Chambre> n'est pas appel� dans les urgences.
 * � la place, Urgences dispose d'un ArrayList<Lit>.</p>
 * @author Jimmy MENDES
 */
public class Urgences extends Service {
	
	private ArrayList<Lit> listeLits;
	
	public Urgences(String nom, char code) {
		this.nomDuService = nom;
    	this.codeService = code;
		listeLits = new ArrayList<Lit>();
		for(int i = 0 ; i < 10 ; i++) {
			listeLits.add(new Lit(i+1, true));
		}
	}
	
	public ArrayList<Lit> getListeLits(){
		return this.listeLits;
	}
	
	public void afficherEtatService() {
    	System.out.println(nomDuService);
    	System.out.println(codeService);
    	for(int i = 0 ; i < listeLits.size() ; i++) {
    		System.out.println(listeLits.get(i).toString());
    	}
    	System.out.println();
    }

}
