package modele.installations;

import java.util.ArrayList;

import modele.materiel.Article;

public class Pharmacie {
	
	private ArrayList<Article> listeArticle;

    public Pharmacie() {
    	listeArticle = new ArrayList<Article>();
    }
    
    // Getters
    
    public ArrayList<Article> getListeArticle(){
    	return this.listeArticle;
    }

}