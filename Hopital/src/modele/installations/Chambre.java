package modele.installations;

import java.util.ArrayList;

/**
 * Classe repr�sentant une chambre dans un h�pital.
 * @author Jimmy MENDES
 */
public class Chambre {

    private int numeroChambre;
    private ArrayList<Lit> listeLits;

    public Chambre(int numero, ArrayList<Lit> listeLits) {
    	this.numeroChambre = numero;
    	this.listeLits = listeLits;
    }
    
    /**
     * Accesseur de numeroChambre.
     * @return numeroChambre
     */
    public int getNumeroChambre() {
    	return this.numeroChambre;
    }
    
    /**
     * Accesseur de listeLits.
     * @return listeLits
     */
    public ArrayList<Lit> getListeLits(){
    	return this.listeLits;
    }
    
}