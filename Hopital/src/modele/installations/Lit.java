package modele.installations;

import modele.humain.Patient;

/**
 * Classe repr�sentant un lit d'h�pital.
 * @author Jimmy MENDES
 */
public class Lit {

    private int numeroLit;
    private boolean litLibre;
    private Patient patientLit;
    
    public Lit(int numero, boolean libre) {
    	this.numeroLit = numero;
    	this.litLibre = libre;
    }
    
    /**
     * Accesseur du boolean litLibre.
     * @return litLibre
     */
    public boolean getLitLibre() {
    	return litLibre;
    }
    
    /**
     * Accesseur du Patient du Lit.
     * @return patientLit
     */
    public Patient getPatientLit() {
    	return this.patientLit;
    }
    
    /**
     * Mutateur du boolean litLibre.
     * @param lit
     */
    public void setLitLibre(boolean lit) {
    	this.litLibre = lit;
    }
    
    public void setPatientLit(Patient patient) {
    	this.patientLit = patient;
    }
    
    /**
     * M�thode r�sumant l'�tat du lit.
     */
    public String toString() {
    	String etat;
    	if(litLibre == true) etat = "vide";
    	else etat = "occup�";
    	return "Lit N�" + numeroLit + " " + etat;
    }

}