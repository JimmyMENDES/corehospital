package modele.installations;

import java.util.ArrayList;

/**
 * Classe repr�sentant un service de l'h�pital.
 * @author Jimmy MENDES
 */
public class Service {

	protected String nomDuService;
    protected char codeService;
	protected ArrayList<Chambre> listeChambres;
	
	public Service() {};

	/**
	 * Constructeur de Service.
	 * <p>Ce constructeur cr�e automatiquement un service de l'h�pital comme suit :
	 * <ul><li>8 chambres de 1 lit</li>
	 * <li>8 chambres de 2 lits</li>
	 * <li>4 chambres de 4 lits</li></ul>
	 * Le tout pour un total de 40 lits par service.
	 * @param nom
	 * @param code
	 * @param listeChambres
	 */
    public Service(String nom, char code, ArrayList<Chambre> listeChambres) {
    	this.nomDuService = nom;
    	this.codeService = code;
    	this.listeChambres = listeChambres;
    	for(int i = 1 ; i < 9 ; i++) {
    		this.listeChambres.add(new Chambre(i, new ArrayList<Lit>()));
    		this.listeChambres.get(i-1).getListeLits().add(new Lit(1, true));
    	}
    	for(int i = 9 ; i < 17 ; i++) {
    		this.listeChambres.add(new Chambre(i, new ArrayList<Lit>()));
    		this.listeChambres.get(i-1).getListeLits().add(new Lit(1, true));
    		this.listeChambres.get(i-1).getListeLits().add(new Lit(2, true));
    	}
    	for(int i = 17 ; i < 21 ; i++) {
    		this.listeChambres.add(new Chambre(i, new ArrayList<Lit>()));
    		this.listeChambres.get(i-1).getListeLits().add(new Lit(1, true));
    		this.listeChambres.get(i-1).getListeLits().add(new Lit(2, true));
    		this.listeChambres.get(i-1).getListeLits().add(new Lit(3, true));
    		this.listeChambres.get(i-1).getListeLits().add(new Lit(4, true));
    	}
    }
    
    public char getCodeService() {
    	return this.codeService;
    }
    
    public ArrayList<Chambre> getListeChambres(){
    	return this.listeChambres;
    }
    
    /**
     * M�thode permettant l'affichage de l'�tat actuel du service.
     */
    public void afficherEtatService() {
    	System.out.println(nomDuService);
    	System.out.println(codeService);
    	for(int i = 0 ; i < listeChambres.size() ; i++) {
    		System.out.println("Chambre " + listeChambres.get(i).getNumeroChambre());
    		for(int j = 0 ; j < listeChambres.get(i).getListeLits().size() ; j++) {
    			System.out.println(listeChambres.get(i).getListeLits().get(j).toString());
    		}
    	}
    	System.out.println();
    }
    
}