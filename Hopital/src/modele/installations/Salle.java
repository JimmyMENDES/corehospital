package modele.installations;

/**
 * Classe repr�sentant une salle d'examen de l'h�pital.
 * @author Jimmy MENDES
 */
public class Salle {
	
	private static final char codeSalle = 'R';
	private int numeroSalle;

    public Salle(int numeroSalle) {
    	this.numeroSalle = numeroSalle;
    }
    
    /**
     * M�thode r�sumant l'�tat de la salle.
     */
    public String toString() {
    	return "Salle " + codeSalle + numeroSalle;
    }
    
}