package modele.immateriel;

import java.time.LocalDate;

import modele.donnees.DonneesDates;
import modele.humain.Medecin;
import modele.humain.Patient;

/**
 * Classe repr�sentant une consultation.
 * @author Jimmy MENDES
 */
public class Consultation {
	
	private int referenceConsultation;
	private Patient patient;
	private Medecin docteur;
	private LocalDate date;
	private String notesConsultation;
	
	public Consultation(int reference, Patient patient, Medecin docteur, LocalDate date, String notes) {
		this.referenceConsultation = reference;
		this.patient = patient;
		this.docteur = docteur;
		this.date = date;
		this.notesConsultation = notes;
	}
	
	/**
	 * Accesseur de la r�f�rence de la consultation.
	 * @return referenceConsultation
	 */
	public int getReferenceConsultation() {
		return this.referenceConsultation;
	}
	
	/**
	 * Accesseur du patient de la consultation.
	 * @return patient
	 */
	public Patient getPatient() {
		return this.patient;
	}
	
	/**
	 * Accesseur du docteur de la consultation.
	 * @return docteur
	 */
	public Medecin getMedecin() {
		return this.docteur;
	}
	
	/**
	 * Accesseur de la date de la consultation.
	 * @return date
	 */
	public LocalDate getDate() {
		return this.date;
	}
	
	/**
	 * Accesseur du corps de la consultation.
	 * @return notesConsultation
	 */
	public String getNotesConsultation() {
		return this.notesConsultation;
	}
	
	/**
	 * Mutateur du corps de la consultation.
	 * @param consultation
	 */
	public void setNotesConsultation(String consultation) {
		this.notesConsultation = consultation;
	}
	
	/**
	 * M�thode r�sumant l'�tat de la consultation.
	 */
	public String toString() {
		return DonneesDates.formatDate(this.date)+" Docteur "+this.docteur.getNom()+" "+this.docteur.getPrenom()+" Patient : "+this.patient.getNom()+" "+this.patient.getPrenom()+"\n"+this.notesConsultation;
	}

}
