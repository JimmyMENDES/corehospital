package modele.immateriel;

import java.time.LocalDateTime;

import modele.donnees.DonneesDates;
import modele.humain.Patient;
import modele.humain.Personnel;
import modele.materiel.AppareilMedical;

/**
 * Classe repr�sentant une analyse m�dicale faite avec un appareil.
 * @author Jimmy MENDES
 */
public class Analyse {
	
	private int referenceAnalyse;
	private Patient patient;
	private Personnel technicien;
	private LocalDateTime dateTime;
	private AppareilMedical appareil;
	private String contenuAnalyse;
	
	// Trouver une autre fa�on de faire pour personnel = tech
	public Analyse(int reference, Patient patient, Personnel technicien, LocalDateTime dateTime, AppareilMedical appareil, String contenu) {
		this.referenceAnalyse = reference;
		this.patient = patient;
		this.technicien = technicien;
		this.dateTime = dateTime;
		this.appareil = appareil;
		this.contenuAnalyse = contenu;
	}
	
	// Getters
	
	public int getReference() {
		return this.referenceAnalyse;
	}
	
	public Patient getPatient() {
		return this.patient;
	}
	
	public Personnel getTechnicien() {
		return this.technicien;
	}
	
	public LocalDateTime getDateTime() {
		return this.dateTime;
	}
	
	public AppareilMedical getAppareil() {
		return this.appareil;
	}

	public String getContenuAnalyse() {
		return this.contenuAnalyse;
	}
	
	// M�thodes
	
	public String toString() {
		return DonneesDates.formatDateTime(this.dateTime)+" "+this.appareil.getTypeAppareil()+" Patient : "+this.patient.getNom()+" "+this.patient.getPrenom()+"\n"+this.contenuAnalyse+"\nTechnicien : "+this.technicien.getNom()+" "+this.technicien.getPrenom();
	}
	
}