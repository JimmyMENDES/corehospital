package modele.immateriel;

import java.time.LocalDate;

//import materiel.Article;
import modele.donnees.DonneesDates;
import modele.humain.Medecin;
import modele.humain.Patient;

/**
 * Classe repr�sentant une ordonnance.
 * @author Jimmy MENDES
 */
public class Ordonnance {
	
	private int referenceOrdonnance;
	private Patient patient;
	private Medecin docteur;
	private LocalDate date;
	private String ordonnance;
	// private ArrayList<Article> listeArticleOrdonnance; id�e � d�velopper plus tard sans doute
	
	
    public Ordonnance(int reference, Patient patient, Medecin docteur, LocalDate date, String ordonnance) {
    	this.referenceOrdonnance = reference;
    	this.patient = patient;
    	this.docteur = docteur;
    	this.date = date;
    	this.ordonnance = ordonnance;
    }
    
    /**
     * Accesseur de la r�f�rence de l'ordonnance.
     * @return referenceOrdonnance
     */
    public int getReferenceOrdonnance() {
    	return this.referenceOrdonnance;
    }
    
    /**
     * Accesseur du patient de l'ordonnance.
     * @return patient
     */
    public Patient getPatient() {
    	return this.patient;
    }
    
    /**
     * Accesseur du medecin de l'ordonnance.
     * @return docteur
     */
    public Medecin getMedecin() {
    	return this.docteur;
    }
    
    /**
     * Accesseur de la date de l'ordonnance.
     * @return date
     */
    public LocalDate getDate() {
    	return this.date;
    }
    
    /**
     * Accesseur du corps de l'ordonnance. 
     * @return ordonnance
     */
    public String getOrdonnance() {
    	return this.ordonnance;
    }
    
    /**
     * M�thode r�sumant l'�tat de l'ordonnance.
     */
    public String toString() {
    	return DonneesDates.formatDate(this.date)+" Docteur "+this.docteur.getNom()+" "+this.docteur.getPrenom()+" Patient : "+this.patient.getNom()+" "+this.patient.getPrenom()+"\n"+this.ordonnance;
    }
    
    /*public String toString() {
    	String resume = afficherDateOrdonnance()+" Docteur "+this.docteur.getNom()+" "+this.docteur.getPrenom()+" Patient : "+this.patient.getNom()+" "+this.patient.getPrenom()+"\nprescrit :";
    	for(int i = 0 ; i < listeArticleOrdonnance.size() ; i++) {
    		if(i == listeArticleOrdonnance.size()-1) resume += listeArticleOrdonnance.get(i).getNom() + " : " + listeArticleOrdonnance.get(i).getQuantite() + ".";
    		resume += listeArticleOrdonnance.get(i).getNom() + " : " + listeArticleOrdonnance.get(i).getQuantite() + ", ";
    	}
    	return resume;
    }*/
    
}