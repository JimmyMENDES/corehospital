package modele.immateriel;

import java.time.LocalDateTime;

//import installations.Salle;
import modele.donnees.DonneesDates;
import modele.humain.Medecin;
import modele.humain.Patient;

/**
 * Classe représentant un rendez-vous.
 * @author Jimmy MENDES
 */
public class RDV {

	private int referenceRDV;
	private Patient patient;
	private Medecin docteur;
	int numeroSalle;
	//private Salle salle;
	private LocalDateTime dateHeure;

    public RDV(int reference, Patient patient, Medecin docteur, int numeroSalle, LocalDateTime date) {
    	this.referenceRDV = reference;
    	this.patient = patient;
    	this.docteur = docteur;
    	this.numeroSalle = numeroSalle;
    	//this.salle = salle;
    	this.dateHeure = date;
    }
    
    /**
     * Accesseur de la reférence du rdv.
     * @return referenceRDV;
     */
    public int getReferenceRDV() {
    	return this.referenceRDV;
    }
    
    /**
     * Accesseur de patient.
     * @return patient
     */
    public Patient getPatient() {
    	return this.patient;
    }
    
    /**
     * Accesseur de docteur.
     * @return docteur
     */
    public Medecin getDocteur() {
    	return this.docteur;
    }
    
    /**
     * Accesseur de salle.
     * @return salle
     */
    public int getSalle() {
    	return this.numeroSalle;
    }
    
    /**
     * Accesseur de dateHeure.
     * @return dateHeure
     */
    public LocalDateTime getDateHeure() {
    	return this.dateHeure;
    }
    
    /**
     * Mutateur de docteur.
     * @param docteur
     */
    public void setDocteur(Medecin docteur) {
    	this.docteur = docteur;
    }
    
    /**
     * Mutateur de dateHeure.
     * @param nouvelleDate
     */
    public void setDate(LocalDateTime nouvelleDateHeure) {
    	this.dateHeure = nouvelleDateHeure;
    }
    
    public String toString() {
    	return "Rendez-vous le "+DonneesDates.formatDateHeure(this.getDateHeure())+" avec le Docteur "+this.getDocteur().getPrenom()+" "+this.getDocteur().getNom()+".";
    }

}