# CoreHospitalAdministrator
Bienvenue dans le README de CoreHospitalAdministrator.

CoreHospitalAdministrator est un logiciel codé en Java, utilisant l'interface Swing et une Base de Donnée MySQL dont le but est d'administrer
un hôpital fictif, CoreHospital.

## Introduction 1 - Mise en place

Avant de lancer l'application, il faut dans un premier temps importer la base de donnée sur phpmyadmin.

Celle-ci est fournie dans le ZIP envoyé avec la soutenance sous le nom hopital.sql

Une fois la base de donnée importée, il suffit de lancer le logiciel avec le main contenu dans le package main.

## Introduction 2 - CoreHospital

CoreHospital est un hôpital fictif composé de 5 services :

1. A - Médecine Générale
2. B - Cardiologie
3. C - Chirurgie
4. D - Pédiatrie
5. U - Urgences

Chaque Service à l'exception des Urgences est composé de :
- 8 Chambres Individuelles
- 8 Chambres Doubles
- 4 Chambres Quadruples

Pour un total de 40 lits par service, soit 160 lits pour l'ensemble des services classiques.

A cela s'ajoute le service des Urgences qui comporte 10 lits.

CoreHospital possède 10 salles utilisés pour les rendez-vous.

CoreHospital est muni de 8 appareils médicaux :

- Électrocardiogramme

- Électrocardiogramme

- Échographe

- Endoscope

- Imagerie par résonnance magnétique

- Pléthysmographe

- Scanner

- Tomographie à émission de positron

*Veuillez noter que CoreHospital représente les paramètres de base. Il est tout à fait possible de créer un autre hôpital arrangé différemment via les fonctions implémentées par le profil SuperAdmin(Voir profil plus bas pour plus de détails).*

## Partie 1 - Profils Utilisateurs

Il existe 6 profils utilisateurs différents : 

1. Médecin

2. Infirmier

3. Pharmacien

4. Technicien de Laboratoire

5. Agent Administrateur

6. SuperAdmin

L'ensemble de leurs fonctionnalités sera documentés plus bas dans leurs parties respectives.

## Partie 2 - Login

Les logins sont construits comme suit : 
- Trois premières lettres du nom de famille
- Première lettre du prénom
- Numéro d'identifiant

Voici l'ensemble des login/mot de passe implémentés par profils

1. Médecins

Albus Dumbledore - DUMA1 / priori_incantatum

Minerva McGonagall - MCGM2 / piertotum_locomotor

Severus Rogue - ROGS3 / always

Filius Flitmick - FLIF4 /wingardium_leviosa

Pomona Chourave - CHOP5 / mandragore

Renée Bibine - BIBR6 / nimbus

Cuthbert Binns - BINC7 / rip

2. Infirmier

Molly Weasley - WEAM8 / prewett

Sirius Black - BLAS9 / patmol

Remus Lupin - LUPR10 / lunard

James Potter - POTJ11 / cornedrue

Narcissa Malefoy - MALN12 / drago

3. Pharmacien

Poppy Pomfresh - POMP13 / episkey

4. Technicien de Laboratoire

Arthur Weasley - WEAA14 / muggle

5. Agent Administrateur

Cornelius Fudge - FUDC15 / bowler_hat

6. SuperAdmin

SuperAdmin / SuperAdmin

## Partie 3 - Fenetre Principale, Onglet Accueil

### I) Vue Médecin

#### Créer Patient

Cette fonction permet d'ajouter un nouveau patient à la base donnée.

Pour cela, il faut renseigner :

- Nom

- Prénom

- Sexe

- Date de Naissance

- Adresse

- Adresse E-Mail (facultatif)

- Numéro de Téléphone

- Pathologies

Un numéro d'identifiant est ensuite attribué à ce nouveau patient.

#### Rechercher Patient

Cette fonction permet de rechercher un patient dans la base de données.

La recherche peut s'effectuer via trois méthodes :

- Par Identifiant

- Par Nom

- Par Date de Naissance

Dans le cas d'une recherche par nom ou date de naissance, il est possible d'avoir plusieurs patients ayant les même données.

Si c'est le cas, un tableau apparaitra en-dessous de la recherche avec l'ensemble des patients ayant le même nom ou la même date de naissance.

Il suffit de double clicker sur le patient recherché pour afficher l'onglet du patient.

#### Mes Rendez-Vous

Cette fonction permet à un médecin de visualiser ses rendez-vous du jour.

Il pourra alors consulter avec quel patient et dans quelle salle il a rendez-vous.

#### Logout

Cette fonction permet à l'utilisateur de se déconnecter du logiciel.

### II) Vue Infirmier

#### Rechercher Patient

Cette fonction permet de rechercher un patient dans la base de données.

La recherche peut s'effectuer via trois méthodes :

- Par Identifiant

- Par Nom

- Par Date de Naissance

Dans le cas d'une recherche par nom ou date de naissance, il est possible d'avoir plusieurs patients ayant les même données.

Si c'est le cas, un tableau apparaitra en-dessous de la recherche avec l'ensemble des patients ayant le même nom ou la même date de naissance.

Il suffit de double clicker sur le patient recherché pour afficher l'onglet du patient.

#### État du Service

Cette fonction permet à un infirmier de visualiser quels lits sont occupés dans le service.

Si le lit est rouge, il est occupé.

Si il est vert, il est vide.

#### Logout

Cette fonction permet à l'utilisateur de se déconnecter du logiciel.

### III) Vue Pharmacien

#### État de la Pharmcie

Cette fonction permet à un pharmacien de visualiser l'inventaire de la pharmacie

Il peut alors visualiser un tableau donnant les informations suivantes :

- La référence des articles

- Les noms des articles

- La quantité en stock des articles

- Le type d'article (Article Général, Médicament ou Équipement Médical)

#### Rechercher Patient

Cette fonction permet de rechercher un patient dans la base de données.

La recherche peut s'effectuer via trois méthodes :

- Par Identifiant

- Par Nom

- Par Date de Naissance

Dans le cas d'une recherche par nom ou date de naissance, il est possible d'avoir plusieurs patients ayant les même données.

Si c'est le cas, un tableau apparaitra en-dessous de la recherche avec l'ensemble des patients ayant le même nom ou la même date de naissance.

Il suffit de double clicker sur le patient recherché pour afficher l'onglet du patient.

#### Ajouter ou Retirer un Article

Cette fonction permet d'ajouter ou retirer des unités d'un article du stock.

Il faut pour cela indiquer la référence de l'article.

#### Créer un nouvel article

Cette fonction permet de créer un nouvel article.

Il faut pour cela indiquer une référence et un nom pour l'article à créer.

#### Supprimer un Article

Cette fonction permet de supprimer totalement un article de l'inventaire.

Il faut pour cela indiquer la référence de l'article.

#### Logout

Cette fonction permet à l'utilisateur de se déconnecter du logiciel.

### IV) Vue Technicien de Laboratoire

#### Rechercher Patient

Cette fonction permet de rechercher un patient dans la base de données.

La recherche peut s'effectuer via trois méthodes :

- Par Identifiant

- Par Nom

- Par Date de Naissance

Dans le cas d'une recherche par nom ou date de naissance, il est possible d'avoir plusieurs patients ayant les même données.

Si c'est le cas, un tableau apparaitra en-dessous de la recherche avec l'ensemble des patients ayant le même nom ou la même date de naissance.

Il suffit de double clicker sur le patient recherché pour afficher l'onglet du patient.

#### Logout

Cette fonction permet à l'utilisateur de se déconnecter du logiciel.

### V) Vue Agent Administrateur

#### Créer Patient

Cette fonction permet d'ajouter un nouveau patient à la base donnée.

Pour cela, il faut renseigner :

- Nom

- Prénom

- Sexe

- Date de Naissance

- Adresse

- Adresse E-Mail (facultatif)

- Numéro de Téléphone

- Pathologies

Un numéro d'identifiant est ensuite attribué à ce nouveau patient.

#### Rechercher Patient

Cette fonction permet de rechercher un patient dans la base de données.

La recherche peut s'effectuer via trois méthodes :

- Par Identifiant

- Par Nom

- Par Date de Naissance

Dans le cas d'une recherche par nom ou date de naissance, il est possible d'avoir plusieurs patients ayant les même données.

Si c'est le cas, un tableau apparaitra en-dessous de la recherche avec l'ensemble des patients ayant le même nom ou la même date de naissance.

Il suffit de double clicker sur le patient recherché pour afficher l'onglet du patient.

#### Logout

Cette fonction permet à l'utilisateur de se déconnecter du logiciel.

### VI) Vue SuperAdmin

#### Ajouter un Employé

Cette fonction permet d'ajouter un nouvel employé à la base donnée.

Pour cela, il faut renseigner :

- Nom

- Prénom

- Sexe

- Date de Naissance

- Adresse

- Adresse E-Mail (facultatif)

- Numéro de Téléphone

- Profession

- N°RPPS (facultatif sauf pour le personnel médical, il s'agit du numéro d'identification des personnels de santé en France)

- Spécialité (facultatif sauf pour les médecins)

- Service (facultatif sauf pour les médecins et les infirmiers)

- Salaire

Un numéro d'identifiant est ensuite attribué à ce nouvel employé.

#### Supprimer un Employé

Cette fonction permet de supprimer totalement un employé de la base de donnée.

Il faut pour cela indiquer son numéro d'identifiant.

#### Ajouter une installation

Cette fonction permet de créer de nouveau services.

Il faut rensigner le code et le nom de ce nouveau service.

Un fois cela fait, on décide ou non de le remplir avec des chambres individuelles, double ou quadruples.

#### Supprimer une installation

Cette fonction permet de supprimer totalement une installation de la base de donnée.

Il faut pour cela indiquer son code.

- Pour un service, il s'agit de la lettre (exemple B pour cardiologie)

- Pour une chambre, il s'agit de la lettre du service et du numéro de chambre (B12 par exmple)

- Pour le lit, il s'agit de la lettre du service, du numéro de chambre et enfin du numéro de lit (exemple B122)

#### Créer un nouvel appareil

Cette fonction permet de créer un nouvel appareil médical.

Il faut pour cela indiquer un code et un type d'appareil pour l'article à créer (exemple : IRM - Imagerie par Résonnance Magnétique)

#### Supprimer un appareil

Cette fonction permet de supprimer totalement un appreil médical de la base de donnée.

Il faut pour cela indiquer son code. (Exemple : IRM)

#### Logout

Cette fonction permet à l'utilisateur de se déconnecter du logiciel.

## Partie 4 - Fenetre Principale, Onglet Patient

Lorsque le membre du personnel crée ou recherche avec succès un patient, un nouvel onglet apparait comportant l'ensemble des données du patient et les intéractions que le membre du pesonnel peut effectuer sur ce patient.

### I) Vue Médecin

Il est possible de double clicker sur les éléments RDV/Consultation/Ordonnance/Analyse présents dans la zone bas/gauche afin de visualiser plus confortablement les données.

Également dans la zone bas/gauche, il est possible sur l'onglet pathologie de modifier les pathologies au fur et à mesure.

#### Nouvelle Consultation

Cette fonction permet aux médecins de rajouter une consultation au dossier du patient.

#### Nouvelle Ordonnance

Cette fonction permet aux médecins de rajouter une ordonnance au dossier du patient.

#### Nouveau RDV

Cette fonction permet aux médecins de rajouter un rendez-vous au dossier du patient.

#### Modifier les Données

Cette fonction permet aux médecins de modifier les données personnelles du patient si nécessaire.

#### Supprimer le Patient

Cette fonction permet aux médecins de supprimer totalement un patient de la base de donnée.

### II) Vue Infirmier

Il est possible de double clicker sur les éléments RDV/Ordonnance présents dans la zone bas/gauche afin de visualiser plus confortablement les données.

#### Nouveau RDV

Cette fonction permet aux infirmiers de rajouter un rendez-vous au dossier du patient.

### III) Vue Pharmacien

Il est possible de double clicker sur les éléments Ordonnance présents dans la zone bas/gauche afin de visualiser plus confortablement les données.

Le pharmacien ne possède pas d'autres fonctions, il peut uniquement visualiser les ordonnances afin de regarder quel type de médicament a été prescrit. 

### IV) Vue Technicien de Laboratoire

Il est possible de double clicker sur les éléments Ordonnance/Analyse présents dans la zone bas/gauche afin de visualiser plus confortablement les données.

#### Nouvelle Analyse

Cette fonction permet aux techniciens de laboratoire de rajouter une analyse au dossier du patient.

### V) Vue Agent Administrateur

Il est possible de double clicker sur les éléments RDV présents dans la zone bas/gauche afin de visualiser plus confortablement les données.

#### Nouveau RDV

Cette fonction permet aux Agent Administrateur de rajouter un rendez-vous au dossier du patient.

#### Hospitaliser

Cette fonction permet aux Agent Administrateur d'hospitalsier un patient.

Il faut rensigner le code du service et les dates d'entrée/sortie. NC pour la sortie signifie Non Connu.

Ils ont deux choix : 

- Automatique (Le choix de la chambre se fait automatiquement par rapport au premier lit libre)

- Manuel (le choix est laissé à l'agent de choisir le lit pour le patient (en fonction des besoins/moyens))

#### Modifier les Données

Cette fonction permet aux médecins de modifier les données personnelles du patient si nécessaire.

#### Supprimer le Patient

Cette fonction permet aux médecins de supprimer totalement un patient de la base de donnée.
